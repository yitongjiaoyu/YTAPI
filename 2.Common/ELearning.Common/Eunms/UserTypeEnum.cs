﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Common.Eunms
{

    /// <summary>
    /// 用户类型
    /// </summary>
    public enum UserTypeEnum
    {
        /// <summary>
        /// 管理员-运营平台
        /// </summary>
        [Description("管理员-运营平台")]
        Platform = 1,
        /// <summary>
        /// 管理员-自服务平台
        /// </summary>
        [Description("管理员-自服务平台")]
        OrgMaster = 2,
        /// <summary>
        /// 教师
        /// </summary>
        [Description("教师")]
        Teacher = 3,
        /// <summary>
        /// 学生
        /// </summary>
        [Description("学生")]
        Student = 4,
        /// <summary>
        /// 其他
        /// </summary>
        [Description("其他")]
        Other = 5,
    }
}


