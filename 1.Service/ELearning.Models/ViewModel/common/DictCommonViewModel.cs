﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models
{
    public class DictCommonViewModel
    {
        /// <summary>
        /// 科目列表-for select
        /// </summary>
        public List<SelectKeyValue> Subject { set; get; } = new List<SelectKeyValue>();

        /// <summary>
        /// 年级列表-for select
        /// </summary>
        public List<SelectKeyValue> Grade { set; get; } = new List<SelectKeyValue>();

        /// <summary>
        /// 等级列表-for select
        /// </summary>
        public List<SelectKeyValue> Level { set; get; } = new List<SelectKeyValue>();
    }

    public class KeyValue
    {
        public string id { get; set; }
        public string label { get; set; }
    }

    public class SelectKeyValue
    {
        public string key { get; set; }
        public string label { get; set; }
    }
}
