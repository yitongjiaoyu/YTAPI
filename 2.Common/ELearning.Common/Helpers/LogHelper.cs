﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ELearning.Common
{
    public class LogHelper
    {
        public const string ARGUMENT_NAME_VALUE_PATTERN = "{0}={1}";
        const string NULL = "NULL";
        public static string FormatArgument(string name, string value)
        {
            return string.Format(ARGUMENT_NAME_VALUE_PATTERN, name, value);
        }
        public static LogEventInfo CreateErrorLogEventInfoWithArgumentPattern(Exception ex, params object[] args)
        {
            if (args.IsNotNullOrEmpty())
            {
                if (args.Length % 2 > 0)
                {
                    return CreateErrorLogEventInfo(ex, args);
                }
                else
                {
                    var arguments = new List<string>();
                    for (int i = 0; i < args.Length / 2; i++)
                    {
                        var name = args[i * 2];
                        var value = args[(i * 2) + 1];
                        name = name ?? string.Empty;
                        value = value ?? NULL;
                        arguments.Add(FormatArgument(name.ToString(), value.ToString()));
                    }
                    return CreateErrorLogEventInfo(ex, arguments.ToArray());
                }
            }
            else
            {
                return CreateErrorLogEventInfo(ex);
            }
        }
        public static LogEventInfo CreateErrorLogEventInfo(Exception ex, params object[] args)
        {
            var context = HttpContext.Current;
            var logEventInfo = new LogEventInfo();
            if (HttpContext.Current.IsNotNull() && HttpContext.Current.Request.IsNotNull())
            {
                logEventInfo.Properties.Add("RequestIP", HttpContext.Current.Request.UserHostAddress);
                logEventInfo.Properties.Add("RequestUrl", HttpContext.Current.Request.Url);
                logEventInfo.Properties.Add("UserAgent", HttpContext.Current.Request.UserAgent);
                logEventInfo.Properties.Add("QueryString", HttpContext.Current.Request.QueryString.ToString());
            }
            var message = ex.Message;
            if (args.IsNotNullOrEmpty())
            {
                var argsString = string.Join(",", args.Select(x => string.Format("[{0}]", x == null ? NULL : x.ToString())));

                message = string.Format("Message:{0},Arguments:{1}", ex.Message, argsString);
            }
            logEventInfo.Properties.Add("Message", message);
            logEventInfo.Properties.Add("ExceptionMessage", ex);
            logEventInfo.Properties.Add("ExceptionStackTrace", ex);
            logEventInfo.Properties.Add("MachineUser", "MachineUser");
            logEventInfo.Properties.Add("Priority", 9);
            logEventInfo.Properties.Add("LogLevel", "Error");
            logEventInfo.Level = LogLevel.Error;

            return logEventInfo;
        }
    }
}
