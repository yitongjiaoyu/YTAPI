//-------------------------------------------------------------------
//文件名称：ESysUser.cs
//模块名称：ESysUserModel层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ELearning.Models
{	
	 /// <summary>
    /// ESysUser表实体类
    /// </summary>
	[Table("ESysUser")]
	public partial class ESysUser
    {
		
		/// <summary>
		/// 
		/// </summary>
				[Key]
	[Display(Name = "")]
		public Guid UserID { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string LoginName { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string UserName { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string UserType { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string OrgCode { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Unumber { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Password { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public int Sex { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public DateTime Birthday { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string HeadPhoto { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Company { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Department { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Position { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string OfficeTel { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string MobilePhone { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Email { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Remark { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public int AccountStatus { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string CardNo { get; set; }
		
		/// <summary>
		/// 邀请码
		/// </summary>
			[Display(Name = "邀请码")]
		public string InviteCode { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Creater { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public DateTime CreateTime { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Modifyer { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public DateTime ModifyTime { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public bool IsDelete { get; set; }
		   
    }
}

