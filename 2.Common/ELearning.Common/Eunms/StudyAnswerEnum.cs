﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Common
{

    /// <summary>
    /// 学生答案是否正确
    /// </summary>
    public enum StudyAnswerEnum
    {
        /// <summary>
        /// 正确
        /// </summary>
        [Description("正确")]
        Right = 1,
        /// <summary>
        /// 错误
        /// </summary>
        [Description("错误")]
        Wrong = 2,
        /// <summary>
        /// 未答题
        /// </summary>
        [Description("未答题")]
        None = 3,
        /// <summary>
        /// 其他
        /// </summary>
        [Description("其他")]
        Other = 0,
    }
}
