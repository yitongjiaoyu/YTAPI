//-------------------------------------------------------------------
//文件名称：RRoleUser.cs
//模块名称：RRoleUser数据访问层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//修改时间：2019年8月25日
//-----------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearning.Models;

namespace ELearning.DAL
{
    /// <summary>
    /// RRoleUser数据访问层
    /// </summary>
	public partial class RRoleUserRepository : BaseRepository<RRoleUser>
    {
        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="ids">id列表</param>
        public int BatchDelete(List<Guid> ids)
        {
            var sb = new StringBuilder();
            foreach (var id in ids)
            {
                sb.Append($"'{id}',");
            }
            var where = sb.ToString();
            if (!string.IsNullOrEmpty(where) && where.Length > 0)
                where = where.Substring(0, where.Length - 1);
            var sql = $@"DELETE FROM dbo.RRoleUser WHERE ID IN ({where})";
            var res = nContext.Database.ExecuteSqlCommand(sql);
            return res;
        }
    }
}


