﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel
{
    public class CommentReportViewModel
    {
        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }
        /// <summary>
        /// 开课日期
        /// </summary>
        public DateTime CourseBeginDate { get; set; }
        /// <summary>
        /// 课程章节数
        /// </summary>
        public int CourseChilds { get; set; }
        /// <summary>
        /// 课程报名数
        /// </summary>
        public int CourseApplys { get; set; }
        /// <summary>
        /// 课程点赞数
        /// </summary>
        public int CourseLikes { get; set; }
        /// <summary>
        /// 评价总分数
        /// </summary>
        public double CommentScores { get; set; }
        /// <summary>
        /// 评价总人数
        /// </summary>
        public int CommentPeoples { get; set; }
        /// <summary>
        /// 评价平均分
        /// </summary>
        public double CommentAvg { get; set; }
    }
}
