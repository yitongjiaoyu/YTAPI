﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using ELearning.BLL;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearning.Common.CommonStr;
using ELearning.Models;
using ELearning.Common;
using System.Globalization;

namespace UserSyncService
{
    public class Service
    {
        ESysUserService userService => new ESysUserService();
        ERoleService roleService => new ERoleService();
        RRoleUserService roleUserService => new RRoleUserService();
        EApplicationLogService logService => new EApplicationLogService();
        public void UserSync()
        {
            try
            {
                //GetUsers();
            }
            catch (Exception ex) { }
        }

        //public void GetUsers()
        //{
        //    EApplicationLog log = new EApplicationLog
        //    {
        //        AppUser = "WebService",
        //        LogDate = DateTime.Now,
        //        LogLevel = "Info",
        //        AspnetMVCController = "JobService.UserSyncService",
        //        AspnetMVCAction = "SyncUserXszs",
        //        Message = "成功",
        //    };

        //    try
        //    {
        //        var url = ConfigInfo.PostUrl;
        //        var appId = ConfigInfo.AppId;
        //        var appKey = ConfigInfo.AppKey;
        //        var timestamp = Common.Get1970ToNowMilliseconds().ToString();
        //        var token = appId + timestamp + appKey;
        //        token = Common.GetMD5(token);

        //        RequestInfo reqInfo = new RequestInfo();
        //        reqInfo.filter.UpdateTime = DateTime.Now.AddMinutes(-5).ToString("yyyyMMddHHmmss");
        //        reqInfo.pageInfo.page = 1;
        //        reqInfo.pageInfo.pageSize = 100;
        //        var json = JsonConvert.SerializeObject(reqInfo);

        //        var response = Post(url, appId, token, timestamp, json);

        //        List<UserAddInfoXszs> userInfo = new List<UserAddInfoXszs>();
        //        List<ESysUser> usersNew = new List<ESysUser>();
        //        List<ESysUser> usersUp = new List<ESysUser>();
        //        List<RRoleUser> roleUsers = new List<RRoleUser>();

        //        var resContent = response.Content;
        //        var res = resContent.ToObject<ResponseInfo>();

        //        while (res.body.result.data.Count == 100)
        //        {
        //            userInfo.AddRange(res.body.result.data);
        //            reqInfo.pageInfo.page += 1;
        //            response = Post(url, appId, token, timestamp, json);
        //            res = resContent.ToObject<ResponseInfo>();
        //        }
        //        userInfo.AddRange(res.body.result.data);

        //        var allUsers = userService.FindList(x => !x.IsDelete && x.UserType == UserType.UserTypeXSZS).ToList();
        //        var role = roleService.Find(x => !x.IsDelete && x.RoleName == "销售助手");

        //        foreach (var u in userInfo)
        //        {
        //            var s = new ESysUser
        //            {
        //                AccountStatus = Int32.Parse(u.zt),
        //                Creater = "销售助手同步服务",
        //                CreateTime = DateTime.Now,
        //                //Birthday = DateTime.ParseExact(u.出生日期, "yyyyMMdd", CultureInfo.CurrentCulture),
        //                Email = string.Empty,
        //                IsDelete = false,
        //                LoginName = u.czymc,
        //                MobilePhone = string.Empty,
        //                Modifyer = "销售助手同步服务",
        //                ModifyTime = DateTime.ParseExact(u.updatetime, "yyyyMMddHHmmssfff", CultureInfo.CurrentCulture),
        //                OrgCode = CommonConst.UserDefaultOrgCode,
        //                Password = CommonConst.UserDefaultPsw,
        //                Remark = CommonConst.UserDefaultRemark,
        //                Sex = 3,
        //                UserID = GuidUtil.NewSequentialId(),
        //                UserName = u.czymc,
        //                UserType = UserType.UserTypeXSZS,
        //                Unumber = u.czydm,
        //                AgentJobName = u.czyzw,
        //                AgentName = "",
        //                AgentDepart = "",
        //                AgentNumber = u.jxsh,
        //                CarMarketManageYear = 0,
        //                CarRepairManageYear = 0,
        //                CarSaleManageYear = 0,
        //                CarServiceManageYear = 0,
        //                CardNo = "",
        //                Company = string.Empty,
        //                Department = string.Empty,
        //                HeadPhoto = CommonConst.UserDefaultHeadPhoto,
        //                JobStatus = 1,
        //                OfficeTel = string.Empty,
        //                OtherMarketManageYear = 0,
        //                OtherRepairManageYear = 0,
        //                OtherSaleManageYear = 0,
        //                Position = string.Empty,
        //                RegisterDevice = "销售助手同步服务",
        //                SGMWRecordJob = ""
        //            };
        //            var sysUser = allUsers.Find(x => x.Unumber == u.czydm && x.AgentNumber == u.jxsh);
        //            if (sysUser == null)
        //            {
        //                usersNew.Add(s);
        //                if (role != null)
        //                {
        //                    var r = new RRoleUser
        //                    {
        //                        ID = GuidUtil.NewSequentialId(),
        //                        RoleID = role.RoleID,
        //                        UserID = s.UserID
        //                    };
        //                    roleUsers.Add(r);
        //                }
        //            }
        //            else
        //            {
        //                sysUser.LoginName = s.LoginName;
        //                sysUser.UserName = s.UserName;
        //                sysUser.AgentJobName = s.AgentJobName;
        //                sysUser.AccountStatus = s.AccountStatus;
        //                sysUser.Modifyer = s.Modifyer;
        //                sysUser.ModifyTime = s.ModifyTime;
        //                usersUp.Add(sysUser);
        //            }
        //        }

        //        if (usersNew.Count > 0)
        //        {
        //            foreach (var a in usersNew)
        //            {
        //                log.ExceptionMessage += a.UserName + "," + a.Unumber + "," + a.AgentNumber + ";";
        //            }
        //            var usersAdd = userService.Adds(usersNew);
        //            log.Message = "新增用户成功：" + usersAdd + "个";
        //            logService.Add(log);
        //        }
        //        if (usersUp.Count > 0)
        //        {
        //            foreach (var a in usersUp)
        //            {
        //                log.ExceptionMessage += a.UserName + "," + a.Unumber + "," + a.AgentNumber + ";";
        //            }
        //            log.AspnetMVCAction = "UpdateUserXszs";
        //            var usersUpdate = userService.Updates(usersUp);
        //            log.Message = "更新用户成功：" + usersUpdate + "个";
        //            logService.Add(log);
        //        }
        //        roleUserService.Adds(roleUsers);
        //    }
        //    catch (Exception ex)
        //    {
        //        log.LogLevel = "Error";
        //        log.InnerException = ex.InnerException.ToString();
        //        log.Message = ex.Message.ToString();
        //        logService.Add(log);
        //    }
        //}

        //public IRestResponse Post(string url, string appId, string token, string timestamp, string req)
        //{
        //    var client = new RestClient(url);
        //    var request = new RestRequest(Method.POST);
        //    request.AddHeader("appId", appId);
        //    request.AddHeader("token", token);
        //    request.AddHeader("times", timestamp);
        //    request.AddParameter("undefined", req, ParameterType.RequestBody);
        //    var response = client.Execute(request);
        //    return response;
        //}

    }
}
