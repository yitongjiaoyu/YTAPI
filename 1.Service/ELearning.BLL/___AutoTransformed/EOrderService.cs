//-------------------------------------------------------------------
//文件名称：EOrder.cs
//模块名称：EOrder业务逻辑接口层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearning.DAL;
using ELearning.Models;

namespace ELearning.BLL
{
    /// <summary>
    /// EOrder业务逻辑接口层
    /// </summary>
	public partial class EOrderService : BaseService<EOrder>
	{
     public  EOrderService() : base(RepositoryFactory.EOrderRepository) { }
	}
}


