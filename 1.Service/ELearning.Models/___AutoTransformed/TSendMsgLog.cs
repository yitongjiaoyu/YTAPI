//-------------------------------------------------------------------
//文件名称：TSendMsgLog.cs
//模块名称：TSendMsgLogModel层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ELearning.Models
{	
	 /// <summary>
    /// TSendMsgLog表实体类
    /// </summary>
	[Table("TSendMsgLog")]
	public partial class TSendMsgLog
    {
		
		/// <summary>
		/// Id
		/// </summary>
				[Key]
	[Display(Name = "Id")]
		public Guid Id { get; set; }
		
		/// <summary>
		/// 短信内容
		/// </summary>
			[Display(Name = "短信内容")]
		public string MsgContent { get; set; }
		
		/// <summary>
		/// 手机号
		/// </summary>
			[Display(Name = "手机号")]
		public string Phone { get; set; }
		
		/// <summary>
		/// 是否发送成功
		/// </summary>
			[Display(Name = "是否发送成功")]
		public bool IsSuccess { get; set; }
		
		/// <summary>
		/// 调用阿里云或者腾讯发送短信接口返回信息
		/// </summary>
			[Display(Name = "调用阿里云或者腾讯发送短信接口返回信息")]
		public string RespMsg { get; set; }
		
		/// <summary>
		/// 创建时间
		/// </summary>
			[Display(Name = "创建时间")]
		public DateTime CreateTime { get; set; }
		
		/// <summary>
		/// 更新时间
		/// </summary>
			[Display(Name = "更新时间")]
		public DateTime UpdateTime { get; set; }
		
		/// <summary>
		/// 创建人Id
		/// </summary>
			[Display(Name = "创建人Id")]
		public Guid CreateUserId { get; set; }
		
		/// <summary>
		/// 编辑人id
		/// </summary>
			[Display(Name = "编辑人id")]
		public Guid UpdateUserId { get; set; }
		
		/// <summary>
		/// 是否删除
		/// </summary>
			[Display(Name = "是否删除")]
		public bool IsDelete { get; set; }
		   
    }
}

