//-------------------------------------------------------------------
//文件名称：TPaperQuestion.cs
//模块名称：TPaperQuestionModel层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ELearning.Models
{	
	 /// <summary>
    /// TPaperQuestion表实体类
    /// </summary>
	[Table("TPaperQuestion")]
	public partial class TPaperQuestion
    {
		
		/// <summary>
		/// Id
		/// </summary>
				[Key]
	[Display(Name = "Id")]
		public Guid Id { get; set; }
		
		/// <summary>
		/// 试卷Id
		/// </summary>
			[Display(Name = "试卷Id")]
		public Guid PaperId { get; set; }
		
		/// <summary>
		/// 题目类型，关联类型EQuestionType表中code字段
		/// </summary>
			[Display(Name = "题目类型，关联类型EQuestionType表中code字段")]
		public string TypeCode { get; set; }
		
		/// <summary>
		/// 题目Id
		/// </summary>
			[Display(Name = "题目Id")]
		public Guid QuestionId { get; set; }
		
		/// <summary>
		/// 序号
		/// </summary>
			[Display(Name = "序号")]
		public double Sort { get; set; }
		
		/// <summary>
		/// 创建时间
		/// </summary>
			[Display(Name = "创建时间")]
		public DateTime CreateTime { get; set; }
		
		/// <summary>
		/// 更新时间
		/// </summary>
			[Display(Name = "更新时间")]
		public DateTime UpdateTime { get; set; }
		
		/// <summary>
		/// 创建人Id
		/// </summary>
			[Display(Name = "创建人Id")]
		public Guid CreateUserId { get; set; }
		
		/// <summary>
		/// 编辑人id
		/// </summary>
			[Display(Name = "编辑人id")]
		public Guid UpdateUserId { get; set; }
		
		/// <summary>
		/// 是否删除
		/// </summary>
			[Display(Name = "是否删除")]
		public bool IsDelete { get; set; }
		   
    }
}

