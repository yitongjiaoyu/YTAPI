﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using ELearning.BLL;
using ELearning.Common.CommonStr;
using ELearning.Common.Eunms;
using ELearning.Common.Extensions;
using ELearning.Common.Helpers;
using ELearning.Common.Model;
using ELearning.Models;
using ELearning.Models.ViewModel.YTJYAdmin.Banner;
using ELearning.Models.ViewModel.YTJYAdmin.Course;
using SiteAPI.Controllers.Common;

namespace SiteAPI.Controllers.Business
{
    /// <summary>
    /// 轮播图控制器
    /// </summary>
    public class BannerController : BaseApiController
    {
        /// <summary>
        /// 轮播图相关操作service
        /// </summary>
        private TBannerService tBannerService => new TBannerService();

        /// <summary>
        /// 获取首页轮播图列表
        /// </summary>
        [HttpPost, Route("api/ytjysite/TBanner/GetModelList")]
        [AllowAnonymous]
        public ServiceResponse<SiteBannerViewModel> GetModelList()
        {
            var models = tBannerService.FindList(b => !b.IsDelete && b.Type == (int)BannerTypeEnum.AppIndex && b.IsUsed).OrderBy(b => b.Sort).ThenBy(b => b.CreateTime).ToList();
            var viewModel = new SiteBannerViewModel { TotalCount = models.Count };
            foreach (var m in models)
            {
                var v = m.MapTo<SiteBannerItem, TBanner>();
                v.ImageUrl = PictureHelper.GetFileFullPath(m.ImageUrl);
                viewModel.Items.Add(v);
            }
            return ServiceResponse<SiteBannerViewModel>.SuccessResponse(CommonConst.OprateSuccessStr, viewModel);
        }
    }
}