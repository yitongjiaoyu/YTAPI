﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Http;
using ELearning.BLL;
using ELearning.Common.Model;
using ELearning.Models;
using ELearning.Models.ViewModel.YTJYAdmin.ECommon;

namespace ELearning.API.Controllers.YTJYAdminController
{
    /// <summary>
    /// 公共方法资源类
    /// </summary>
    public class YCommonController : BaseApiController
    {
        /// <summary>
        /// 套餐相关操作service
        /// </summary>
        private EPackageService ePackageService => new EPackageService();

        /// <summary>
        /// 获取全部套餐列表，用于下拉框选择
        /// </summary>
        [HttpPost, Route("api/YCommon/GetAllPackageList")]
        public ServiceResponse<AllPackageViewModel> GetAllPackageList(AllPackageRequest request)
        {
            Expression<Func<EPackage, bool>> whereLamdba = x => !x.IsDelete;
            var models = ePackageService.FindList(whereLamdba);

            var viewModel = new AllPackageViewModel
            {
                TotalCount = models.Count(),
                Items = models.Select(x => new AllPackageItem {Id = x.Id, Name = x.Name}).ToList()
            };

            return ServiceResponse<AllPackageViewModel>.SuccessResponse(viewModel);
        }
    }
}