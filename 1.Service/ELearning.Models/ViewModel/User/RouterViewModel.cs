﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models
{
    public class RouterViewModel
    {

        public string path { get; set; }

        public string component { set; get; }

        public bool hidden { set; get; }

        public string redirect { get; set; }

        public string name { get; set; }
        public Meta meta { get; set; }
        public List<RouterViewModel> children { get; set; }
    }
    public class Meta
    {
        public string title { get; set; }

        public string icon { get; set; }

        public bool noCache { get; set; }

        public bool affix { get; set; }
    }
}
