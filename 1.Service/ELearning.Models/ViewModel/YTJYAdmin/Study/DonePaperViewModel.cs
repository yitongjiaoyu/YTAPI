﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.YTJYAdmin.Study
{
    /// <summary>
    /// 获取历史记录分页参数
    /// </summary>
    public class DonePaperRequest : BaseSearchRequest
    {
        /// <summary>
        /// 类型：1.真题，2.模拟题
        /// </summary>
        [Display(Name = "类型：1.真题，2.模拟题")]
        public int Type { get; set; }
    }

    public class DonePaperViewModel
    {
        /// <summary>
        /// 列表
        /// </summary>
        public List<DonePaperItem> Items { get; set; } = new List<DonePaperItem>();

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { get; set; }
    }

    public class DonePaperItem
    {
        /// <summary>
        /// 历史记录Id
        /// </summary>
        public Guid PaperTestHistoryId { get; set; }

        /// <summary>
        /// 试卷Id
        /// </summary>
        [Display(Name = "试卷Id")]
        public Guid PaperId { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        [Display(Name = "课程Id")]
        public Guid CourseId { get; set; }

        /// <summary>
        /// 章节id
        /// </summary>
        [Display(Name = "章节id")]
        public Guid ChapterId { get; set; }

        /// <summary>
        /// 套餐Id
        /// </summary>
        [Display(Name = "套餐Id")]
        public Guid PackageId { get; set; }

        /// <summary>
        /// 考试状态，0.未开始，1.进行中，2.已完成
        /// </summary>
        [Display(Name = "考试状态，0.未开始，1.进行中，2.已完成")]
        public int Status { get; set; }

        /// <summary>
        /// 状态字符串
        /// </summary>
        public string StatusStr
        {
            get
            {
                var str = "未开始";
                switch (Status)
                {
                    case 1:
                        str = "进行中";
                        break;
                    case 2:
                        str = "已完成";
                        break;
                }

                return str;
            }
        }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 类型：1.真题，2.模拟题
        /// </summary>
        [Display(Name = "类型：1.真题，2.模拟题")]
        public int Type { get; set; }

        /// <summary>
        /// 类型字符串
        /// </summary>
        public string TypeStr => Type == 1 ? "真题" : "模拟题";

        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public string CreateTime { get; set; }
    }
}
