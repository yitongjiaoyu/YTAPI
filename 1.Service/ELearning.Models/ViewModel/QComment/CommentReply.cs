﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models
{
public   class CommentReply
{

    /// <summary>
    /// ID
    /// </summary>          
    [Display(Name = "ID")]
    public Guid ID { get; set; }
    /// <summary>
    /// 主表ID
    /// </summary>          
    [Display(Name = "主表ID")]
    public Guid CSumID { get; set; }
    /// <summary>
    /// 评论内容
    /// </summary>          
    [Display(Name = "评论内容")]
    public string Contents { get; set; }
    /// <summary>
    /// 点赞
    /// </summary>          
    [Display(Name = "点赞")]
    public bool Zan { get; set; }
    /// <summary>
    /// 评论用户
    /// </summary>          
    [Display(Name = "评论用户")]
    public Guid FromUID { get; set; }
    /// <summary>
    /// 目标用户
    /// </summary>          
    [Display(Name = "目标用户")]
    public Guid ToUID { get; set; }
    /// <summary>
    /// 更新时间
    /// </summary>          
    [Display(Name = "更新时间")]
    public DateTime UpdateTime { get; set; }
    /// <summary>
    /// 评论级别
    /// </summary>          
    [Display(Name = "评论级别")]
    public int Levels { get; set; }
    }
}
