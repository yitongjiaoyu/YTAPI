//-------------------------------------------------------------------
//文件名称：EVideoStudyLog.cs
//模块名称：EVideoStudyLogModel层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ELearning.Models
{	
	 /// <summary>
    /// EVideoStudyLog表实体类
    /// </summary>
	[Table("EVideoStudyLog")]
	public partial class EVideoStudyLog
    {
		
		/// <summary>
		/// 
		/// </summary>
				[Key]
	[Display(Name = "")]
		public Guid Id { get; set; }
		
		/// <summary>
		/// 套餐名称
		/// </summary>
			[Display(Name = "套餐名称")]
		public Guid PackageId { get; set; }
		
		/// <summary>
		/// 套餐id
		/// </summary>
			[Display(Name = "套餐id")]
		public string PackageName { get; set; }
		
		/// <summary>
		/// 课程ID
		/// </summary>
			[Display(Name = "课程ID")]
		public Guid CourseId { get; set; }
		
		/// <summary>
		/// 课程名称
		/// </summary>
			[Display(Name = "课程名称")]
		public string CourseName { get; set; }
		
		/// <summary>
		/// 章节id
		/// </summary>
			[Display(Name = "章节id")]
		public Guid ChapterId { get; set; }
		
		/// <summary>
		/// 章节名称
		/// </summary>
			[Display(Name = "章节名称")]
		public string ChapterName { get; set; }
		
		/// <summary>
		/// 视频id
		/// </summary>
			[Display(Name = "视频id")]
		public Guid VideoId { get; set; }
		
		/// <summary>
		/// 视频名称
		/// </summary>
			[Display(Name = "视频名称")]
		public string VideoName { get; set; }
		
		/// <summary>
		/// 开始学习时间
		/// </summary>
			[Display(Name = "开始学习时间")]
		public DateTime StartTime { get; set; }
		
		/// <summary>
		/// 结束学习时间
		/// </summary>
			[Display(Name = "结束学习时间")]
		public DateTime EndTime { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string OrgCode { get; set; }
		
		/// <summary>
		/// 学习时长，单位：秒
		/// </summary>
			[Display(Name = "学习时长，单位：秒")]
		public int TimeSpan { get; set; }
		
		/// <summary>
		/// 用户名字
		/// </summary>
			[Display(Name = "用户名字")]
		public string UserName { get; set; }
		
		/// <summary>
		/// 用户id
		/// </summary>
			[Display(Name = "用户id")]
		public Guid UserId { get; set; }
		
		/// <summary>
		/// 是否学习完成
		/// </summary>
			[Display(Name = "是否学习完成")]
		public bool IsFinish { get; set; }
		
		/// <summary>
		/// 创建时间
		/// </summary>
			[Display(Name = "创建时间")]
		public DateTime CreateTime { get; set; }
		   
    }
}

