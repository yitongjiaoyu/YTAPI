﻿using ELearning.BLL;
using ELearning.Common;
using ELearning.Common.Extensions;
using ELearning.Common.Model;
using ELearning.Models;
using ELearning.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace ELearning.API.Controllers.ReportControllers
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ProjectReportController : BaseApiController
    {
        EDealerInfoService dealerInfoService => new EDealerInfoService();
        EAreaCityService areaCityService => new EAreaCityService();

        EProjectService projectService => new EProjectService();

        /// <summary>
        /// 项目完成情况-经销商
        /// </summary>
        /// <param name="ProjectID"></param>
        /// <returns></returns>
        [HttpGet, Route("api/report/projectByDealerReport")]
        public ServiceResponse<ChartView> ProjectByDealerReport(Guid ProjectID)
        {
            var data = dealerInfoService.ProjectByDealerReport(ProjectID);

            var dealerInfos = dealerInfoService.FindList(x => !x.IsDelete).ToList();
            var DelearCodes = data.Select(x => x.DealerCode).Distinct().OrderBy(m => m).ToList();

            ChartView view = new ChartView();

            foreach (var a in DelearCodes)
            {
                var delearName = dealerInfos.Find(x => x.DealerCode == a)?.DealerName;
                view.legend.Add(delearName);
            }

            view.series = new List<ProjectSeries>
            {
                new ProjectSeries
                {
                    name="已报名",
                    stack="1",
                },
                new ProjectSeries
                {
                    name="未报名",
                    stack="1",
                },
                new ProjectSeries
                {
                    name="待审核",
                    stack="2",
                },
                new ProjectSeries
                {
                    name="已通过",
                    stack="2",
                },
                new ProjectSeries
                {
                    name="未通过",
                    stack="2",
                },
                new ProjectSeries
                {
                    name="已完成",
                    stack="3",
                },
                new ProjectSeries
                {
                    name="未完成",
                    stack="3",
                },
            };

            foreach (var a in DelearCodes)
            {
                var data1 = data.Find(x => x.ConfirmStatus == 1 && x.DealerCode == a)?.UserNum ?? 0;
                var data2 = data.Find(x => x.ConfirmStatus == 2 && x.DealerCode == a)?.UserNum ?? 0;
                var data3 = data.Find(x => x.ConfirmStatus == 3 && x.DealerCode == a)?.UserNum ?? 0;
                var data4 = data.Find(x => x.ConfirmStatus == 4 && x.DealerCode == a)?.UserNum ?? 0;
                var data5 = data.Find(x => x.ConfirmStatus == 5 && x.DealerCode == a)?.UserNum ?? 0;
                view.series[0].data.Add(data1 + data2 + data3);
                view.series[1].data.Add(data5 - (data1 + data2 + data3));
                view.series[2].data.Add(data1);
                view.series[3].data.Add(data2);
                view.series[4].data.Add(data3);
                view.series[5].data.Add(data4);
                view.series[6].data.Add(data2 - data4);
            }

            return ServiceResponse<ChartView>.SuccessResponse("成功", view);
        }

        /// <summary>
        /// 项目完成情况-省份
        /// </summary>
        /// <param name="ProjectID"></param>
        /// <returns></returns>
        [HttpGet, Route("api/report/projectByProvinceReport")]
        public ServiceResponse<ChartView> ProjectByProvinceReport(Guid ProjectID)
        {
            var data = dealerInfoService.ProjectByProvinceReport(ProjectID);

            var areaCityInfos = areaCityService.FindList(x => !x.IsDelete && x.TypeLevel == 2).ToList();
            var ProvinceCodes = data.Select(x => x.ProvinceCode).Distinct().OrderBy(m => m).ToList();

            ChartView view = new ChartView();

            foreach (var a in ProvinceCodes)
            {
                var provinceName = areaCityInfos.Find(x => x.ProvinceCode == a)?.ProvinceName;
                view.legend.Add(provinceName);
            }

            view.series = new List<ProjectSeries>
            {
                new ProjectSeries
                {
                    name="分配门店数",
                },
                new ProjectSeries
                {
                    name="已完成门店数",
                },
                new ProjectSeries
                {
                    name="门店完成率",
                },
                new ProjectSeries
                {
                    name="已有人员参训门店数",
                },
                new ProjectSeries
                {
                    name="无人员参训门店数",
                },
                new ProjectSeries
                {
                    name="分配人数",
                },
                new ProjectSeries
                {
                    name="已参训人数",
                },
                new ProjectSeries
                {
                    name="未参训人数",
                },
                new ProjectSeries
                {
                    name="人数完成率",
                },
            };

            foreach (var a in ProvinceCodes)
            {
                // 分配门店数
                var data1 = data.Find(x => x.FxType == 1 && x.ProvinceCode == a)?.DealerNum ?? 0;
                // 已完成门店数
                var data2 = data.Find(x => x.FxType == 2 && x.ProvinceCode == a)?.DealerNum ?? 0;
                // 已有人员参训门店数
                var data3 = data.Find(x => x.FxType == 3 && x.ProvinceCode == a)?.DealerNum ?? 0;
                // 分配人数
                var data4 = data.Find(x => x.FxType == 4 && x.ProvinceCode == a)?.DealerNum ?? 0;
                // 已参训人数
                var data5 = data.Find(x => x.FxType == 5 && x.ProvinceCode == a)?.DealerNum ?? 0;
                // 已完成人数
                var data6 = data.Find(x => x.FxType == 6 && x.ProvinceCode == a)?.DealerNum ?? 0;

                view.series[0].data.Add(data1);
                view.series[1].data.Add(data2);
                view.series[2].data.Add(Math.Round((double)data2 / data1, 2) * 100);
                view.series[3].data.Add(data3);
                view.series[4].data.Add(data1 - data3);
                view.series[5].data.Add(data4);
                view.series[6].data.Add(data5);
                view.series[7].data.Add(data4 - data5);
                view.series[8].data.Add(Math.Round((double)data6 / data4, 2) * 100);
            }

            return ServiceResponse<ChartView>.SuccessResponse("成功", view);
        }

        /// <summary>
        /// 项目完成情况-区域
        /// </summary>
        /// <param name="ProjectID"></param>
        /// <returns></returns>
        [HttpGet, Route("api/report/projectByAreaReport")]
        public ServiceResponse<ChartView> ProjectByAreaReport(Guid ProjectID)
        {
            var data = dealerInfoService.ProjectByAreaReport(ProjectID);

            var areaCityInfos = areaCityService.FindList(x => !x.IsDelete && x.TypeLevel == 2).ToList();
            var AreaCodes = data.Select(x => x.AreaCode).Distinct().OrderBy(m => m).ToList();

            ChartView view = new ChartView();

            foreach (var a in AreaCodes)
            {
                var areaName = areaCityInfos.Find(x => x.AreaCode == a)?.AreaName;
                view.legend.Add(areaName);
            }

            view.series = new List<ProjectSeries>
            {
                new ProjectSeries
                {
                    name="分配门店数",
                },
                new ProjectSeries
                {
                    name="已完成门店数",
                },
                new ProjectSeries
                {
                    name="门店完成率",
                },
                new ProjectSeries
                {
                    name="已有人员参训门店数",
                },
                new ProjectSeries
                {
                    name="无人员参训门店数",
                },
                new ProjectSeries
                {
                    name="分配人数",
                },
                new ProjectSeries
                {
                    name="已参训人数",
                },
                new ProjectSeries
                {
                    name="未参训人数",
                },
                new ProjectSeries
                {
                    name="人数完成率",
                },
            };

            foreach (var a in AreaCodes)
            {
                // 分配门店数
                var data1 = data.Find(x => x.FxType == 1 && x.AreaCode == a)?.DealerNum ?? 0;
                // 已完成门店数
                var data2 = data.Find(x => x.FxType == 2 && x.AreaCode == a)?.DealerNum ?? 0;
                // 已有人员参训门店数
                var data3 = data.Find(x => x.FxType == 3 && x.AreaCode == a)?.DealerNum ?? 0;
                // 分配人数
                var data4 = data.Find(x => x.FxType == 4 && x.AreaCode == a)?.DealerNum ?? 0;
                // 已参训人数
                var data5 = data.Find(x => x.FxType == 5 && x.AreaCode == a)?.DealerNum ?? 0;
                // 已完成人数
                var data6 = data.Find(x => x.FxType == 6 && x.AreaCode == a)?.DealerNum ?? 0;

                view.series[0].data.Add(data1);
                view.series[1].data.Add(data2);
                view.series[2].data.Add(Math.Round((double)data2 / data1, 2) * 100);
                view.series[3].data.Add(data3);
                view.series[4].data.Add(data1 - data3);
                view.series[5].data.Add(data4);
                view.series[6].data.Add(data5);
                view.series[7].data.Add(data4 - data5);
                view.series[8].data.Add(Math.Round((double)data6 / data4, 2) * 100);
            }

            return ServiceResponse<ChartView>.SuccessResponse("成功", view);
        }

        /// <summary>
        /// 项目完成情况-经销商
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/report/getProjects")]
        public ServiceResponse<List<EProject>> GetProjects()
        {
            var data = projectService.FindList(x => !x.IsDelete).OrderByDescending(m => m.ModifyTime).ToList();

            return ServiceResponse<List<EProject>>.SuccessResponse("成功", data);
        }
    }
}