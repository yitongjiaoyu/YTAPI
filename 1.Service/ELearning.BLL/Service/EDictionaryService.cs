﻿using ELearning.DAL;
using ELearning.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.BLL
{
    public partial class EDictionaryService : BaseService<EDictionary>
    {
        public List<EDictionary> GetLevelListById(Guid id)
        {
            var rs = RepositoryFactory.EDictionaryRepository.GetDicListById(id);
            return rs;
        }

        public List<EDictionary> GetDicNoParent(string code)
        {
            var rs = RepositoryFactory.EDictionaryRepository.GetDicNoParent(code);
            return rs;
        }

        public List<EDictionary> GetDicInParent(string code)
        {
            var rs = RepositoryFactory.EDictionaryRepository.GetDicInParent(code);
            return rs;
        }

        public List<EDictionary> GetDicParent(string code)
        {
            var rs = RepositoryFactory.EDictionaryRepository.GetDicParent(code);
            return rs;
        }

        /// <summary>
        /// 根据多个以逗号分隔的父节点获取所有子节点
        /// </summary>
        /// <param name="id">等级id</param>
        public List<string> GetDicChildAll(string codeArr)
        {
            var rs = RepositoryFactory.EDictionaryRepository.GetDicChildAll(codeArr);
            return rs;
        }

        /// <summary>
        /// 字典数据删除，逻辑：被使用的字典不可删除
        /// </summary>
        /// <param name="id">字典ID</param>
        public int GetDicUsedCount(string dicID)
        {
            var rs = RepositoryFactory.EDictionaryRepository.GetDicUsedCount(dicID);
            return rs;
        }

    }
}
