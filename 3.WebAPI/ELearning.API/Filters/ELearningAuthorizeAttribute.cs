﻿using System.Web.Http;
using System.Web.Http.Controllers;

namespace ELearning.API
{
    public class ELearningAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            base.OnAuthorization(actionContext);
        }
    }
}