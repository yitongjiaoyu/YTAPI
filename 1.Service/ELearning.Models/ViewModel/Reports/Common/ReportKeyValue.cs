﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel
{
    public class ReportKeyValue
    {
        public string Key { get; set; } = string.Empty;
        public int Values { get; set; } = 0;
    }
}
