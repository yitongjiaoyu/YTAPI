//-------------------------------------------------------------------
//文件名称：EOrg.cs
//模块名称：EOrgModel层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ELearning.Models
{	
	 /// <summary>
    /// EOrg表实体类
    /// </summary>
	[Table("EOrg")]
	public partial class EOrg
    {
		
		/// <summary>
		/// 
		/// </summary>
				[Key]
	[Display(Name = "")]
		public Guid org_id { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string org_code { get; set; }
		
		/// <summary>
		/// 父级编码
		/// </summary>
			[Display(Name = "父级编码")]
		public string parent_code { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string org_name { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public int org_type { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string org_owner { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string owner_call { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string owner_phone { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string org_adress { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string org_remark { get; set; }
		
		/// <summary>
		/// 邀请码
		/// </summary>
			[Display(Name = "邀请码")]
		public string InviteCode { get; set; }
		
		/// <summary>
		/// 学生账号上限
		/// </summary>
			[Display(Name = "学生账号上限")]
		public int AccountLimit { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Creater { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public DateTime CreateTime { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Modifyer { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public DateTime ModifyTime { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public bool IsDelete { get; set; }
		   
    }
}

