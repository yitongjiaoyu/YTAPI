﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Common
{
    public enum QueTypeEunm
    {
        /// <summary>
        /// 单选题
        /// </summary>
        [Description("单选题")]
        Single = 1,
        /// <summary>
        /// 多选题
        /// </summary>
        [Description("多选题")]
        Multiple = 2,
        /// <summary>
        /// 判断题
        /// </summary>
        [Description("判断题")]
        Judge = 3,
        /// <summary>
        /// 填空题
        /// </summary>
        [Description("填空题")]
        Fill = 4,
    }
}

