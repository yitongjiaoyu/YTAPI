﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.Question
{
    public class QuestionModel
    {
        [Display(Name = "Id")]
        public Guid Id { get; set; }

        /// <summary>
        /// 选项ID
        /// </summary>
        public Guid? OptionId { get; set; }

        /// <summary>
        /// 主题干，材料题，解析题等类型需要
        /// </summary>
        [Display(Name = "主题干，材料题，解析题等类型需要")]
        public string MainName { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 正确答案,例如：/id1/id2/
        /// </summary>
        [Display(Name = "正确答案,例如：/id1/id2/")]
        public string RightAnswer { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [Display(Name = "序号")]
        public double Sort { get; set; }
       
        /// <summary>
        /// 分数
        /// </summary>
        [Display(Name = "分数")]
        public double Grade { get; set; }

        /// <summary>
        /// 大类分类Code
        /// </summary>
        [Display(Name = "大类分类Code")]
        public string CategoryId { get; set; }

        /// <summary>
        /// 分类名称
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// 班级基础分类code,基础班，高级版
        /// </summary>
        [Display(Name = "班级基础分类code,基础班，高级版")]
        public string TypeCode { get; set; }

        /// <summary>
        /// 基础分类名称
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// 题目类型，关联类型EQuestionType表中code字段
        /// </summary>
        [Display(Name = "题目类型，关联类型EQuestionType表中code字段")]
        public string QuestionTypeCode { get; set; }
        
        /// <summary>
        /// 题目类型名称
        /// </summary>
        public string QuestionTypeName { get; set; }

        /// <summary>
        /// 组织编码
        /// </summary>
        [Display(Name = "组织编码")]
        public string OrgCode { get; set; }

        /// <summary>
        /// 题目解析
        /// </summary>
        [Display(Name = "题目解析")]
        public string Analysis { get; set; }

        /// <summary>
        /// 题目难度，1.简单，2.一般，3.困难
        /// </summary>
        [Display(Name = "题目难度，1.简单，2.一般，3.困难")]
        public int Difficulty { get; set; }

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        [Display(Name = "更新时间")]
        public DateTime UpdateTime { get; set; }

        /// <summary>
        /// 创建人Id
        /// </summary>
        [Display(Name = "创建人Id")]
        public Guid CreateUserId { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 编辑人id
        /// </summary>
        [Display(Name = "编辑人id")]
        public Guid UpdateUserId { get; set; }
    }

    /// <summary>
    /// 导入题目实体
    /// </summary>
    public class ImportQuestionModel
    {
        /// <summary>
        /// 题目类型名称
        /// </summary>
        public string QuestionTypeName { get; set; }
        
        /// <summary>
        /// 主题干，材料题，解析题等类型需要
        /// </summary>
        [Display(Name = "主题干，材料题，解析题等类型需要")]
        public string MainName { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 大类分类名称
        /// </summary>
        [Display(Name = "大类分类名称")]
        public string CategoryName { get; set; }

        /// <summary>
        /// 班级基础分类code,基础班，高级版
        /// </summary>
        [Display(Name = "班级基础分类code,基础班，高级版")]
        public string TypeName { get; set; }

        /// <summary>
        /// 原因
        /// </summary>
        public string Reason { get; set; }

        /// <summary>
        /// 选项A
        /// </summary>
        public string OptionNameA { get; set; }

        /// <summary>
        /// 选项B
        /// </summary>
        public string OptionNameB { get; set; }

        /// <summary>
        /// 选项C
        /// </summary>
        public string OptionNameC { get; set; }

        /// <summary>
        /// 选项D
        /// </summary>
        public string OptionNameD { get; set; }

        /// <summary>
        /// 选项E
        /// </summary>
        public string OptionNameE { get; set; }

        /// <summary>
        /// 选项F
        /// </summary>
        public string OptionNameF { get; set; }

        /// <summary>
        /// 选项G
        /// </summary>
        public string OptionNameG { get; set; }

        /// <summary>
        /// 选项H
        /// </summary>
        public string OptionNameH { get; set; }

        /// <summary>
        /// 正确答案,例如：ABC
        /// </summary>
        [Display(Name = "正确答案")]
        public string RightAnswer { get; set; }

        /// <summary>
        /// 题目解析
        /// </summary>
        [Display(Name = "题目解析")]
        public string Analysis { get; set; }

        /// <summary>
        /// 题目难度，1.简单，2.一般，3.困难
        /// </summary>
        [Display(Name = "题目难度，1.简单，2.一般，3.困难")]
        public string DifficultyStr { get; set; }

        /// <summary>
        /// 题目难度
        /// </summary>
        public int Difficulty {
            get
            {
                var i = 2;//默认难度一般
                if (DifficultyStr == "简单")
                    i = 1;
                if (DifficultyStr == "困难")
                    i = 3;
                return i;
            } }
    }
}
