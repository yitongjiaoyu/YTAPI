﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel
{
    public class UserPic : CourseApplyDetail
    {
        /// <summary>
        /// 积分
        /// </summary>
        public decimal Credit { get; set; }
        /// <summary>
        /// 头衔
        /// </summary>
        public string ForumTitle { get; set; }
        /// <summary>
        /// 发帖数量
        /// </summary>
        public int PostedNum { get; set; }
        /// <summary>
        /// 回帖数量
        /// </summary>
        public int ReplyNum { get; set; }
        /// <summary>
        /// 回复他人数量
        /// </summary>
        public int ReplyOtherNum { get; set; }
        /// <summary>
        /// 课程报名数量
        /// </summary>
        public int CourseApplyNum { get; set; }
        /// <summary>
        /// 课程完成数量
        /// </summary>
        public int CourseFinishNum { get; set; }
        /// <summary>
        /// 考试平均分
        /// </summary>
        public decimal AvgScore { get; set; }
        /// <summary>
        /// 考试次数
        /// </summary>
        public int ExamdNum { get; set; }

    }
}
