﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;
using ELearning.BLL;
using ELearning.Common.CommonStr;
using ELearning.Common.Extensions;
using ELearning.Common.Helpers;
using ELearning.Common.Model;
using ELearning.Models;
using ELearning.Models.ViewModel.Question;
using ELearning.Models.ViewModel.YTJYAdmin.Paper;

namespace ELearning.API.Controllers.YTJYAdminController
{
    /// <summary>
    /// 试卷相关控制器
    /// </summary>
    public class TPaperController : BaseApiController
    {
        /// <summary>
        /// 试卷相关操作service
        /// </summary>
        private TPaperService tPaperService => new TPaperService();

        /// <summary>
        /// 试卷题目相关操作service
        /// </summary>
        private TPaperQuestionService tPaperQuestionService => new TPaperQuestionService();

        /// <summary>
        /// 字典操作service  
        /// </summary>
        private EDictionaryService eDictionaryService => new EDictionaryService();

        /// <summary>
        /// 题目类型操作service
        /// </summary>
        private TQuestionTypeService tQuestionTypeService => new TQuestionTypeService();

        /// <summary>
        /// 获取课程表
        /// </summary>
        [HttpPost, Route("api/ytjyadmin/TPaper/GetModelList")]
        public ServiceResponse<TPaperViewModel> GetModelList(TPaperRequest request)
        {
            var viewModel = tPaperService.GetModelList(request);

            return ServiceResponse<TPaperViewModel>.SuccessResponse(CommonConst.OprateSuccessStr, viewModel);
        }

        /// <summary>
        /// 新增或者更新信息
        /// </summary>
        [HttpPost, Route("api/ytjyadmin/TPaper/PostModel")]
        public ServiceResponse<bool> PostModel(TPaperPostModel viewModel)
        {
            var model = tPaperService.Find(v => v.Id == viewModel.Id);
            if (string.IsNullOrEmpty(viewModel.OrgCode))
                viewModel.OrgCode = CurrentUserModel?.OrgCode ?? string.Empty;
            if (model.IsNotNull())//编辑
            {
                model.Name = viewModel.Name;
                model.CategoryId = viewModel.CategoryId;
                model.TypeCode = viewModel.TypeCode;
                model.Description = viewModel.Description;
                model.TimeSpan = viewModel.TimeSpan;
                model.TotalCount = viewModel.TotalCount;
                model.TotalScore = viewModel.TotalScore;
                model.Type = viewModel.Type;
                model.Year = viewModel.Year;
                model.OrgCode = viewModel.OrgCode;
                model.Sort = viewModel.Sort;

                model.UpdateTime = DateTime.Now;
                model.UpdateUserId = CurrentUserModel?.UserId ?? Guid.Empty;

                tPaperService.Update(model);
            }
            else//新增
            {
                model = viewModel.MapTo<TPaper, TPaperPostModel>();
                model.UpdateTime = DateTime.Now;
                model.UpdateUserId = CurrentUserModel?.UserId ?? Guid.Empty;

                model.CreateTime = DateTime.Now;
                model.CreateUserId = CurrentUserModel?.UserId ?? Guid.Empty;
                model.Id = Guid.NewGuid();
                model.IsUsed = true;

                tPaperService.Add(model);
            }

            return ServiceResponse<bool>.SuccessResponse(CommonConst.OprateSuccessStr, true);
        }

        /// <summary>
        /// 删除
        /// </summary>
        [HttpGet, Route("api/ytjyadmin/TPaper/DeleteModel")]
        public ServiceResponse<bool> DeleteModel(Guid id)
        {
            var model = tPaperService.Find(v => v.Id == id);

            if (model.IsNotNull())
            {
                model.IsDelete = true;
                model.UpdateTime = DateTime.Now;
                model.UpdateUserId = CurrentUserModel?.UserId ?? Guid.Empty;
                tPaperService.Update(model);
            }

            return ServiceResponse<bool>.SuccessResponse(CommonConst.OprateSuccessStr, true);
        }

        /// <summary>
        /// 获取关联或者不关联的列表
        /// </summary>
        [HttpPost, Route("api/ytjyadmin/TPaper/GetModelRelevantList")]
        public ServiceResponse<TPaperQuestionViewModel> GetModelRelevantList(TPaperQuestionRequest request)
        {
            var viewModel = tPaperService.GetModelRelevantList(request);

            return ServiceResponse<TPaperQuestionViewModel>.SuccessResponse(CommonConst.OprateSuccessStr, viewModel);
        }

        /// <summary>
        /// 保存关联或者不关联的元素
        /// </summary>
        [HttpPost, Route("api/ytjyadmin/TPaper/SaveModelRelevante")]
        public ServiceResponse<bool> SaveModelRelevante(TPaperQuestionPostModel request)
        {
            if (request.Type == 0)//要进行关联操作
            {
                if (request.QuestionIdList.Any())
                {
                    var addList = new List<TPaperQuestion>();
                    foreach (var id in request.QuestionIdList)
                    {
                        var addModel = new TPaperQuestion
                        {
                            Id = Guid.NewGuid(),
                            QuestionId = id,
                            CreateUserId = CurrentUserModel.UserId,
                            CreateTime = DateTime.Now,
                            IsDelete = false,
                            Sort = 0,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId,
                            PaperId = request.PaperId
                        };
                        addList.Add(addModel);
                    }
                    if (addList.Any())
                    {
                        var dt = addList.ToDataTable(null);
                        dt.TableName = "TPaperQuestion";
                        SqlBulkCopyHelper.SaveTable(dt);
                    }
                }
            }
            else//要进行取消关联操作
            {
                if (request.QuestionIdList.Any())
                {
                    var deleteList = tPaperQuestionService.FindList(r => !r.IsDelete && r.PaperId == request.PaperId && request.QuestionIdList.Contains(r.QuestionId)).ToList();
                    if (deleteList.Any())
                    {
                        foreach (var d in deleteList)
                        {
                            d.IsDelete = true;
                            d.UpdateTime = DateTime.Now;
                            d.UpdateUserId = CurrentUserModel.UserId;
                            tPaperQuestionService.UpdateNoSaveChange(d);
                        }

                        tPaperQuestionService.SaveChange();
                    }
                }
            }

            return ServiceResponse<bool>.SuccessResponse(CommonConst.OprateSuccessStr, true);
        }

        /// <summary>
        /// 获取试卷列表，用户下拉框选择
        /// </summary>
        [HttpGet, Route("api/ytjyadmin/TPaper/GetAllPaper")]
        public ServiceResponse<object> GetAllPaper()
        {
            var models = tPaperService.FindList(p => !p.IsDelete);
            var respModel = models.Select(p => new { value = p.Id, label = p.Name }).ToList();

            return ServiceResponse<object>.SuccessResponse(CommonConst.OprateSuccessStr, respModel);
        }

        /// <summary>
        /// 导入题目
        /// </summary>
        [HttpPost, Route("api/ytjyadmin/TPaper/ImportModels")]
        public object ImportModels(Guid paperId)
        {
            var filelist = HttpContext.Current.Request.Files;
            var models = new List<ImportQuestionModel>();
            var path = HttpContext.Current.Server.MapPath("/ImportConfig/importQueestionConfig.xml");
            var xr = XMLHelper.ReadToHashtable(path);
            if (filelist.Count > 0)
            {
                var file = filelist[0];
                var fileName = file.FileName;
                var fn = fileName.Split('\\');
                if (fn.Length > 1)
                {
                    fileName = fn[fn.Length - 1];
                }
                DataTable dataTable = null;
                var fs = fileName.Split('.');
                if (fs.Length > 1)
                {
                    var ext = fs[fs.Length - 1];
                    if (ext == "xlsx")
                        dataTable = ExcelHelp.ExcelToTableForXLSX(file.InputStream, xr.ImportHashtable, true); //excel转成datatable
                    else
                        dataTable = ExcelHelp.ExcelToTableForXLS(file.InputStream); //excel转成datatable
                }
                models = dataTable.ToDataList<ImportQuestionModel>(); //datatable转成list
            }

            var faile = new List<ImportQuestionModel>();
            var exportList = new List<ImportQuestionModel>();
            var dics = eDictionaryService.FindList(d => !d.IsDelete).ToList();
            var categroy = dics.FirstOrDefault(d => d.TypeCode == DictionaryConst.CATEGORY_CODE);
            var categorys = dics.Where(d => d.ParentID == categroy?.ID).ToList();

            var cls = dics.FirstOrDefault(d => d.TypeCode == DictionaryConst.CLASS_TYPE_CODE);
            var clss = dics.Where(d => d.ParentID == cls?.ID).ToList();

            var succeQues = new List<TQuestion>();
            var queItems = new List<TQuestionItem>();
            var options = new List<TOption>();
            var rQuestionOptions = new List<RQuestionOption>();

            TQuestion que = null;
            TQuestionItem queItem = null;

            var qts = tQuestionTypeService.FindList(x => !x.IsDelete).ToList();

            var tempOptions = new List<TOption>();//配伍选择题使用
            var i = 2;//第几行,第一行为须知，第二行为列名
            //数据list转成数据库实体对应的list
            foreach (var m in models)
            {
                i++;
                //判断题目类型输入是否正确
                var qt = qts.FirstOrDefault(x => x.TypeName == m.QuestionTypeName.Trim())?.TypeCode ?? string.Empty;
                if (string.IsNullOrEmpty(qt))
                {
                    m.Reason = $"第{i}行题目类型错误";
                    faile.Add(m);
                    exportList.Add(m);
                    continue;
                }

                var ca = categorys.FirstOrDefault(x => x.TypeName == m.CategoryName.Trim());
                if (ca.IsNull())
                {
                    m.Reason = $"第{i}行类型名称不存在，请确认后重新上传！";
                    faile.Add(m);
                    exportList.Add(m);
                    continue;
                }

                var ct = clss.FirstOrDefault(x => x.TypeName == m.TypeName.Trim());
                if (ct.IsNull())
                {
                    m.Reason = $"第{i}行班级类型不存在，请确认后重新上传！";
                    faile.Add(m);
                    exportList.Add(m);
                    continue;
                }

                int.TryParse(qt, out var iqt);

                //判断当前记录与上一条记录是否为同一个题目
                if (que.IsNotNull() && que.QuestionTypeCode != qt)
                {
                    que = null;
                    queItem = null;
                }

                // 单选题、多选题、判断题、填空题
                if (iqt < 5)
                {
                    que = null;
                    queItem = null;

                    if (que.IsNull() && string.IsNullOrEmpty(m.Name))
                    {
                        m.Reason = $"第{i}行题目必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }
                    if (que.IsNull() && string.IsNullOrEmpty(m.QuestionTypeName))
                    {
                        m.Reason = $"第{i}行试题类型必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }
                    if (que.IsNull() && string.IsNullOrEmpty(m.OptionNameA))
                    {
                        m.Reason = $"第{i}行选项A必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }
                    //填空题可以写一个选项
                    if (que.IsNull() && string.IsNullOrEmpty(m.OptionNameB) && iqt != 4)
                    {
                        m.Reason = $"第{i}行选项B必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }
                    if (que.IsNull() && string.IsNullOrEmpty(m.RightAnswer))
                    {
                        m.Reason = $"第{i}行正确答案必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }

                    que = new TQuestion
                    {
                        Id = Guid.NewGuid(),
                        Analysis = m.Analysis,
                        CategoryId = ca.TypeCode,
                        TypeCode = ct.TypeCode,
                        IsDelete = false,
                        CreateTime = DateTime.Now,
                        OrgCode = CurrentUserModel.OrgCode,
                        CreateUserId = CurrentUserModel.UserId,
                        UpdateTime = DateTime.Now,
                        UpdateUserId = CurrentUserModel.UserId,
                        Name = m.Name,
                        Sort = 0,
                        RightAnswer = "/",
                        Grade = 0,
                        QuestionTypeCode = qt,
                        Difficulty = m.Difficulty,
                        MainName = string.Empty,
                        Precondition = string.Empty
                    };
                    succeQues.Add(que);

                    queItem = new TQuestionItem
                    {
                        Id = Guid.NewGuid(),
                        Analysis = m.Analysis,
                        CategoryId = ca.TypeCode,
                        TypeCode = ct.TypeCode,
                        IsDelete = false,
                        CreateTime = DateTime.Now,
                        OrgCode = CurrentUserModel.OrgCode,
                        CreateUserId = CurrentUserModel.UserId,
                        UpdateTime = DateTime.Now,
                        UpdateUserId = CurrentUserModel.UserId,
                        Name = m.Name,
                        Sort = 0,
                        RightAnswer = "/",
                        Grade = 0,
                        QuestionTypeCode = qt,
                        Difficulty = m.Difficulty,
                        Precondition = string.Empty,
                        QuestionId = que.Id
                    };
                    queItems.Add(queItem);

                    #region 保存选项到列表

                    //所有的选项都放到一个列表中
                    var ops = new List<TOption>
                    {
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameA,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        },
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameB,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        },
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameC,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        },
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameD,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        },
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameE,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        },
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameF,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        },
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameG,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        },
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameH,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        }
                    };

                    var rops = new List<RQuestionOption>();
                    //先正确的选项加进去
                    foreach (var a in m.RightAnswer)
                    {
                        var index = 0;
                        switch (a)
                        {
                            case 'A':
                                index = 0;
                                break;
                            case 'B':
                                index = 1;
                                break;
                            case 'C':
                                index = 2;
                                break;
                            case 'D':
                                index = 3;
                                break;
                            case 'E':
                                index = 4;
                                break;
                            case 'F':
                                index = 5;
                                break;
                            case 'G':
                                index = 6;
                                break;
                            case 'H':
                                index = 7;
                                break;
                        }

                        var op = ops[index];
                        rops.Add(new RQuestionOption
                        {
                            Id = Guid.NewGuid(),
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId,
                            IsRight = true,
                            OptionId = op.Id,
                            QuestionId = que.Id,
                            QuestionItemId = queItem.Id,
                            Sort = 0
                        });
                    }

                    //把其他的选项也加入到这个列表中
                    foreach (var op in ops)
                    {
                        if (string.IsNullOrEmpty(op.Name))
                            continue;
                        options.Add(op);
                        var t = rops.FirstOrDefault(x => x.OptionId == op.Id);
                        if (t.IsNull())
                        {
                            rops.Add(new RQuestionOption
                            {
                                Id = Guid.NewGuid(),
                                IsDelete = false,
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId,
                                IsRight = false,
                                OptionId = op.Id,
                                QuestionId = que.Id,
                                QuestionItemId = queItem.Id,
                                Sort = 0
                            });
                        }
                    }
                    rQuestionOptions.AddRange(rops);

                    #endregion
                }
                else if (iqt == 5) //配伍选择题
                {
                    queItem = null;

                    //所有的选项都放到一个列表中
                    var ops = new List<TOption>
                    {
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameA,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        },
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameB,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        },
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameC,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        },
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameD,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        },
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameE,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        },
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameF,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        },
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameG,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        },
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameH,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        }
                    };

                    var haveOps = ops.Where(x => !string.IsNullOrEmpty(x.Name)).Select(x => x.Name).ToList();
                    var mainName = string.Empty;
                    if (haveOps.Any())
                    {
                        mainName = string.Join("/", haveOps);
                        tempOptions = ops;//临时变量里面的选项置为下一个配伍题的选项
                        foreach (var op in ops)
                        {
                            if (!string.IsNullOrEmpty(op.Name))
                                options.Add(op);
                        }
                    }

                    //下一个配伍题
                    if (!string.IsNullOrEmpty(mainName) && que.IsNotNull() && !que.MainName.Equals(mainName))
                    {
                        que = null;
                    }

                    if (que.IsNull() && string.IsNullOrEmpty(m.Name))
                    {
                        m.Reason = $"第{i}行题目必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }
                    if (que.IsNull() && string.IsNullOrEmpty(m.QuestionTypeName))
                    {
                        m.Reason = $"第{i}行试题类型必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }
                    if (que.IsNull() && string.IsNullOrEmpty(m.OptionNameA))
                    {
                        m.Reason = $"第{i}行选项A必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }
                    if (que.IsNull() && string.IsNullOrEmpty(m.OptionNameB))
                    {
                        m.Reason = $"第{i}行选项B必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }
                    if (que.IsNull() && string.IsNullOrEmpty(m.RightAnswer))
                    {
                        m.Reason = $"第{i}行正确答案必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }

                    if (que.IsNull())
                    {
                        que = new TQuestion
                        {
                            Id = Guid.NewGuid(),
                            Analysis = m.Analysis,
                            CategoryId = ca.TypeCode,
                            TypeCode = ct.TypeCode,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            OrgCode = CurrentUserModel.OrgCode,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId,
                            Name = m.Name,
                            Sort = 0,
                            RightAnswer = "/",
                            Grade = 0,
                            QuestionTypeCode = qt,
                            Difficulty = m.Difficulty,
                            MainName = mainName,
                            Precondition = string.Empty
                        };
                        succeQues.Add(que);
                    }

                    if (queItem.IsNull())
                    {
                        queItem = new TQuestionItem
                        {
                            Id = Guid.NewGuid(),
                            Analysis = m.Analysis,
                            CategoryId = ca.TypeCode,
                            TypeCode = ct.TypeCode,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            OrgCode = CurrentUserModel.OrgCode,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId,
                            Name = m.Name,
                            Sort = 0,
                            RightAnswer = "/",
                            Grade = 0,
                            QuestionTypeCode = qt,
                            Difficulty = m.Difficulty,
                            Precondition = string.Empty,
                            QuestionId = que.Id
                        };
                        queItems.Add(queItem);
                    }

                    var rops = new List<RQuestionOption>();
                    //先正确的选项加进去
                    foreach (var a in m.RightAnswer)
                    {
                        var index = 0;
                        switch (a)
                        {
                            case 'A':
                                index = 0;
                                break;
                            case 'B':
                                index = 1;
                                break;
                            case 'C':
                                index = 2;
                                break;
                            case 'D':
                                index = 3;
                                break;
                            case 'E':
                                index = 4;
                                break;
                            case 'F':
                                index = 5;
                                break;
                            case 'G':
                                index = 6;
                                break;
                            case 'H':
                                index = 7;
                                break;
                        }

                        var op = tempOptions[index];
                        rops.Add(new RQuestionOption
                        {
                            Id = Guid.NewGuid(),
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId,
                            IsRight = true,
                            OptionId = op.Id,
                            QuestionId = que.Id,
                            QuestionItemId = queItem.Id,
                            Sort = 0
                        });
                    }

                    //把其他的选项也加入到这个列表中
                    foreach (var op in tempOptions)
                    {
                        if (string.IsNullOrEmpty(op.Name))
                            continue;
                        var t = rops.FirstOrDefault(x => x.OptionId == op.Id);
                        if (t.IsNull())
                        {
                            rops.Add(new RQuestionOption
                            {
                                Id = Guid.NewGuid(),
                                IsDelete = false,
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId,
                                IsRight = false,
                                OptionId = op.Id,
                                QuestionId = que.Id,
                                QuestionItemId = queItem.Id,
                                Sort = 0
                            });
                        }
                    }
                    rQuestionOptions.AddRange(rops);
                }
                //案例分析选择题、综合分析选择题
                else if (iqt == 6 || iqt == 7) //案例分析选择题、综合分析选择题
                {
                    //当前记录类型与上一条记录类型不一致，实体置为空
                    if (que.IsNotNull() && que.QuestionTypeCode != qt)
                    {
                        que = null;
                        queItem = null;
                    }
                    // 共用题干不一致，说明是下一道题目
                    if (que.IsNotNull() && !string.IsNullOrEmpty(m.MainName) && !que.MainName.Equals(m.MainName))
                    {
                        que = null;
                        queItem = null;
                    }

                    if (que.IsNotNull() && !string.IsNullOrEmpty(m.Name) && !queItem.Name.Equals(m.Name))
                    {
                        queItem = null;
                    }

                    if (que.IsNull() && string.IsNullOrEmpty(m.MainName))
                    {
                        m.Reason = $"第{i}行共用题干必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }

                    if (string.IsNullOrEmpty(m.Name))
                    {
                        m.Reason = $"第{i}行题目必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }

                    if (string.IsNullOrEmpty(m.QuestionTypeName))
                    {
                        m.Reason = $"第{i}行试题类型必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }
                    if (string.IsNullOrEmpty(m.OptionNameA))
                    {
                        m.Reason = $"第{i}行选项A必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }

                    if (string.IsNullOrEmpty(m.OptionNameB))
                    {
                        m.Reason = $"第{i}行选项B必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }
                    if (string.IsNullOrEmpty(m.RightAnswer))
                    {
                        m.Reason = $"第{i}行正确答案必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }

                    if (que.IsNull())
                    {
                        que = new TQuestion
                        {
                            Id = Guid.NewGuid(),
                            Analysis = m.Analysis,
                            CategoryId = ca.TypeCode,
                            TypeCode = ct.TypeCode,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            OrgCode = CurrentUserModel.OrgCode,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId,
                            Name = m.Name,
                            Sort = 0,
                            RightAnswer = "/",
                            Grade = 0,
                            QuestionTypeCode = qt,
                            Difficulty = m.Difficulty,
                            MainName = m.MainName,
                            Precondition = string.Empty
                        };
                        succeQues.Add(que);
                    }

                    if (queItem.IsNull())
                    {
                        queItem = new TQuestionItem
                        {
                            Id = Guid.NewGuid(),
                            Analysis = m.Analysis,
                            CategoryId = ca.TypeCode,
                            TypeCode = ct.TypeCode,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            OrgCode = CurrentUserModel.OrgCode,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId,
                            Name = m.Name,
                            Sort = 0,
                            RightAnswer = "/",
                            Grade = 0,
                            QuestionTypeCode = qt,
                            Difficulty = m.Difficulty,
                            Precondition = string.Empty,
                            QuestionId = que.Id
                        };
                        queItems.Add(queItem);

                        #region 保存选项到列表

                        //所有的选项都放到一个列表中
                        var ops = new List<TOption>
                        {
                            new TOption
                            {
                                Id = Guid.NewGuid(),
                                Name = m.OptionNameA,
                                IsDelete = false,
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId
                            },
                            new TOption
                            {
                                Id = Guid.NewGuid(),
                                Name = m.OptionNameB,
                                IsDelete = false,
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId
                            },
                            new TOption
                            {
                                Id = Guid.NewGuid(),
                                Name = m.OptionNameC,
                                IsDelete = false,
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId
                            },
                            new TOption
                            {
                                Id = Guid.NewGuid(),
                                Name = m.OptionNameD,
                                IsDelete = false,
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId
                            },
                            new TOption
                            {
                                Id = Guid.NewGuid(),
                                Name = m.OptionNameE,
                                IsDelete = false,
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId
                            },
                            new TOption
                            {
                                Id = Guid.NewGuid(),
                                Name = m.OptionNameF,
                                IsDelete = false,
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId
                            },
                            new TOption
                            {
                                Id = Guid.NewGuid(),
                                Name = m.OptionNameG,
                                IsDelete = false,
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId
                            },
                            new TOption
                            {
                                Id = Guid.NewGuid(),
                                Name = m.OptionNameH,
                                IsDelete = false,
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId
                            }
                        };

                        var rops = new List<RQuestionOption>();
                        //先正确的选项加进去
                        foreach (var a in m.RightAnswer)
                        {
                            var index = 0;
                            switch (a)
                            {
                                case 'A':
                                    index = 0;
                                    break;
                                case 'B':
                                    index = 1;
                                    break;
                                case 'C':
                                    index = 2;
                                    break;
                                case 'D':
                                    index = 3;
                                    break;
                                case 'E':
                                    index = 4;
                                    break;
                                case 'F':
                                    index = 5;
                                    break;
                                case 'G':
                                    index = 6;
                                    break;
                                case 'H':
                                    index = 7;
                                    break;
                            }

                            var op = ops[index];
                            rops.Add(new RQuestionOption
                            {
                                Id = Guid.NewGuid(),
                                IsDelete = false,
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId,
                                IsRight = true,
                                OptionId = op.Id,
                                QuestionId = que.Id,
                                QuestionItemId = queItem.Id,
                                Sort = 0
                            });
                        }

                        //把其他的选项也加入到这个列表中
                        foreach (var op in ops)
                        {
                            if (string.IsNullOrEmpty(op.Name))
                                continue;
                            options.Add(op);
                            var t = rops.FirstOrDefault(x => x.OptionId == op.Id);
                            if (t.IsNull())
                            {
                                rops.Add(new RQuestionOption
                                {
                                    Id = Guid.NewGuid(),
                                    IsDelete = false,
                                    CreateTime = DateTime.Now,
                                    CreateUserId = CurrentUserModel.UserId,
                                    UpdateTime = DateTime.Now,
                                    UpdateUserId = CurrentUserModel.UserId,
                                    IsRight = false,
                                    OptionId = op.Id,
                                    QuestionId = que.Id,
                                    QuestionItemId = queItem.Id,
                                    Sort = 0
                                });
                            }
                        }
                        rQuestionOptions.AddRange(rops);

                        #endregion
                    }
                }
                //主观题、综合分析简答题
                else if (iqt == 8 || iqt == 9) //主观题、综合分析简答题  -- 完成
                {
                    queItem = null;

                    //当前记录类型与上一条记录类型不一致，实体置为空
                    if (que.IsNotNull() && que.QuestionTypeCode != qt)
                    {
                        que = null;
                        queItem = null;
                    }
                    // 共用题干不一致，说明是下一道题目
                    if (que.IsNotNull() && !string.IsNullOrEmpty(m.MainName) && !que.MainName.Equals(m.MainName))
                    {
                        que = null;
                        queItem = null;
                    }

                    if (queItem.IsNotNull() && !string.IsNullOrEmpty(m.Name) && !queItem.Name.Equals(m.Name))
                    {
                        queItem = null;
                    }

                    if (que.IsNull() && string.IsNullOrEmpty(m.MainName))
                    {
                        m.Reason = $"第{i}行共用题干必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }
                    if (string.IsNullOrEmpty(m.Name))
                    {
                        m.Reason = $"第{i}行题目必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }
                    if (string.IsNullOrEmpty(m.QuestionTypeName))
                    {
                        m.Reason = $"第{i}行试题类型必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }
                    if (string.IsNullOrEmpty(m.OptionNameA))
                    {
                        m.Reason = $"第{i}行选项A必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }

                    if (que.IsNull())
                    {
                        que = new TQuestion
                        {
                            Id = Guid.NewGuid(),
                            Analysis = m.Analysis,
                            CategoryId = ca.TypeCode,
                            TypeCode = ct.TypeCode,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            OrgCode = CurrentUserModel.OrgCode,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId,
                            Name = m.Name,
                            Sort = 0,
                            RightAnswer = "/",
                            Grade = 0,
                            QuestionTypeCode = qt,
                            Difficulty = m.Difficulty,
                            MainName = m.MainName,
                            Precondition = string.Empty
                        };
                        succeQues.Add(que);
                    }

                    queItem = new TQuestionItem
                    {
                        Id = Guid.NewGuid(),
                        Analysis = m.Analysis,
                        CategoryId = ca.TypeCode,
                        TypeCode = ct.TypeCode,
                        IsDelete = false,
                        CreateTime = DateTime.Now,
                        OrgCode = CurrentUserModel.OrgCode,
                        CreateUserId = CurrentUserModel.UserId,
                        UpdateTime = DateTime.Now,
                        UpdateUserId = CurrentUserModel.UserId,
                        Name = m.Name,
                        Sort = 0,
                        RightAnswer = "/",
                        Grade = 0,
                        QuestionTypeCode = qt,
                        Difficulty = m.Difficulty,
                        Precondition = string.Empty,
                        QuestionId = que.Id,
                        Answer = m.OptionNameA
                    };
                    queItems.Add(queItem);
                }

            }

            //构造保存数据库的实体
            //批量保存题目
            if (succeQues.Any())
            {
                var dt = succeQues.ToDataTable(null);
                dt.TableName = "TQuestion";
                SqlBulkCopyHelper.SaveTable(dt);

                var rpqs = new List<TPaperQuestion>();
                foreach (var question in succeQues)
                {
                    rpqs.Add(new TPaperQuestion
                    {
                        Id = Guid.NewGuid(),
                        QuestionId = question.Id,
                        PaperId = paperId,
                        IsDelete = false,
                        CreateTime = DateTime.Now,
                        CreateUserId = CurrentUserModel.UserId,
                        UpdateTime = DateTime.Now,
                        UpdateUserId = CurrentUserModel.UserId,
                        Sort = 0,
                        TypeCode = question.QuestionTypeCode
                    });
                }

                var sdt = rpqs.ToDataTable(null);
                sdt.TableName = "TPaperQuestion";
                SqlBulkCopyHelper.SaveTable(sdt);
            }

            //批量保存子题目
            if (queItems.Any())
            {
                var dt = queItems.ToDataTable(null);
                dt.TableName = "TQuestionItem";
                SqlBulkCopyHelper.SaveTable(dt);
            }

            //批量保存题目的选项
            if (options.Any())
            {
                var dt = options.ToDataTable(null);
                dt.TableName = "TOption";
                SqlBulkCopyHelper.SaveTable(dt);
            }
            //批量保存题目
            if (rQuestionOptions.Any())
            {
                var dt = rQuestionOptions.ToDataTable(null);
                dt.TableName = "RQuestionOption";
                SqlBulkCopyHelper.SaveTable(dt);
            }

            var url = string.Empty;
            if (exportList.Any())
            {
                var extDt = exportList.ToDataTable(xr.ExportHashtable);
                var str = GetSavePath();
                var strs = str.Split(',');
                if (strs.Any() && strs.Length > 1)
                {
                    var fileFullPath = strs[0];
                    url = strs[1];
                    ExcelHelp.TableToExcelForXLSX(extDt, fileFullPath);
                }
            }

            var list = new { failed = faile.Take(100).ToList(), failedCount = faile.Count }; //数据太多的话，浏览器会崩溃

            if (!exportList.Any())
            {
                var json = new { list, msg = "添加成功", url };
                return ServiceResponse<object>.SuccessResponse(json);
            }
            else
            {
                var json = new { list, msg = "添加失败", url };
                return ServiceResponse<object>.SuccessResponse(CommonConst.OprateFailStr, json);
            }
        }
    }
}