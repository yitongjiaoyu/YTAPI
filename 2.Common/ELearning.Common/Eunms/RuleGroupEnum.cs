﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Common
{
    /// <summary>
    /// 组卷规则分组
    /// </summary>
    public enum RuleGroupEnum
    {
        /// <summary>
        /// 试卷规则
        /// </summary>
        [Description("试卷规则")]
        Paper = 1,
        /// <summary>
        /// 考试规则
        /// </summary>
        [Description("考试规则")]
        Exam = 2,
    }
}
