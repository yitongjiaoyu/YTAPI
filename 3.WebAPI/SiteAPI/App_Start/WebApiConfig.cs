﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using SiteAPI.HandleAttribute;

namespace SiteAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //异常捕捉
            config.Filters.Add(new ApiErrorHandleAttribute());

            // Web API 配置和服务
            var allowOrigins = ConfigurationManager.AppSettings["cors_allowOrigins"];
            var globalCors = new EnableCorsAttribute(allowOrigins, "*", "*")
            {
                SupportsCredentials = true
            };
            config.EnableCors(globalCors);
            // Web API 路由
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
