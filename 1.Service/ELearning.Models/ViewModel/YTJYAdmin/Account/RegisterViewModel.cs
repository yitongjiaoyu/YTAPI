﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.YTJYAdmin.Account
{
    /// <summary>
    /// 注册参数
    /// </summary>
    public class RegisterRequest
    {
        /// <summary>
        /// 手机号
        /// </summary>
        public string MobilePhone { set; get; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 邀请码
        /// </summary>
        public string InviteCode { get; set; }
    }

    public class RegisterViewModel
    {
    }
}
