﻿using System;

namespace ELearning.Common.Http
{
    public class IpHelp
    {
        /// <summary>
        /// 获取客户端Ip 
        /// </summary>
        /// <returns>ip地址，默认返回127.0.0.1</returns>
        public static string GetClientIp()
        {
            var clientIp = "127.0.0.1";
            if (System.Web.HttpContext.Current != null)
            {
                clientIp = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (string.IsNullOrEmpty(clientIp) || clientIp.ToLower() == "unknown")
                {
                    clientIp = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_REAL_IP"];
                    if (string.IsNullOrEmpty(clientIp))
                    {
                        clientIp = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                    }
                }
                else
                {
                    try
                    {
                        clientIp = clientIp.Split(',')[0];
                    }
                    catch (Exception)
                    {

                    }
                }
            }

            if (!string.IsNullOrEmpty(clientIp) && "::1".Equals(clientIp))
            {
                clientIp = "127.0.0.1";
            }
            return clientIp;
        }
    }
}