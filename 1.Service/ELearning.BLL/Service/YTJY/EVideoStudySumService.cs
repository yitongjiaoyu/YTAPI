﻿//-------------------------------------------------------------------
//文件名称：EVideoStudySum.cs
//模块名称：EVideoStudySum业务逻辑接口层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearning.Common.Extensions;
using ELearning.Common.Helpers;
using ELearning.DAL;
using ELearning.Models;
using ELearning.Models.ViewModel.YTJYAdmin.Package;
using ELearning.Models.ViewModel.YTJYAdmin.Study;

namespace ELearning.BLL
{
    /// <summary>
    /// EVideoStudySum业务逻辑接口层
    /// </summary>
    public partial class EVideoStudySumService : BaseService<EVideoStudySum>
    {
        /// <summary>
        /// 获取我的视频学习记录
        /// </summary>
        public VideoLogViewModel GetVideoLogList(VideoLogRequest request, Guid userId)
        {
            var models = RepositoryFactory.EVideoStudySumRepository.GetVideoLogList(request, userId);
            var viewModel = new VideoLogViewModel { TotalCount = models.FirstOrDefault()?.TotalCount ?? 0 };
            foreach (var m in models)
            {
                var v = m.MapTo<VideoLogItem, VideoLogModel>();
                v.CoverUrl = PictureHelper.GetFileFullPath(v.CoverUrl);
                viewModel.Items.Add(v);
            }
            return viewModel;
        }
    }
}


