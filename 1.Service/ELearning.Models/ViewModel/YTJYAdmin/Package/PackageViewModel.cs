﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.YTJYAdmin.Package
{
    /// <summary>
    /// 获取列表参数实体
    /// </summary>
    public class SitePackageRequest : BaseSearchRequest
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { set; get; }

        /// <summary>
        /// 大类分类编码
        /// </summary>
        public string CategoryId { set; get; }

        /// <summary>
        /// 基础分类编码
        /// </summary>
        public string TypeCode { set; get; }
    }

    public class SitePackageViewModel
    {
        /// <summary>
        /// 列表
        /// </summary>
        public List<SitePackageItem> Items { set; get; } = new List<SitePackageItem>();

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { set; get; }
    }

    public class SitePackageItem
    {
        /// <summary>
        /// 套餐id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 封面图片地址
        /// </summary>
        [Display(Name = "封面图片地址")]
        public string CoverUrl { get; set; }

        /// <summary>
        /// 展示所用
        /// </summary>
        public string ShowCoverUrl { get; set; }

        /// <summary>
        /// 大类分类Code
        /// </summary>
        [Display(Name = "大类分类Code")]
        public string CategoryId { get; set; }

        /// <summary>
        /// 大类分类名称
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// 班级基础分类code,基础班，高级版
        /// </summary>
        [Display(Name = "班级基础分类code,基础班，高级版")]
        public string TypeCode { get; set; }

        /// <summary>
        /// 班级基础分类名称
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [Display(Name = "序号")]
        public double Sort { get; set; }

        /// <summary>
        /// 是否购买
        /// </summary>
        public bool IsBuy { get; set; }

        /// <summary>
        /// 报名人数 
        /// </summary>
        public int EnrollCount { set; get; }

        /// <summary>
        /// 原价
        /// </summary>
        public double OldPrice { get; set; }

        public string ShowOldPrice => $"{OldPrice:F2}元";

        /// <summary>
        /// 现在价格
        /// </summary>
        public double NowPrice { get; set; }

        /// <summary>
        /// 现价展示字符串
        /// </summary>
        public string ShowNowPrice => $"{NowPrice:F2}元";

        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        [Display(Name = "更新时间")]
        public DateTime UpdateTime { get; set; }

        /// <summary>
        /// 创建人Id
        /// </summary>
        [Display(Name = "创建人Id")]
        public Guid CreateUserId { get; set; }

        /// <summary>
        /// 编辑人id
        /// </summary>
        [Display(Name = "编辑人id")]
        public Guid UpdateUserId { get; set; }
    }

    public class SitePackageModel
    {
        public Guid Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 封面图片地址
        /// </summary>
        [Display(Name = "封面图片地址")]
        public string CoverUrl { get; set; }

        /// <summary>
        /// 大类分类Code
        /// </summary>
        [Display(Name = "大类分类Code")]
        public string CategoryId { get; set; }

        /// <summary>
        /// 大类分类名称
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// 班级基础分类code,基础班，高级版
        /// </summary>
        [Display(Name = "班级基础分类code,基础班，高级版")]
        public string TypeCode { get; set; }

        /// <summary>
        /// 班级基础分类名称
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [Display(Name = "序号")]
        public double Sort { get; set; }

        /// <summary>
        /// 原价
        /// </summary>
        public double OldPrice { get; set; }

        /// <summary>
        /// 现在价格
        /// </summary>
        public double NowPrice { get; set; }

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { set; get; }

        /// <summary>
        /// 当前用户购买该套餐数量
        /// </summary>
        public int BuyCount { set; get; }

        /// <summary>
        /// 报名人数 
        /// </summary>
        public int EnrollCount { set; get; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        [Display(Name = "更新时间")]
        public DateTime UpdateTime { get; set; }

        /// <summary>
        /// 创建人Id
        /// </summary>
        [Display(Name = "创建人Id")]
        public Guid CreateUserId { get; set; }

        /// <summary>
        /// 编辑人id
        /// </summary>
        [Display(Name = "编辑人id")]
        public Guid UpdateUserId { get; set; }
    }
    
}
