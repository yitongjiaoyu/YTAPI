﻿using System.Configuration;
namespace ELearning.Common
{
    /// <summary>
    /// The config file helper.
    /// </summary>
    public static class ConfigHelper
    {
        /// <summary>
        /// Gets the configuration boolean.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="default">The default value.</param>
        /// <returns>The configured bool value.</returns>
        public static bool GetConfigBoolean(string key, bool @default = false)
        {
            string configValue = GetConfigString(key);
            if (bool.TryParse(configValue, out bool result))
            {
                return result;
            }
            return @default;
        }
        /// <summary>
        /// Gets the configuration int.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="default">The default value.</param>
        /// <returns>The configured int value.</returns>
        public static int GetConfigInt(string key, int @default = 0)
        {
            string configValue = GetConfigString(key);

            if (int.TryParse(configValue, out int result))
            {
                return result;
            }
            return @default;
        }
        /// <summary>
        /// Gets the configuration string.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="default">The default value.</param>
        /// <returns>The configured string value.</returns>
        public static string GetConfigString(string key, string @default = "")
        {
            return ConfigurationManager.AppSettings[key] ?? @default;
        }
        /// <summary>
        /// Gets the connection string.
        /// </summary>
        /// <param name="key">The connection string key.</param>
        /// <returns>The connection string.</returns>
        public static string GetConnectionString(string key)
        {
            return ConfigurationManager.ConnectionStrings[key].ConnectionString;
        }
    }
}