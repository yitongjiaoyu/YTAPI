﻿using ELearning.Models.OAuth2;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace ELearning.API.Controllers
{
    /// <summary>
    /// OAuth 授权Token获取测试控制器
    /// </summary>
    public class OAuth2Controller : ApiController
    {
        /// <summary>
        /// Gets an OAuth 2.0 token for client credential.
        /// This action only for get access token tests.
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/OAuth2/GetToken")]
        public async Task<Token> GetToken()
        {
            using (var client = new HttpClient())
            {
                List<KeyValuePair<string, string>> requestData = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("username", "sgmwadmin"),
                    new KeyValuePair<string, string>("password", "P@ssw0rd"),
                    new KeyValuePair<string, string>("client_id", "2961C96D-4DB0-4FCF-99FA-FE18AC9A496A")
                };
                FormUrlEncodedContent requestBody = new FormUrlEncodedContent(requestData);
                var request = await client.PostAsync($"https://" + HttpContext.Current.Request.Url.Authority + "/api/connect/token", requestBody);
                var response = await request.Content.ReadAsStringAsync();
                var token = JsonConvert.DeserializeObject<Token>(response);
                return token;
            }
        }
        /// <summary>
        /// 获取销售助手Token
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/OAuth2/GetAssistantToken")]
        public async Task<Token> GetAssistantToken()
        {
            using (var client = new HttpClient())
            {
               
                //助手
                List<KeyValuePair<string, string>> requestData = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("grant_type", "password"),                    
                    new KeyValuePair<string, string>("username", "丁林玲"),
                    new KeyValuePair<string, string>("czydm", "600116"),
                    new KeyValuePair<string, string>("jxsh", "000000"),                    
                    new KeyValuePair<string, string>("client_id", "138B0798-8D43-4588-B291-DB8545865C8A")
                };
                FormUrlEncodedContent requestBody = new FormUrlEncodedContent(requestData);
                var request = await client.PostAsync($"https://" + HttpContext.Current.Request.Url.Authority + "/api/connect/token", requestBody);
                var response = await request.Content.ReadAsStringAsync();
                var token = JsonConvert.DeserializeObject<Token>(response);
                return token;
            }
        }
        /// <summary>
        /// 获取骏客token
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/OAuth2/GetJKToken")]
        public async Task<Token> GetJKToken()
        {
            using (var client = new HttpClient())
            {

                //助手
                List<KeyValuePair<string, string>> requestData = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("grant_type", "password"),                
                    new KeyValuePair<string, string>("username", "13031356766"),
                    new KeyValuePair<string, string>("jxsh", "B650005011"),
                    new KeyValuePair<string, string>("client_id", "41BC3798-5392-49A4-A191-0891F322920B")
                };
                FormUrlEncodedContent requestBody = new FormUrlEncodedContent(requestData);
                var request = await client.PostAsync($"https://" + HttpContext.Current.Request.Url.Authority+"/api/connect/token", requestBody);
                var response = await request.Content.ReadAsStringAsync();
                var token = JsonConvert.DeserializeObject<Token>(response);
                return token;
            }
        }
    }
}