﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ELearning.Models.ViewModel;

namespace ELearning.Models
{
    /// <summary>
    /// 参数实体模型
    /// </summary>
    public class GetMenusRequest : BaseRequest
    {
        /// <summary>
        /// 平台类型:JXS,SGMW,ISV
        /// </summary>
        [DataMember(Name = "PlatformType")]
        public string PlatformType { get; set; }

        /// <summary>
        /// 菜单id
        /// </summary>
        [DataMember(Name = "MenuId")]
        public Guid MenuId { set; get; }

    }

    /// <summary>
    /// 菜单信息
    /// </summary>
    [DataContract]
    public class MenuViewModel
    {
        /// <summary>
        /// 菜单id
        /// </summary>
        [DataMember(Name = "id")]
        public Guid Id { set; get; }

        /// <summary>
        /// 父级id
        /// </summary>
        [DataMember(Name = "parentId")]
        public Guid ParentId { set; get; }

        /// <summary>
        /// 父级菜单名称
        /// </summary>
        [DataMember(Name = "parentName")]
        public string ParentName { set; get; }

        /// <summary>
        /// 平台类型:JXS,SGMW,ISV
        /// </summary>
        [DataMember(Name = "platformType")]
        public string PlatformType { get; set; }

        /// <summary>
        /// 菜单标识
        /// </summary>
        [DataMember(Name = "code")]
        public string Code { set; get; }

        /// <summary>
        /// 菜单类型：1.菜单，2.按钮
        /// </summary>
        [DataMember(Name = "type")]
        public int Type { get; set; }

        /// <summary>
        /// 菜单名称
        /// </summary>
        [DataMember(Name = "title")]
        public string Title { set; get; }

        /// <summary>
        /// 菜单跳转路径
        /// </summary>
        [DataMember(Name = "path")]
        public string Path { set; get; }
        [DataMember(Name = "redirect")]
        public string Redirect { set; get; }

        /// <summary>
        /// 菜单别称
        /// </summary>
        [DataMember(Name = "name")]
        public string Name { set; get; }

        /// <summary>
        /// 菜单对应页面地址
        /// </summary>
        [DataMember(Name = "component")]
        public string Component { set; get; }

        /// <summary>
        /// meta
        /// </summary>
        [DataMember(Name = "meta")]
        public MetaModel Meta { set; get; }

        /// <summary>
        /// 是否隐藏
        /// </summary>
        [DataMember(Name = "hidden")]
        public bool Hidden { set; get; }

        /// <summary>
        /// 是否有子节点
        /// </summary>
        [DataMember(Name = "hasChildren")]
        public bool hasChildren { set; get; }

        /// <summary>
        /// 排序序号
        /// </summary>
        [DataMember(Name = "sort")]
        public int Sort { set; get; }

        /// <summary>
        /// 子菜单列表
        /// </summary>
        [DataMember(Name = "children")]
        public List<MenuViewModel> Children { set; get; } = new List<MenuViewModel>();
    }

    /// <summary>
    /// 菜单名称信息
    /// </summary>
    [DataContract]
    public class MetaModel
    {
        /// <summary>
        /// 菜单名称
        /// </summary>
        [DataMember(Name = "title")]
        public string Title { set; get; }

        [DataMember(Name = "noCache")]
        public bool NoCache { set; get; }

        /// <summary>
        /// 菜单图标
        /// </summary>
        [DataMember(Name = "icon")]
        public string Icon { set; get; }
        /// <summary>
        /// 是否固定
        /// </summary>
        [DataMember(Name = "affix")]
        public bool Affix { set; get; }

        /// <summary>
        /// 角色名称列表
        /// </summary>
        [DataMember(Name = "roles")]
        public List<string> Roles { set; get; } = new List<string>();
    }
}
