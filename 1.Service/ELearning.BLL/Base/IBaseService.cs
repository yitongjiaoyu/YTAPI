﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.BLL
{
    /// <summary>
    /// 接口基类
    /// <remarks>创建：2018.07.11</remarks>
    /// </summary>
    public interface IBaseService<T> : IDependency where T : class
    {
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="entity">数据实体</param>
        /// <returns>添加后的数据实体</returns>
        T Add(T entity);

        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="entitys">数据实体集合</param>
        /// <returns>添加后的数据实体</returns>
        int Adds(List<T> entitys);

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="entity">数据实体</param>
        /// <returns>是否成功</returns>
        bool Update(T entity);


        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="entity">数据实体</param>
        /// <returns>是否成功</returns>
        int Updates(List<T> entity);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="entity">数据实体</param>
        /// <returns>是否成功</returns>
        bool Delete(T entity);

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="entitys">数据实体列表</param>
        /// <returns>是否成功</returns>
        bool Deletes(List<T> entitys);

        T Find(Expression<Func<T, bool>> whereLambda);

        /// <summary>
        /// 列表
        /// </summary>
        /// <param name="pageIndex">页码数</param>
        /// <param name="pageSize">每页记录数</param>
        /// <param name="totalRecord">总记录数</param>
        /// <param name="sortBy">排序字段</param>
        /// <param name="sortDir">排序（asc 升序）</param>
        /// <param name="whereLamdba">全部查询（u => true）</param>
        /// <returns>列表</returns>
        IQueryable<T> FindPageList(int pageIndex, int pageSize, out int totalRecord, string sortBy, string sortDir, Expression<Func<T, bool>> whereLamdba);

        /// <summary>
        /// 列表查询
        /// </summary>
        /// <param name="whereLamdba">lamd表达式</param>
        /// <param name="orderName">排序字段（为空时不排序）</param>
        /// <param name="isAsc">是否正序</param>
        /// <returns>列表查询</returns>
        IQueryable<T> FindList(Expression<Func<T, bool>> whereLamdba, string orderName, bool isAsc);

        /// <summary>
        /// 列表查询(默认排序字段为空)
        /// </summary>
        /// <param name="whereLamdba">lamd表达式</param>
        /// <returns>列表查询</returns>
        IQueryable<T> FindList(Expression<Func<T, bool>> whereLamdba);

        /// <summary>
        /// 列表查询(默认排序字段为空)
        /// </summary>
        /// <param name="whereLamdba">lamd表达式</param>
        /// <param name="path" >需要加载的导航属性名称</param>
        /// <returns>列表查询</returns>
        IQueryable<T> FindList(Expression<Func<T, bool>> whereLamdba, string path);

        #region no savechanges

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="entity">数据实体</param>
        /// <returns>添加后的数据实体</returns>
        T AddNoSaveChange(T entity);

        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="entitys">数据实体集合</param>
        /// <returns>添加后的数据实体</returns>
        int AddsNoSaveChange(List<T> entitys);

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="entity">数据实体</param>
        /// <returns>是否成功</returns>
        bool UpdateNoSaveChange(T entity);

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="entity">数据实体</param>
        /// <returns>是否成功</returns>
        int UpdatesNoSaveChange(List<T> entitys);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="entity">数据实体</param>
        /// <returns>是否成功</returns>
        bool DeleteNoSaveChange(T entity);

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="entitys">数据实体列表</param>
        /// <returns>是否成功</returns>
        bool DeletesNoSaveChange(List<T> entitys);

        /// <summary>
        /// 事务提交
        /// </summary>
        bool SaveChange();

        #endregion
    }

    /// <summary>
    /// 接口
    /// </summary>
    public interface IDependency
    {
    }
}
