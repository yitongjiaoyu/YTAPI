//-------------------------------------------------------------------
//文件名称：流媒体服务表.cs
//模块名称：流媒体服务表Model层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Xiaosheng
//修改时间：2019年8月8日
//-----------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ELearning.Models
{	
	 /// <summary>
    /// 流媒体服务表表实体类
    /// </summary>
	[Table("流媒体服务表")]
	public partial class 流媒体服务表
    {
		
		/// <summary>
		/// 
		/// </summary>
				[Key]
	[Display(Name = "")]
		public Guid ID { get; set; }
		
		/// <summary>
		/// 服务名称
		/// </summary>
			[Display(Name = "服务名称")]
		public string MServiceName { get; set; }
		
		/// <summary>
		/// 频道名称
		/// </summary>
			[Display(Name = "频道名称")]
		public string Name { get; set; }
		
		/// <summary>
		/// 状态
		/// </summary>
			[Display(Name = "状态")]
		public string State { get; set; }
		
		/// <summary>
		/// 直播推流路径
		/// </summary>
			[Display(Name = "直播推流路径")]
		public string Url { get; set; }
		
		/// <summary>
		/// 直播开关
		/// </summary>
			[Display(Name = "直播开关")]
		public bool IsStar { get; set; }
		
		/// <summary>
		/// 创建者
		/// </summary>
			[Display(Name = "创建者")]
		public string Creater { get; set; }
		
		/// <summary>
		/// 创建时间
		/// </summary>
			[Display(Name = "创建时间")]
		public DateTime CreateTime { get; set; }
		
		/// <summary>
		/// 修改者
		/// </summary>
			[Display(Name = "修改者")]
		public string Modifyer { get; set; }
		
		/// <summary>
		/// 修改时间
		/// </summary>
			[Display(Name = "修改时间")]
		public DateTime ModifyTime { get; set; }
		
		/// <summary>
		/// 是否逻辑删除
		/// </summary>
			[Display(Name = "是否逻辑删除")]
		public bool IsDelete { get; set; }
		   
    }
}

