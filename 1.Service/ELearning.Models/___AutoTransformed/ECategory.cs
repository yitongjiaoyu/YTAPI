//-------------------------------------------------------------------
//文件名称：ECategory.cs
//模块名称：ECategoryModel层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ELearning.Models
{	
	 /// <summary>
    /// ECategory表实体类
    /// </summary>
	[Table("ECategory")]
	public partial class ECategory
    {
		
		/// <summary>
		/// 
		/// </summary>
				[Key]
	[Display(Name = "")]
		public Guid ID { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public Guid ParentID { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Code { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string RouteURL { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string CategoryName { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string URL { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public int Sort { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public int States { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public bool IsAuthorization { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string AppCode { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Creater { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public DateTime CreateTime { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Modifyer { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public DateTime ModifyTime { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public bool IsDelete { get; set; }
		   
    }
}

