//-------------------------------------------------------------------
//文件名称：TBanner.cs
//模块名称：TBanner业务逻辑接口层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearning.DAL;
using ELearning.Models;

namespace ELearning.BLL
{
    /// <summary>
    /// TBanner业务逻辑接口层
    /// </summary>
	public partial class TBannerService : BaseService<TBanner>
	{
     public  TBannerService() : base(RepositoryFactory.TBannerRepository) { }
	}
}


