﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models 
{
    public class CommentStat
    {
        /// <summary>
        /// 评论条目
        /// </summary>          
        [Display(Name = "评论条目")]
        public int CountNo { get; set; }
        /// <summary>
        /// 评论评价分数
        /// </summary>
        [Display(Name = "评论评价分数")]
        public double Score { get; set; }

        /// <summary>
        /// 评论评价分数
        /// </summary>
        [Display(Name = "点赞数量")]
        public int Zan { get; set; }

    }

    public class CommModel
    {
        /// <summary>
        /// 评价对象ID
        /// </summary>
        public string TopicID { get; set; }

        /// <summary>
        /// 课程ID
        /// </summary>
        public Guid CourseID { get; set; }

        /// <summary>
        /// 评分
        /// </summary>
        public int Score { get; set; }
        /// <summary>
        /// 评论内容
        /// </summary>
        public string Contents { get; set; }
        /// <summary>
        /// 评论对象type
        /// </summary>
        public string TopicType { get; set; }
        
    }
}
