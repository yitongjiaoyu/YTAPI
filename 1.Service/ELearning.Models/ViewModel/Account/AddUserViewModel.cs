﻿

using System;

namespace ELearning.Models.ViewModel.Account
{
    public class AddUserViewModel
    {
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { set; get; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { set; get; }

        /// <summary>
        /// 组织码
        /// </summary>
        public string OrgCode { set; get; }

        /// <summary>
        /// 用户类型：Student/Teacher
        /// </summary>
        public string UserType { set; get; }

         /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { set; get; }
        
        /// <summary>
        /// 用户邮箱
        /// </summary>
        public string Email { set; get; }

    }

    public class UpdateUserViewModel
    {
        /// <summary>
        /// 用户id
        /// </summary>
        public Guid UserId { set; get; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { set; get; }
        
        /// <summary>
        /// 用户邮箱
        /// </summary>
        public string Email { set; get; }

    }
}
