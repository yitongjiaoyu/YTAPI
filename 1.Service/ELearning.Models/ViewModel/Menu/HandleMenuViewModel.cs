﻿

using System;
using System.Collections.Generic;
using ELearning.Models.ViewModel.Role;
using ELearning.Models.ViewModel.User;

namespace ELearning.Models.ViewModel.Menu
{
    public class HandleMenuUserViewModel
    {
        /// <summary>
        /// 菜单id
        /// </summary>
        public Guid MenuId { set; get; }

        /// <summary>
        /// 分配的用户列表
        /// </summary>
        public List<GetUserItem> Users { set; get; }
    }

    public class HandleMenuRoleViewModel
    {
        /// <summary>
        /// 菜单id
        /// </summary>
        public Guid MenuId { set; get; }

        /// <summary>
        /// 分配的角色Id列表
        /// </summary>
        public List<Guid> RoleIds { set; get; }
    }
}
