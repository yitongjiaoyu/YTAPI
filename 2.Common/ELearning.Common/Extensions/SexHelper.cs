﻿
namespace ELearning.Common.Extensions
{
    public class SexHelper
    {
        /// <summary>
        /// 性别类型转化成字符串
        /// </summary>
        /// <param name="sex">1.保密,2.男,3.女</param>
        public static string SexToStr(int sex)
        {
            string str;
            switch (sex)
            {
                case 1:
                    str = "保密";
                    break;
                case 2:
                    str = "男";
                    break;
                case 3:
                    str = "女";
                    break;
                default:
                    str = "保密";
                    break;
            }
            return str;
        }

        /// <summary>
        /// 性别字符串转成数字方便保存到数据库
        /// </summary>
        /// <param name="sex">1.保密,2.男,3.女</param>
        public static int SexToInt(string sex)
        {
            int i;
            switch (sex)
            {
                case "保密":
                    i = 1;
                    break;
                case "男":
                    i = 2;
                    break;
                case "女":
                    i = 3;
                    break;
                default:
                    i = 2;
                    break;
            }
            return i;
        }
    }
}
