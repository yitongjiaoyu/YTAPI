﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Common
{
    public enum PublishStateEnum
    {
        /// <summary>
        /// 上架
        /// </summary>
        [Description("上架")]
        Putaway = 1,
        /// <summary>
        /// 下架
        /// </summary>
        [Description("上架")]
        Soldout = 2,
        /// <summary>
        /// 其他
        /// </summary>
        [Description("3")]
        Other = 3,
    }
}
