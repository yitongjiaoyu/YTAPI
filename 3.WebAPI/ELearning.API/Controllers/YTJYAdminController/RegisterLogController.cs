﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Http;
using ELearning.BLL;
using ELearning.Common;
using ELearning.Common.CommonStr;
using ELearning.Common.Extensions;
using ELearning.Common.Model;
using ELearning.Models;
using ELearning.Models.ViewModel.YTJYAdmin.RegisterLog;

namespace ELearning.API.Controllers.YTJYAdminController
{
    /// <summary>
    /// 注册记录操作service
    /// </summary>
    public class RegisterLogController : BaseApiController
    {
        /// <summary>
        /// 注册日志操作service
        /// </summary>
        private TRegisterLogService tRegisterLogService => new TRegisterLogService();

        /// <summary>
        /// 用户操作service
        /// </summary>
        private ESysUserService eSysUserService => new ESysUserService();

        /// <summary>
        /// 组织操作service
        /// </summary>
        private EOrgService orgService => new EOrgService();

        /// <summary>
        /// 获取列表
        /// </summary>
        [HttpPost, Route("api/ytjyadmin/RegisterLog/GetModelList")]
        public ServiceResponse<RegisterLogViewModel> GetModelList(RegisterLogRequest request)
        {
            Expression<Func<TRegisterLog, bool>> whereLamdba = p => !p.IsDelete && p.Status == 0;
            if (!string.IsNullOrEmpty(request.MobilePhone))
                whereLamdba = whereLamdba.And(p => p.MobilePhone.Contains(request.MobilePhone));
            var models = tRegisterLogService.FindPageList(request.PageIndex, request.PageSize, out var total, "CreateTime", "DESC", whereLamdba);
            var viewModel = new RegisterLogViewModel { TotalCount = total };

            foreach (var m in models)
            {
                var v = m.MapTo<RegisterLogItem, TRegisterLog>();
                v.CreateTime = m.CreateTime.ToCommonStr();
                viewModel.Items.Add(v);
            }

            return ServiceResponse<RegisterLogViewModel>.SuccessResponse(CommonConst.OprateSuccessStr, viewModel);
        }

        /// <summary>
        /// 分配用户到组织
        /// </summary>
        [HttpPost, Route("api/ytjyadmin/RegisterLog/PostModel")]
        public ServiceResponse<bool> PostModel(RegisterLogPostModel viewModel)
        {
            if(string.IsNullOrEmpty(viewModel.OrgCode))
                return ServiceResponse<bool>.SuccessResponse(CommonConst.ORGCODE_NOT_NULL, false);
            var model = tRegisterLogService.Find(r => r.Id == viewModel.Id);
            if (model.IsNotNull())
            {
                //判断组织可分配的学生数量是否达到上限
                var org = orgService.Find(o => o.org_code == viewModel.OrgCode && !o.IsDelete);
                if (org.IsNotNull())
                {
                    var users = eSysUserService.FindList(u => !u.IsDelete && u.OrgCode == org.org_code).Count();
                    if(users>=org.AccountLimit)
                        return ServiceResponse<bool>.SuccessResponse(CommonConst.ORGCODE_STUDENT_COUNT_UPPER_LIMIT, false);
                }

                //更新学生的组织码
                var user = eSysUserService.Find(u => u.UserID == viewModel.UserId);
                if (user.IsNotNull())
                {
                    user.OrgCode = viewModel.OrgCode;
                    user.ModifyTime = DateTime.Now;
                    user.Modifyer = CurrentUserModel.LoginName;
                    eSysUserService.UpdateNoSaveChange(user);
                }

                model.Status = 1;
                model.OrgCode = viewModel.OrgCode;
                model.UpdateTime = DateTime.Now;
                model.UpdateUserId = CurrentUserModel.UserId;
                tRegisterLogService.Update(model);
            }
            return ServiceResponse<bool>.SuccessResponse(CommonConst.OprateSuccessStr, true);
        }

        /// <summary>
        /// 分配用户到组织
        /// </summary>
        [HttpGet, Route("api/ytjyadmin/RegisterLog/DeleteModel")]
        public ServiceResponse<bool> DeleteModel(Guid id)
        {
            var model = tRegisterLogService.Find(r => r.Id == id);
            if (model.IsNotNull())
            {
                model.IsDelete = true;
                model.UpdateTime = DateTime.Now;
                model.UpdateUserId = CurrentUserModel.UserId;
                tRegisterLogService.Update(model);
            }
            return ServiceResponse<bool>.SuccessResponse(CommonConst.OprateSuccessStr, true);
        }
    }
}