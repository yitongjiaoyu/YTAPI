//-------------------------------------------------------------------
//文件名称：Comment.cs
//模块名称：CommentModel层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Xiaosheng
//修改时间：2019年8月8日
//-----------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ELearning.Models
{	
	 /// <summary>
    /// Comment表实体类
    /// </summary>
	[Table("Comment")]
	public partial class Comment
    {
		
		/// <summary>
		/// ??
		/// </summary>
				[Key]
	[Display(Name = "??")]
		public Guid ID { get; set; }
		
		/// <summary>
		/// ??ID
		/// </summary>
			[Display(Name = "??ID")]
		public Guid CSumID { get; set; }
		
		/// <summary>
		/// ??ID
		/// </summary>
			[Display(Name = "??ID")]
		public Guid TopicID { get; set; }
		
		/// <summary>
		/// ????
		/// </summary>
			[Display(Name = "????")]
		public string Contents { get; set; }
		
		/// <summary>
		/// ??
		/// </summary>
			[Display(Name = "??")]
		public double Score { get; set; }
		
		/// <summary>
		/// ??
		/// </summary>
			[Display(Name = "??")]
		public bool Zan { get; set; }
		
		/// <summary>
		/// ??ID
		/// </summary>
			[Display(Name = "??ID")]
		public Guid UserID { get; set; }
		
		/// <summary>
		/// ???
		/// </summary>
			[Display(Name = "???")]
		public string Creater { get; set; }
		
		/// <summary>
		/// ????
		/// </summary>
			[Display(Name = "????")]
		public DateTime CreateTime { get; set; }
		
		/// <summary>
		/// ????
		/// </summary>
			[Display(Name = "????")]
		public DateTime UpdateTime { get; set; }
		
		/// <summary>
		/// ??????
		/// </summary>
			[Display(Name = "??????")]
		public bool IsDelete { get; set; }
		
		/// <summary>
		/// ????
		/// </summary>
			[Display(Name = "????")]
		public bool istu { get; set; }
		   
    }
}

