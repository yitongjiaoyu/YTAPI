//-------------------------------------------------------------------
//文件名称：TQuestionType.cs
//模块名称：TQuestionType业务逻辑接口层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearning.DAL;
using ELearning.Models;

namespace ELearning.BLL
{
    /// <summary>
    /// TQuestionType业务逻辑接口层
    /// </summary>
	public partial class TQuestionTypeService : BaseService<TQuestionType>
	{
     public  TQuestionTypeService() : base(RepositoryFactory.TQuestionTypeRepository) { }
	}
}


