﻿using System;
using System.Diagnostics;
using System.IO;
using System.Web;
using System.Web.Http;
using ELearning.Common.CommonStr;
using ELearning.Common.Extensions;
using ELearning.Common.Model;
using ELearning.Common.Helpers;
using ELearning.Models.ViewModel.Upload;

namespace ELearning.API.Controllers
{
    /// <summary>
    /// 上传文件控制器
    /// </summary>
    public class UploadController : BaseApiController
    {
        [HttpPost, Route("api/Upload/ProcessRequest")]
        [AllowAnonymous]
        private ServiceResponse<UploadViewModel> ProcessRequest()
        {
            var v = new UploadViewModel();
            var fileName = HttpContext.Current.Request.Form["fileName"];

            try
            {
                var virtualPath = "/UploadFile/Files/";
                var path = HttpContext.Current.Server.MapPath(virtualPath);
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                var filePath = $"{path}{fileName}";

                v.Url = filePath;

                //创建一个追加（FileMode.Append）方式的文件流
                using (var fs = new FileStream(filePath, FileMode.Append, FileAccess.Write))
                {
                    using (var bw = new BinaryWriter(fs))
                    {
                        //读取文件流
                        var br = new BinaryReader(HttpContext.Current.Request.Files[0].InputStream);
                        //将文件留转成字节数组
                        var bytes = br.ReadBytes((int)HttpContext.Current.Request.Files[0].InputStream.Length);
                        //将字节数组追加到文件
                        bw.Write(bytes);
                    }
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return ServiceResponse<UploadViewModel>.SuccessResponse(CommonConst.OprateSuccessStr, v);
        }

        /// <summary>
        /// 获取转码成功后的url
        /// </summary>
        /// <param name="job">作业id</param>
        [HttpGet, Route("api/Upload/GetStr")]
        public ServiceResponse<string> GetStr(string job)
        {
            //var ams = new AzureMediaServiceController();
            //var status = ams.GetUrl(job);
            //if (string.IsNullOrEmpty(status))
            //    status = CommonConst.CourseProcessing;
            //status = HttpUtility.UrlDecode(status);
            return ServiceResponse<string>.SuccessResponse(CommonConst.OprateSuccessStr, "");
        }

        /// <summary>
        /// 上传附件到项目根目录下面
        /// </summary>
        [HttpPost, Route("api/Upload/UploadAttachmentBak")]
        [AllowAnonymous]
        public ServiceResponse<UploadRespModel> UploadAttachmentBak()
        {
            var viewModel = new UploadRespModel();
            var code = 200;
            var msg = "上传失败!";

            var virtualPath = "/UploadFile/Files/";
            var path = HttpContext.Current.Server.MapPath(virtualPath);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            var filelist = HttpContext.Current.Request.Files;
            if (filelist.Count > 0)
            {
                var file = filelist[0];
                var fileName = file.FileName;
                var fn = fileName.Split('\\');
                if (fn.Length > 1)
                {
                    fileName = fn[fn.Length - 1];
                }
                var fs = fileName.Split('.');
                var ext = fs[fs.Length - 1];
                var folderName = BlobHelper.GetBlobName(ext);

                fileName = $"{DateTime.Now:yyyyMMddHHmmss}{fileName}";

                path = $"{path}{folderName}";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                var fileFullPath = $"{path}/{fileName}";

                try
                {
                    file.SaveAs(fileFullPath);
                    viewModel.SaveUrl = $"{virtualPath}/{folderName}/{fileName}";
                    viewModel.DownloadUrl = PictureHelper.GetFileFullPath(viewModel.SaveUrl);

                    msg = "成功";
                }
                catch (Exception ex)
                {
                    code = CommonConst.Code_OprateError;
                    msg = "上传文件写入失败：" + ex.InnerException + ex.Message + ex.InnerException?.InnerException + "fileFullPath=" + fileFullPath;
                }
            }

            return ServiceResponse<UploadRespModel>.SuccessResponse(msg, viewModel, code);
        }

        /// <summary>
        /// 上传文件到共享文件夹
        /// </summary>
        [HttpPost, Route("api/Upload/UploadAttachment")]
        [AllowAnonymous]
        public ServiceResponse<UploadRespModel> UploadAttachment()
        {
            var viewModel = new UploadRespModel();
            var code = 200;
            var msg = "上传失败!";

            var path = FileUploadConst.UploadPath; //@"\\172.16.10.130\Resource";
            var s = connectState(path, FileUploadConst.UserName, FileUploadConst.Password);

            if (s)
            {
                var filelist = HttpContext.Current.Request.Files;
                if (filelist.Count > 0)
                {
                    var file = filelist[0];
                    var fileName = file.FileName;
                    var blobName = FileHelper.GetSaveFolder(fileName);
                    path = $@"{path}\{blobName}\";

                    fileName = $"{DateTime.Now:yyyyMMddHHmmss}{fileName}";

                    //共享文件夹的目录
                    var theFolder = new DirectoryInfo(path);
                    var remotePath = theFolder.ToString();
                    Transport(file.InputStream, remotePath, fileName);

                    viewModel.SaveUrl = $"{blobName}/{fileName}";
                    viewModel.DownloadUrl = PictureHelper.GetFileFullPath(viewModel.SaveUrl);

                    msg = "上传成功";
                }
            }
            else
            {
                code = CommonConst.Code_OprateError;
                msg = "链接服务器失败";
            }

            return ServiceResponse<UploadRespModel>.SuccessResponse(msg, viewModel, code);
        }

        /// <summary>
        /// 连接远程共享文件夹
        /// </summary>
        /// <param name="path">远程共享文件夹的路径</param>
        /// <param name="userName">用户名</param>
        /// <param name="passWord">密码</param>
        private static bool connectState(string path, string userName, string passWord)
        {
            bool Flag = false;
            Process proc = new Process();
            try
            {
                proc.StartInfo.FileName = "cmd.exe";
                proc.StartInfo.UseShellExecute = false;
                proc.StartInfo.RedirectStandardInput = true;
                proc.StartInfo.RedirectStandardOutput = true;
                proc.StartInfo.RedirectStandardError = true;
                proc.StartInfo.CreateNoWindow = true;
                proc.Start();
                string dosLine = "net use " + path + " " + passWord + " /user:" + userName;
                WriteHelper.WriteFile($"dosLine:{dosLine}");
                proc.StandardInput.WriteLine(dosLine);
                proc.StandardInput.WriteLine("exit");
                while (!proc.HasExited)
                {
                    proc.WaitForExit(1000);
                }

                string errormsg = proc.StandardError.ReadToEnd();
                proc.StandardError.Close();
                WriteHelper.WriteFile($"errormsg:{errormsg}");
                if (string.IsNullOrEmpty(errormsg))
                {
                    Flag = true;
                }
                else
                {
                    throw new Exception(errormsg);
                }
            }
            catch (Exception ex)
            {
                WriteHelper.WriteFile(ex);
                throw ex;
            }
            finally
            {
                proc.Close();
                proc.Dispose();
            }

            return Flag;
        }

        /// <summary>
        /// 向远程文件夹保存本地内容，或者从远程文件夹下载文件到本地
        /// </summary>
        /// <param name="inFileStream">要保存的文件的路径，如果保存文件到共享文件夹，这个路径就是本地文件路径如：@"D:\1.avi"</param>
        /// <param name="dst">保存文件的路径，不含名称及扩展名</param>
        /// <param name="fileName">保存文件的名称以及扩展名</param>
        private static void Transport(Stream inFileStream, string dst, string fileName)
        {
            WriteHelper.WriteFile($"目录-Transport：{dst}");
            if (!Directory.Exists(dst))
            {
                Directory.CreateDirectory(dst);
            }

            dst = dst + fileName;

            if (!File.Exists(dst))
            {
                WriteHelper.WriteFile($"文件不存在，开始保存");
                var outFileStream = new FileStream(dst, FileMode.Create, FileAccess.Write);

                var buf = new byte[inFileStream.Length];

                int byteCount;

                while ((byteCount = inFileStream.Read(buf, 0, buf.Length)) > 0)
                {
                    outFileStream.Write(buf, 0, byteCount);
                }
                WriteHelper.WriteFile($"保存完成");
                inFileStream.Flush();

                inFileStream.Close();

                outFileStream.Flush();

                outFileStream.Close();
            }
        }

        /// <summary>
        /// 获取腾讯云上传视频签名
        /// </summary>
        [HttpGet]
        [Route("api/Upload/getSignature")]
        public object GetSignature()
        {
            var sign = new Signature();
            //当前时间
            sign.MQwNowTime = Signature.GetIntTimeStamp();
            sign.MiRandom = new Random().Next(0, 1000000);
            //签名有效期
            sign.MiSignValidDuration = 3600 * 24 * 2;

            var strSign = sign.GetUploadSignature();
            var viewModel = new { info = strSign, msg = true, resultCode = 200, message = "获取成功" };

            return strSign;
        }

    }
}