﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.YTJYAdmin.RegisterLog
{
    public class RegisterLogRequest : BaseSearchRequest
    {
        /// <summary>
        /// 注册时的手机号
        /// </summary>
        [Display(Name = "注册时的手机号")]
        public string MobilePhone { get; set; }
    }

    public class RegisterLogViewModel
    {
        /// <summary>
        /// 列表
        /// </summary>
        public List<RegisterLogItem> Items { set; get; } = new List<RegisterLogItem>();

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { set; get; }
    }

    public class RegisterLogItem
    {
        /// <summary>
        /// Id
        /// </summary>
        [Display(Name = "Id")]
        public Guid Id { get; set; }

        /// <summary>
        /// 注册人用户id
        /// </summary>
        [Display(Name = "注册人用户id")]
        public Guid UserId { get; set; }

        /// <summary>
        /// 注册时邀请码
        /// </summary>
        [Display(Name = "注册时邀请码")]
        public string InviteCode { get; set; }

        /// <summary>
        /// 注册时的手机号
        /// </summary>
        [Display(Name = "注册时的手机号")]
        public string MobilePhone { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public string CreateTime { get; set; }
    }

    /// <summary>
    /// 更新用户组织code
    /// </summary>
    public class RegisterLogPostModel
    {
        /// <summary>
        /// Id
        /// </summary>
        [Display(Name = "Id")]
        public Guid Id { get; set; }

        /// <summary>
        /// 注册人用户id
        /// </summary>
        [Display(Name = "注册人用户id")]
        public Guid UserId { get; set; }

        /// <summary>
        /// 组织编码
        /// </summary>
        [Display(Name = "组织编码")]
        public string OrgCode { get; set; }
    }
}
