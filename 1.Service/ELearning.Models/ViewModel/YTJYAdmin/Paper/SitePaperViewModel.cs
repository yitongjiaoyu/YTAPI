﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ELearning.Models.ViewModel.YTJYAdmin.Paper
{
    public class SitePaperRequest : BaseSearchRequest
    {
        /// <summary>
        /// 类型：1.真题，2.模拟题
        /// </summary>
        [Display(Name = "类型：1.真题，2.模拟题")]
        public int Type { get; set; }
    }

    /// <summary>
    /// 客户端试卷列表视图
    /// </summary>
    public class SitePaperViewModel
    {
        /// <summary>
        /// 列表
        /// </summary>
        public List<SitePaperItem> Items { set; get; } = new List<SitePaperItem>();

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { set; get; }
    }

    /// <summary>
    /// 客户端试卷列表元素
    /// </summary>
    public class SitePaperItem
    {
        /// <summary>
        /// 试卷id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 类型：1.真题，2.模拟题
        /// </summary>
        [Display(Name = "类型：1.真题，2.模拟题")]
        public int Type { get; set; }

        /// <summary>
        /// 已做次数
        /// </summary>
        public int DoCount { get; set; }
    }

    /// <summary>
    /// 试卷数据库实体
    /// </summary>
    public class SitePaperModel
    {
        public Guid Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 类型：1.真题，2.模拟题
        /// </summary>
        [Display(Name = "类型：1.真题，2.模拟题")]
        public int Type { get; set; }

        /// <summary>
        /// 已做次数
        /// </summary>
        public int DoCount { get; set; }

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { get; set; }
       
    }
}
