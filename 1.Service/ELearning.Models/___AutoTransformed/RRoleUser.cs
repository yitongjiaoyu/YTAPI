//-------------------------------------------------------------------
//文件名称：RRoleUser.cs
//模块名称：RRoleUserModel层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ELearning.Models
{	
	 /// <summary>
    /// RRoleUser表实体类
    /// </summary>
	[Table("RRoleUser")]
	public partial class RRoleUser
    {
		
		/// <summary>
		/// 
		/// </summary>
				[Key]
	[Display(Name = "")]
		public Guid ID { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public Guid RoleID { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public Guid UserID { get; set; }
		   
    }
}

