﻿using ELearning.BLL;
using ELearning.Common.Extensions;
using ELearning.Common.Model;
using ELearning.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Http;

namespace ELearning.API.Controllers.ElearningController
{
    public class EStudentSubscribeController : BaseApiController
    {
        private EStudentSubscribeService eStudentSubscribeService = new EStudentSubscribeService();
        private ESysUserService userService = new ESysUserService();
        /// <summary>
        /// 关注教师
        /// </summary>
        /// <param name="TopicID"></param>
        /// <returns></returns>
        [HttpGet, Route("api/EStudentSubscribe/SaveSubscribe")]
        public ServiceResponse<bool> SaveSubscribe(string TopicID)
        {
            var UserID = CurrentUserModel.UserId;
            var flag = false;
            try
            {
                Expression<Func<EStudentSubscribe, bool>> roleexp = u => u.TeacherID.ToString() == TopicID && u.StuID == UserID;

                var getinf = eStudentSubscribeService.Find(roleexp);
                if (getinf == null)
                {
                    var addModel = new EStudentSubscribe
                    {
                        StuID = UserID,
                        TeacherID = new Guid(TopicID),
                         SubscribeTime = DateTime.Now,
                        CreateTime = DateTime.Now,
                        Creater = CurrentUserModel.LoginName,
                        ID = Guid.NewGuid(),
                        Modifyer = CurrentUserModel.LoginName,
                        IsDelete = false,
                        ModifyTime = DateTime.Now
                    };

                    var newrole = eStudentSubscribeService.Add(addModel);
                    flag = newrole != null;
                }
                else
                {
                    getinf.IsDelete = false;
                    getinf.Modifyer = CurrentUserModel.LoginName;
                    getinf.ModifyTime = DateTime.Now;
                    flag = eStudentSubscribeService.Update(getinf);
                }
            }
            catch (Exception ex)
            {
                flag = false;
            }
            return ServiceResponse<bool>.SuccessResponse(flag);
        }

        /// <summary>
        /// 取消关注教师
        /// </summary>
        /// <param name="TopicID"></param>
        /// <returns></returns>
        [HttpGet, Route("api/EStudentSubscribe/CancelSubscribe")]
        public ServiceResponse<bool> CancelSubscribe(string TopicID)
        {
            var UserID = CurrentUserModel.UserId;
            var flag = false;
            try
            {
                Expression<Func<EStudentSubscribe, bool>> roleexp = u => u.TeacherID.ToString() == TopicID && u.StuID == UserID && u.IsDelete == false;

                var getinf = eStudentSubscribeService.Find(roleexp);
                if (getinf == null)
                {
                    flag = false;
                }
                else
                {
                    flag = eStudentSubscribeService.Delete(getinf);
                }
            }
            catch (Exception ex)
            {
                flag = false;
            }
            return ServiceResponse<bool>.SuccessResponse(flag);
        }


        /// <summary>
        /// 获取关注教师信息
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost, Route("api/EStudentSubscribe/GetSubscribeList")]
        public ServiceResponse<StudentSubscribeViewModel> GetSubscribeList(StudentSubscribeRequest request)
        {
            var userid = CurrentUserModel.UserId;
            Expression<Func<EStudentSubscribe, bool>> whereLamdba = u => !u.IsDelete && u.StuID == userid;

          
            var modelList = eStudentSubscribeService.FindPageList(request.Page, request.PageSize, out int total, request.OrderBy, request.SortDir, whereLamdba).ToList();
            var viewModel = new StudentSubscribeViewModel { TotalCount = total };
            var userlist = modelList.Select(w=>w.TeacherID).ToList();
            var users = userService.FindList(u => !u.IsDelete && userlist.Contains(u.UserID)).ToList();
            
            foreach (var stunode in modelList)
            {
                var user = users.Find(w => w.UserID == stunode.TeacherID);
                viewModel.TeacherItems.Add(new TeacherInfo
                {
                    ID=stunode.ID,
                    StuID = stunode.StuID,
                    TopicID=stunode.TeacherID,
                    PraiseTime=stunode.SubscribeTime,
                    UserName = user?.UserName,
                    LoginName = user?.LoginName,
                    AccountStatus = user==null?0: user.AccountStatus,
                    MobilePhone = user?.MobilePhone,
                    Sex = SexHelper.SexToStr(user==null?0:user.Sex),
                    UserType = user?.UserType,
                    HeadPhoto = user?.HeadPhoto,
                    Position = user?.Position,
                    Remark = user?.Remark
                });
            }
            return ServiceResponse<StudentSubscribeViewModel>.SuccessResponse(viewModel);
        }
    }
}