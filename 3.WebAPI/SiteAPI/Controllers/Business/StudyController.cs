﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Http;
using ELearning.BLL;
using ELearning.Common.CommonStr;
using ELearning.Common.Extensions;
using ELearning.Common.Helpers;
using ELearning.Common.Model;
using ELearning.Models;
using ELearning.Models.ViewModel.YTJYAdmin.Package;
using ELearning.Models.ViewModel.YTJYAdmin.Paper;
using ELearning.Models.ViewModel.YTJYAdmin.Study;
using SiteAPI.Controllers.Common;

namespace SiteAPI.Controllers.Business
{
    /// <summary>
    /// 学习相关控制器
    /// </summary>
    public class StudyController : BaseApiController
    {
        /// <summary>
        /// 试卷相关操作service
        /// </summary>
        private TPaperService tPaperService => new TPaperService();

        /// <summary>
        /// 学习记录操作service
        /// </summary>
        private EVideoStudySumService eVideoStudySumService => new EVideoStudySumService();

        /// <summary>
        /// 试卷历史操作service
        /// </summary>
        private TPaperTestHistoryService tPaperTestHistoryService => new TPaperTestHistoryService();

        /// <summary>
        /// 获取我的视频学习记录
        /// </summary>
        [HttpPost, Route("api/ytjysite/EStudy/GetVideoLogList")]
        public ServiceResponse<VideoLogViewModel> GetVideoLogList(VideoLogRequest request)
        {
            var viewModel = eVideoStudySumService.GetVideoLogList(request, CurrentUserModel.UserId);

            return ServiceResponse<VideoLogViewModel>.SuccessResponse(CommonConst.OprateSuccessStr, viewModel);
        }

        /// <summary>
        /// 获取做题历史记录试卷列表
        /// </summary>
        [HttpPost, Route("api/ytjysite/EStudy/GetDonePaperList")]
        public ServiceResponse<DonePaperViewModel> GetDonePaperList(DonePaperRequest request)
        {
            Expression<Func<TPaperTestHistory, bool>> whereLamdba = p => !p.IsDelete;
            if (request.Type > 0)
                whereLamdba = whereLamdba.And(p => p.Type == request.Type);
            var models = tPaperTestHistoryService.FindPageList(request.PageIndex, request.PageSize, out var total, "CreateTime", "DESC", whereLamdba);

            var viewModel = new DonePaperViewModel { TotalCount = total };
            foreach (var m in models)
            {
                var v = new DonePaperItem
                {
                    PaperId = m.PaperId,
                    CourseId = m.CourseId,
                    ChapterId = m.ChapterId,
                    CreateTime = m.CreateTime.ToString("yyyy-MM-dd HH:mm:ss"),
                    Description = m.Description,
                    Name = m.Name,
                    PackageId = m.PackageId,
                    PaperTestHistoryId = m.Id,
                    Status = m.Status,
                    Type = m.Type
                };

                viewModel.Items.Add(v);
            }
            
            return ServiceResponse<DonePaperViewModel>.SuccessResponse(CommonConst.OprateSuccessStr, viewModel);
        }

        /// <summary>
        /// 获取试卷做题详情
        /// </summary>
        [HttpPost, Route("api/ytjysite/EStudy/GetDonePaperDetail")]
        [AllowAnonymous]
        public ServiceResponse<DonePaperDetailViewModel> GetDonePaperDetail(DonePaperDetailRequest request)
        {
            var viewModel = tPaperService.GetDonePaperDetail(request.PaperTestHistoryId);

            return ServiceResponse<DonePaperDetailViewModel>.SuccessResponse(CommonConst.OprateSuccessStr, viewModel);
        }
    }
}