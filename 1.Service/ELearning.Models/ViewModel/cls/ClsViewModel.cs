﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models
{
    public class ClsViewModel
    {
        /// <summary>
        /// 班级人数
        /// </summary>
        [Display(Name = "班级人数")]
        public int ClassNum { get; set; }

        /// <summary>
        /// 班级ID
        /// </summary>
        [Display(Name = "班级ID")]
        public Guid ClassID { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        [Display(Name = "班级名称")]
        public string ClassName { get; set; }

        /// <summary>
        /// 班级类型
        /// </summary>
        [Display(Name = "班级类型")]
        public int ClassType { get; set; }

        /// <summary>
        /// 班级状态
        /// </summary>
        [Display(Name = "班级状态")]
        public bool Status { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Display(Name = "备注")]
        public string Remark { get; set; }

        /// <summary>
        /// 所属机构
        /// </summary>
        [Display(Name = "所属机构")]
        public string OrgCode { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        [Display(Name = "创建人")]
        public string Creater { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 修改人
        /// </summary>
        [Display(Name = "修改人")]
        public string Modifyer { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        [Display(Name = "修改时间")]
        public DateTime ModifyTime { get; set; }

        /// <summary>
        /// 是否逻辑删除
        /// </summary>
        [Display(Name = "是否逻辑删除")]
        public bool IsDelete { get; set; }
    }
}
