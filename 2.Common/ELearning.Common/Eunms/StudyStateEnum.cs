﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Common
{
    /// <summary>
    /// 学生考试、作业答题状态
    /// </summary>
    public enum StudyStateEnum
    {
        /// <summary>
        /// 未开始
        /// </summary>
        [Description("未开始")]
        Wait = 1,
        /// <summary>
        /// 进行中
        /// </summary>
        [Description("进行中")]
        Process = 2,
        /// <summary>
        /// 已完成
        /// </summary>
        [Description("已完成")]
        Finished = 3,
        /// <summary>
        /// 其他
        /// </summary>
        [Description("其他")]
        Other = 0,
    }
}
