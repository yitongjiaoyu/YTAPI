//-------------------------------------------------------------------
//文件名称：ERole.cs
//模块名称：ERoleModel层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ELearning.Models
{	
	 /// <summary>
    /// ERole表实体类
    /// </summary>
	[Table("ERole")]
	public partial class ERole
    {
		
		/// <summary>
		/// 
		/// </summary>
				[Key]
	[Display(Name = "")]
		public Guid RoleID { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public Guid ParentID { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Code { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string OrgCode { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string RoleName { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Text { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Type { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Creater { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public DateTime CreateTime { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Modifyer { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public DateTime ModifyTime { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public bool IsDelete { get; set; }
		   
    }
}

