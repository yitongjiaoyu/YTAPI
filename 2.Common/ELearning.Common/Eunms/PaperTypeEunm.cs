﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Common
{
    public enum PaperTypeEunm
    {
        /// <summary>
        /// 随机组卷
        /// </summary>
        [Description("随机组卷")]
        Random = 1,
        /// <summary>
        /// 固定组卷
        /// </summary>
        [Description("固定组卷")]
        Fixation = 2,
    }
}

