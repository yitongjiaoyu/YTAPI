﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ELearning.Models.ViewModel.YTJYAdmin.Package
{
    /// <summary>
    /// 课程视频关联请求参数
    /// </summary>
    public class PackageCourseRequest : BaseSearchRequest
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { set; get; }

        /// <summary>
        /// 0.不关联，1.已关联
        /// </summary>
        public int Type { set; get; }

        /// <summary>
        /// 套餐id
        /// </summary>
        public Guid PackageId { set; get; }
    }

    public class PackageCourseViewModel
    {
        /// <summary>
        /// 列表
        /// </summary>
        public List<PackageCourseItem> Items { set; get; } = new List<PackageCourseItem>();

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { set; get; }
    }

    /// <summary>
    /// 列表元素
    /// </summary>
    public class PackageCourseItem
    {
        public Guid Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 大类分类名称
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// 班级基础分类名称
        /// </summary>
        public string TypeName { get; set; }

    }

    /// <summary>
    /// 课程列表实体
    /// </summary>
    public class PackageCourseModel
    {
        public Guid Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 大类分类名称
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// 班级基础分类名称
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { set; get; }

    }

    /// <summary>
    /// 课程套餐关联实体
    /// </summary>
    public class PackageCoursePostModel
    {
        /// <summary>
        /// 传入的关联类型：0.不关联,需要进行关联操作，1.已关联,需要进行取消关联操作
        /// </summary>
        public int Type { set; get; }

        /// <summary>
        /// 套餐id
        /// </summary>
        public Guid PackageId { set; get; }

        /// <summary>
        /// 课程Id列表
        /// </summary>
        public List<Guid> CourseIdList { set; get; }
    }

}
