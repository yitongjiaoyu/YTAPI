﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Common
{
    /// <summary>
    /// 订单审核状态
    /// </summary>
    public enum OrderConfirmEnum
    {
        /// <summary>
        /// 待确认
        /// </summary>
        [Description("待确认")]
        Confirming = 1,
        /// <summary>
        /// 已确认
        /// </summary>
        [Description("已确认")]
        Confirmed = 2,
    }
}
