﻿using ELearning.BLL;
using ELearning.Common;
using ELearning.Common.Extensions;
using ELearning.Common.Model;
using ELearning.Models;
using ELearning.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Http;

namespace ELearning.API.Controllers.ReportControllers
{
    /// <summary>
    /// 经销商基础信息相关业务
    /// </summary>
    public partial class DealerInfoController : BaseApiController
    {
        EDealerInfoService dealerInfoService => new EDealerInfoService();
        EAreaCityService areaCityService => new EAreaCityService();


        /// <summary>
        /// 获取经销商基础信息
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost, Route("api/dealer/getDealerInfos")]
        public ServiceResponse<Page<DealerInfoViewModel>> GetDealerInfos(DealerRequest req)
        {
            Expression<Func<EDealerInfo, bool>> whereLamdba = (x) => x.IsDelete == false;

            if (!string.IsNullOrEmpty(req.DealerName)) whereLamdba = whereLamdba.And(x => x.DealerName.Contains(req.DealerName));
            if (!string.IsNullOrEmpty(req.DealerCode)) whereLamdba = whereLamdba.And(x => x.DealerCode == req.DealerCode);
            if (!string.IsNullOrEmpty(req.ManagementName)) whereLamdba = whereLamdba.And(x => x.ManagementName.Contains(req.ManagementName));
            if (!string.IsNullOrEmpty(req.ManagementCode)) whereLamdba = whereLamdba.And(x => x.ManagementCode == req.ManagementCode);

            if (req.AreaCode != 0) whereLamdba = whereLamdba.And(x => x.AreaCode == req.AreaCode);
            if (req.ProvinceCode != 0) whereLamdba = whereLamdba.And(x => x.ProvinceCode == req.ProvinceCode);
            if (req.CityCode != 0) whereLamdba = whereLamdba.And(x => x.CityCode == req.CityCode);

            var data = dealerInfoService.FindPageList(req.Page, req.PageSize, out int total, req.OrderBy, req.IsAsc ? "asc" : "desc", whereLamdba).ToList();

            var areaCitys = areaCityService.FindList(x => !x.IsDelete && x.TypeLevel == 3).ToList();

            List<DealerInfoViewModel> models = new List<DealerInfoViewModel>();

            foreach (var a in data)
            {
                var areaCity = areaCitys.Find(x => x.CityCode == a.CityCode);
                DealerInfoViewModel model = new DealerInfoViewModel
                {
                    DealerInfo = a,
                    AreaName = areaCity.AreaName,
                    ProvinceName = areaCity.ProvinceName,
                    CityName = areaCity.CityName,
                };
                models.Add(model);
            }

            Page<DealerInfoViewModel> page = new Page<DealerInfoViewModel>
            {
                Items = models,
                TotalItems = total,
            };

            return ServiceResponse<Page<DealerInfoViewModel>>.SuccessResponse(page);
        }

        /// <summary>
        /// 添加经销商基础信息
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        [HttpPost, Route("api/dealer/addDealerInfo")]
        public ServiceResponse<bool> AddDealerInfo(EDealerInfo post)
        {
            var exist = dealerInfoService.Find(x => !x.IsDelete && x.DealerCode == post.DealerCode);
            if (exist.IsNotNull()) return ServiceResponse<bool>.SuccessResponse("经销商代码已存在", false);

            post.ID = Guid.NewGuid();
            post.Creater = post.Modifyer = CurrentUserModel.UserId.ToString();
            post.CreateTime = post.ModifyTime = DateTime.Now;
            post.IsDelete = false;

            dealerInfoService.Add(post);

            return ServiceResponse<bool>.SuccessResponse(true);
        }

        /// <summary>
        /// 更新经销商基础信息
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        [HttpPost, Route("api/dealer/updateDealerInfo")]
        public ServiceResponse<bool> UpdateDealerInfo(EDealerInfo post)
        {
            var exist = dealerInfoService.Find(x => !x.IsDelete && x.DealerCode == post.DealerCode && x.ID != post.ID);
            if (exist.IsNotNull()) return ServiceResponse<bool>.SuccessResponse("经销商代码已存在", false);

            post.Modifyer = CurrentUserModel.UserId.ToString();
            post.ModifyTime = DateTime.Now;

            dealerInfoService.Update(post);

            return ServiceResponse<bool>.SuccessResponse(true);
        }

    }
}