﻿
using ELearning.DAL;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace ELearning.BLL
{

    /// <summary>
    /// 服务基类
    /// <remarks></remarks>
    /// </summary>
    public abstract class BaseService<T> : IBaseService<T> where T : class
    {
        protected Logger Logger => LogManager.GetCurrentClassLogger();

        protected IBaseRepository<T> CurrentRepository { get; set; }

        public BaseService(IBaseRepository<T> currentRepository) { CurrentRepository = currentRepository; }

        public T Add(T entity) { return CurrentRepository.Add(entity); }

        public int Adds(List<T> entitys) { return CurrentRepository.Adds(entitys); }

        public bool Update(T entity) { return CurrentRepository.Update(entity); }

        public int Updates(List<T> entitys) { return CurrentRepository.Updates(entitys); }

        public bool Delete(T entity) { return CurrentRepository.Delete(entity); }

        public bool Deletes(List<T> entitys) { return CurrentRepository.Deletes(entitys); }

        public T Find(Expression<Func<T, bool>> whereLambda)
        {
            return CurrentRepository.Find(whereLambda);
        }
        public IQueryable<T> FindList(Expression<Func<T, bool>> whereLamdba, string orderName, bool isAsc)
        {
            return CurrentRepository.FindList(whereLamdba, orderName, isAsc);
        }
        public IQueryable<T> FindList(Expression<Func<T, bool>> whereLamdba)
        {
            return FindList(whereLamdba, "", true);
        }
        public IQueryable<T> FindList(Expression<Func<T, bool>> whereLamdba, string path)
        {
            return CurrentRepository.FindList(whereLamdba, path, null, true);
        }
        /// <summary>
        /// 列表
        /// </summary>
        /// <param name="pageIndex">页码数</param>
        /// <param name="pageSize">每页记录数</param>
        /// <param name="totalRecord">总记录数</param>
        /// <param name="sortBy">排序字段</param>
        /// <param name="sortDir">排序（asc 升序）</param>
        /// <param name="whereLamdba">全部查询（u => true）</param>
        /// <returns>列表</returns>
        public IQueryable<T> FindPageList(int pageIndex, int pageSize, out int totalRecord, string sortBy, string sortDir, Expression<Func<T, bool>> whereLamdba)
        {
            bool _isAsc = sortDir.ToUpper() == "DESC" ? true : false;
            if (whereLamdba == null)
            {
                whereLamdba = u => true;
            }
            if (string.IsNullOrWhiteSpace(sortBy))
            {
                sortBy = "CreateTime";
            }
            return CurrentRepository.FindPageList(pageIndex, pageSize, out totalRecord, whereLamdba, sortBy, _isAsc);
        }

        #region no savechanges

        public T AddNoSaveChange(T entity) { return CurrentRepository.AddNoSaveChange(entity); }

        public int AddsNoSaveChange(List<T> entitys) { return CurrentRepository.AddsNoSaveChange(entitys); }

        public bool UpdateNoSaveChange(T entity) { return CurrentRepository.UpdateNoSaveChange(entity); }

        public int UpdatesNoSaveChange(List<T> entitys) { return CurrentRepository.UpdatesNoSaveChange(entitys); }

        public bool DeleteNoSaveChange(T entity) { return CurrentRepository.DeleteNoSaveChange(entity); }

        public bool DeletesNoSaveChange(List<T> entitys) { return CurrentRepository.DeletesNoSaveChange(entitys); }

        public bool SaveChange() { return CurrentRepository.SaveChange(); }

        #endregion
    }
}
