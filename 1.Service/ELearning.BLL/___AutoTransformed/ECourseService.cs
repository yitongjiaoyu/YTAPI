//-------------------------------------------------------------------
//文件名称：ECourse.cs
//模块名称：ECourse业务逻辑接口层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearning.DAL;
using ELearning.Models;

namespace ELearning.BLL
{
    /// <summary>
    /// ECourse业务逻辑接口层
    /// </summary>
	public partial class ECourseService : BaseService<ECourse>
	{
     public  ECourseService() : base(RepositoryFactory.ECourseRepository) { }
	}
}


