﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.YTJYAdmin.Course
{
    public class ChapterRequest : BaseSearchRequest
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { set; get; }

        /// <summary>
        /// 课程Id
        /// </summary>
        [Display(Name = "课程Id")]
        public Guid CourseId { get; set; }
    }

    public class ChapterViewModel
    {
        /// <summary>
        /// 列表
        /// </summary>
        public List<ChapterItem> Items { set; get; } = new List<ChapterItem>();

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { set; get; }
    }

    public class ChapterItem
    {
        /// <summary>
        /// 章节id
        /// </summary>
        [Display(Name = "Id")]
        public Guid Id { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        [Display(Name = "课程Id")]
        public Guid CourseId { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 看完该视频必须做完一套题目达到这个分数才能看下一章
        /// </summary>
        [Display(Name = "看完该视频必须做完一套题目达到这个分数才能看下一章")]
        public double PassGrade { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [Display(Name = "序号")]
        public double Sort { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        [Display(Name = "更新时间")]
        public string UpdateTime { get; set; }

        /// <summary>
        /// 关联试卷ID
        /// </summary>
        public Guid PaperId { get; set; }

        /// <summary>
        /// 关联视频id
        /// </summary>
        public Guid VideoLibId { get; set; }
    }

    /// <summary>
    /// 更新或者添加实体
    /// </summary>
    public class ChapterPostModel
    {
        public Guid Id { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        [Display(Name = "课程Id")]
        public Guid CourseId { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 看完该视频必须做完一套题目达到这个分数才能看下一章
        /// </summary>
        [Display(Name = "看完该视频必须做完一套题目达到这个分数才能看下一章")]
        public double PassGrade { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [Display(Name = "序号")]
        public double Sort { get; set; }

        /// <summary>
        /// 关联试卷ID
        /// </summary>
        public Guid PaperId { get; set; }

        /// <summary>
        /// 关联视频id
        /// </summary>
        public Guid VideoLibId { get; set; }
    }

    /// <summary>
    /// 批量添加实体
    /// </summary>
    public class ChapterAddModel
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        [Display(Name = "课程Id")]
        public Guid CourseId { get; set; }

        /// <summary>
        /// 视频列表
        /// </summary>
        public List<ChapterAddItem> Items { get; set; } = new List<ChapterAddItem>();
    }

    /// <summary>
    /// 视频更新或者添加实体
    /// </summary>
    public class ChapterAddItem
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 文件Id
        /// </summary>
        [Display(Name = "文件Id")]
        public string FileId { get; set; }

        /// <summary>
        /// 文件大小,单位：KB
        /// </summary>
        [Display(Name = "文件大小,单位：KB")]
        public double Size { get; set; }

        /// <summary>
        /// 视频下载地址
        /// </summary>
        [Display(Name = "视频下载地址")]
        public string DownloadUrl { get; set; }
    }
}
