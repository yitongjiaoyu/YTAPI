﻿using ELearning.BLL;
using ELearning.Common;
using ELearning.Common.Extensions;
using ELearning.Common.Model;
using ELearning.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Http;

namespace ELearning.API.Controllers.AdminControllers
{
    /// <summary>
    /// 学生考试（作业）
    /// </summary>
    public class StuPaperController : BaseApiController
    {
        EDictionaryService dictionaryService = new EDictionaryService();
        EPaperService paperService = new EPaperService();
        RPaperQuestionService paperQService = new RPaperQuestionService();
        EQuestionService questionService = new EQuestionService();
        EPaperRuleService ruleService = new EPaperRuleService();
        ECourseSeriesService seriesService = new ECourseSeriesService();
        EStuScoreService scoreService = new EStuScoreService();
        EStuExamService stuExamService = new EStuExamService();
        ESysUserService userService = new ESysUserService();

        ///// <summary>
        ///// 学生试卷初始化
        ///// </summary>
        ///// <param name="eid">考试ID</param>
        ///// <returns></returns>
        //[Route("api/study/stuPaperInit")]
        //[HttpPost]
        //public object StuPaperInit(string eid)
        //{
        //    var Eid = Guid.Parse(eid);
        //    var Sid = CurrentUserModel.UserId;
        //    var stuScore = scoreService.Find(x => x.IsDelete == false && x.Eid == Eid && x.Sid == Sid);

        //    if (stuScore == null) return ServiceResponse<object>.SuccessResponse("试卷初始化失败，当前学生没有查看此作业的权限", false);

        //    switch (stuScore.StudyState)
        //    {
        //        //case 1:
        //        //    return HomeworkInit(stuScore.ID.ToString());
        //        //case 2:
        //        //    return HomeworkInit(stuScore.ID.ToString());
        //        case 3:
        //            return GetStuScore(stuScore.Eid.ToString(), "");
        //        default:
        //            return null;
        //    }
        //}

        ///// <summary>
        ///// 学生考试初始化
        ///// </summary>
        ///// <param name="eid"></param>
        ///// <returns></returns>
        //public object ExamInit(string eid)
        //{

        //}
    }
}