﻿using ELearning.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models
{
    /// <summary>
    /// ESysUser表实体类
    /// </summary>
    public class UserFilterViewModel : BaseRequest
    {
        public string Unumber { get; set; }
        /// <summary>
        /// 用户姓名
        /// </summary>
        public string UserName { set; get; }

        /// <summary>
        /// 登录名
        /// </summary>          
        public string LoginName { get; set; }

        /// <summary>
        /// 移动电话
        /// </summary>          
        public string MobilePhone { get; set; }

        /// <summary>
        /// 用户类型
        /// </summary>
        public string UserType { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        [Display(Name = "性别")]
        public int Sex { get; set; }

        /// <summary>
        /// 用户状态
        /// </summary>
        [Display(Name = "用户状态")]
        public int AccountStatus { get; set; }

    }
}
