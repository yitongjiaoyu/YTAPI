﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Common.Extensions
{
    /// <summary>
    /// 生成随机数
    /// </summary>
    public static class RandomHelper
    {

        /// <summary>
        /// 生成不重复的随机数
        /// </summary>
        /// <param name="n">随机数个数</param>
        /// <param name="min">范围最小值</param>
        /// <param name="max">范围最大值</param>
        /// <returns></returns>
        public static List<int> F(int n, int min, int max)
        {
            int[] a = new int[n];
            if (min > max || n < 1) return new List<int>();
            if (n > (max - min)) n = max - min + 1;
            int j;
            int[] b = new int[n];
            Random r = new Random();
            for (j = 0; j < n; j++)
            {
                int i = r.Next(min, max + 1);
                int num = 0;
                for (int k = 0; k < j; k++)
                {
                    if (b[k] == i)
                    {
                        num = num + 1;
                    }
                }
                if (num == 0)
                {
                    b[j] = i;
                }
                else
                {
                    j = j - 1;
                }
            }
            return b.ToList();
        }

    }
}
