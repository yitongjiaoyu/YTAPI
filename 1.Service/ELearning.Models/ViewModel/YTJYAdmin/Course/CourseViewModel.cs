﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.YTJYAdmin.Course
{
    /// <summary>
    /// 获取视频列表参数实体
    /// </summary>
    public class CourseRequest : BaseSearchRequest
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { set; get; }

        /// <summary>
        /// 大类分类编码
        /// </summary>
        public string CategoryId { set; get; }

        /// <summary>
        /// 基础分类编码
        /// </summary>
        public string TypeCode { set; get; }
    }

    public class CourseViewModel
    {
        /// <summary>
        /// 课程列表
        /// </summary>
        public List<CourseItem> Items { set; get; } = new List<CourseItem>();

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { set; get; }
    }

    public class CourseItem
    {
        public Guid Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 封面图片地址
        /// </summary>
        [Display(Name = "封面图片地址")]
        public string CoverUrl { get; set; }

        /// <summary>
        /// 展示所用
        /// </summary>
        public string ShowCoverUrl { get; set; }

        /// <summary>
        /// 大类分类Code
        /// </summary>
        [Display(Name = "大类分类Code")]
        public string CategoryId { get; set; }

        /// <summary>
        /// 大类分类名称
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// 班级基础分类code,基础班，高级版
        /// </summary>
        [Display(Name = "班级基础分类code,基础班，高级版")]
        public string TypeCode { get; set; }

        /// <summary>
        /// 班级基础分类名称
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [Display(Name = "序号")]
        public double Sort { get; set; }

        /// <summary>
        /// 原价
        /// </summary>
        [Display(Name = "原价")]
        public double OldPrice { get; set; }

        /// <summary>
        /// 现在价格
        /// </summary>
        [Display(Name = "现在价格")]
        public double NowPrice { get; set; }

        /// <summary>
        /// 讲师id
        /// </summary>
        [Display(Name = "讲师id")]
        public Guid TeacherId { get; set; }

        /// <summary>
        /// 是否为公开课
        /// </summary>
        [Display(Name = "是否为公开课")]
        public bool IsPublic { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        [Display(Name = "更新时间")]
        public DateTime UpdateTime { get; set; }

        /// <summary>
        /// 创建人Id
        /// </summary>
        [Display(Name = "创建人Id")]
        public Guid CreateUserId { get; set; }

        /// <summary>
        /// 编辑人id
        /// </summary>
        [Display(Name = "编辑人id")]
        public Guid UpdateUserId { get; set; }

        /// <summary>
        /// 关联试卷ID
        /// </summary>
        public List<Guid> PaperIds { get; set; } = new List<Guid>();
    }

    public class CourseModel
    {
        public Guid Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 封面图片地址
        /// </summary>
        [Display(Name = "封面图片地址")]
        public string CoverUrl { get; set; }

        /// <summary>
        /// 大类分类Code
        /// </summary>
        [Display(Name = "大类分类Code")]
        public string CategoryId { get; set; }

        /// <summary>
        /// 大类分类名称
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// 班级基础分类code,基础班，高级版
        /// </summary>
        [Display(Name = "班级基础分类code,基础班，高级版")]
        public string TypeCode { get; set; }

        /// <summary>
        /// 班级基础分类名称
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [Display(Name = "序号")]
        public double Sort { get; set; }

        /// <summary>
        /// 原价
        /// </summary>
        [Display(Name = "原价")]
        public double OldPrice { get; set; }

        /// <summary>
        /// 现在价格
        /// </summary>
        [Display(Name = "现在价格")]
        public double NowPrice { get; set; }

        /// <summary>
        /// 讲师id
        /// </summary>
        [Display(Name = "讲师id")]
        public Guid TeacherId { get; set; }

        /// <summary>
        /// 是否为公开课
        /// </summary>
        [Display(Name = "是否为公开课")]
        public bool IsPublic { get; set; }

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { set; get; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        [Display(Name = "更新时间")]
        public DateTime UpdateTime { get; set; }

        /// <summary>
        /// 创建人Id
        /// </summary>
        [Display(Name = "创建人Id")]
        public Guid CreateUserId { get; set; }

        /// <summary>
        /// 编辑人id
        /// </summary>
        [Display(Name = "编辑人id")]
        public Guid UpdateUserId { get; set; }

        /// <summary>
        /// 试卷ID
        /// </summary>
        public Guid? PaperId { get; set; }
    }

    /// <summary>
    /// 更新或者添加实体
    /// </summary>
    public class CoursePostModel
    {
        public Guid Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 封面图片地址
        /// </summary>
        [Display(Name = "封面图片地址")]
        public string CoverUrl { get; set; }

        /// <summary>
        /// 大类分类Code
        /// </summary>
        [Display(Name = "大类分类Code")]
        public string CategoryId { get; set; }

        /// <summary>
        /// 班级基础分类code,基础班，高级版
        /// </summary>
        [Display(Name = "班级基础分类code,基础班，高级版")]
        public string TypeCode { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [Display(Name = "序号")]
        public double Sort { get; set; }

        /// <summary>
        /// 原价
        /// </summary>
        [Display(Name = "原价")]
        public double OldPrice { get; set; }

        /// <summary>
        /// 现在价格
        /// </summary>
        [Display(Name = "现在价格")]
        public double NowPrice { get; set; }

        /// <summary>
        /// 讲师id
        /// </summary>
        [Display(Name = "讲师id")]
        public Guid TeacherId { get; set; }

        /// <summary>
        /// 是否为公开课
        /// </summary>
        [Display(Name = "是否为公开课")]
        public bool IsPublic { get; set; }

        /// <summary>
        /// 关联试卷ID
        /// </summary>
        public List<Guid> PaperIds { get; set; }
    }

    /// <summary>
    /// 导入课程实体映射
    /// </summary>
    public class ImportCourseViewModel
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 大类分类名称
        /// </summary>
        [Display(Name = "大类分类名称")]
        public string CategoryName { get; set; }

        /// <summary>
        /// 班级基础分类code,基础班，高级版
        /// </summary>
        [Display(Name = "班级基础分类code,基础班，高级版")]
        public string TypeName { get; set; }

        /// <summary>
        /// 章名称
        /// </summary>
        public string ChapterName { get; set; }

        /// <summary>
        /// 讲师名字
        /// </summary>
        [Display(Name = "讲师名字")]
        public string TeacherName { get; set; }

        /// <summary>
        /// 错误原因
        /// </summary>
        public string Reason { get; set; }
    }

    /// <summary>
    /// 保存课程实体映射
    /// </summary>
    public class SaveCourseModel
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 大类分类名称
        /// </summary>
        [Display(Name = "大类分类名称")]
        public string CategoryName { get; set; }

        /// <summary>
        /// 大类分类Code
        /// </summary>
        [Display(Name = "大类分类Code")]
        public string CategoryId { get; set; }

        /// <summary>
        /// 班级基础分类code,基础班，高级版
        /// </summary>
        [Display(Name = "班级基础分类code,基础班，高级版")]
        public string TypeCode { get; set; }

        /// <summary>
        /// 班级基础分类名称,基础班，高级版
        /// </summary>
        [Display(Name = "班级基础分类名称,基础班，高级版")]
        public string TypeName { get; set; }

        /// <summary>
        /// 章名称列表
        /// </summary>
        public List<string> ChapterNames { get; set; } = new List<string>();

        /// <summary>
        /// 讲师名字
        /// </summary>
        [Display(Name = "讲师名字")]
        public string TeacherName { get; set; }
    }
}
