﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ELearning.Models.ViewModel.User;

namespace ELearning.Models.ViewModel.Role
{
    public class HandleRoleUserViewModel
    {
        /// <summary>
        /// 角色id
        /// </summary>
        public Guid RoleId { set; get; }

        /// <summary>
        /// 操作类型：0.授权，1.取消授权
        /// </summary>
        public int OprateType { set; get; }

        /// <summary>
        /// 分配的用户列表
        /// 选中的用户列表
        /// </summary>
        public List<GetUserItem> Users { set; get; }

        /// <summary>
        /// 当前页面的用户列表
        /// </summary>
        public List<GetUserItem> PageUserList { set; get; }

    }

    /// <summary>
    /// 为角色分配指定搜索条件下的用户
    /// </summary>
    public class HandleBatchRoleUserViewModel
    {
        /// <summary>
        /// 角色id
        /// </summary>
        public Guid RoleId { set; get; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        public string UserName { set; get; }

        /// <summary>
        /// 登录名
        /// </summary>          
        public string LoginName { get; set; }

        /// <summary>
        /// 移动电话
        /// </summary>          
        public string MobilePhone { get; set; }

        /// <summary>
        /// 用户类型
        /// </summary>
        public string UserType { get; set; }

        /// <summary>
        /// 操作类型：0.授权指定用户，1.取消授权指定用户
        /// </summary>
        public int OprateType { set; get; }

        /// <summary>
        /// 员工编号
        /// </summary>
        public string Unumber { get; set; }

        /// <summary>
        /// 经销商号码
        /// </summary>
        public string AgentNumber { get; set; }
    }

    /// <summary>
    /// 获取角色下的用户列表参数
    /// </summary>
    public class RoleUserRequest : BaseRequest
    {
        /// <summary>
        /// 角色id
        /// </summary>
        public Guid RoleId { set; get; }
    }

    /// <summary>
    /// 选择角色下拉框元素，层级分明
    /// </summary>
    [DataContract]
    public class RoleOptionItem
    {
        /// <summary>
        /// 角色id
        /// </summary>
        [DataMember(Name = "id")]
        public Guid RoleId { set; get; }

        /// <summary>
        /// 角色名称
        /// </summary>
        [DataMember(Name = "label")]
        public string RoleName { set; get; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "ParentId")]
        public Guid ParentID { get; set; }

        /// <summary>
        /// 子列表元素
        /// </summary>
        [DataMember(Name = "children")]
        public List<RoleOptionItem> SubItems = new List<RoleOptionItem>();
    }
}
