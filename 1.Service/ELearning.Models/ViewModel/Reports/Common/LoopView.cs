﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel
{
    public class LoopView
    {
        public string name { get; set; } = string.Empty;
        public int y { get; set; } = 0;
    }
}
