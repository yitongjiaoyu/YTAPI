﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using ELearning.BLL;
using ELearning.Common;
using ELearning.Common.Helpers;
using ELearning.Common.Http;
using ELearning.Common.Oprator;
using ELearning.Models;
using SiteAPI.HandleAttribute;

namespace SiteAPI.Controllers.Common
{
    /// <summary>
    /// API基类
    /// </summary>
    [RequestAuthorize]
    public class BaseApiController : ApiController
    {
        /// <summary>
        /// 当前用户信息实体
        /// </summary>
        public OperatorModel CurrentUserModel
        {
            get
            {
                var values = HttpContext.Current.Request.Headers.GetValues("Authorization");
                if (values.IsNotNull() && values.Length > 0)
                {
                    var authorization = values[0];
                    if (Guid.TryParse(authorization, out var g))
                    {
                        var currentUserModel = OperatorProvider.Provider.GetCurrent(authorization);
                        if (currentUserModel == null)
                        {
                            currentUserModel = new OperatorModel();
                            //var tlLoginTokenService = new TLoginTokenService();
                            //var userService = new ESysUserService();

                            //var m = tlLoginTokenService.Find(i => i.Token == authorization && i.PassDateTime > DateTime.Now);
                            //if (m.IsNotNull())
                            //{
                            //    var user = userService.Find(u => !u.IsDelete && u.UserID == m.UserId);
                            //    if (user.IsNotNull())
                            //    {
                            //        currentUserModel = new OperatorModel
                            //        {
                            //            Email = user.Email,
                            //            HeadPhoto = user.HeadPhoto,
                            //            LoginName = user.LoginName,
                            //            OrgCode = user.OrgCode,
                            //            MobilePhone = user.MobilePhone,
                            //            Position = user.Position,
                            //            Remark = user.Remark,
                            //            UserId = user.UserID,
                            //            UserIp = IpHelp.GetClientIp(),
                            //            UserName = user.UserName,
                            //            UserType = user.UserType,
                            //            CardNo = user.CardNo
                            //        };

                            //        OperatorProvider.Provider.AddCurrent(authorization, currentUserModel, m.PassDateTime);
                            //    }
                            //}


                        }
                        return currentUserModel;
                    }
                }
                return new OperatorModel();
            }
        }

        /// <summary>
        /// 生成邀请码并记录到数据库
        /// </summary>
        /// <param name="orgCode">组织编码,如果生成组织的邀请码，传递组织的编码，如果生成用户的邀请码，则传递用户所属的组织编码</param>
        /// <param name="userId">用户id，如果是为组织生成邀请码，userid可传 Guid.Empty</param>
        /// <returns></returns>
        protected string GetInviteCode(string orgCode, Guid userId)
        {
            #region 生成邀请码
            var tInviteCodeLogService = new TInviteCodeLogService();
            var sort = tInviteCodeLogService.FindList(p => !p.IsDelete).OrderByDescending(p => p.Sort).FirstOrDefault()?.Sort ?? 0;
            var count = 0;
            var inviteCode = $"{CodeHelper.CreateCodePre()}{sort.ToString().PadLeft(8, '0')}";
            var code = inviteCode;
            var log = tInviteCodeLogService.Find(i => i.InviteCode == code);
            while (log.IsNotNull())//查重，是否有重复邀请码
            {
                sort++;
                inviteCode = $"{CodeHelper.CreateCodePre()}{sort.ToString().PadLeft(8, '0')}";
                var code1 = inviteCode;
                log = tInviteCodeLogService.Find(i => i.InviteCode == code1);
                count++;
                if (count > 5)
                {
                    inviteCode = DateTimeHelper.GetNowTimestamp().ToString();
                    break;
                }
            }

            //邀请码生成成功，存入数据库
            tInviteCodeLogService.AddNoSaveChange(new TInviteCodeLog
            {
                Id = Guid.NewGuid(),
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now,
                IsDelete = false,
                CreateUserId = CurrentUserModel.UserId,
                UpdateUserId = CurrentUserModel.UserId,
                InviteCode = inviteCode,
                OrgCode = orgCode,
                UserId = userId,
                Sort = sort
            });

            #endregion

            return inviteCode;
        }
    }
}