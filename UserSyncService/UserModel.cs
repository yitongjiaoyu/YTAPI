﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserSyncService
{

    public class RequestInfo
    {
        public filter filter { get; set; } = new filter();
        public pageInfo pageInfo { get; set; } = new pageInfo();
    }
    public class filter
    {
        public string name { get; set; }
        public string UpdateTime { get; set; }

    }
    public class pageInfo
    {
        public int page { get; set; }
        public int pageSize { get; set; }
    }

    public class ResponseInfo
    {
        public int status { get; set; }
        public body body { get; set; } = new body();
    }
    public class body
    {
        public result result { get; set; } = new result();
    }
    public class result
    {
        public List<UserAddInfoXszs> data { get; set; } = new List<UserAddInfoXszs>();
        public int page { get; set; }
    }
    public class UserAddInfoXszs
    {
        public string dd { get; set; }
        public string updatetime { get; set; }
        public string zjdlsjApp { get; set; }
        public string dlsbcs { get; set; }
        public string zjdlsj { get; set; }
        public string xgmmsj { get; set; }
        public string yhje { get; set; }
        public string fgsh { get; set; }
        public string dlzt { get; set; }
        public string czyqx { get; set; }
        public string zt { get; set; }
        public string czyzw { get; set; }
        public string rq2 { get; set; }
        public string rq1 { get; set; }
        public string czymc { get; set; }
        public string jxsh { get; set; }
        public string czydm { get; set; }
    }

    public static class ConfigInfo
    {
        public static string PostUrl { set; get; } = Common.GetConfigString("PostUrl");
        public static string AppId { set; get; } = Common.GetConfigString("AppId");
        public static string AppKey { set; get; } = Common.GetConfigString("AppKey");

    }
}
