//-------------------------------------------------------------------
//文件名称：TPaperTestHistory.cs
//模块名称：TPaperTestHistoryModel层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ELearning.Models
{	
	 /// <summary>
    /// TPaperTestHistory表实体类
    /// </summary>
	[Table("TPaperTestHistory")]
	public partial class TPaperTestHistory
    {
		
		/// <summary>
		/// Id
		/// </summary>
				[Key]
	[Display(Name = "Id")]
		public Guid Id { get; set; }
		
		/// <summary>
		/// 试卷Id
		/// </summary>
			[Display(Name = "试卷Id")]
		public Guid PaperId { get; set; }
		
		/// <summary>
		/// 用户Id
		/// </summary>
			[Display(Name = "用户Id")]
		public Guid UserId { get; set; }
		
		/// <summary>
		/// 课程Id
		/// </summary>
			[Display(Name = "课程Id")]
		public Guid CourseId { get; set; }
		
		/// <summary>
		/// 套餐Id
		/// </summary>
			[Display(Name = "套餐Id")]
		public Guid PackageId { get; set; }
		
		/// <summary>
		/// 章节id
		/// </summary>
			[Display(Name = "章节id")]
		public Guid ChapterId { get; set; }
		
		/// <summary>
		/// 组织编码
		/// </summary>
			[Display(Name = "组织编码")]
		public string OrgCode { get; set; }
		
		/// <summary>
		/// 考试状态，0.未开始，1.进行中，2.已完成
		/// </summary>
			[Display(Name = "考试状态，0.未开始，1.进行中，2.已完成")]
		public int Status { get; set; }
		
		/// <summary>
		/// 考试开始时间
		/// </summary>
			[Display(Name = "考试开始时间")]
		public DateTime StartTime { get; set; }
		
		/// <summary>
		/// 考试结束时间
		/// </summary>
			[Display(Name = "考试结束时间")]
		public DateTime EndTime { get; set; }
		
		/// <summary>
		/// 考试总时长,单位：秒
		/// </summary>
			[Display(Name = "考试总时长,单位：秒")]
		public int TimeSpan { get; set; }
		
		/// <summary>
		/// 题目总数量
		/// </summary>
			[Display(Name = "题目总数量")]
		public int TotalCount { get; set; }
		
		/// <summary>
		/// 已做题目总数量
		/// </summary>
			[Display(Name = "已做题目总数量")]
		public int DoneCount { get; set; }
		
		/// <summary>
		/// 做对题目数量
		/// </summary>
			[Display(Name = "做对题目数量")]
		public int RightCount { get; set; }
		
		/// <summary>
		/// 做错题目数量
		/// </summary>
			[Display(Name = "做错题目数量")]
		public int ErrorCount { get; set; }
		
		/// <summary>
		/// 上次做的题目id
		/// </summary>
			[Display(Name = "上次做的题目id")]
		public Guid LastQuestionId { get; set; }
		
		/// <summary>
		/// 成绩
		/// </summary>
			[Display(Name = "成绩")]
		public double TotalScore { get; set; }
		
		/// <summary>
		/// 名称
		/// </summary>
			[Display(Name = "名称")]
		public string Name { get; set; }
		
		/// <summary>
		/// 描述
		/// </summary>
			[Display(Name = "描述")]
		public string Description { get; set; }
		
		/// <summary>
		/// 类型：1.真题，2.模拟题
		/// </summary>
			[Display(Name = "类型：1.真题，2.模拟题")]
		public int Type { get; set; }
		
		/// <summary>
		/// 大类分类Code
		/// </summary>
			[Display(Name = "大类分类Code")]
		public string CategoryId { get; set; }
		
		/// <summary>
		/// 班级基础分类code,基础班，高级版
		/// </summary>
			[Display(Name = "班级基础分类code,基础班，高级版")]
		public string TypeCode { get; set; }
		
		/// <summary>
		/// 年份
		/// </summary>
			[Display(Name = "年份")]
		public int Year { get; set; }
		
		/// <summary>
		/// 试卷时长，单位：分钟
		/// </summary>
			[Display(Name = "试卷时长，单位：分钟")]
		public int PaperTimeSpan { get; set; }
		
		/// <summary>
		/// 试卷总分
		/// </summary>
			[Display(Name = "试卷总分")]
		public double PaperTotalScore { get; set; }
		
		/// <summary>
		/// 序号
		/// </summary>
			[Display(Name = "序号")]
		public double Sort { get; set; }
		
		/// <summary>
		/// 创建时间
		/// </summary>
			[Display(Name = "创建时间")]
		public DateTime CreateTime { get; set; }
		
		/// <summary>
		/// 更新时间
		/// </summary>
			[Display(Name = "更新时间")]
		public DateTime UpdateTime { get; set; }
		
		/// <summary>
		/// 创建人Id
		/// </summary>
			[Display(Name = "创建人Id")]
		public Guid CreateUserId { get; set; }
		
		/// <summary>
		/// 编辑人id
		/// </summary>
			[Display(Name = "编辑人id")]
		public Guid UpdateUserId { get; set; }
		
		/// <summary>
		/// 是否删除
		/// </summary>
			[Display(Name = "是否删除")]
		public bool IsDelete { get; set; }
		   
    }
}

