﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using ELearning.Common.CommonStr;

namespace ELearning.Common.Helpers
{
    public class PictureHelper
    {
        /// <summary>
        /// 拼接文件全路径
        /// </summary>
        /// <param name="filePath">文件相对地址</param>
        public static string GetFileFullPath(string filePath)
        {
            var str = string.Empty;
            if (!string.IsNullOrEmpty(filePath))
            {
                if (filePath.Contains("http://") || filePath.Contains("https://"))
                {
                    str = filePath;
                }
                else
                {
                    //var host = ConfigHelper.GetConfigString("FilePreUrl");
                    str = $"{FileUploadConst.DownloadPath}/{filePath}";
                }
            }
            return str;
        }
    }
}
