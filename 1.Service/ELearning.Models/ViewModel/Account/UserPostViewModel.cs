﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.Account
{
    /// <summary>
    /// 修改密码实体
    /// </summary>
    public class UpdatePasswordViewModel
    {
        /// <summary>
        /// 原来密码
        /// </summary>
        public string OldPassword { set; get; }

        /// <summary>
        /// 新密码
        /// </summary>
        public string NewPassword { set; get; }
    }

    /// <summary>
    /// 更新密码实体
    /// </summary>
    public class UpdatePhoneViewModel
    {
        /// <summary>
        /// 移动电话
        /// </summary>          
        public string MobilePhone { get; set; }
    }

    /// <summary>
    /// 更新身份证号码实体
    /// </summary>
    public class UpdateCardNoViewModel
    {
        /// <summary>
        /// 身份证号码
        /// </summary>
        public string CardNo { get; set; }
    }

    /// <summary>
    /// 更新用户头像实体
    /// </summary>
    public class UpdateUserPhotoViewModel
    {
        /// <summary>
        /// 头像地址,图片相对地址
        /// </summary>
        public string HeadPhoto { get; set; }
    }
}
