﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.YTJYAdmin.VideoLib
{
    /// <summary>
    /// 回调参数
    /// </summary>
    public class ReqCallback
    {
        /// <summary>
        /// 事件类型，ProcedureStateChanged
        /// </summary>
        public string EventType { set; get; }

        /// <summary>
        /// 任务流状态变更事件，当事件类型为 ProcedureStateChanged 时有效。
        /// 注意：此字段可能返回 null，表示取不到有效值。
        /// </summary>
        public ChangeEventModel ProcedureStateChangeEvent { set; get; } = new ChangeEventModel();
    }

    /// <summary>
    /// 任务流状态变更事件内容
    /// </summary>
    public class ChangeEventModel
    {
        /// <summary>
        /// 任务状态，有PROCESSING，SUCCESS，FAIL三种
        /// </summary>
        public string Status { set; get; }

        /// <summary>
        /// 错误信息
        /// </summary>
        public string Message { set; get; }

        /// <summary>
        /// 错误码。 0: 成功, 其他值: 失败,其中30009为原文件异常失败；30010为系统失败或未知
        /// </summary>
        public int ErrCode { set; get; }

        /// <summary>
        /// 文件id
        /// </summary>
        public string FileId { set; get; }

        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { set; get; }

        /// <summary>
        /// 文件下载地址
        /// </summary>
        public string FileUrl { set; get; }

        /// <summary>
        /// 视频处理任务的执行状态与结果
        /// </summary>
        public List<ResultSet> MediaProcessResultSet { set; get; } = new List<ResultSet>();
    }

    /// <summary>
    /// 回调接口类型
    /// </summary>
    public static class TencentCallBackType
    {
        /// <summary>
        /// 视频拼接类型
        /// </summary>
        public static string ConcatComplete = "ConcatComplete";

        /// <summary>
        /// 视频转码类型
        /// </summary>
        public static string TranscodeComplete = "TranscodeComplete";

        /// <summary>
        /// 任务流状态变更事件
        /// </summary>
        public static string ProcedureStateChanged = "ProcedureStateChanged";
    }

    /// <summary>
    /// 视频处理任务的执行状态与结果。
    /// </summary>
    public class ResultSet
    {
        /// <summary>
        /// 任务的类型，可以取的值有：
        /// Transcode：转码
        /// AnimatedGraphics：转动图
        /// SnapshotByTimeOffset：时间点截图
        /// SampleSnapshot：采样截图
        /// ImageSprites：雪碧图
        /// CoverBySnapshot：截图做封面
        /// AdaptiveDynamicStreaming：自适应码流
        /// </summary>
        public string Type { set; get; }

        /// <summary>
        /// 视频转码任务的查询结果，当任务类型为 Transcode 时有效。注意：此字段可能返回 null，表示取不到有效值。
        /// </summary>
        public MediaProcessTaskTranscodeResult TranscodeTask = new MediaProcessTaskTranscodeResult();
    }

    /// <summary>
    /// 任务信息
    /// </summary>
    public class MediaProcessTaskTranscodeResult
    {
        /// <summary>
        /// 任务状态，有PROCESSING，SUCCESS，FAIL三种
        /// </summary>
        public string Status { set; get; }

        /// <summary>
        /// 错误信息
        /// </summary>
        public string Message { set; get; }

        /// <summary>
        /// 错误码。 0: 成功, 其他值: 失败,其中30009为原文件异常失败；30010为系统失败或未知
        /// </summary>
        public int ErrCode { set; get; }

        /// <summary>
        /// 任务的输入信息
        /// </summary>
        public InPut Input { set; get; }

        /// <summary>
        /// 任务的输出信息，任务成功时会有该字段，失败则没有
        /// </summary>
        public MediaTranscodeItem Output { set; get; } = new MediaTranscodeItem();
    }

    /// <summary>
    /// 任务的输入信息
    /// </summary>
    public class InPut
    {
        /// <summary>
        /// 模板ID
        /// </summary>
        public int Definition { set; get; }

        /// <summary>
        /// 是否设置水印，1为设置，0为没设置。
        /// </summary>
        public int Watermark { set; get; }

        /// <summary>
        /// 动图在视频中的起始时间。单位：秒
        /// </summary>
        public int StartTime { set; get; }

        /// <summary>
        /// 动图在视频中的结束时间。单位：秒
        /// </summary>
        public int EndTime { set; get; }

        /// <summary>
        /// 截图方式。Time：依照时间点截图；Percent：依照百分比截图
        /// </summary>
        public string PositionType { set; get; }

        /// <summary>
        /// 截图位置。
        /// </summary>
        public int Position { set; get; }
    }

    /// <summary>
    /// 任务的输出信息，转码信息
    /// </summary>
    public class MediaTranscodeItem
    {
        /// <summary>
        /// 视频url
        /// </summary>
        public string Url { set; get; }

        /// <summary>
        /// 作为视频封面的截图url
        /// </summary>
        public string ImageUrl { set; get; }

        /// <summary>
        /// 转码模板id
        /// </summary>
        public string Definition { set; get; }

        /// <summary>
        /// 视频时长
        /// </summary>
        public double Duration { set; get; }
    }
}
