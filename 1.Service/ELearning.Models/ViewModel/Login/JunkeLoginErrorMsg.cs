﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models
{
    /// <summary>
    /// 骏客登录返回的错误信息
    /// </summary>
    [DataContract]
    public class JunkeLoginErrorMsg
    {
        /// <summary>
        /// Gets or sets the error MSG.
        /// </summary>
        /// <value>
        /// The error MSG.
        /// </value>
        [DataMember]
        public string errorMsg { get; set; }
    }
}
