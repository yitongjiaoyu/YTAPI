﻿//-------------------------------------------------------------------
//文件名称：TQuestion.cs
//模块名称：TQuestion业务逻辑接口层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearning.Common.Extensions;
using ELearning.Common.Helpers;
using ELearning.DAL;
using ELearning.Models;
using ELearning.Models.ViewModel.Question;
using ELearning.Models.ViewModel.YTJYAdmin.Course;

namespace ELearning.BLL
{
    /// <summary>
    /// TQuestion业务逻辑接口层
    /// </summary>
    public partial class TQuestionService : BaseService<TQuestion>
    {
        /// <summary>
        /// 获取题目列表
        /// </summary>
        public TQuestionViewModel GetModelList(TQuestionRequest request)
        {
            var models = RepositoryFactory.TQuestionRepository.GetModelList(request);
            var viewModel = new TQuestionViewModel { TotalCount = models.FirstOrDefault()?.TotalCount ?? 0 };
            foreach (var m in models)
            {
                var v = viewModel.Items.FirstOrDefault(t => t.Id == m.Id);
                if (v.IsNull())
                {
                    v = m.MapTo<TQuestionShowItem, QuestionModel>();
                    viewModel.Items.Add(v);
                }
            }

            return viewModel;
        }
    }
}


