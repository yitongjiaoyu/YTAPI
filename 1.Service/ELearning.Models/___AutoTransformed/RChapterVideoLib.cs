//-------------------------------------------------------------------
//文件名称：RChapterVideoLib.cs
//模块名称：RChapterVideoLibModel层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ELearning.Models
{	
	 /// <summary>
    /// RChapterVideoLib表实体类
    /// </summary>
	[Table("RChapterVideoLib")]
	public partial class RChapterVideoLib
    {
		
		/// <summary>
		/// Id
		/// </summary>
				[Key]
	[Display(Name = "Id")]
		public Guid Id { get; set; }
		
		/// <summary>
		/// 课程Id
		/// </summary>
			[Display(Name = "课程Id")]
		public Guid CourseId { get; set; }
		
		/// <summary>
		/// 章节id
		/// </summary>
			[Display(Name = "章节id")]
		public Guid ChapterId { get; set; }
		
		/// <summary>
		/// 视频id
		/// </summary>
			[Display(Name = "视频id")]
		public Guid VideoLibId { get; set; }
		
		/// <summary>
		/// 序号
		/// </summary>
			[Display(Name = "序号")]
		public double Sort { get; set; }
		
		/// <summary>
		/// 创建时间
		/// </summary>
			[Display(Name = "创建时间")]
		public DateTime CreateTime { get; set; }
		
		/// <summary>
		/// 更新时间
		/// </summary>
			[Display(Name = "更新时间")]
		public DateTime UpdateTime { get; set; }
		
		/// <summary>
		/// 创建人Id
		/// </summary>
			[Display(Name = "创建人Id")]
		public Guid CreateUserId { get; set; }
		
		/// <summary>
		/// 编辑人id
		/// </summary>
			[Display(Name = "编辑人id")]
		public Guid UpdateUserId { get; set; }
		
		/// <summary>
		/// 是否删除
		/// </summary>
			[Display(Name = "是否删除")]
		public bool IsDelete { get; set; }
		   
    }
}

