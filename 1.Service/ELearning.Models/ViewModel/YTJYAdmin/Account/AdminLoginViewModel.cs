﻿namespace ELearning.Models.ViewModel.YTJYAdmin.Account
{
    /// <summary>
    /// 运营平台登录参数
    /// </summary>
    public class AdminLoginRequest
    {
        /// <summary>
        /// 登录名
        /// </summary>
        public string LoginName { get; set; }

        /// <summary>
        /// 登录密码
        /// </summary>
        public string Password { get; set; }
    }

    /// <summary>
    /// 登录成功返回值
    /// </summary>
    public class AdminLoginViewModel
    {
        public string Token { get; set; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        public string UserName { set; get; }

        /// <summary>
        /// 头像
        /// </summary>          
        public string HeadPhoto { get; set; }

        /// <summary>
        /// 职务
        /// </summary>          
        public string Position { get; set; }

        /// <summary>
        /// 移动电话
        /// </summary>          
        public string MobilePhone { get; set; }

        /// <summary>
        /// 电子邮件
        /// </summary>          
        public string Email { get; set; }

        /// <summary>
        /// 个人简介
        /// </summary>          
        public string Remark { get; set; }

        /// <summary>
        /// 身份证号码
        /// </summary>
        public string CardNo { get; set; }
    }
}
