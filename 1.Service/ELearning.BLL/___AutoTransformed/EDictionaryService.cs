//-------------------------------------------------------------------
//文件名称：EDictionary.cs
//模块名称：EDictionary业务逻辑接口层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearning.DAL;
using ELearning.Models;

namespace ELearning.BLL
{
    /// <summary>
    /// EDictionary业务逻辑接口层
    /// </summary>
	public partial class EDictionaryService : BaseService<EDictionary>
	{
     public  EDictionaryService() : base(RepositoryFactory.EDictionaryRepository) { }
	}
}


