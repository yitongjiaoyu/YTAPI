﻿using ELearning.BLL;
using ELearning.Common;
using ELearning.Common.Extensions;
using ELearning.Common.Model;
using ELearning.Models;
using ELearning.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;


namespace ELearning.API.Controllers.ReportControllers
{
    /// <summary>
    /// 用户画像
    /// </summary>
    public partial class UserReportController : BaseApiController
    {
        EDealerInfoService dealerInfoService => new EDealerInfoService();

        /// <summary>
        /// 用户画像-学生
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost, Route("api/report/userPicStuReport")]
        public ServiceResponse<Page<UserPic>> UserPicStuReport(UserPicRequest req)
        {
            var data = dealerInfoService.UserPicStuReport(req);

            return ServiceResponse<Page<UserPic>>.SuccessResponse("成功", data);
        }

        /// <summary>
        /// 用户画像-教师
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost, Route("api/report/userPicTeaReport")]
        public ServiceResponse<Page<UserPicTea>> UserPicTeaReport(UserPicRequest req)
        {
            var data = dealerInfoService.UserPicTeaReport(req);

            return ServiceResponse<Page<UserPicTea>>.SuccessResponse("成功", data);
        }

    }
}