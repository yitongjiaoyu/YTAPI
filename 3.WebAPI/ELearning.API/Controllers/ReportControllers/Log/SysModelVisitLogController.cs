﻿using ELearning.BLL;
using ELearning.Common;
using ELearning.Common.Extensions;
using ELearning.Common.Model;
using ELearning.Models;
using ELearning.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace ELearning.API.Controllers.ReportControllers
{
    /// <summary>
    /// 功能模块访问相关业务
    /// </summary>
    public partial class SysModelVisitLogController : BaseApiController
    {
        ESysModelVisitLogService sysModelVisitLogService => new ESysModelVisitLogService();

        /// <summary>
        /// 添加功能模块访问记录
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        [HttpPost, Route("api/log/addSysModelVisitLog")]
        public ServiceResponse<bool> AddSysModelVisitLog(ESysModelVisitLog post)
        {
            if (string.IsNullOrEmpty(post.ModelCode)) return ServiceResponse<bool>.SuccessResponse("ModelCode不可为空", false);

            post.ID = Guid.NewGuid();
            post.UserID = CurrentUserModel.UserId;
            post.VisitDate = DateTime.Now;
            post.UserIP = HttpContext.Current.Request.UserHostAddress;

            // 5分钟内访问同一个模块只记录一条访问记录
            var date5 = DateTime.Now.AddMinutes(-5);
            var logExit = sysModelVisitLogService.Find(x => x.UserID == post.UserID && x.ModelCode == post.ModelCode && x.VisitSource == post.VisitSource && x.VisitDate > date5);
            if (logExit.IsNotNull()) return ServiceResponse<bool>.SuccessResponse(true);

            sysModelVisitLogService.Add(post);
            return ServiceResponse<bool>.SuccessResponse(true);
        }

        /// <summary>
        /// 系统功能模块访问记录
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost, Route("api/log/sysModelVisitLogPieDown")]
        public ServiceResponse<VisitLogReport> SysModelVisitLogPieDown(ReportBaseRequest req)
        {
            var data = sysModelVisitLogService.SearchVisitLogs(req);

            var totals = data.Sum(m => m.Total);

            VisitLogReport report = new VisitLogReport();

            List<string> filter = new List<string>
            {
                "zx",
                "zhuangyuanshuo",
                "pxb",
                "kszx",
                "zhibo",
                "zxkc",
                "lt",
            };

            foreach (var a in filter)
            {
                var total = data.FindAll(x => x.ModelCode == a).Sum(m => m.Total);
                PieDown pie = new PieDown
                {
                    name = data.Find(x => x.ModelCode == a)?.ModelName + "(" + total + ")",
                    drilldown = a,
                    y = totals == 0 ? 0 : (double)total / totals * 100,
                };
                if (pie.y != 0) report.pieDowns.Add(pie);

                var arr1Total = data.Find(x => x.ModelCode == a && x.VisitSource == "APP")?.Total ?? 0;
                object[] arr1 = new object[]
                {
                      "APP" + "(" + arr1Total + ")",
                       total == 0 ? 0 : (double)arr1Total / total * 100 ,
                };
                var arr2Total = data.Find(x => x.ModelCode == a && x.VisitSource == "PC")?.Total ?? 0;
                object[] arr2 = new object[]
                {
                      "PC" + "(" + arr2Total + ")",
                      total == 0 ? 0 : (double)arr2Total / total * 100  ,
                };

                Drilldown drilldown = new Drilldown
                {
                    id = a,
                    name = data.Find(x => x.ModelCode == a)?.ModelName,
                };
                if ((double)arr1[1] != 0) drilldown.data.Add(arr1);
                if ((double)arr2[1] != 0) drilldown.data.Add(arr2);
                if (pie.y != 0) report.drilldowns.Add(drilldown);
            }

            report.pieDowns = report.pieDowns.OrderByDescending(x => x.y).ToList();

            return ServiceResponse<VisitLogReport>.SuccessResponse(report);
        }

        /// <summary>
        /// 系统访问记录
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost, Route("api/log/sysVisitLog")]
        public ServiceResponse<object> SysVisitLog(ReportBaseRequest req)
        {
            var data = sysModelVisitLogService.SearchSysVisitLogs(req);

            data.ForEach(x => x.LongDate = DateTimeHelper.ConvertDateTimeToJS(x.StoreDate.AddHours(8)));

            var array = ListToObject.ConvertLogModel(data);

            return ServiceResponse<object>.SuccessResponse(array);
        }

    }
}