﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.DAL
{
    /// <summary>
    /// 接口基类
    /// <remarks>创建：创建：2019.6.17 </remarks>
    /// </summary>
    /// <typeparam name="T">类型</typeparam>
    public interface IBaseRepository<T>
    {
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="entity">数据实体</param>
        /// <returns>添加后的数据实体</returns>
        T Add(T entity);

        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="entitys">数据实体集合</param>
        /// <returns>添加后的数据实体</returns>
        int Adds(List<T> entitys);

        /// <summary>
        /// 查询记录数
        /// </summary>
        /// <param name="predicate">条件表达式</param>
        /// <returns>记录数</returns>
        int Count(Expression<Func<T, bool>> predicate);

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="entity">数据实体</param>
        /// <returns>是否成功</returns>
        bool Update(T entity);

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="entity">数据实体</param>
        /// <returns>是否成功</returns>
        int Updates(List<T> entitys);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="entity">数据实体</param>
        /// <returns>是否成功</returns>
        bool Delete(T entity);

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="entitys">数据实体列表</param>
        /// <returns>是否成功</returns>
        bool Deletes(List<T> entitys);

        /// <summary>
        /// 是否存在
        /// </summary>
        /// <param name="anyLambda">查询表达式</param>
        /// <returns>布尔值</returns>
        bool Exist(Expression<Func<T, bool>> anyLambda);

        /// <summary>
        /// 查询数据
        /// </summary>
        /// <param name="whereLambda">查询表达式</param>
        /// <returns>实体</returns>
        T Find(Expression<Func<T, bool>> whereLambda);

        /// <summary>
        /// 查找数据列表
        /// </summary>
        /// <param name="strSql">sql</param>
        /// <param name="parameters">参数</param>
        /// <returns>数据列表</returns>
        IList<T> FindList(string strSql, params object[] parameters);

        /// <summary>
        /// 查找数据列表
        /// </summary>
        /// <param name="whereLamdba">排序表达式</param>
        /// <param name="orderName">排序名称</param>
        /// <param name="isAsc">是否升序</param>
        /// <returns></returns>
        IQueryable<T> FindList(Expression<Func<T, bool>> whereLamdba, string orderName, bool isAsc);

        /// <summary>
        /// 查找数据列表
        /// </summary>
        /// <param name="whereLamdba">过滤条件表达式</param>
        /// <param name="path">需要加载的导航属性名称</param>
        /// <param name="orderName">排序名称</param>
        /// <param name="isAsc">是否升序</param>
        /// <returns></returns>
        IQueryable<T> FindList(Expression<Func<T, bool>> whereLamdba, string path, string orderName, bool isAsc);

        /// <summary>
        /// 查找分页数据列表
        /// </summary>
        /// <typeparam name="S">排序</typeparam>
        /// <param name="pageIndex">当前页</param>
        /// <param name="pageSize">每页记录数</param>
        /// <param name="totalRecord">总记录数</param>
        /// <param name="whereLamdba">查询表达式</param>
        /// <param name="orderName">排序名称</param>
        /// <param name="isAsc">是否升序</param>
        /// <returns></returns>
        IQueryable<T> FindPageList(int pageIndex, int pageSize, out int totalRecord, Expression<Func<T, bool>> whereLamdba, string orderName, bool isAsc);

        #region no savechanges

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="entity">数据实体</param>
        /// <returns>添加后的数据实体</returns>
        T AddNoSaveChange(T entity);

        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="entitys">数据实体集合</param>
        /// <returns>添加后的数据实体</returns>
        int AddsNoSaveChange(List<T> entitys);

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="entity">数据实体</param>
        /// <returns>是否成功</returns>
        bool UpdateNoSaveChange(T entity);

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="entity">数据实体</param>
        /// <returns>是否成功</returns>
        int UpdatesNoSaveChange(List<T> entitys);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="entity">数据实体</param>
        /// <returns>是否成功</returns>
        bool DeleteNoSaveChange(T entity);

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="entitys">数据实体列表</param>
        /// <returns>是否成功</returns>
        bool DeletesNoSaveChange(List<T> entitys);

        /// <summary>
        /// 事务提交
        /// </summary>
        bool SaveChange();

        #endregion
    }

}
