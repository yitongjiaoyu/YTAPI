﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.YTJYAdmin.Study
{
    public class VideoLogRequest : BaseSearchRequest
    {

    }

    /// <summary>
    /// 我的视频学习记录视图模型
    /// </summary>
    public class VideoLogViewModel
    {
        /// <summary>
        /// 列表
        /// </summary>
        public List<VideoLogItem> Items { get; set; } = new List<VideoLogItem>();

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { get; set; }
    }

    public class VideoLogItem
    {
        /// <summary>
        /// 记录id
        /// </summary>
        [Display(Name = "")]
        public Guid Id { get; set; }

        /// <summary>
        /// 套餐id
        /// </summary>
        [Display(Name = "套餐id")]
        public Guid PackageId { get; set; }

        /// <summary>
        /// 视频封面地址
        /// </summary>
        public string CoverUrl { get; set; }

        /// <summary>
        /// 套餐名称
        /// </summary>
        public string PackageName { get; set; }

        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }

        /// <summary>
        /// 视频名称
        /// </summary>
        public string VideoName { get; set; }

        /// <summary>
        /// 课程ID
        /// </summary>
        [Display(Name = "课程ID")]
        public Guid CourseId { get; set; }

        /// <summary>
        /// 视频id
        /// </summary>
        [Display(Name = "视频id")]
        public Guid VideoId { get; set; }

        /// <summary>
        /// 学习时长，单位：秒
        /// </summary>
        [Display(Name = "学习时长，单位：秒")]
        public int TimeSpan { get; set; }

        /// <summary>
        /// 上次学习到的位置，上次学习到第几秒
        /// </summary>
        [Display(Name = "上次学习到的位置，上次学习到第几秒")]
        public int LastStudyTime { get; set; }

        /// <summary>
        /// 是否学习完成
        /// </summary>
        [Display(Name = "是否学习完成")]
        public bool IsFinish { get; set; }
    }

    public class VideoLogModel
    {
        /// <summary>
        /// 记录id
        /// </summary>
        [Display(Name = "")]
        public Guid Id { get; set; }

        /// <summary>
        /// 套餐id
        /// </summary>
        [Display(Name = "套餐id")]
        public Guid PackageId { get; set; }

        /// <summary>
        /// 视频封面地址
        /// </summary>
        public string CoverUrl { get; set; }

        /// <summary>
        /// 套餐名称
        /// </summary>
        public string PackageName { get; set; }

        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }

        /// <summary>
        /// 视频名称
        /// </summary>
        public string VideoName { get; set; }

        /// <summary>
        /// 课程ID
        /// </summary>
        [Display(Name = "课程ID")]
        public Guid CourseId { get; set; }

        /// <summary>
        /// 视频id
        /// </summary>
        [Display(Name = "视频id")]
        public Guid VideoId { get; set; }

        /// <summary>
        /// 学习时长，单位：秒
        /// </summary>
        [Display(Name = "学习时长，单位：秒")]
        public int TimeSpan { get; set; }

        /// <summary>
        /// 上次学习到的位置，上次学习到第几秒
        /// </summary>
        [Display(Name = "上次学习到的位置，上次学习到第几秒")]
        public int LastStudyTime { get; set; }

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { get; set; }

        /// <summary>
        /// 是否学习完成
        /// </summary>
        [Display(Name = "是否学习完成")]
        public bool IsFinish { get; set; }
    }
}
