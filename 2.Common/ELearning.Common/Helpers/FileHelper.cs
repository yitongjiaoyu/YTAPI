﻿using System;
using NLog;

namespace ELearning.Common.Helpers
{
    /// <summary>
    /// 记录日志帮助类
    /// </summary>
    public class WriteHelper
    {
        public static void WriteFile(object data)
        {
            var log = LogManager.GetCurrentClassLogger();
            log.Info($"----产生时间：{DateTime.Now:yyyy-MM-dd HH:mm:ss}---------------------------------------------------------------------");
            log.Info(data);
        }
    }

    public class FileHelper
    {
        /// <summary>
        /// 根据文件名（包含文件扩展名）获取要保存的文件夹名称
        /// </summary>
        /// <param name="fileName">文件名（包含文件扩展名）</param>
        public static string GetSaveFolder(string fileName)
        {
            var fs = fileName.Split('.');
            var ext = fs[fs.Length - 1];
            var str = string.Empty;
            var t = ext.ToLower();
            switch (t)
            {
                case "jpg":
                case "jpeg":
                case "png":
                case "gif":
                    str = "images";
                    break;
                case "mp4":
                case "mkv":
                case "rmvb":
                    str = "video";
                    break;
                case "apk":
                case "wgt":
                    str = "app";
                    break;
                case "ppt":
                case "pptx":
                case "doc":
                case "docx":
                case "xls":
                case "xlsx":
                case "pdf":
                    str = "file";
                    break;
                default:
                    str = "file";
                    break;
            }

            return str;
        }

    }
}
