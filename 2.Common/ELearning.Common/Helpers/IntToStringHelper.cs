﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Common.Helpers
{
    public class IntToStringHelper
    {
        /// <summary>
        /// 获取腾讯云转码状态字符串
        /// </summary>
        /// <param name="status">视频转码状态</param>
        /// <returns>转码状态：0.不需要转码，1.待转码，2.转码中，3.转码成功，4.转码失败，5.部分转码成功</returns>
        public static string GetConvertStatsString(int status)
        {
            var str = string.Empty;
            switch (status)
            {
                case 0:
                    str = "不需要转码";
                    break;
                case 1:
                    str = "待转码";
                    break;
                case 2:
                    str = "转码中";
                    break;
                case 3:
                    str = "转码成功";
                    break;
                case 4:
                    str = "转码失败";
                    break;
                case 5:
                    str = "部分转码成功";
                    break;
            }
            return str;
        }
    }
}
