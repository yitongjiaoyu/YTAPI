//-------------------------------------------------------------------
//文件名称：EMenu.cs
//模块名称：EMenu业务逻辑接口层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearning.DAL;
using ELearning.Models;

namespace ELearning.BLL
{
    /// <summary>
    /// EMenu业务逻辑接口层
    /// </summary>
	public partial class EMenuService : BaseService<EMenu>
	{
     public  EMenuService() : base(RepositoryFactory.EMenuRepository) { }
	}
}


