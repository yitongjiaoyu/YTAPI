﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Common
{
    public enum PaperGroupEnum
    {
        /// <summary>
        /// 等级考试
        /// </summary>
        [Description("等级考试")]
        Class = 1,
        /// <summary>
        /// 模拟考试
        /// </summary>
        [Description("模拟考试")]
        Simulate = 2,
        /// <summary>
        /// 课堂练习
        /// </summary>
        [Description("课堂练习")]
        Practice = 3,
        /// <summary>
        /// 问卷调查
        /// </summary>
        [Description("问卷调查")]
        Qna = 4,
    }
}

