﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel
{
    public class VisitLogReport
    {
        public List<PieDown> pieDowns = new List<PieDown>();
        public List<Drilldown> drilldowns = new List<Drilldown>();
    }
}
