﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Common.Oprator
{
    /// <summary>
    /// 销售助手
    /// </summary>
    public class AppAssistantResutl
    {
        [DataMember(Name = "success")]
        public string Success { get; set; }


        [DataMember(Name = "total")]
        public string Total { get; set; }

        /// <summary>
        /// 错误消息
        /// </summary>
        [DataMember(Name = "error")]
        public string Error { get; set; }

        [DataMember(Name = "data")]
        public UserData Data { get; set; }


        [DataMember(Name = "elapsedMilliseconds")]
        public string ElapsedMilliseconds { get; set; }


        [DataMember(Name = "currentSystemTime")]
        public DateTime CurrentSystemTime { get; set; }
    }
    /// <summary>
    /// 用户数据
    /// </summary>
    public class UserData
    {
        /// <summary>
        /// 经销商号
        /// </summary>
        [DataMember(Name = "jxsh")]
        public string Jxsh { get; set; }

        /// <summary>
        /// 经销商名称
        /// </summary>
        [DataMember(Name = "jxsmc")]
        public string Jxsmc { get; set; }

        /// <summary>
        /// 操作员代码
        /// </summary>
        [DataMember(Name = "czydm")]
        public string Czydm { get; set; }

        /// <summary>
        /// 操作员名称
        /// </summary>
        [DataMember(Name = "czymc")]
        public string Czymc { get; set; }

        /// <summary>
        /// 操作员职务
        /// </summary>
        [DataMember(Name = "czyzw")]
        public string Czyzw { get; set; }
    }
    /// <summary>
    /// 骏客APP
    /// </summary>
    public class AppAssistantResutlJK
    {

        public string status { get; set; }

        public string msg { get; set; }

        public UserDataJK data { get; set; }

    }
    /// <summary>
    /// 用户数据-骏客APP
    /// </summary>
    public class UserDataJK
    {
        /// <summary>
        /// 经销商号
        /// </summary>
        public string dealerCode { get; set; }

        /// <summary>
        /// 员工编号
        /// </summary>
        public string employeeNo { get; set; }


    }
}
