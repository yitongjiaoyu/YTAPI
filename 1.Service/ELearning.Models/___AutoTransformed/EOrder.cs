//-------------------------------------------------------------------
//文件名称：EOrder.cs
//模块名称：EOrderModel层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ELearning.Models
{	
	 /// <summary>
    /// EOrder表实体类
    /// </summary>
	[Table("EOrder")]
	public partial class EOrder
    {
		
		/// <summary>
		/// Id
		/// </summary>
				[Key]
	[Display(Name = "Id")]
		public Guid Id { get; set; }
		
		/// <summary>
		/// 订单编号
		/// </summary>
			[Display(Name = "订单编号")]
		public string OrderNumber { get; set; }
		
		/// <summary>
		/// 套餐Id
		/// </summary>
			[Display(Name = "套餐Id")]
		public Guid PackageId { get; set; }
		
		/// <summary>
		/// 名称
		/// </summary>
			[Display(Name = "名称")]
		public string Name { get; set; }
		
		/// <summary>
		/// 描述
		/// </summary>
			[Display(Name = "描述")]
		public string Description { get; set; }
		
		/// <summary>
		/// 封面图片地址
		/// </summary>
			[Display(Name = "封面图片地址")]
		public string CoverUrl { get; set; }
		
		/// <summary>
		/// 组织编码
		/// </summary>
			[Display(Name = "组织编码")]
		public string OrgCode { get; set; }
		
		/// <summary>
		/// 大类分类Code
		/// </summary>
			[Display(Name = "大类分类Code")]
		public string CategoryId { get; set; }
		
		/// <summary>
		/// 班级基础分类code,基础班，高级版
		/// </summary>
			[Display(Name = "班级基础分类code,基础班，高级版")]
		public string TypeCode { get; set; }
		
		/// <summary>
		/// 序号
		/// </summary>
			[Display(Name = "序号")]
		public double Sort { get; set; }
		
		/// <summary>
		/// 原价
		/// </summary>
			[Display(Name = "原价")]
		public double OldPrice { get; set; }
		
		/// <summary>
		/// 现在价格
		/// </summary>
			[Display(Name = "现在价格")]
		public double NowPrice { get; set; }
		
		/// <summary>
		/// 购买价格
		/// </summary>
			[Display(Name = "购买价格")]
		public double BuyPrice { get; set; }
		
		/// <summary>
		/// 购买人id
		/// </summary>
			[Display(Name = "购买人id")]
		public Guid BuyUserId { get; set; }
		
		/// <summary>
		/// 主讲人id
		/// </summary>
			[Display(Name = "主讲人id")]
		public Guid TeacherUserId { get; set; }
		
		/// <summary>
		/// 购买渠道：0.后台，1.安卓，2.IOS，3.web
		/// </summary>
			[Display(Name = "购买渠道：0.后台，1.安卓，2.IOS，3.web")]
		public int Source { get; set; }
		
		/// <summary>
		/// 支付方式：0.后台添加，1.支付宝，2.微信
		/// </summary>
			[Display(Name = "支付方式：0.后台添加，1.支付宝，2.微信")]
		public int PayWay { get; set; }
		
		/// <summary>
		/// 第三方商户订单编号
		/// </summary>
			[Display(Name = "第三方商户订单编号")]
		public string PayNumber { get; set; }
		
		/// <summary>
		/// 支付状态：0.未支付，1.支付中，2.支付成功，3.支付失败
		/// </summary>
			[Display(Name = "支付状态：0.未支付，1.支付中，2.支付成功，3.支付失败")]
		public int PayStatus { get; set; }
		
		/// <summary>
		/// 支付备注信息
		/// </summary>
			[Display(Name = "支付备注信息")]
		public string PayNote { get; set; }
		
		/// <summary>
		/// 创建时间
		/// </summary>
			[Display(Name = "创建时间")]
		public DateTime CreateTime { get; set; }
		
		/// <summary>
		/// 更新时间
		/// </summary>
			[Display(Name = "更新时间")]
		public DateTime UpdateTime { get; set; }
		
		/// <summary>
		/// 创建人Id
		/// </summary>
			[Display(Name = "创建人Id")]
		public Guid CreateUserId { get; set; }
		
		/// <summary>
		/// 编辑人id
		/// </summary>
			[Display(Name = "编辑人id")]
		public Guid UpdateUserId { get; set; }
		
		/// <summary>
		/// 是否删除
		/// </summary>
			[Display(Name = "是否删除")]
		public bool IsDelete { get; set; }
		   
    }
}

