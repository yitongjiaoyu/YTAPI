//-------------------------------------------------------------------
//文件名称：TPaperQuestionTestHistory.cs
//模块名称：TPaperQuestionTestHistoryModel层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ELearning.Models
{	
	 /// <summary>
    /// TPaperQuestionTestHistory表实体类
    /// </summary>
	[Table("TPaperQuestionTestHistory")]
	public partial class TPaperQuestionTestHistory
    {
		
		/// <summary>
		/// Id
		/// </summary>
				[Key]
	[Display(Name = "Id")]
		public Guid Id { get; set; }
		
		/// <summary>
		/// 考试试卷记录id
		/// </summary>
			[Display(Name = "考试试卷记录id")]
		public Guid PaperTestHistoryId { get; set; }
		
		/// <summary>
		/// 试卷Id
		/// </summary>
			[Display(Name = "试卷Id")]
		public Guid PaperId { get; set; }
		
		/// <summary>
		/// 题目Id
		/// </summary>
			[Display(Name = "题目Id")]
		public Guid QuestionId { get; set; }
		
		/// <summary>
		/// 名称
		/// </summary>
			[Display(Name = "名称")]
		public string MainName { get; set; }
		
		/// <summary>
		/// 子题目Id
		/// </summary>
			[Display(Name = "子题目Id")]
		public Guid QuestionItemId { get; set; }
		
		/// <summary>
		/// 名称
		/// </summary>
			[Display(Name = "名称")]
		public string Name { get; set; }
		
		/// <summary>
		/// 正确答案,例如：/id1/id2/
		/// </summary>
			[Display(Name = "正确答案,例如：/id1/id2/")]
		public string RightAnswer { get; set; }
		
		/// <summary>
		/// 主题干序号
		/// </summary>
			[Display(Name = "主题干序号")]
		public double MainSort { get; set; }
		
		/// <summary>
		/// 序号
		/// </summary>
			[Display(Name = "序号")]
		public double Sort { get; set; }
		
		/// <summary>
		/// 分数
		/// </summary>
			[Display(Name = "分数")]
		public double Grade { get; set; }
		
		/// <summary>
		/// 题目类型，关联类型EQuestionType表中code字段
		/// </summary>
			[Display(Name = "题目类型，关联类型EQuestionType表中code字段")]
		public string QuestionTypeCode { get; set; }
		
		/// <summary>
		/// 组织编码
		/// </summary>
			[Display(Name = "组织编码")]
		public string OrgCode { get; set; }
		
		/// <summary>
		/// 用户输入的答案，主观题使用的字段
		/// </summary>
			[Display(Name = "用户输入的答案，主观题使用的字段")]
		public string InputAnswer { get; set; }
		
		/// <summary>
		/// 题目答案，主观题，简答题等类型使用该字段
		/// </summary>
			[Display(Name = "题目答案，主观题，简答题等类型使用该字段")]
		public string Answer { get; set; }
		
		/// <summary>
		/// 题目解析
		/// </summary>
			[Display(Name = "题目解析")]
		public string Analysis { get; set; }
		
		/// <summary>
		/// 题目难度，1.简单，2.一般，3.困难
		/// </summary>
			[Display(Name = "题目难度，1.简单，2.一般，3.困难")]
		public int Difficulty { get; set; }
		
		/// <summary>
		/// 用户选择选项id字符串，例如：/id1/id2/
		/// </summary>
			[Display(Name = "用户选择选项id字符串，例如：/id1/id2/")]
		public string ChoosedOptionId { get; set; }
		
		/// <summary>
		/// 题目状态：0.未做，1.正确，2.错误
		/// </summary>
			[Display(Name = "题目状态：0.未做，1.正确，2.错误")]
		public int Status { get; set; }
		
		/// <summary>
		/// 用户得分
		/// </summary>
			[Display(Name = "用户得分")]
		public double UserGrade { get; set; }
		
		/// <summary>
		/// 创建时间
		/// </summary>
			[Display(Name = "创建时间")]
		public DateTime CreateTime { get; set; }
		
		/// <summary>
		/// 更新时间
		/// </summary>
			[Display(Name = "更新时间")]
		public DateTime UpdateTime { get; set; }
		
		/// <summary>
		/// 创建人Id
		/// </summary>
			[Display(Name = "创建人Id")]
		public Guid CreateUserId { get; set; }
		
		/// <summary>
		/// 编辑人id
		/// </summary>
			[Display(Name = "编辑人id")]
		public Guid UpdateUserId { get; set; }
		
		/// <summary>
		/// 是否删除
		/// </summary>
			[Display(Name = "是否删除")]
		public bool IsDelete { get; set; }
		   
    }
}

