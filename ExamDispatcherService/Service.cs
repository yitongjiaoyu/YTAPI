﻿using ELearning.BLL;
using ELearning.Common;
using ELearning.Common.Extensions;
using ELearning.Common.Helpers;
using ELearning.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamDispatcherService
{
    public class Service
    {
        EExamService examService => new EExamService();
        EStuScoreService stuScoreService => new EStuScoreService();
        EStuExamService stuExamService => new EStuExamService();
        EExamTopicService examTopicService => new EExamTopicService();
        ERoleService roleService => new ERoleService();
        RRoleUserService roleUserService => new RRoleUserService();

        public void Run()
        {
            try
            {
                ExamDispatcherForNewStu();
            }
            catch (Exception ex)
            {

            }
        }

        public void ExamDispatcherForNewStu()
        {
            var ndate = DateTime.Now;
            List<EExam> exams = new List<EExam>();
            var examsRRole = examService.FindList(x => !x.IsDelete && x.EType == 2 && x.EState == 22 && x.PublishState == 1 && x.ExamBegin < ndate && x.ExamEnd > ndate).ToList();

            var examsRPaper = examService.FindList(x => !x.IsDelete && x.EType == 5 && x.EState == 22 && x.PublishState == 1).ToList();

            exams.AddRange(examsRRole);
            exams.AddRange(examsRPaper);

            // 组织分发数据-为每个学员创建考试/问卷信息
            List<EStuScore> nStuScores = new List<EStuScore>();

            foreach (var e in exams)
            {
                var examRRoles = examTopicService.FindList(x => !x.IsDelete && x.TopicCode == "ERole" && x.Eid == e.ID).ToList();

                // 获取分发的学员；注意：过滤重复数据
                List<Guid> roles = new List<Guid>();
                foreach (var r in examRRoles)
                {
                    var roleChilds = roleService.GetRoleSubList(r.TopicId).Select(m => m.RoleID).ToList();
                    roles.AddRange(roleChilds);
                }
                roles.Distinct();

                // 拥有考试权限的学员
                var stuIDs = roleUserService.FindList(x => roles.Contains(x.RoleID)).Select(m => m.UserID).Distinct().ToList();
                // 已参加考试的学员
                var stuIDsScored = stuScoreService.FindList(x => !x.IsDelete && x.Eid == e.ID).Select(m => m.Sid).ToList();
                // 还未参加考试的学员
                var stuNScore = stuIDs.Where(x => !stuIDsScored.Contains(x)).Distinct().ToList();

                // 注意：问卷为后台组卷，所以需为学员配置具体的试卷
                // 注意：考试为前台组卷，所以学员的试卷应该为空试卷，需配置默认空试卷
                // 注意：数据初始化的时候，应在EPaper表默认创建一条ID为 Guid.Empty 的默认空试卷
                var paperID = Guid.Empty;
                if (e.EType == 5)
                {
                    var examRPaper = examTopicService.Find(x => !x.IsDelete && x.Eid == e.ID && x.TopicCode == "EPaper");
                    paperID = examRPaper.TopicId;
                }
                // 为每个学员初始化考试数据
                foreach (var s in stuNScore)
                {
                    EStuScore nStuScore = new EStuScore
                    {
                        Creater = "系统分发任务",
                        Modifyer = "系统分发任务",
                        StarTime = DateTime.Now,
                        EndTime = DateTime.Now,
                        CreateTime = DateTime.Now,
                        ModifyTime = DateTime.Now,
                        IsDelete = false,

                        Eid = e.ID,
                        ID = Guid.NewGuid(),
                        Pid = paperID,
                        Sid = s,
                        StudyState = 1,
                        TotalScore = 0,
                    };
                    nStuScores.Add(nStuScore);
                }
            }

            // 大批量数据插入方法
            var dt = nStuScores.ToDataTable(null);
            dt.TableName = "EStuScore";
            SqlBulkCopyHelper.SaveTable(dt);
        }

    }

}
