﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ELearning.Common.Oprator
{
    /// <summary>
    /// 用户信息实体类
    /// </summary>
    public class OperatorModel
    {
        /// <summary>
        /// 用户id
        /// </summary>
        public Guid UserId { set; get; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        public string UserName { set; get; }

        /// <summary>
        /// 登录名
        /// </summary>          
        public string LoginName { get; set; }

        /// <summary>
        /// 所属机构编码
        /// </summary>          
        public string OrgCode { get; set; }

        /// <summary>
        /// 所属机构名称
        /// </summary>          
        public string OrgName { get; set; }

        /// <summary>
        /// 头像
        /// </summary>          
        public string HeadPhoto { get; set; }

        /// <summary>
        /// 用户类别
        /// </summary>          
        public string UserType { get; set; }

        /// <summary>
        /// 用户角色列表
        /// </summary>          
        public List<string> UserRoles { get; set; }

        /// <summary>
        /// 职务
        /// </summary>          
        public string Position { get; set; }

        /// <summary>
        /// 移动电话
        /// </summary>          
        public string MobilePhone { get; set; }

        /// <summary>
        /// 电子邮件
        /// </summary>          
        public string Email { get; set; }

        /// <summary>
        /// 个人简介
        /// </summary>          
        public string Remark { get; set; }

        /// <summary>
        /// 用户ip地址
        /// </summary>
        public string UserIp { set; get; }

        /// <summary>
        /// 经销商名称
        /// </summary>
        public string Jxsmc { set; get; }

        /// <summary>
        /// 经销商号
        /// </summary>
        public string Jxsh { set; get; }

        /// <summary>
        /// 操作员代码
        /// </summary>
        public string Czydm { set; get; }

        /// <summary>
        /// 身份证号码
        /// </summary>
        public string CardNo { get; set; }

        /// <summary>
        /// 权限列表
        /// </summary>
        public List<Permission> Permissions { get; set; } = new List<Permission>();
        //public string Czymc { set; get; }

        /// <summary>
        /// 操作员职务
        /// </summary>
        public string Czyzw { set; get; }

        /// <summary>
        /// Gets or sets the credit.
        /// 用户积分
        /// </summary>
        public decimal Credit { get; set; }

        /// <summary>
        /// Gets or sets the forum title.
        /// 用户头衔名称
        /// </summary>
        public string ForumTitle { get; set; }

        /// <summary>
        /// 勋章名字
        /// </summary>
        public string BadgeName { get; set; } = string.Empty;

    }

    public class LoginResponse
    {
        [DataMember(Name = "token")]
        public string Token { get; set; }

        [DataMember(Name = "user")]
        public OperatorModel User { get; set; }

        [DataMember(Name = "权限列表")]
        public List<string> PermissionCodes { get; set; }

        public string Result { get; set; }
    }

    public class Permission
    {
        public string Key { get; set; }
        public List<string> Value { get; set; }
    }
}
