﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel
{
    public class TreeViewModel
    {
        public Guid id { get; set; }
        public Guid ParentID { get; set; }
        public string value { get; set; }
        public string label { get; set; }
        public List<TreeViewModel> children { get; set; }
    }



}
