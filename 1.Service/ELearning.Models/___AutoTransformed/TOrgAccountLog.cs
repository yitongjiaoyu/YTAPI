//-------------------------------------------------------------------
//文件名称：TOrgAccountLog.cs
//模块名称：TOrgAccountLogModel层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ELearning.Models
{	
	 /// <summary>
    /// TOrgAccountLog表实体类
    /// </summary>
	[Table("TOrgAccountLog")]
	public partial class TOrgAccountLog
    {
		
		/// <summary>
		/// Id
		/// </summary>
			[Display(Name = "Id")]
		public Guid Id { get; set; }
		
		/// <summary>
		/// 组织编码
		/// </summary>
			[Display(Name = "组织编码")]
		public string OrgCode { get; set; }
		
		/// <summary>
		/// 原来的学生账号上限
		/// </summary>
			[Display(Name = "原来的学生账号上限")]
		public int OldAccountLimit { get; set; }
		
		/// <summary>
		/// 现在的学生账号上限
		/// </summary>
			[Display(Name = "现在的学生账号上限")]
		public int NowAccountLimit { get; set; }
		
		/// <summary>
		/// 创建时间
		/// </summary>
			[Display(Name = "创建时间")]
		public DateTime CreateTime { get; set; }
		
		/// <summary>
		/// 更新时间
		/// </summary>
			[Display(Name = "更新时间")]
		public DateTime UpdateTime { get; set; }
		
		/// <summary>
		/// 创建人Id
		/// </summary>
			[Display(Name = "创建人Id")]
		public Guid CreateUserId { get; set; }
		
		/// <summary>
		/// 编辑人id
		/// </summary>
			[Display(Name = "编辑人id")]
		public Guid UpdateUserId { get; set; }
		
		/// <summary>
		/// 是否删除
		/// </summary>
			[Display(Name = "是否删除")]
		public bool IsDelete { get; set; }
		   
    }
}

