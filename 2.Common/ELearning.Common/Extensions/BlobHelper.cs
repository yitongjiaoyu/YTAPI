﻿
namespace ELearning.Common.Extensions
{
    public class BlobHelper
    {
        /// <summary>
        /// 根据文件名（包含文件扩展名）获取azure容器的名字
        /// </summary>
        /// <param name="fileName">文件名（包含文件扩展名）</param>
        public static string GetBlobName(string fileName, bool bFlag = false)
        {
            if (bFlag)
            {
                return "tmp";
            }
            var fs = fileName.Split('.');
            var ext = fs[fs.Length - 1];
            var str = string.Empty;
            var t = ext.ToLower();
            switch (t)
            {
                case "jpg":
                case "jpeg":
                case "png":
                case "gif":
                    str = "picture";
                    break;
                case "mp4":
                    str = "video";
                    break;
                case "ppt":
                case "pptx":
                case "doc":
                case "docx":
                case "xls":
                case "xlsx":
                    str = "document";
                    break;
                default:
                    str = "document";
                    break;
            }

            return str;
        }
    }
}
