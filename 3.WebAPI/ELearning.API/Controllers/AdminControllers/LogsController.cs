﻿using ELearning.BLL;
using ELearning.Common.Extensions;
using ELearning.Common.Model;
using ELearning.Models.ViewModel.Logs;
using System.Web.Http;

namespace ELearning.API.Controllers.AdminControllers
{
    public class LogsController : BaseApiController
    {
        protected EApplicationLogService LogsService = new EApplicationLogService();
        /// <summary>
        /// 获取注册用户列表
        /// </summary>
        [HttpPost, Route("api/Logs/GetLogsList")]
        public ServiceResponse<LogsViewModel> GetRegesterList(GetLogsRequest request)
        {
            var viewModel = new LogsViewModel();
            var logPage = LogsService.GetLogsList(request);
            if (logPage != null)
            {
                viewModel.TotalCount = logPage.TotalItems.ObjToInt();
                foreach (var log in logPage.Items)
                {
                    viewModel.LogsItems.Add(log);
                }
            }
            return ServiceResponse<LogsViewModel>.SuccessResponse("success", viewModel);
        }
    }
}