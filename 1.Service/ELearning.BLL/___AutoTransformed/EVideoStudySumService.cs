//-------------------------------------------------------------------
//文件名称：EVideoStudySum.cs
//模块名称：EVideoStudySum业务逻辑接口层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearning.DAL;
using ELearning.Models;

namespace ELearning.BLL
{
    /// <summary>
    /// EVideoStudySum业务逻辑接口层
    /// </summary>
	public partial class EVideoStudySumService : BaseService<EVideoStudySum>
	{
     public  EVideoStudySumService() : base(RepositoryFactory.EVideoStudySumRepository) { }
	}
}


