﻿using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Common
{
    public class UrlDataSource : IStaticDataSource
    {
        public string Url { get; set; }

        public UrlDataSource(string url)
        {
            this.Url = url;
        }

        public Stream GetSource()
        {
            return GetStreamUrl();
        }
        public Stream GetStreamUrl()
        {
            try
            {
                WebRequest req = WebRequest.Create(Url);
                WebResponse result = req.GetResponse();

                MemoryStream ms = new MemoryStream();
                result.GetResponseStream().CopyTo(ms);

                ms.Position = 0;
                return ms;
            }
            catch
            {
            }
            return null;
        }
    }
}
