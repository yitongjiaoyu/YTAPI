﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Common.Eunms
{
    /// <summary>
    /// 轮播图类型
    /// </summary>
    public enum BannerTypeEnum
    {
        /// <summary>
        /// APP首页
        /// </summary>
        [Description("APP首页")]
        AppIndex = 1
    }
}
