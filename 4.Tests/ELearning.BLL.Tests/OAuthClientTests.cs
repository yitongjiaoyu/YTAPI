﻿using ELearning.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ELearning.BLL.Tests
{
    [TestClass]
    public class OAuthClientTests
    {
        [TestMethod]
        public void EOAuth2Client_GenerateClientIdAndSecretTest_Test()
        {
            var clientEntity = new EOAuth2Client
            {
                AppId = Guid.NewGuid(),
                CreateTime = DateTime.Now,
                ModifyTime = DateTime.Now,
                AppName = "测试客户端" + Guid.NewGuid().ToString("N"),
                IsDeleted = false,
                RedirectUri = string.Empty,
                Description = "测试客户端描述" + Guid.NewGuid().ToString("N")
            };
            clientEntity.AppSecret = GenerateClientSecret(clientEntity.AppId);
            //添加Client
            var service = new EOAuth2ClientService();
            service.Add(clientEntity);
            //获取Client
            var dbClient = service.Find(x => !x.IsDeleted && x.AppId == clientEntity.AppId);
            Assert.IsNotNull(dbClient);
            //验证AppId和AppSecret
            var verifyResult = Hasher.VerifyHashed(dbClient.AppSecret, clientEntity.AppId.ToString("N"));
            Assert.IsTrue(verifyResult);
        }


        private string GenerateClientSecret(Guid clientId)
        {
            var secret = Hasher.Hash(clientId.ToString("N"));
            if (secret.Contains("+"))
            {
                return GenerateClientSecret(clientId);
            }
            return secret;
        }
    }
}