﻿using System;
using System.Web;
using System.Web.Http;
using ELearning.BLL;
using ELearning.Common;
using ELearning.Common.CommonStr;
using ELearning.Common.Extensions;
using ELearning.Common.Helpers;
using ELearning.Common.Http;
using ELearning.Common.Model;
using ELearning.Common.Oprator;
using ELearning.Models.ViewModel.Account;
using ELearning.Models.ViewModel.YTJYAdmin.Account;
using SiteAPI.Controllers.Common;

namespace SiteAPI.Controllers.Account
{
    /// <summary>
    /// 账号相关
    /// </summary>
    public class UserController : BaseApiController
    {
        /// <summary>
        /// 用户操作service
        /// </summary>
        private ESysUserService userService = new ESysUserService();

        /// <summary>
        /// 登录token记录操作service
        /// </summary>
        private TLoginTokenService tlLoginTokenService => new TLoginTokenService();

        /// <summary>
        /// 获取当前用户信息
        /// </summary>
        [HttpGet, Route("api/user/GetCurrenUser")]
        public ServiceResponse<AdminLoginViewModel> GetCurrenUser()
        {
            var user = userService.Find(u => !u.IsDelete && u.UserID == CurrentUserModel.UserId);
            if (user.IsNotNull())
            {
                var viewModel = new AdminLoginViewModel
                {
                    HeadPhoto = user.HeadPhoto,
                    Position = user.Position,
                    UserName = user.UserName,
                    Email = user.Email,
                    MobilePhone = user.MobilePhone,
                    Remark = user.Remark,
                    Token = string.Empty,
                    CardNo = user.CardNo
                };

                return ServiceResponse<AdminLoginViewModel>.SuccessResponse(viewModel);
            }
            return ServiceResponse<AdminLoginViewModel>.SuccessResponse(CommonConst.USER_NO_LOGIN, null);
        }

        /// <summary>
        /// 修改用户密码
        /// </summary>
        [HttpPost, Route("api/user/UpdatePassword")]
        public ServiceResponse<bool> UpdatePassword(UpdatePasswordViewModel viewModel)
        {
            var userId = CurrentUserModel?.UserId ?? Guid.Empty;
            var user = userService.Find(u => !u.IsDelete && u.UserID == userId);
            if (user.IsNotNull())
            {
                var oldPsw = viewModel.OldPassword.Trim();
                if (user.Password == oldPsw)
                {
                    var newPsw = viewModel.NewPassword.Trim();
                    user.Password = newPsw;
                    user.ModifyTime = DateTime.Now;
                    userService.Update(user);
                    return ServiceResponse<bool>.SuccessResponse(CommonConst.OprateSuccessStr, true);
                }

                return ServiceResponse<bool>.SuccessResponse(CommonConst.OLD_PASSWORD_ERROR, false);
            }

            return ServiceResponse<bool>.SuccessResponse(CommonConst.USER_NO_LOGIN, false);
        }

        /// <summary>
        /// 修改手机号
        /// </summary>
        [HttpPost, Route("api/user/UpdatePhone")]
        public ServiceResponse<bool> UpdatePhone(UpdatePhoneViewModel viewModel)
        {
            var user = userService.Find(u => !u.IsDelete && u.UserID == CurrentUserModel.UserId);
            if (user.IsNotNull())
            {
                user.MobilePhone = viewModel.MobilePhone;

                user.ModifyTime = DateTime.Now;
                userService.Update(user);

                return ServiceResponse<bool>.SuccessResponse(CommonConst.OprateSuccessStr, true);
            }

            return ServiceResponse<bool>.SuccessResponse(CommonConst.USER_NO_LOGIN, false);
        }

        /// <summary>
        /// 修改身份证号码
        /// </summary>
        [HttpPost, Route("api/user/UpdateCardNo")]
        public ServiceResponse<bool> UpdateCardNo(UpdateCardNoViewModel viewModel)
        {
            var user = userService.Find(u => !u.IsDelete && u.UserID == CurrentUserModel.UserId);
            if (user.IsNotNull())
            {
                user.CardNo = viewModel.CardNo;

                user.ModifyTime = DateTime.Now;
                userService.Update(user);
                
                return ServiceResponse<bool>.SuccessResponse(CommonConst.OprateSuccessStr, true);
            }

            return ServiceResponse<bool>.SuccessResponse(CommonConst.USER_NO_LOGIN, false);
        }

        /// <summary>
        /// 修改用户头像
        /// </summary>
        [HttpPost, Route("api/user/UpdateUserPhoto")]
        public ServiceResponse<bool> UpdateUserPhoto(UpdateUserPhotoViewModel viewModel)
        {
            var user = userService.Find(u => !u.IsDelete && u.UserID == CurrentUserModel.UserId);
            if (user.IsNotNull())
            {
                user.HeadPhoto = viewModel.HeadPhoto;

                user.ModifyTime = DateTime.Now;
                userService.Update(user);
                
                return ServiceResponse<bool>.SuccessResponse(CommonConst.OprateSuccessStr, true);
            }

            return ServiceResponse<bool>.SuccessResponse(CommonConst.USER_NO_LOGIN, false);
        }

    }
}