﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.YTJYAdmin.Banner
{
    public class SiteBannerViewModel
    {
        /// <summary>
        /// 列表
        /// </summary>
        public List<SiteBannerItem> Items { set; get; } = new List<SiteBannerItem>();

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { set; get; }
    }

    /// <summary>
    /// 轮播图元素
    /// </summary>
    public class SiteBannerItem
    {
        public Guid Id { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        [Display(Name = "标题")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 图片地址
        /// </summary>
        [Display(Name = "图片地址")]
        public string ImageUrl { get; set; }

        /// <summary>
        /// 跳转地址
        /// </summary>
        [Display(Name = "跳转地址")]
        public string HrefUrl { get; set; }
    }
}
