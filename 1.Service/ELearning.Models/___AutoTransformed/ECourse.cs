//-------------------------------------------------------------------
//文件名称：ECourse.cs
//模块名称：ECourseModel层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ELearning.Models
{	
	 /// <summary>
    /// ECourse表实体类
    /// </summary>
	[Table("ECourse")]
	public partial class ECourse
    {
		
		/// <summary>
		/// 主键
		/// </summary>
				[Key]
	[Display(Name = "主键")]
		public Guid Id { get; set; }
		
		/// <summary>
		/// 名称
		/// </summary>
			[Display(Name = "名称")]
		public string Name { get; set; }
		
		/// <summary>
		/// 描述
		/// </summary>
			[Display(Name = "描述")]
		public string Description { get; set; }
		
		/// <summary>
		/// 封面图片地址
		/// </summary>
			[Display(Name = "封面图片地址")]
		public string CoverUrl { get; set; }
		
		/// <summary>
		/// 大类分类Code
		/// </summary>
			[Display(Name = "大类分类Code")]
		public string CategoryId { get; set; }
		
		/// <summary>
		/// 班级基础分类code,基础班，高级版
		/// </summary>
			[Display(Name = "班级基础分类code,基础班，高级版")]
		public string TypeCode { get; set; }
		
		/// <summary>
		/// 组织编码
		/// </summary>
			[Display(Name = "组织编码")]
		public string OrgCode { get; set; }
		
		/// <summary>
		/// 序号
		/// </summary>
			[Display(Name = "序号")]
		public double Sort { get; set; }
		
		/// <summary>
		/// 原价
		/// </summary>
			[Display(Name = "原价")]
		public double OldPrice { get; set; }
		
		/// <summary>
		/// 现在价格
		/// </summary>
			[Display(Name = "现在价格")]
		public double NowPrice { get; set; }
		
		/// <summary>
		/// 讲师id
		/// </summary>
			[Display(Name = "讲师id")]
		public Guid TeacherId { get; set; }
		
		/// <summary>
		/// 是否为公开课
		/// </summary>
			[Display(Name = "是否为公开课")]
		public bool IsPublic { get; set; }
		
		/// <summary>
		/// 创建时间
		/// </summary>
			[Display(Name = "创建时间")]
		public DateTime CreateTime { get; set; }
		
		/// <summary>
		/// 更新时间
		/// </summary>
			[Display(Name = "更新时间")]
		public DateTime UpdateTime { get; set; }
		
		/// <summary>
		/// 创建人Id
		/// </summary>
			[Display(Name = "创建人Id")]
		public Guid CreateUserId { get; set; }
		
		/// <summary>
		/// 编辑人id
		/// </summary>
			[Display(Name = "编辑人id")]
		public Guid UpdateUserId { get; set; }
		
		/// <summary>
		/// 是否删除
		/// </summary>
			[Display(Name = "是否删除")]
		public bool IsDelete { get; set; }
		
		/// <summary>
		/// 是否启用
		/// </summary>
			[Display(Name = "是否启用")]
		public bool IsUsed { get; set; }
		   
    }
}

