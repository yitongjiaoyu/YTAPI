﻿using ELearning.BLL;
using ELearning.Common.CommonStr;
using ELearning.Common.Extensions;
using ELearning.Common.Model;
using ELearning.Models;
using ELearning.Models.ViewModel.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace ELearning.API.Controllers.ElearningController
{
    public class ENewsController : BaseApiController
    {
        private ENewsService eNewsService = new ENewsService();
        private RNewsTypeService rNewsTypeService = new RNewsTypeService();
        private RNewsImageService rNewsImageService = new RNewsImageService();
        /// <summary>
        /// 前台获取banner 滚动图片信息
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/ENews/GetBannerList")]
        public ServiceResponse<NewsViewModel> GetBannerList()
        {
            NewsRequest request = new NewsRequest { Code = "banner", Page = 1, PageSize = 10, OrderBy = "Sort", IsAsc = true, SortDir = "Sort", States = 1 };
            var viewModel = new NewsViewModel();
            var news = eNewsService.GetNewsPageList(request);
            if (news != null)
            {
                viewModel.TotalCount = news.TotalItems.ObjToInt();

                foreach (var stunode in news.Items)
                {
                    var v = stunode.MapTo<NewsItem, ENews>();
                    viewModel.NewsItems.Add(v);
                }

                //foreach (var item in news.Items)
                //{
                //    var n = new NewsItem
                //    {
                //        Id = item.Id,
                //        Code = item.Code,
                //        ReadCount = item.ReadCount,
                //        Title = item.Title,
                //        Text = item.Text,
                //        Signature = item.Signature,
                //        Sort = item.Sort,
                //        SourceUrl = item.SourceURL,
                //        States = item.States,
                //        SubTitle = item.SubTitle,
                //        URLType = item.URLType,
                //        HttpURL = item.HttpURL
                //    };

                //    viewModel.NewsItems.Add(n);
                //}
            }

            return ServiceResponse<NewsViewModel>.SuccessResponse(CommonConst.OprateSuccessStr, viewModel);
        }


        /// <summary>
        /// 获取手机启动图片
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet, Route("api/ENews/GetAppPic")]
        public ServiceResponse<NewsViewModel> GetAppPic()
        {
            NewsRequest request = new NewsRequest { Code = "apppic", Page = 1, PageSize = 1, OrderBy = "ReleaseTime", IsAsc = true, SortDir = "ReleaseTime", States = 1 };
            var viewModel = new NewsViewModel();
            var news = eNewsService.GetNewsPageList(request);
            if (news != null)
            {
                viewModel.TotalCount = news.TotalItems.ObjToInt();

                foreach (var stunode in news.Items)
                {
                    var v = stunode.MapTo<NewsItem, ENews>();
                    viewModel.NewsItems.Add(v);
                }
            }

            return ServiceResponse<NewsViewModel>.SuccessResponse(CommonConst.OprateSuccessStr, viewModel);
        }


        /// <summary>
        /// 获取新闻列表
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost, Route("api/ENews/GetNewsList")]
        public ServiceResponse<NewsViewModel> GetNewsList(NewsRequest request)
        {
            var viewModel = new NewsViewModel();
            var news = eNewsService.GetNewsPageList(request);
            if (news != null)
            {
                var newsIds = news.Items.Select(n => n.Id).ToList();
                var rnewimages = rNewsImageService.FindList(r => newsIds.Contains(r.NewsID)).ToList();
                viewModel.TotalCount = news.TotalItems.ObjToInt();
                foreach (var stunode in news.Items)
                {
                    var v = stunode.MapTo<NewsItem, ENews>();
                    var sourseurl = rnewimages.Where(w => w.NewsID == v.Id).OrderBy(w => w.Sort).Select(w => w.SourceURL).ToList();
                    v.SourceURLList.AddRange(sourseurl);
                    viewModel.NewsItems.Add(v);
                }
            }
            return ServiceResponse<NewsViewModel>.SuccessResponse(CommonConst.OprateSuccessStr, viewModel);
        }

        /// <summary>
        /// 根据id 获取新闻信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Route("api/ENews/GetNewsByID")]
        public ServiceResponse<ENews> GetNewsByID(Guid id)
        {
            var news = eNewsService.Find(w => w.Id == id);
            return ServiceResponse<ENews>.SuccessResponse(CommonConst.OprateSuccessStr, news);
        }

        /// <summary>
        /// 根据课程id 获取相关课程列表
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Route("api/ENews/GetRelationNewsByID")]
        public ServiceResponse<List<ENews>> GetRelationNewsByID(Guid id)
        {
            List<ENews> newslist = new List<ENews>();
            var news = eNewsService.Find(w => w.Id == id);
            if (news != null)
            {
                var typelist = rNewsTypeService.FindList(w=>w.NewsId==news.Id).Select(w=>w.NewsTypeId).ToList();
                var newsidlist= rNewsTypeService.FindList(w => typelist.Contains(w.NewsTypeId)).Select(w => w.NewsId).ToList();
                 newslist = eNewsService.FindList(w=> newsidlist.Contains(w.Id)&&w.States==1&&w.IsDelete==false&&w.Id!=news.Id).OrderByDescending(w=>w.ReleaseTime).Take(5).ToList();
            }
            return ServiceResponse<List<ENews>>.SuccessResponse(CommonConst.OprateSuccessStr, newslist);
        }
        /// <summary>
        /// 更新新闻的阅读量
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Route("api/ENews/UpdateNewsReadCount")]
        public ServiceResponse<bool> UpdateNewsReadCount(Guid id)
        {
            bool b = false;
            var news = eNewsService.Find(w=>w.Id==id);
            if (news != null)
            {
                news.ReadCount += 1;
                b=eNewsService.Update(news);
            }
            return ServiceResponse<bool>.SuccessResponse(CommonConst.OprateSuccessStr, b);
        }
    }
}