﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using ELearning.BLL;
using ELearning.Common.CommonStr;
using ELearning.Common.Extensions;
using ELearning.Common.Helpers;
using ELearning.Common.Model;
using ELearning.Models;
using ELearning.Models.ViewModel.YTJYAdmin.Package;
using SiteAPI.Controllers.Common;

namespace SiteAPI.Controllers.Business
{
    /// <summary>
    /// 套餐相关控制器
    /// </summary>
    public class PackageController : BaseApiController
    {
        /// <summary>
        /// 套餐相关操作service
        /// </summary>
        private EPackageService ePackageService => new EPackageService();

        /// <summary>
        /// 获取客户端套餐列表
        /// </summary>
        [HttpPost, Route("api/ytjysite/EPackage/GetModelList")]
        [AllowAnonymous]
        public ServiceResponse<SitePackageViewModel> GetModelList(SitePackageRequest request)
        {
            var viewModel = ePackageService.GetSiteModelList(request, CurrentUserModel.UserId);

            return ServiceResponse<SitePackageViewModel>.SuccessResponse(CommonConst.OprateSuccessStr, viewModel);
        }

        /// <summary>
        /// 获取用户的套餐列表
        /// </summary>
        [HttpPost, Route("api/ytjysite/EPackage/GetUserModelList")]
        public ServiceResponse<UserPackageViewModel> GetUserModelList()
        {
            var userId = CurrentUserModel?.UserId ?? Guid.Empty;
            var viewModel = ePackageService.GetUserModelList(userId);

            return ServiceResponse<UserPackageViewModel>.SuccessResponse(CommonConst.OprateSuccessStr, viewModel);
        }
    }
}