﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Common.CommonStr
{
    public class DictionaryConst
    {
        /// <summary>
        /// 分类编码
        /// </summary>
        public const string CATEGORY_CODE = "YTJY";

        /// <summary>
        /// 班级基础分类编码
        /// </summary>
        public const string CLASS_TYPE_CODE = "banji";
    }
}
