﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Http;
using ELearning.BLL;
using ELearning.Common;
using ELearning.Common.Extensions;
using ELearning.Common.Model;
using ELearning.Models;
using ELearning.Models.ViewModel;
using ELearning.Models.ViewModel.Level;
using ELearning.Models.ViewModel.Product;

namespace ELearning.API.Controllers.AdminControllers
{
    /// <summary>
    /// 产品相关操作
    /// </summary>
    public class ProductController : BaseApiController
    {
        protected EDictionaryService EDictionaryService = new EDictionaryService();
        /// <summary>
        /// 产品操作service
        /// </summary>
        private EProductService productService = new EProductService();
        private ECourseSeriesService eCourseSeriesService = new ECourseSeriesService();

        private EOrderService orderService = new EOrderService();

        /// <summary>
        /// 获取产品列表
        /// </summary>
        [HttpPost, Route("api/Product/GetProductList")]
        public ServiceResponse<ProductViewModel> GetProductList(ProductListRequest request)
        {
            Expression<Func<EProduct, bool>> whereLamdba = u => !u.IsDelete;
            if (!string.IsNullOrEmpty(request.Title))
                whereLamdba = whereLamdba.And(c => c.Title.Contains(request.Title));
            if (!string.IsNullOrEmpty(request.TeachTypeCode))
                whereLamdba = whereLamdba.And(c => c.TeachTypeCode==request.TeachTypeCode);
            if (request.SubjectID!=Guid.Empty)
            {
                var pdictionary = EDictionaryService.Find(w => w.ID == request.SubjectID);
                List<Guid> listid = EDictionaryService.GetLevelListById(pdictionary.ID).ToList().Select(w=>w.ID).ToList();
                whereLamdba = whereLamdba.And(c => listid.Contains(c.SubjectID));
            }
            if (request.LeaveID != Guid.Empty)
            {
                var pdictionary = EDictionaryService.Find(w => w.ID == request.LeaveID);
                List<Guid> listid = EDictionaryService.GetLevelListById(pdictionary.ID).ToList().Select(w => w.ID).ToList();
                whereLamdba = whereLamdba.And(c => listid.Contains(c.LeaveID));
            }
            var modelList = productService.FindPageList(request.Page, request.PageSize, out int total, request.OrderBy, request.SortDir, whereLamdba).ToList();
            
            var viewModel = new ProductViewModel { TotalCount = total };
            foreach (var product in modelList)
            {
                var v = product.MapTo<ProductListItem, EProduct>();
                v.CreateTime = product.CreateTime.ToHSString();
                v.PutawayTimeStr = product.PutawayTime.ToHSString();
                viewModel.ProductList.Add(v);
            }

            return ServiceResponse<ProductViewModel>.SuccessResponse(viewModel);
        }

       
        /// <summary>
        /// 更新产品信息
        /// </summary>
        [HttpPost, Route("api/Product/UpdateModel")]
        public ServiceResponse<bool> UpdateModel(ProductListItem v)
        {
            var model = productService.Find(c => !c.IsDelete && c.ID == v.Id);
            if (model != null)
            {
                model.Title = v.Title;
                model.ImageUrl = v.ImageUrl;
                model.Description = v.Description;
                model.SubjectID = v.SubjectID; //;==Guid.Empty?model.SubjectID:v.SubjectID;
                model.SubjectName = EDictionaryService.Find(w => w.ID == v.SubjectID)?.TypeName;// v.SubjectName;==null?model.SubjectName:v.SubjectName;
                model.LeaveID = v.LeaveID; // ==Guid.Empty ? model.LeaveID:v.LeaveID;
                model.LeaveName = EDictionaryService.Find(w => w.ID == v.LeaveID)?.TypeName;// == null ? model.LeaveName : v.LeaveName;
                model.IsHot = v.IsHot;
                model.IsRecommend = v.IsRecommend;
                model.StartTime = v.StartTime < DateTime.MinValue ? model.StartTime : v.StartTime;
                model.EndTime = v.EndTime < DateTime.MinValue ? model.EndTime : v.EndTime;
                model.ModifyTime = DateTime.Now;
                model.Modifyer = CurrentUserModel.LoginName;
                model.Price = v.Price;
                model.Total = v.Total;
                //model.Residues = v.Residues;

                productService.Update(model);
            }
            return ServiceResponse<bool>.SuccessResponse("success", true);
        }

        /// <summary>
        /// 产品重新审核 
        /// </summary>
        /// <param name="id">产品id</param>
        /// <param name="status">课程待审核状态</param>
        /// <returns></returns>
        [HttpPost, Route("api/Product/GoBack")]
        public ServiceResponse<bool> GoBack(Guid id, int status)
        {
            var model = productService.Find(c => !c.IsDelete && c.ID == id);
            if (model != null)
            {
                var orderinfo = orderService.Find(w => w.CSID == model.ID);
                if (orderinfo != null)
                {
                    return ServiceResponse<bool>.SuccessResponse("该课程已经有人报名学习,不能重新审核", false);
                }
                else
                { 
                    var course = eCourseSeriesService.Find(c => !c.IsDelete && c.ID == model.PRID);
                    if (course != null)
                    {
                        course.State = status;
                        course.ModifyTime = DateTime.Now;
                        course.Modifyer = CurrentUserModel.LoginName;
                        eCourseSeriesService.Update(course);
                    }
                    model.IsDelete = true;
                    productService.Update(model);
                }
            }
            return ServiceResponse<bool>.SuccessResponse("success", true);
        }
        

    }
}