﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.Cls
{
    public class UserClassViewModel
    {
        /// <summary>
        /// 班级列表
        /// </summary>
        public List<ClsItem> ClsList = new List<ClsItem>();
    }

    [DataContract]
    public class ClsItem
    {
        /// <summary>
        /// 班级ID
        /// </summary>
        [DataMember(Name = "id")]
        public Guid ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        [DataMember(Name = "label")]
        public string ClassName { get; set; }
    }
}
