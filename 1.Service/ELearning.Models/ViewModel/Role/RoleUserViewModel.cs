﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearning.Models.ViewModel.User;

namespace ELearning.Models.ViewModel.Role
{
    public class RoleUserViewModel
    {
        /// <summary>
        /// 用户列表
        /// </summary>
        public List<GetUserItem> Items = new List<GetUserItem>();

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { set; get; }
    }

    /// <summary>
    /// 获取用户列表参数
    /// </summary>
    public class GetRoleUserRequest : BaseRequest
    {
        /// <summary>
        /// 0.获取角色关联的用户列表
        /// 1.获取角色不关联的用户列表
        /// </summary>
        public int ReqestType { set; get; }

        /// <summary>
        /// 角色id
        /// </summary>
        public Guid RoleId { set; get; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        public string UserName { set; get; }

        /// <summary>
        /// 登录名
        /// </summary>          
        public string LoginName { get; set; }

        /// <summary>
        /// 移动电话
        /// </summary>          
        public string MobilePhone { get; set; }

        /// <summary>
        /// 用户类型
        /// </summary>
        public string UserType { get; set; }

        /// <summary>
        /// 用户组织
        /// </summary>
        public string OrgCode { get; set; }

        /// <summary>
        /// 员工编号
        /// </summary>
        public string Unumber { get; set; }

        /// <summary>
        /// 经销商号码
        /// </summary>
        public string AgentNumber { get; set; }

        /// <summary>
        /// SGMW备案岗位
        /// </summary>
        public string SGMWRecordJob { get; set; }

        /// <summary>
        /// 区域标识
        /// </summary>       
        public int AreaCode { get; set; }

        /// <summary>
        /// 省份标识
        /// </summary>       
        public int ProvinceCode { get; set; }

        /// <summary>
        /// 城市标识
        /// </summary>      
        public int CityCode { get; set; }

    }
}
