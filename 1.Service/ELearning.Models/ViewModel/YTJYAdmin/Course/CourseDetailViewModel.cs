﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.YTJYAdmin.Course
{
    public class CourseDetailRequest
    {
        /// <summary>
        /// 课程id
        /// </summary>
        public Guid CourseId { set; get; }
    }

    public class CourseDetailViewModel
    {
        /// <summary>
        /// 章节列表
        /// </summary>
        public List<CourseDetailItem> Items { set; get; } = new List<CourseDetailItem>();

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { set; get; }

        /// <summary>
        /// 课程描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 上次看的章节id
        /// </summary>
        public Guid LastStudyChapterId { get; set; }
    }

    public class CourseDetailItem
    {
        /// <summary>
        /// 视频id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 章节id
        /// </summary>
        public Guid ChapterId { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 视频下载地址
        /// </summary>
        [Display(Name = "视频下载地址")]
        public string DownloadUrl { get; set; }

        /// <summary>
        /// 视频播放地址
        /// </summary>
        public string PlayUrl { get; set; }

        /// <summary>
        /// 转码状态：0.不需要转码，1.待转码，2.转码中，3.转码成功，4.转码失败，5.部分转码成功
        /// </summary>
        [Display(Name = "转码状态：0.不需要转码，1.待转码，2.转码中，3.转码成功，4.转码失败，5.部分转码成功")]
        public int Status { get; set; }

        /// <summary>
        /// 转码状态字符串
        /// </summary>
        public string StatusStr { get; set; }

        /// <summary>
        /// 上次学习到的位置，上次学习到第几秒
        /// </summary>
        public int LastStudyTime { get; set; }

        /// <summary>
        /// 视频时长，单位:秒
        /// </summary>
        [Display(Name = "视频时长，单位:秒")]
        public int Duration { get; set; }

        /// <summary>
        /// 是否已经学习过
        /// </summary>
        public bool IsStudy { get; set; }

        /// <summary>
        /// 试卷id
        /// </summary>
        public Guid? PaperId { get; set; }

        /// <summary>
        /// 章节序号
        /// </summary>
        public double ChapterSort { get; set; }

        /// <summary>
        /// 及格分数
        /// </summary>
        public double PassGrade { get; set; }

        /// <summary>
        /// 是否可以看下一个视频
        /// </summary>
        public bool IsCanNext { get; set; }

        /// <summary>
        /// 是否学习完成该章节
        /// </summary>
        public bool IsFinish { get; set; }
    }

    public class CourseDetailModel
    {
        public Guid Id { get; set; }

        /// <summary>
        /// 章节id
        /// </summary>
        public Guid ChapterId { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 视频下载地址
        /// </summary>
        [Display(Name = "视频下载地址")]
        public string DownloadUrl { get; set; }

        /// <summary>
        /// 视频播放地址
        /// </summary>
        public string PlayUrl { get; set; }

        /// <summary>
        /// 试卷id
        /// </summary>
        public Guid? PaperId { get; set; }

        /// <summary>
        /// 章节序号
        /// </summary>
        public double ChapterSort { get; set; }

        /// <summary>
        /// 及格分数
        /// </summary>
        public double PassGrade { get; set; }

        /// <summary>
        /// 是否学习完成该章节
        /// </summary>
        public bool? IsFinish { get; set; }

        /// <summary>
        /// 试卷得分，最高一次得分
        /// </summary>
        public double? Score { get; set; }

        /// <summary>
        /// 上次看的章节id
        /// </summary>
        public Guid? LastStudyChapterId { get; set; }

        /// <summary>
        /// 转码状态：0.不需要转码，1.待转码，2.转码中，3.转码成功，4.转码失败，5.部分转码成功
        /// </summary>
        [Display(Name = "转码状态：0.不需要转码，1.待转码，2.转码中，3.转码成功，4.转码失败，5.部分转码成功")]
        public int? Status { get; set; }

        /// <summary>
        /// 上次学习到的位置，上次学习到第几秒
        /// </summary>
        public int? LastStudyTime { get; set; }

        /// <summary>
        /// 视频时长，单位:秒
        /// </summary>
        [Display(Name = "视频时长，单位:秒")]
        public int Duration { get; set; }

        /// <summary>
        /// 学习该视频的次数
        /// </summary>
        public int StudyCount { get; set; }

    }
}
