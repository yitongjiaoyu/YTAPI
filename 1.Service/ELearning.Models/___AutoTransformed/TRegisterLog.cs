//-------------------------------------------------------------------
//文件名称：TRegisterLog.cs
//模块名称：TRegisterLogModel层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ELearning.Models
{	
	 /// <summary>
    /// TRegisterLog表实体类
    /// </summary>
	[Table("TRegisterLog")]
	public partial class TRegisterLog
    {
		
		/// <summary>
		/// Id
		/// </summary>
				[Key]
	[Display(Name = "Id")]
		public Guid Id { get; set; }
		
		/// <summary>
		/// 注册人用户id
		/// </summary>
			[Display(Name = "注册人用户id")]
		public Guid UserId { get; set; }
		
		/// <summary>
		/// 组织编码
		/// </summary>
			[Display(Name = "组织编码")]
		public string OrgCode { get; set; }
		
		/// <summary>
		/// 注册时邀请码
		/// </summary>
			[Display(Name = "注册时邀请码")]
		public string InviteCode { get; set; }
		
		/// <summary>
		/// 注册时的手机号
		/// </summary>
			[Display(Name = "注册时的手机号")]
		public string MobilePhone { get; set; }
		
		/// <summary>
		/// 状态：0.无组织，1.已分配组织
		/// </summary>
			[Display(Name = "状态：0.无组织，1.已分配组织")]
		public int Status { get; set; }
		
		/// <summary>
		/// 创建时间
		/// </summary>
			[Display(Name = "创建时间")]
		public DateTime CreateTime { get; set; }
		
		/// <summary>
		/// 更新时间
		/// </summary>
			[Display(Name = "更新时间")]
		public DateTime UpdateTime { get; set; }
		
		/// <summary>
		/// 创建人Id
		/// </summary>
			[Display(Name = "创建人Id")]
		public Guid CreateUserId { get; set; }
		
		/// <summary>
		/// 编辑人id
		/// </summary>
			[Display(Name = "编辑人id")]
		public Guid UpdateUserId { get; set; }
		
		/// <summary>
		/// 是否删除
		/// </summary>
			[Display(Name = "是否删除")]
		public bool IsDelete { get; set; }
		   
    }
}

