﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ELearning.Models.ViewModel.User
{
    public class GetUserViewModel
    {
        /// <summary>
        /// 用户列表
        /// </summary>
        public List<GetUserItem> UserItems { set; get; } = new List<GetUserItem>();

        /// <summary>
        /// 总数量
        /// </summary>
        public int TotalCount { set; get; }
    }

    /// <summary>
    /// 用户信息实体
    /// </summary>
    public class GetUserItem
    {
        /// <summary>
        /// 用户id
        /// </summary>
        public Guid UserId { set; get; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        public string UserName { set; get; }

        /// <summary>
        /// 职员代码
        /// </summary>
        public string Unumber { set; get; }

        /// <summary>
        /// 登录名
        /// </summary>          
        public string LoginName { get; set; }

        /// <summary>
        /// 登录密码
        /// </summary>          
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

        /// <summary>
        /// 出生日期
        /// </summary>          
        public DateTime Birthday { get; set; }

        /// <summary>
        /// 出生日期字符串
        /// </summary>
        public string BirthdayStr
        {
            get
            {
                var str = Birthday.ToString("yyyy-MM-dd");
                if (Birthday < new DateTime(1890, 1, 1))
                    str = string.Empty;
                return str;
            }
        }

        /// <summary>
        /// 头像
        /// </summary>          
        public string HeadPhoto { get; set; }

        /// <summary>
        /// 用户头像全地址
        /// </summary>
        public string ShowHeadPhoto { get; set; }

        /// <summary>
        /// 性别
        /// </summary>          
        public string Sex { get; set; }

        /// <summary>
        /// 身份证号码
        /// </summary>
        public string CardNo { get; set; }

        /// <summary>
        /// 用户类别
        /// </summary>          
        public string UserType { get; set; }

        /// <summary>
        /// 所属机构
        /// </summary>
        public string OrgCode { get; set; }

        /// <summary>
        /// 用户组织
        /// </summary>
        public string OrgName { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>          
        public string Company { get; set; }

        /// <summary>
        /// 部门信息
        /// </summary>          
        public string Department { get; set; }

        /// <summary>
        /// 职务
        /// </summary>          
        public string Position { get; set; }

        /// <summary>
        /// 办公电话
        /// </summary>          
        public string OfficeTel { get; set; }

        /// <summary>
        /// 移动电话
        /// </summary>          
        public string MobilePhone { get; set; }

        /// <summary>
        /// 电子邮件
        /// </summary>          
        public string Email { get; set; }

        /// <summary>
        /// 用户状态
        /// </summary>          
        public int AccountStatus { get; set; }

        /// <summary>
        /// 用户状态
        /// </summary> 
        public bool Status
        {
            get
            {
                var b = false;
                if (AccountStatus == 1)
                    b = true;
                return b;
            }
        }

      

        /// <summary>
        /// 个人简介
        /// </summary>          
        public string Remark { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>          
        public string Creater { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>          
        public string CreateTime { get; set; }

        /// <summary>
        /// 修改人
        /// </summary>          
        public string Modify { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>          
        public DateTime ModifyTime { get; set; }

        /// <summary>
        /// 邀请码
        /// </summary>
        public string InviteCode { get; set; }

        /// <summary>
        /// ????
        /// </summary>
        public string AreaName { get; set; }

        /// <summary>
        /// ????
        /// </summary>
        public string ProvinceName { get; set; }

        /// <summary>
        /// ????
        /// </summary>
        public string CityName { get; set; }

        /// <summary>
        /// 角色id列表
        /// </summary>
        public List<Guid> RoleIdList = new List<Guid>();

        /// <summary>
        /// 菜单id数组
        /// </summary>
        public List<Guid> MenuIdList { get; set; } = new List<Guid>();

        /// <summary>
        /// 套餐id列表
        /// </summary>
        public List<Guid> PackageIdList { get; set; } = new List<Guid>();

        /// <summary>
        /// 半选中节点id列表
        /// </summary>
        public List<Guid> HalfMenuIdList { get; set; } = new List<Guid>();
    }

    /// <summary>
    /// 讲师元素
    /// </summary>
    [DataContract]
    public class TeacherListViewModel
    {
        /// <summary>
        /// 用户id
        /// </summary>
        [DataMember(Name = "id")]
        public Guid UserId { set; get; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        [DataMember(Name = "label")]
        public string UserName { set; get; }

        /// <summary>
        /// 职员代码
        /// </summary>
        public string Unumber { set; get; }

        /// <summary>
        /// 登录名
        /// </summary>          
        public string LoginName { get; set; }

        /// <summary>
        /// 登录密码
        /// </summary>          
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

        /// <summary>
        /// 出生日期
        /// </summary>          
        public DateTime Birthday { get; set; }

        /// <summary>
        /// 出生日期字符串
        /// </summary>
        public string BirthdayStr => Birthday.ToString("yyyy-MM-dd");

        /// <summary>
        /// 头像
        /// </summary>          
        public string HeadPhoto { get; set; }

        /// <summary>
        /// 性别
        /// </summary>          
        public string Sex { get; set; }

        /// <summary>
        /// 身份证号码
        /// </summary>
        public string CardNo { get; set; }

        /// <summary>
        /// 用户类别
        /// </summary>          
        public string UserType { get; set; }

        /// <summary>
        /// 所属机构
        /// </summary>
        public string OrgCode { get; set; }

        /// <summary>
        /// 用户组织
        /// </summary>
        public string OrgName { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>          
        public string Company { get; set; }

        /// <summary>
        /// 部门信息
        /// </summary>          
        public string Department { get; set; }

        /// <summary>
        /// 职务
        /// </summary>          
        public string Position { get; set; }

        /// <summary>
        /// 办公电话
        /// </summary>          
        public string OfficeTel { get; set; }

        /// <summary>
        /// 移动电话
        /// </summary>          
        public string MobilePhone { get; set; }

        /// <summary>
        /// 电子邮件
        /// </summary>          
        public string Email { get; set; }

        /// <summary>
        /// 用户状态
        /// </summary>          
        public int AccountStatus { get; set; }

        /// <summary>
        /// 注册来源
        /// </summary>          
        public string RegisterDevice { get; set; }

        /// <summary>
        /// 个人简介
        /// </summary>          
        public string Remark { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>          
        public string Creater { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>          
        public string CreateTime { get; set; }

        /// <summary>
        /// 修改人
        /// </summary>          
        public string Modify { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>          
        public DateTime ModifyTime { get; set; }

        /// <summary>
        /// 经销商部门
        /// </summary>
        public string AgentDepart { get; set; }

        /// <summary>
        /// SGMW备案岗位
        /// </summary>
        public string SGMWRecordJob { get; set; }

        /// <summary>
        /// 状态:0.离职，1.在职
        /// </summary>
        public int JobStatus { get; set; }

        /// <summary>
        /// 汽车行业销售管理工作年限
        /// </summary>
        public int CarSaleManageYear { get; set; }

        /// <summary>
        /// 汽车行业市场运作管理工作年限
        /// </summary>
        public int CarMarketManageYear { get; set; }

        /// <summary>
        /// 其他行业销售管理工作年限
        /// </summary>
        public int OtherSaleManageYear { get; set; }

        /// <summary>
        /// 其他行业市场运作管理工作年限
        /// </summary>
        public int OtherMarketManageYear { get; set; }

        /// <summary>
        /// 汽车售后服务管理工作年限
        /// </summary>
        public int CarServiceManageYear { get; set; }

        /// <summary>
        /// 汽车维修技术管理工作年限
        /// </summary>
        public int CarRepairManageYear { get; set; }

        /// <summary>
        /// 其他行业维修工作年限
        /// </summary>
        public int OtherRepairManageYear { get; set; }

        /// <summary>
        /// 经销商号
        /// </summary>
        public string AgentNumber { get; set; }

        /// <summary>
        /// 经销商名称
        /// </summary>
        public string AgentName { get; set; }

        /// <summary>
        /// 经销商岗位
        /// </summary>
        public string AgentJobName { get; set; }

        /// <summary>
        /// 角色名称，角色之间用逗号连接
        /// </summary>
        public string RoleNames { get; set; }
    }

    /// <summary>
    /// 重置密码
    /// </summary>
    public class ResetPwdRequest
    {
        /// <summary>
        /// 用户id
        /// </summary>
        public Guid UserId { set; get; }

        /// <summary>
        /// 登录密码
        /// </summary>          
        public string Password { get; set; }
    }

    /// <summary>
    /// 获取用户列表参数
    /// </summary>
    public class GetUserRequest : BaseRequest
    {
        /// <summary>
        /// 用户姓名
        /// </summary>
        public string UserName { set; get; }

        /// <summary>
        /// 登录名
        /// </summary>          
        public string LoginName { get; set; }

        /// <summary>
        /// 移动电话
        /// </summary>          
        public string MobilePhone { get; set; }

        /// <summary>
        /// 用户类型
        /// </summary>
        public string UserType { get; set; }

        /// <summary>
        /// 用户组织
        /// </summary>
        public string OrgCode { get; set; }

        /// <summary>
        /// 员工编号
        /// </summary>
        public string Unumber { get; set; }

        /// <summary>
        /// 经销商号码
        /// </summary>
        public string AgentNumber { get; set; }
        
        /// <summary>
        /// SGMW备案岗位
        /// </summary>
        public string SGMWRecordJob { get; set; }

        /// <summary>
        /// 区域标识
        /// </summary>       
        public int AreaCode { get; set; }

        /// <summary>
        /// 省份标识
        /// </summary>       
        public int ProvinceCode { get; set; }

        /// <summary>
        /// 城市标识
        /// </summary>      
        public int CityCode { get; set; }

    }

    /// <summary>
    /// 导入用户视图模型
    /// </summary>
    public class InsertAdminUsersViewModel
    {
        /// <summary>
        /// 经销商号
        /// </summary>
        public string 经销商号 { set; get; }
        public string 职员代码 { set; get; }
        public string 经销商部门 { set; get; }
        public string 职员名称 { set; get; }
        public string 性别 { set; get; }
        public string 出生日期 { set; get; }
        public string 身份证号 { set; get; }
        public string SGMW备案岗位 { set; get; }
        public string 状态 { set; get; }
        public string 汽车行业销售管理工作年限 { set; get; }
        public string 汽车行业市场运作管理工作年限 { set; get; }
        public string 其他行业销售管理工作年限 { set; get; }
        public string 其他行业市场运作管理工作年限 { set; get; }
        public string 汽车售后服务管理工作年限 { set; get; }
        public string 汽车维修技术管理工作年限 { set; get; }
        public string 其他行业维修工作年限 { set; get; }
        public string 经销商名称 { set; get; }
        public string 经销商岗位 { set; get; }
        public string 更新时间 { set; get; }
        public string 原因 { set; get; }
    }

    /// <summary>
    /// 导入平台用户模型视图
    /// </summary>
    public class InsertSGMWAdminUsersViewModel
    {
        public string 姓名 { set; get; }
        public string 登录名 { set; get; }
        public string 职务 { set; get; }
        public string 移动电话 { set; get; }
        public string 邮箱 { set; get; }
        public string 性别 { set; get; }
        public string 出生日期 { set; get; }
        public string 个人简介 { set; get; }
        public string 原因 { set; get; }
    }

    /// <summary>
    /// 导入骏客用户模型
    /// </summary>
    public class InsertJKAdminUsersViewModel
    {
        public string 员工编号 { set; get; }
        public string 账号 { set; get; }
        public string 员工姓名 { set; get; }
        public string 性别 { set; get; }
        public string 身份证号 { set; get; }
        public string 手机号 { set; get; }
        public string 在职状态 { set; get; }
        public string 是否锁定 { set; get; }
        public string 创建人 { set; get; }
        public string 创建时间 { set; get; }
        public string 原因 { set; get; }
    }

    /// <summary>
    /// 导入骏客用户实体类
    /// </summary>
    public class ImportUserjk
    {
        /// <summary>
        /// 登录账号
        /// </summary>
        public string loginName { get; set; }
        /// <summary>
        /// 经销商号
        /// </summary>
        public string agentNumber { get; set; }
        /// <summary>
        /// 职员代码
        /// </summary>
        public string unumber { get; set; }
        /// <summary>
        /// 职员名称
        /// </summary>
        public string userName { get; set; }
        /// <summary>
        /// 性别
        /// 1-保密
        /// 2-男
        /// 3-女
        /// </summary>
        public string sex { get; set; }
        /// <summary>
        /// 身份证号
        /// </summary>
        public string cardNo { get; set; }
        /// <summary>
        /// 手机
        /// </summary>
        public string mobilePhone { get; set; }
        /// <summary>
        /// 状态
        /// 0-禁用
        /// 1-启用
        /// </summary>
        public int accountStatus { get; set; }
        /// <summary>
        /// 经销商名称
        /// </summary>
        public string agentName { get; set; }
        /// <summary>
        /// 经销商岗位
        /// </summary>
        public string agentJobName { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public string UPDATED_AT { get; set; }

        /// <summary>
        /// 原因
        /// </summary>
        public string Reason { set; get; }
    }

    /// <summary>
    /// 销售助手用户信息
    /// </summary>
    public class Userxszs
    {
        /// <summary>
        /// 经销商号
        /// </summary>
        public string AgentNumber { get; set; }
        /// <summary>
        /// 经销商名称
        /// </summary>
        public string AgentName { get; set; }
        /// <summary>
        /// 经销商岗位
        /// </summary>
        public string AgentJobName { get; set; }
        /// <summary>
        /// 经销商部门
        /// </summary>
        public string AgentDepart { get; set; }
        /// <summary>
        /// 职员代码
        /// </summary>
        public string Unumber { get; set; }
        /// <summary>
        /// 职员名称
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 身份证号
        /// </summary>
        public string CardNo { get; set; }
        /// <summary>
        /// 性别
        /// 1-保密
        /// 2-男
        /// 3-女
        /// </summary>
        public int Sex { get; set; }
        /// <summary>
        /// 出生日期
        /// </summary>
        public DateTime Birthday { get; set; }
        /// <summary>
        /// 汽车行业市场运作管理工作年限
        /// </summary>
        public int CarMarketManageYear { get; set; }
        /// <summary>
        /// 汽车维修技术管理工作年限
        /// </summary>
        public int CarRepairManageYear { get; set; }
        /// <summary>
        /// 汽车行业销售管理工作年限
        /// </summary>
        public int CarSaleManageYear { get; set; }
        /// <summary>
        /// 汽车售后服务管理工作年限
        /// </summary>
        public int CarServiceManageYear { get; set; }
        /// <summary>
        /// 其他行业市场运作管理工作年限
        /// </summary>
        public int OtherMarketManageYear { get; set; }
        /// <summary>
        /// 其他行业维修工作年限
        /// </summary>
        public int OtherRepairManageYear { get; set; }
        /// <summary>
        /// 其他行业销售管理工作年限
        /// </summary>
        public int OtherSaleManageYear { get; set; }
        /// <summary>
        /// SGMW备案岗位
        /// </summary>
        public string SGMWRecordJob { get; set; }
    }


    /// <summary>
    /// 骏客APP用户信息
    /// </summary>
    public class Userjk
    {
        /// <summary>
        /// 登录账号
        /// </summary>
        public string LoginName { get; set; }
        /// <summary>
        /// 经销商号
        /// </summary>
        public string AgentNumber { get; set; }
        /// <summary>
        /// 职员代码
        /// </summary>
        public string Unumber { get; set; }
        /// <summary>
        /// 职员名称
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 性别
        /// 1-保密
        /// 2-男
        /// 3-女
        /// </summary>
        public int Sex { get; set; }
        /// <summary>
        /// 身份证号
        /// </summary>
        public string CardNo { get; set; }
        /// <summary>
        /// 手机
        /// </summary>
        public string MobilePhone { get; set; }
        /// <summary>
        /// 状态
        /// 0-禁用
        /// 1-启用
        /// </summary>
        public int AccountStatus { get; set; }
        /// <summary>
        /// 经销商名称
        /// </summary>
        public string AgentName { get; set; }
        /// <summary>
        /// 经销商岗位
        /// </summary>
        public string AgentJobName { get; set; }
    }

}
