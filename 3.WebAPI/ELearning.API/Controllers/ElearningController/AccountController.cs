﻿using System;
using System.Web.Http;
using ELearning.BLL;
using ELearning.Common;
using ELearning.Common.CommonStr;
using ELearning.Common.Extensions;
using ELearning.Common.Http;
using ELearning.Common.Model;
using ELearning.Common.Oprator;
using ELearning.Models;
using ELearning.Models.ViewModel.Account;

namespace ELearning.API.Controllers.ElearningController
{
    /// <summary>
    /// 账号相关操作
    /// </summary>
    public class AccountController : BaseApiController
    {
        /// <summary>
        /// 用户操作service
        /// </summary>
        protected ESysUserService userService = new ESysUserService();

        protected EOrgService orgService = new EOrgService();

        protected EMenuService menuService = new EMenuService();

        protected UserController UserController = new UserController();

        /// <summary>
        /// 注册用户信息保存
        /// </summary>
        [HttpPost, Route("api/account/AddUser")]
        [AllowAnonymous]
        public ServiceResponse<ESysUser> AddUser(AddUserViewModel viewModel)
        {
            var org = orgService.Find(o => !o.IsDelete && o.org_code == viewModel.OrgCode);
            var orgCode = CommonConst.DEFAULT_ORGCODE;
            var status = 0;
            if (org != null)
            {
                status = 1;
                orgCode = org.org_code;
            }

            var user = new ESysUser
            {
                UserID = GuidUtil.NewSequentialId(),
                AccountStatus = status,
                Birthday = DateTime.Now,
                CreateTime = DateTime.Now,
                Creater = string.Empty,
                Company = string.Empty,
                Department = string.Empty,
                Email = viewModel.Email,
                HeadPhoto = CommonConst.UserDefaultHeadPhoto,
                IsDelete = false,
                ModifyTime = DateTime.Now,
                Position = string.Empty,
                MobilePhone = viewModel.Phone,
                Remark = CommonConst.UserDefaultRemark,
                LoginName = viewModel.UserName,
                UserName = viewModel.UserName,
                UserType = viewModel.UserType,
                OfficeTel = string.Empty,
                OrgCode = orgCode,
                Password = viewModel.Password,
                Sex = 1
            };
            var m = userService.Find(u => !u.IsDelete && u.LoginName == viewModel.UserName);
            if (m != null)
                return ServiceResponse<ESysUser>.SuccessResponse("success", m);
            userService.Add(user);
            return ServiceResponse<ESysUser>.SuccessResponse("success", user);
        }

        /// <summary>
        /// 更新用户手机号和邮箱，完善个人信息
        /// </summary>
        [HttpPost, Route("api/account/UpdateUser")]
        [AllowAnonymous]
        public ServiceResponse<ESysUser> UpdateUser(UpdateUserViewModel viewModel)
        {
            var user = userService.Find(u => !u.IsDelete && u.UserID == viewModel.UserId);
            if (user != null)
            {
                user.MobilePhone = viewModel.Phone;
                user.Email = viewModel.Email;

                userService.Add(user);
            }

            return ServiceResponse<ESysUser>.SuccessResponse("success", user);
        }

        /// <summary>
        /// 用户登录
        /// </summary>
        [HttpPost, Route("api/eaccount/Login")]
        [AllowAnonymous]
        public ServiceResponse<LoginResponse> Login(LoginRequest viewModel)
        {
            var token = string.Empty;
            var p = viewModel.Password.Trim();
            var user = userService.Find(u => !u.IsDelete && u.LoginName == viewModel.LoginName.Trim() && u.Password == p && u.AccountStatus == 1);
            if (user != null)
            {
                token = GuidUtil.NewSequentialId().ToString();
                var currentUserModel = new OperatorModel
                {
                    Email = user.Email,
                    HeadPhoto = user.HeadPhoto,
                    LoginName = user.LoginName,
                    OrgCode = user.OrgCode,
                    //OrgName = orgService.Find(w => w.org_code == user.OrgCode)?.org_name ?? string.Empty,
                    MobilePhone = user.MobilePhone,
                    Position = user.Position,
                    Remark = user.Remark,
                    UserId = user.UserID,
                    UserIp = IpHelp.GetClientIp(),
                    UserName = user.UserName,
                    UserType = user.UserType,
                };

                OperatorProvider.Provider.AddCurrent(token, currentUserModel);

                var result = new LoginResponse
                {
                    Token = token,
                    User = currentUserModel,
                };
                return ServiceResponse<LoginResponse>.SuccessResponse(CommonConst.OprateSuccessStr, result);
            }
            else
            {
                var result = new LoginResponse
                {
                    Token = token,
                    User = new OperatorModel()
                };
                return ServiceResponse<LoginResponse>.WarningResponse(CommonConst.Code_LoginError, CommonConst.Msg_LoginError, result);
            }
        }
        
        /// <summary>
        /// 注销登录
        /// </summary>
        [HttpGet, Route("api/eaccount/Logout")]
        [AllowAnonymous]
        public ServiceResponse<bool> Logout(string token)
        {
            OperatorProvider.Provider.RemoveCurrent(token);
            return ServiceResponse<bool>.SuccessResponse(true);
        }
    }
}