﻿using ELearning.BLL;
using ELearning.Common;
using ELearning.Common.Extensions;
using ELearning.Common.Model;
using ELearning.Models;
using ELearning.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace ELearning.API.Controllers.ReportControllers
{
    /// <summary>
    /// 词云
    /// </summary>
    public partial class KeywordSearchLogController : BaseApiController
    {
        EKeywordSearchLogService keywordSearchLogService => new EKeywordSearchLogService();
        ESysModelVisitLogService sysModelVisitLogService => new ESysModelVisitLogService();

        /// <summary>
        /// 新增关键词搜索记录
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        [HttpPost, Route("api/log/addKeyWordLog")]
        public ServiceResponse<bool> AddKeyWordLog(EKeywordSearchLog post)
        {
            if (string.IsNullOrEmpty(post.Keyword)) return ServiceResponse<bool>.SuccessResponse("关键词不可为空", false);

            post.ID = Guid.NewGuid();
            post.UserID = CurrentUserModel.UserId;
            post.VisitDate = DateTime.Now;
            post.UserIP = HttpContext.Current.Request.UserHostAddress;

            // 1分钟内同一个关键词一个用户只记录一次
            var date1 = DateTime.Now.AddMinutes(-1);
            var logExit = keywordSearchLogService.Find(x => x.UserID == post.UserID && x.Keyword == post.Keyword && x.VisitSource == post.VisitSource && x.VisitDate > date1);
            if (logExit.IsNotNull()) return ServiceResponse<bool>.SuccessResponse(true);

            keywordSearchLogService.Add(post);
            return ServiceResponse<bool>.SuccessResponse(true);
        }

        /// <summary>
        /// 词云
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost, Route("api/log/wordCloud")]
        public ServiceResponse<string> WordCloud(ReportBaseRequest req)
        {
            var data = sysModelVisitLogService.SearchKeywordLogs(req);

            var str = string.Join(",", data) ?? "";

            return ServiceResponse<string>.SuccessResponse("成功", str);
        }


        /// <summary>
        /// 平台文件资源情况
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/log/fileAssetReport")]
        public ServiceResponse<List<LoopView>> FileAssetReport()
        {
            var data = sysModelVisitLogService.FileAssetReport();

            data.ForEach(x => x.name = x.name + "(" + x.y + ")");

            return ServiceResponse<List<LoopView>>.SuccessResponse("成功", data);
        }


    }
}