﻿using System;
using ELearning.BLL;
using ELearning.Common.Model;
using ELearning.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using ELearning.Common;
using ELearning.Models.ViewModel.Level;

namespace ELearning.API.Controllers.AdminControllers
{
    public class LevelController : BaseApiController
    {
        protected ELevelService LevelService = new ELevelService();

        /// <summary>
        /// 等级试卷关联service
        /// </summary>
        private RLevelPaperService levelPaperService = new RLevelPaperService();

        /// <summary>
        /// 等级课程系列关联service
        /// </summary>
        private RLevelCourseSeriesService levelCourseSeriesService = new RLevelCourseSeriesService();

        ///// <summary>
        ///// 试卷管理service
        ///// </summary>
        //private E_paperService paperService = new E_paperService();

        ///// <summary>
        ///// 获取等级列表
        ///// </summary>
        //[HttpPost, Route("api/Level/GetLevelList")]
        //public ServiceResponse<List<LevelListItem>> GetLevelList(LevelListRequest request)
        //{
        //    var modelList = LevelService.FindList(l => !l.IsDelete).ToList();
        //    var lids = modelList.Select(l => l.ID).ToList();
        //    var rlps = levelPaperService.FindList(r => lids.Contains(r.LevelID)).ToList();
        //    var rlpIds = rlps.Select(r => r.PaperID).ToList();
        //    var papers = paperService.FindList(p => !p.IsDelete && rlpIds.Contains(p.paper_id)).ToList();

        //    var viewModels = new List<LevelListItem>();
        //    var pId = Guid.Parse("00000000-0000-0000-0000-000000000000");
        //    GenerateLevelList(modelList, viewModels, pId, papers, rlps, request.Status);
        //    return ServiceResponse<List<LevelListItem>>.SuccessResponse("success", viewModels);
        //}

        ///// <summary>
        ///// 构造等级列表
        ///// </summary>
        //private void GenerateLevelList(List<ELevel> modelList, List<LevelListItem> viewModels, Guid pId, List<E_paper> papers, List<RLevelPaper> rlps, int status)
        //{
        //    var subList = modelList.Where(l => !l.IsDelete && l.ParentLevelID == pId).OrderBy(l => l.SortNum).ToList();
        //    var pModel = modelList.FirstOrDefault(l => l.ID == pId) ?? new ELevel { Title = "/" };
        //    foreach (var eLevel in subList)
        //    {
        //        var v = new LevelListItem
        //        {
        //            ID = eLevel.ID,
        //            Title = eLevel.Title,
        //            ParentTitle = pModel.Title,
        //            ParentId = pModel.ID,
        //            SortNum = eLevel.SortNum
        //        };
        //        if (pId != Guid.Parse("00000000-0000-0000-0000-000000000000") && status == 0)
        //            v.Disabled = true;
        //        if (pId == Guid.Parse("00000000-0000-0000-0000-000000000000") && status == 1)
        //            v.Disabled = true;
        //        var ts = rlps.Where(r => r.LevelID == v.ID).Select(r => r.PaperID).ToList();
        //        var ps = papers.Where(p => ts.Contains(p.paper_id)).ToList();
        //        v.PaperIds.AddRange(ps);
        //        GenerateLevelList(modelList, v.Children, v.ID, papers, rlps, status);
        //        if (!v.Children.Any())
        //            v.Children = null;
        //        viewModels.Add(v);
        //    }
        //}

        /// <summary>
        /// 获取全部的等级列表，未分层级
        /// </summary>
        [HttpGet, Route("api/Level/GetAllLevelList")]
        public ServiceResponse<List<ELevel>> GetAllLevelList()
        {
            var modelList = LevelService.FindList(l => !l.IsDelete).ToList();
            var viewModels = new List<ELevel>();
            var pId = Guid.Parse("00000000-0000-0000-0000-000000000000");
            GenerateAllLevelList(modelList, viewModels, pId);
            return ServiceResponse<List<ELevel>>.SuccessResponse("success", viewModels);
        }

        /// <summary>
        /// 构造全部等级列表，不包含children
        /// </summary>
        private void GenerateAllLevelList(List<ELevel> modelList, List<ELevel> viewModels, Guid pId, int i = 1)
        {
            i++;
            var subList = modelList.Where(l => !l.IsDelete && l.ParentLevelID == pId).OrderBy(l => l.CreateTime).ThenBy(l => l.SortNum).ToList();
            foreach (var eLevel in subList)
            {
                var str = "";
                //父级菜单不缩进  
                for (var j = 1; j < i; j++)
                {
                    str += HttpUtility.HtmlDecode("&nbsp;&nbsp;");
                }
                if (i > 2)
                    str += "├";
                var v = new ELevel
                {
                    ID = eLevel.ID,
                    Title = $"{str}{eLevel.Title}"
                };
                viewModels.Add(v);
                GenerateAllLevelList(modelList, viewModels, v.ID, i);
            }
        }

        ///// <summary>
        ///// 获取课程系列下的等级列表
        ///// </summary>
        ///// <param name="csId">课程封面id</param>
        //[HttpGet, Route("api/Level/GetLevelListByCourseId")]
        //public ServiceResponse<List<LevelListItem>> GetLevelListByCourseId(Guid csId)
        //{
        //    var levelId = levelCourseSeriesService.Find(r => r.CSID == csId)?.LevelID ?? Guid.Empty;
        //    var modelList = LevelService.GetLevelListById(levelId);

        //    var lids = modelList.Select(l => l.ID).ToList();
        //    var rlps = levelPaperService.FindList(r => lids.Contains(r.LevelID)).ToList();
        //    var rlpIds = rlps.Select(r => r.PaperID).ToList();
        //    var papers = paperService.FindList(p => !p.IsDelete && rlpIds.Contains(p.paper_id)).ToList();

        //    var viewModels = new List<LevelListItem>();
        //    var pId = Guid.Parse("00000000-0000-0000-0000-000000000000");
        //    GenerateLevelList(modelList, viewModels, pId, papers, rlps, 1);
        //    return ServiceResponse<List<LevelListItem>>.SuccessResponse("success", viewModels);
        //}

        ///// <summary>
        ///// 更新等级
        ///// </summary>
        //[HttpPost, Route("api/Level/UpdateLevel")]
        //public ServiceResponse<ELevel> UpdateLevel(LevelListViewModel viewModel)
        //{
        //    var m = LevelService.Find(l => !l.IsDelete && l.ID == viewModel.Id);
        //    if (m != null)
        //    {
        //        m.Title = viewModel.LevelTitle;
        //        m.ModifyTime = DateTime.Now;
        //        m.Modifyer = CurrentUserModel.LoginName;
        //        m.SortNum = viewModel.SortNum;

        //        var rlps = levelPaperService.FindList(r => r.LevelID == m.ID).ToList();
        //        levelPaperService.Deletes(rlps);//先删除原来的关联试卷
        //        if (viewModel.PaperIds.Any())
        //        {
        //            //把新的关联试卷添加到数据库
        //            var temps = new List<RLevelPaper>();
        //            foreach (var paper in viewModel.PaperIds)
        //            {
        //                var model = new RLevelPaper
        //                {
        //                    ID = GuidUtil.NewSequentialId(),
        //                    LevelID = m.ID,
        //                    PaperID = paper.paper_id
        //                };
        //                temps.Add(model);
        //            }
        //            levelPaperService.Adds(temps);
        //        }
        //        LevelService.Update(m);
        //    }
        //    return ServiceResponse<ELevel>.SuccessResponse(m);
        //}

        ///// <summary>
        ///// 添加等级
        ///// </summary>
        //[HttpPost, Route("api/Level/AddLevel")]
        //public ServiceResponse<ELevel> AddLevel(LevelListViewModel viewModel)
        //{
        //    var m = new ELevel
        //    {
        //        ID = GuidUtil.NewSequentialId(),
        //        ParentLevelID = viewModel.ParentId,
        //        Title = viewModel.LevelTitle,
        //        CreateTime = DateTime.Now,
        //        Creater = CurrentUserModel.LoginName,
        //        Description = string.Empty,
        //        IsDelete = false,
        //        ModifyTime = DateTime.Now,
        //        Modifyer = CurrentUserModel.LoginName,
        //        SortNum = viewModel.SortNum
        //    };
        //    if (viewModel.PaperIds.Any())
        //    {
        //        //把新的关联试卷添加到数据库
        //        var temps = new List<RLevelPaper>();
        //        foreach (var paper in viewModel.PaperIds)
        //        {
        //            var model = new RLevelPaper
        //            {
        //                ID = GuidUtil.NewSequentialId(),
        //                LevelID = m.ID,
        //                PaperID = paper.paper_id
        //            };
        //            temps.Add(model);
        //        }
        //        levelPaperService.Adds(temps);
        //    }
        //    LevelService.Add(m);
        //    return ServiceResponse<ELevel>.SuccessResponse(m);
        //}
    }
}