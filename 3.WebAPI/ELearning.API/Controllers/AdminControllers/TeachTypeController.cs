﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Http;
using ELearning.BLL;
using ELearning.Common;
using ELearning.Common.CommonStr;
using ELearning.Common.Extensions;
using ELearning.Common.Model;
using ELearning.Models;
using ELearning.Models.ViewModel.CourseSeries;

namespace ELearning.API.Controllers.AdminControllers
{
    /// <summary>
    /// 课程分类相关操作
    /// </summary>
    public class TeachTypeController : BaseApiController
    {
        /// <summary>
        /// 课程类型操作service
        /// </summary>
        private ETeachTypeService TeachTypeService = new ETeachTypeService();

        /// <summary>
        /// 获取全部课程分类列表
        /// </summary>
        [HttpPost, Route("api/teachType/GetTeachTypeList")]
        public ServiceResponse<TeachTypeModel> GetTeachTypeList(TeachTypeRequest request)
        {
            Expression<Func<ETeachType, bool>> whereLamdba = (u) => u.IsDelete == false;
            if (!string.IsNullOrEmpty(request.Title))
            {
                whereLamdba = whereLamdba.And(t => t.Title.Contains(request.Title));
            }
            var typeList = TeachTypeService.FindPageList(request.Page, request.PageSize, out var totalRecord, "ModifyTime", "DESC", whereLamdba).ToList();

            var viewModel = new TeachTypeModel
            {
                TotalCount = totalRecord
            };
            foreach (var teachType in typeList)
            {
                var t = new TeachTypeItem
                {
                    ID = teachType.ID,
                    Code = teachType.Code,
                    Description = teachType.Description,
                    Title = teachType.Title,
                    ModifyTime = teachType.ModifyTime.ToString("yyyy-MM-dd HH:mm")
                };
                viewModel.Items.Add(t);
            }

            return ServiceResponse<TeachTypeModel>.SuccessResponse(CommonConst.OprateSuccessStr, viewModel);
        }

        /// <summary>
        /// 添加课程分类
        /// </summary>
        [HttpPost, Route("api/teachType/AddModel")]
        public object AddModel(TeachTypeItem viewModel)
        {
            var model = new ETeachType
            {
                ID = GuidUtil.NewSequentialId(),
                CreateTime = DateTime.Now,
                Code = viewModel.Code,
                Creater = CurrentUserModel?.LoginName??string.Empty,
                Description = viewModel.Description,
                IsDelete = false,
                ModifyTime = DateTime.Now,
                Modifyer = CurrentUserModel?.LoginName ?? string.Empty,
                Title = viewModel.Title
            };

            var dbModel = TeachTypeService.Find(t => t.Code == viewModel.Code);
            if(dbModel!=null)
                return ServiceResponse<bool>.SuccessResponse(CommonConst.TeachTypeRepeat, false);
            TeachTypeService.Add(model);
            return ServiceResponse<bool>.SuccessResponse(CommonConst.OprateSuccessStr, true);
        }

        /// <summary>
        /// 更新课程分类
        /// </summary>
        [HttpPost, Route("api/teachType/UpdateModel")]
        public object UpdateModel(TeachTypeItem viewModel)
        {
            var model = TeachTypeService.Find(t => t.ID == viewModel.ID);
            model.Title = viewModel.Title;
            model.Description = viewModel.Description;
            model.ModifyTime = DateTime.Now;
            model.Modifyer = CurrentUserModel?.LoginName ?? string.Empty;
            TeachTypeService.Update(model);
            return ServiceResponse<bool>.SuccessResponse(CommonConst.OprateSuccessStr, true);
        }

        /// <summary>
        /// 删除课程分类
        /// </summary>
        /// <param name="id">课程分类id</param>
        [HttpPost, Route("api/teachType/DeleteModel")]
        public object DeleteModel(Guid id)
        {
            var model = TeachTypeService.Find(t => t.ID == id);
            if (model != null)
            {
                model.IsDelete = true;
                model.ModifyTime = DateTime.Now;
                model.Modifyer = CurrentUserModel?.LoginName ?? string.Empty;
                TeachTypeService.Update(model);
            }

            return ServiceResponse<int>.SuccessResponse(CommonConst.OprateSuccessStr, 1);
        }
    }
}