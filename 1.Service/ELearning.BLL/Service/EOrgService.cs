﻿using ELearning.Common.Extensions;
using ELearning.Common.Model;
using ELearning.Models;
using ELearning.Models.ViewModel.Logs;
using ELearning.Models.ViewModel.Org;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.BLL
{
    public partial class EOrgService : BaseService<EOrg>
    {
        public Page<EOrg> GetOrgList(GetOrgRequest request)
        {
            Expression<Func<EOrg, bool>> whereLamdba = (u) => u.IsDelete == false;
            //管理学校组织
            whereLamdba = whereLamdba.And(u => string.IsNullOrEmpty(u.parent_code));

            if (!string.IsNullOrEmpty(request.org_code))
                whereLamdba = whereLamdba.And(u => u.org_code.Contains(request.org_code));
            if (!string.IsNullOrEmpty(request.org_name))
                whereLamdba = whereLamdba.And(u => u.org_name.Contains(request.org_name));
            if (!string.IsNullOrEmpty(request.org_type))
                whereLamdba = whereLamdba.And(u => u.org_type.ToString().Contains(request.org_type));
            if (!string.IsNullOrEmpty(request.org_owner))
                whereLamdba = whereLamdba.And(u => u.org_owner.Contains(request.org_owner));

            var items = CurrentRepository.FindPageList(request.Page, request.PageSize, out int total, whereLamdba, request.OrderBy, request.IsAsc).ToList();
            Page<EOrg> page = new Page<EOrg>
            {
                TotalItems = total,
                CurrentPage = request.Page,
                Items = items,
                ItemsPerPage = request.PageSize
            };
            return page;
        }

        public Page<EOrg> GetSubOrgList(GetOrgRequest request)
        {
            Expression<Func<EOrg, bool>> whereLamdba = (u) => u.IsDelete == false;

            //获取本身以及子组织
            whereLamdba = whereLamdba.And(u => u.org_code.Equals(request.parent_code) || u.parent_code.Equals(request.parent_code));
            if (!string.IsNullOrEmpty(request.org_code))
                whereLamdba = whereLamdba.And(u => u.org_code.Contains(request.org_code));
            if (!string.IsNullOrEmpty(request.org_name))
                whereLamdba = whereLamdba.And(u => u.org_name.Contains(request.org_name));
            if (!string.IsNullOrEmpty(request.org_type))
                whereLamdba = whereLamdba.And(u => u.org_type.ToString().Contains(request.org_type));
            if (!string.IsNullOrEmpty(request.org_owner))
                whereLamdba = whereLamdba.And(u => u.org_owner.Contains(request.org_owner));

            var items = CurrentRepository.FindPageList(request.Page, request.PageSize, out int total, whereLamdba, request.OrderBy, request.IsAsc).ToList();
            Page<EOrg> page = new Page<EOrg>
            {
                TotalItems = total,
                CurrentPage = request.Page,
                Items = items,
                ItemsPerPage = request.PageSize
            };
            return page;
        }
    }
}
