//-------------------------------------------------------------------
//文件名称：RMenuUR.cs
//模块名称：RMenuUR业务逻辑接口层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearning.DAL;
using ELearning.Models;

namespace ELearning.BLL
{
    /// <summary>
    /// RMenuUR业务逻辑接口层
    /// </summary>
	public partial class RMenuURService : BaseService<RMenuUR>
	{
     public  RMenuURService() : base(RepositoryFactory.RMenuURRepository) { }
	}
}


