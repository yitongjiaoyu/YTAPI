//-------------------------------------------------------------------
//文件名称：TOrder.cs
//模块名称：TOrderModel层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ELearning.Models
{	
	 /// <summary>
    /// TOrder表实体类
    /// </summary>
	[Table("TOrder")]
	public partial class TOrder
    {
		
		/// <summary>
		/// Id
		/// </summary>
				[Key]
	[Display(Name = "Id")]
		public Guid Id { get; set; }
		
		/// <summary>
		/// 用户id
		/// </summary>
			[Display(Name = "用户id")]
		public Guid UserId { get; set; }
		
		/// <summary>
		/// 购买来源：1.后台直接添加，2.网页，3.安卓，4.IOS
		/// </summary>
			[Display(Name = "购买来源：1.后台直接添加，2.网页，3.安卓，4.IOS")]
		public int SourceType { get; set; }
		
		/// <summary>
		/// 订单编号
		/// </summary>
			[Display(Name = "订单编号")]
		public string OrderNumber { get; set; }
		
		/// <summary>
		/// 订单价格，原价
		/// </summary>
			[Display(Name = "订单价格，原价")]
		public double OrderPrice { get; set; }
		
		/// <summary>
		/// 实际价格，折扣价
		/// </summary>
			[Display(Name = "实际价格，折扣价")]
		public double RealPrice { get; set; }
		
		/// <summary>
		/// 实际付款价格
		/// </summary>
			[Display(Name = "实际付款价格")]
		public double PayPrice { get; set; }
		
		/// <summary>
		/// 订单状态：1.未支付，2.支付中，3.支付成功，4.支付失败
		/// </summary>
			[Display(Name = "订单状态：1.未支付，2.支付中，3.支付成功，4.支付失败")]
		public int Status { get; set; }
		
		/// <summary>
		/// 支付渠道：0.无，1.支付宝，2.微信，3.银行卡
		/// </summary>
			[Display(Name = "支付渠道：0.无，1.支付宝，2.微信，3.银行卡")]
		public int PayChannel { get; set; }
		
		/// <summary>
		/// 第三方商户编号，支付宝、微信、银联商户订单编号
		/// </summary>
			[Display(Name = "第三方商户编号，支付宝、微信、银联商户订单编号")]
		public string ThirdOrderNumber { get; set; }
		
		/// <summary>
		/// 创建时间
		/// </summary>
			[Display(Name = "创建时间")]
		public DateTime CreateTime { get; set; }
		
		/// <summary>
		/// 更新时间
		/// </summary>
			[Display(Name = "更新时间")]
		public DateTime UpdateTime { get; set; }
		
		/// <summary>
		/// 创建人Id
		/// </summary>
			[Display(Name = "创建人Id")]
		public Guid CreateUserId { get; set; }
		
		/// <summary>
		/// 编辑人id
		/// </summary>
			[Display(Name = "编辑人id")]
		public Guid UpdateUserId { get; set; }
		
		/// <summary>
		/// 是否删除
		/// </summary>
			[Display(Name = "是否删除")]
		public bool IsDelete { get; set; }
		   
    }
}

