﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.YTJYAdmin.ECommon
{
    /// <summary>
    /// 获取套餐列表参数
    /// </summary>
    public class AllPackageRequest
    {

    }

    /// <summary>
    /// 全部套餐列表
    /// </summary>
    public class AllPackageViewModel
    {
        /// <summary>
        /// 列表
        /// </summary>
        public List<AllPackageItem> Items { set; get; } = new List<AllPackageItem>();

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { set; get; }
    }

    public class AllPackageItem
    {
        /// <summary>
        /// 套餐id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }

}
