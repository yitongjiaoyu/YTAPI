﻿using System.Runtime.Serialization;

namespace ELearning.Models.ViewModel
{
    /// <summary>
    /// 获取数据列表接口基类
    /// </summary>
    public class BaseRequest
    {
        /// <summary>
        /// 页码数
        /// </summary>
        public int Page { set; get; }

        /// <summary>
        /// 每页大小
        /// </summary>
        public int PageSize { set; get; }

        /// <summary>
        /// 排序字段
        /// </summary>
        public string OrderBy { get; set; } = string.Empty;

        /// <summary>
        /// 排序（asc 升序,desc 降序）
        /// </summary>
        public string SortDir { get; set; } = string.Empty;

        /// <summary>
        /// 是否升序
        /// true：升序
        /// false：降序
        /// </summary>
        public bool IsAsc { get; set; }
    }
}
