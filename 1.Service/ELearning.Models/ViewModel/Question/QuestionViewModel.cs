﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.Question
{
    public class TQuestionRequest : BaseSearchRequest
    {
        /// <summary>
        /// 主题干，材料题，解析题等类型需要
        /// </summary>
        public string MainName { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { set; get; }

        /// <summary>
        /// 大类分类编码
        /// </summary>
        public string CategoryId { set; get; }

        /// <summary>
        /// 基础分类编码
        /// </summary>
        public string TypeCode { set; get; }
    }

    public class TQuestionViewModel
    {
        /// <summary>
        /// 课程列表
        /// </summary>
        public List<TQuestionShowItem> Items { set; get; } = new List<TQuestionShowItem>();

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { set; get; }
    }

    /// <summary>
    /// 题目元素
    /// </summary>
    public class TQuestionShowItem
    {
        [Display(Name = "Id")]
        public Guid Id { get; set; }

        /// <summary>
        /// 主题干，材料题，解析题等类型需要
        /// </summary>
        [Display(Name = "主题干，材料题，解析题等类型需要")]
        public string MainName { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 正确答案,例如：/id1/id2/
        /// </summary>
        [Display(Name = "正确答案,例如：/id1/id2/")]
        public string RightAnswer { get; set; }

        public List<string> RightAnswerArr
        {
            get
            {
                var arr = new List<string>();
                if (!string.IsNullOrEmpty(RightAnswer))
                    arr = RightAnswer.Split('/').ToList();

                return arr;
            }
        }

        /// <summary>
        /// 序号
        /// </summary>
        [Display(Name = "序号")]
        public double Sort { get; set; }

        /// <summary>
        /// 分数
        /// </summary>
        [Display(Name = "分数")]
        public double Grade { get; set; }

        /// <summary>
        /// 大类分类Code
        /// </summary>
        [Display(Name = "大类分类Code")]
        public string CategoryId { get; set; }

        /// <summary>
        /// 分类名称
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// 班级基础分类code,基础班，高级版
        /// </summary>
        [Display(Name = "班级基础分类code,基础班，高级版")]
        public string TypeCode { get; set; }

        /// <summary>
        /// 基础分类名称
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// 题目类型，关联类型EQuestionType表中code字段
        /// </summary>
        [Display(Name = "题目类型，关联类型EQuestionType表中code字段")]
        public string QuestionTypeCode { get; set; }

        /// <summary>
        /// 题目类型名称
        /// </summary>
        public string QuestionTypeName { get; set; }

        /// <summary>
        /// 组织编码
        /// </summary>
        [Display(Name = "组织编码")]
        public string OrgCode { get; set; }

        /// <summary>
        /// 题目解析
        /// </summary>
        [Display(Name = "题目解析")]
        public string Analysis { get; set; }

        /// <summary>
        /// 题目难度，1.简单，2.一般，3.困难
        /// </summary>
        [Display(Name = "题目难度，1.简单，2.一般，3.困难")]
        public int Difficulty { get; set; }

        public string DifficultyStr
        {
            get
            {
                var str = string.Empty;
                switch (Difficulty)
                {
                    case 1:
                        str = "简单";
                        break;
                    case 2:
                        str = "一般";
                        break;
                    case 3:
                        str = "困难";
                        break;
                }
                return str;
            }
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        [Display(Name = "更新时间")]
        public DateTime UpdateTime { get; set; }

        /// <summary>
        /// 创建人Id
        /// </summary>
        [Display(Name = "创建人Id")]
        public Guid CreateUserId { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 编辑人id
        /// </summary>
        [Display(Name = "编辑人id")]
        public Guid UpdateUserId { get; set; }

    }

    public class TQuestionPostModel
    {
        /// <summary>
        /// 题目id
        /// </summary>
        [Display(Name = "Id")]
        public Guid Id { get; set; }

        /// <summary>
        /// 子题目id
        /// </summary>
        public Guid QuestionItemId { get; set; }

        /// <summary>
        /// 主题干，材料题，解析题等类型需要
        /// </summary>
        [Display(Name = "主题干，材料题，解析题等类型需要")]
        public string MainName { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 正确答案,例如：/id1/id2/
        /// </summary>
        [Display(Name = "正确答案,例如：/id1/id2/")]
        public string RightAnswer { get; set; }

        /// <summary>
        /// 传递的数组
        /// </summary>
        public List<string> RightAnswerArr { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [Display(Name = "序号")]
        public double Sort { get; set; }

        /// <summary>
        /// 分数
        /// </summary>
        [Display(Name = "分数")]
        public double Grade { get; set; }

        /// <summary>
        /// 大类分类Code
        /// </summary>
        [Display(Name = "大类分类Code")]
        public string CategoryId { get; set; }

        /// <summary>
        /// 班级基础分类code,基础班，高级版
        /// </summary>
        [Display(Name = "班级基础分类code,基础班，高级版")]
        public string TypeCode { get; set; }

        /// <summary>
        /// 题目类型，关联类型EQuestionType表中code字段
        /// </summary>
        [Display(Name = "题目类型，关联类型EQuestionType表中code字段")]
        public string QuestionTypeCode { get; set; }
        
        /// <summary>
        /// 组织编码
        /// </summary>
        [Display(Name = "组织编码")]
        public string OrgCode { get; set; }

        /// <summary>
        /// 题目解析
        /// </summary>
        [Display(Name = "题目解析")]
        public string Analysis { get; set; }

        /// <summary>
        /// 题目难度，1.简单，2.一般，3.困难
        /// </summary>
        [Display(Name = "题目难度，1.简单，2.一般，3.困难")]
        public int Difficulty { get; set; }

        /// <summary>
        /// 选项列表
        /// </summary>
        public List<TOptionItem> Items { get; set; } = new List<TOptionItem>();

        /// <summary>
        /// 子题目列表
        /// </summary>
        public List<TQuestionItemModel> QueItems { get; set; } = new List<TQuestionItemModel>();

    }

    /// <summary>
    /// 子题目元素
    /// </summary>
    public class TQuestionItemModel
    {
        /// <summary>
        /// 子题目id
        /// </summary>
        [Display(Name = "Id")]
        public Guid Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }
        
        /// <summary>
        /// 序号
        /// </summary>
        [Display(Name = "序号")]
        public double Sort { get; set; }

        /// <summary>
        /// 分数
        /// </summary>
        [Display(Name = "分数")]
        public double Grade { get; set; }

        /// <summary>
        /// 大类分类Code
        /// </summary>
        [Display(Name = "大类分类Code")]
        public string CategoryId { get; set; }

        /// <summary>
        /// 班级基础分类code,基础班，高级版
        /// </summary>
        [Display(Name = "班级基础分类code,基础班，高级版")]
        public string TypeCode { get; set; }

        /// <summary>
        /// 题目类型，关联类型EQuestionType表中code字段
        /// </summary>
        [Display(Name = "题目类型，关联类型EQuestionType表中code字段")]
        public string QuestionTypeCode { get; set; }

        /// <summary>
        /// 组织编码
        /// </summary>
        [Display(Name = "组织编码")]
        public string OrgCode { get; set; }

        /// <summary>
        /// 题目答案，主观题，简答题等类型使用该字段
        /// </summary>
        [Display(Name = "题目答案，主观题，简答题等类型使用该字段")]
        public string Answer { get; set; }

        /// <summary>
        /// 题目解析
        /// </summary>
        [Display(Name = "题目解析")]
        public string Analysis { get; set; }

        /// <summary>
        /// 题目难度，1.简单，2.一般，3.困难
        /// </summary>
        [Display(Name = "题目难度，1.简单，2.一般，3.困难")]
        public int Difficulty { get; set; }

        /// <summary>
        /// 前提（配伍选择题用）
        /// </summary>
        [Display(Name = "前提（配伍选择题用）")]
        public string Precondition { get; set; }

        /// <summary>
        /// 选项列表
        /// </summary>
        public List<TOptionItem> Items { get; set; } = new List<TOptionItem>();

    }

    /// <summary>
    /// 选项元素
    /// </summary>
    public class TOptionItem
    {
        /// <summary>
        /// 选项Id
        /// </summary>
        public Guid OptionId { set; get; }

        /// <summary>
        /// 选项内容
        /// </summary>
        public string OptionName { set; get; }

        /// <summary>
        /// 序号
        /// </summary>
        public double OptionSort { set; get; }

        /// <summary>
        /// 是否是正确答案
        /// </summary>
        public bool IsRight { set; get; }
    }
}
