//-------------------------------------------------------------------
//文件名称：TRegisterLog.cs
//模块名称：TRegisterLog数据访问层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearning.Models;

namespace ELearning.DAL
{
    /// <summary>
    /// TRegisterLog数据访问层
    /// </summary>
	public partial class TRegisterLogRepository : BaseRepository<TRegisterLog>
	{
	
	}
}


