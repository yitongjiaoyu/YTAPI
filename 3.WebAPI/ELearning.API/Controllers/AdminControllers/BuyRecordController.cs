﻿using ELearning.BLL;
using ELearning.Common.Extensions;
using ELearning.Common.Model;
using ELearning.Models;
using ELearning.Models.ViewModel;
using ELearning.Models.ViewModel.Logs;
using System;
using System.Linq.Expressions;
using System.Web.Http;

namespace ELearning.API.Controllers.AdminControllers
{
    public class BuyRecordController : BaseApiController
    {
        protected EBuyRecordService buyService = new EBuyRecordService();
        protected EProductService productService = new EProductService();
        protected EOrgService orgService = new EOrgService();

        [HttpPost, Route("api/Buy/GetBuyRecordList")]
        public ServiceResponse<BuyRecordViewModel> GetBuyRecordList(GetBuyRequest request)
        {
            var viewModel = new BuyRecordViewModel();
            var buyPage =buyService.GetBuyRecordList(request);
            if (buyPage != null)
            {
                foreach (var buy in buyPage.Items)
                {
                    var u = new BuyRecordItem
                    {
                        ID = buy.ID,
                        OrderNo = buy.OrderNo,
                        CreateTime = buy.CreateTime,
                        Creater = buy.Creater,
                        ConfirmStatus = buy.ConfirmStatus,
                        Description = buy.Description,
                        IsPay = buy.IsPay,
                        Buyer = buy.Buyer,
                        ModifyTime = buy.ModifyTime,
                        OrgCode = buy.OrgCode,
                        OrgName = orgService.Find(w=>w.org_code==buy.OrgCode).org_name,
                        IsDelete = buy.IsDelete
                    };
                   if (!string.IsNullOrEmpty(request.OrgName))
                    {
                        if (u.OrgName.Contains(request.OrgName))
                        {
                            viewModel.BuyRecordItems.Add(u);
                        }
                    }
                    else {
                        viewModel.BuyRecordItems.Add(u);
                    }
                }
                viewModel.TotalCount = buyPage.TotalItems.ObjToInt();
            }
            return ServiceResponse<BuyRecordViewModel>.SuccessResponse("success", viewModel);
        }

        [HttpGet, Route("api/Buy/changeBuyStatus")]
        public ServiceResponse<bool> changeBuyStatus(string id,   string state = "stoped")
        { 
            var buy = buyService.Find(w=>w.ID.ToString()==id);
            if (buy != null)
            {
                buy.ConfirmStatus = buy.IsPay?1:2;
                buy.IsPay = !buy.IsPay;
                if (buy.IsPay)
                {
                    buy.PayDate = DateTime.Now;
                }
                bool b=  buyService.Update(buy);
                if (b)
                {
                    return ServiceResponse<bool>.SuccessResponse("操作成功", true);
                }
                else
                {
                    return ServiceResponse<bool>.SuccessResponse("操作失败", false);
                }
            }
            else
            {
            return ServiceResponse<bool>.SuccessResponse("查找失败!", false);
            }
        }
        
    }
}