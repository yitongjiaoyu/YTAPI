﻿using System;
using System.Runtime.Serialization;

namespace ELearning.Models
{
    public class BaseSearchRequest
    {
        private int _pageIndex;
        public int PageIndex
        {
            get
            {
                if (_pageIndex < 1)
                {
                    _pageIndex = 1;
                }
                return _pageIndex;
            }
            set
            {
                _pageIndex = value;
            }
        }

        public int PageSize { get; set; } = 20;

        /// <summary>
        /// 开始索引
        /// </summary>
        public int StartIndex => (PageIndex - 1) * PageSize + 1;

        /// <summary>
        /// 结束索引
        /// </summary>
        public int EndIndex => PageIndex * PageSize;
    }
}
