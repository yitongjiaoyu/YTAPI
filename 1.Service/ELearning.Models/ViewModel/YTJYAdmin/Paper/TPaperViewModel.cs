﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.YTJYAdmin.Paper
{
    /// <summary>
    /// 试卷列表请求参数
    /// </summary>
    public class TPaperRequest : BaseSearchRequest
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { set; get; }

        /// <summary>
        /// 大类分类编码
        /// </summary>
        public string CategoryId { set; get; }

        /// <summary>
        /// 基础分类编码
        /// </summary>
        public string TypeCode { set; get; }
    }

    public class TPaperViewModel
    {
        /// <summary>
        /// 列表
        /// </summary>
        public List<TPaperItem> Items { set; get; } = new List<TPaperItem>();

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { set; get; }
    }

    /// <summary>
    /// 返回给请求元素
    /// </summary>
    public class TPaperItem
    {
        [Display(Name = "Id")]
        public Guid Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 类型：1.真题，2.模拟题
        /// </summary>
        [Display(Name = "类型：1.真题，2.模拟题")]
        public int Type { get; set; }

        public string TypeStr
        {
            get
            {
                var str = "真题";
                if (Type == 2)
                    str = "模拟题";
                return str;
            }
        }

        /// <summary>
        /// 组织编码
        /// </summary>
        [Display(Name = "组织编码")]
        public string OrgCode { get; set; }

        /// <summary>
        /// 题目总数量
        /// </summary>
        [Display(Name = "题目总数量")]
        public int TotalCount { get; set; }

        /// <summary>
        /// 总分数
        /// </summary>
        [Display(Name = "总分数")]
        public double TotalScore { get; set; }

        /// <summary>
        /// 考试时长，单位：分钟
        /// </summary>
        [Display(Name = "考试时长，单位：分钟")]
        public int TimeSpan { get; set; }

        /// <summary>
        /// 大类分类Code
        /// </summary>
        [Display(Name = "大类分类Code")]
        public string CategoryId { get; set; }

        /// <summary>
        /// 大类分类名称
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// 班级基础分类code,基础班，高级版
        /// </summary>
        [Display(Name = "班级基础分类code,基础班，高级版")]
        public string TypeCode { get; set; }

        /// <summary>
        /// 班级基础分类名称
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// 年份
        /// </summary>
        [Display(Name = "年份")]
        public int Year { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [Display(Name = "序号")]
        public double Sort { get; set; }
    }

    /// <summary>
    /// 数据库映射实体
    /// </summary>
    public class TPaperModel
    {
        [Display(Name = "Id")]
        public Guid Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 类型：1.真题，2.模拟题
        /// </summary>
        [Display(Name = "类型：1.真题，2.模拟题")]
        public int Type { get; set; }

        /// <summary>
        /// 组织编码
        /// </summary>
        [Display(Name = "组织编码")]
        public string OrgCode { get; set; }

        /// <summary>
        /// 题目总数量
        /// </summary>
        [Display(Name = "题目总数量")]
        public int TotalCount { get; set; }

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int Total { get; set; }

        /// <summary>
        /// 总分数
        /// </summary>
        [Display(Name = "总分数")]
        public double TotalScore { get; set; }

        /// <summary>
        /// 考试时长，单位：分钟
        /// </summary>
        [Display(Name = "考试时长，单位：分钟")]
        public int TimeSpan { get; set; }

        /// <summary>
        /// 大类分类Code
        /// </summary>
        [Display(Name = "大类分类Code")]
        public string CategoryId { get; set; }

        /// <summary>
        /// 大类分类名称
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// 班级基础分类code,基础班，高级版
        /// </summary>
        [Display(Name = "班级基础分类code,基础班，高级版")]
        public string TypeCode { get; set; }

        /// <summary>
        /// 班级基础分类名称
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// 年份
        /// </summary>
        [Display(Name = "年份")]
        public int Year { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [Display(Name = "序号")]
        public double Sort { get; set; }
    }

    /// <summary>
    /// 保存试卷信息实体
    /// </summary>
    public class TPaperPostModel
    {
        [Display(Name = "Id")]
        public Guid Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 类型：1.真题，2.模拟题
        /// </summary>
        [Display(Name = "类型：1.真题，2.模拟题")]
        public int Type { get; set; }

        /// <summary>
        /// 组织编码
        /// </summary>
        [Display(Name = "组织编码")]
        public string OrgCode { get; set; }

        /// <summary>
        /// 题目总数量
        /// </summary>
        [Display(Name = "题目总数量")]
        public int TotalCount { get; set; }

        /// <summary>
        /// 总分数
        /// </summary>
        [Display(Name = "总分数")]
        public double TotalScore { get; set; }

        /// <summary>
        /// 考试时长，单位：分钟
        /// </summary>
        [Display(Name = "考试时长，单位：分钟")]
        public int TimeSpan { get; set; }

        /// <summary>
        /// 大类分类Code
        /// </summary>
        [Display(Name = "大类分类Code")]
        public string CategoryId { get; set; }

        /// <summary>
        /// 班级基础分类code,基础班，高级版
        /// </summary>
        [Display(Name = "班级基础分类code,基础班，高级版")]
        public string TypeCode { get; set; }

        /// <summary>
        /// 年份
        /// </summary>
        [Display(Name = "年份")]
        public int Year { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [Display(Name = "序号")]
        public double Sort { get; set; }
    }
}
