﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.Fawvw.Infrastructure
{
    /// <summary>
    /// 获取位置信息
    /// </summary>
    public static class Location
    {
        /// <summary>
        /// 百度Api服务
        /// </summary>
        private const string url = "http://api.map.baidu.com/location/ip?ak=plrxFyaFhXQ3yjDQxb3lr9qL&ip=";

        /// <summary>
        /// 获取位置信息
        /// </summary>
        /// <param name="IP">IP地址</param>
        /// <returns></returns>
        public static AddressInfo GetLocationByIP(string IP)
        {
            try
            {
                string strResult = string.Empty;
                if (!string.IsNullOrEmpty(IP))
                {
                    strResult = ServiceApi.GetWebApiResult(url + IP);
                }
                return JsonConvert.DeserializeObject<AddressInfo>(strResult);
            }
            catch (Exception ex)
            {
                //log ex
                return null;
            }
        }

    }
    /// <summary>
    /// 地址信息
    /// </summary>
    public class AddressInfo
    {
        /// <summary>
        /// 地址信息
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 详细内容
        /// </summary>
        public ContentInfo Content { get; set; }

    }
    /// <summary>
    /// 内容
    /// </summary>
    public class ContentInfo
    {
        /// <summary>
        /// 详细地址信息
        /// </summary>
        public AddressDetailInfo Address_Detail { get; set; }
        /// <summary>
        /// 百度经纬度坐标值
        /// </summary>
        public PointInfo Point { get; set; }
        /// <summary>
        /// 简要地址
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 返回状态码
        /// </summary>
        public int Status { get; set; }
    }
    /// <summary>
    /// 百度经纬度坐标值
    /// </summary>
    public class PointInfo
    {
        /// <summary>
        /// X
        /// </summary>
        public string X { get; set; }
        /// <summary>
        /// Y
        /// </summary>
        public string Y { get; set; }
    }
    /// <summary>
    /// 详细地址信息
    /// </summary>
    public class AddressDetailInfo
    {
        /// <summary>
        /// 省份
        /// </summary>
        public string Province { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// 区县
        /// </summary>
        public string District { get; set; }
        /// <summary>
        /// 街道
        /// </summary>
        public string Street { get; set; }
        /// <summary>
        /// 门址
        /// </summary>
        public string Street_Number { get; set; }
        /// <summary>
        /// 百度城市代码
        /// </summary>
        public string City_Code { get; set; }
    }

}
