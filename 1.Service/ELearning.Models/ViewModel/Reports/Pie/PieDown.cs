﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel
{
    /// <summary>
    /// 可下钻的饼图
    /// </summary>
    public class PieDown
    {
        /// <summary>
        /// 下钻ID
        /// </summary>
        public string drilldown { get; set; } = string.Empty;
        /// <summary>
        /// 当前节点名称-显示名称
        /// </summary>
        public string name { get; set; } = string.Empty;
        /// <summary>
        /// 当前节点数据-百分比*100
        /// </summary>
        public double y { get; set; } = 0.00;
        ///// <summary>
        ///// 切片状态
        ///// </summary>
        //public bool sliced { get; set; } = false;
        ///// <summary>
        ///// 选中状态
        ///// </summary>
        //public bool selected { get; set; } = false;
    }

    /// <summary>
    /// 下钻视图
    /// </summary>
    public class Drilldown
    {
        /// <summary>
        /// 下钻ID
        /// </summary>
        public string id { get; set; } = string.Empty;
        /// <summary>
        /// 当前节点名称-显示名称
        /// </summary>
        public string name { get; set; } = string.Empty;
        /// <summary>
        /// 当前节点数据-百分比*100
        /// </summary>
        public List<object[]> data { get; set; } = new List<object[]>();
    }
}
