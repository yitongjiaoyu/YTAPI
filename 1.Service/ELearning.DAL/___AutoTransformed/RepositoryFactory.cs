﻿
//------------------------------------------------------------------
//文件名称：RepositoryFactory
//模块名称：工厂Factory
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ELearning.DAL
{
    public static partial class RepositoryFactory
    {
	 
 public static EApplicationLogRepository EApplicationLogRepository { get { return new EApplicationLogRepository(); } }
   
 public static EAppVersionRepository EAppVersionRepository { get { return new EAppVersionRepository(); } }
   
 public static ECategoryRepository ECategoryRepository { get { return new ECategoryRepository(); } }
   
 public static ECourseRepository ECourseRepository { get { return new ECourseRepository(); } }
   
 public static EDictionaryRepository EDictionaryRepository { get { return new EDictionaryRepository(); } }
   
 public static EFeedbackRepository EFeedbackRepository { get { return new EFeedbackRepository(); } }
   
 public static ELoginUserRepository ELoginUserRepository { get { return new ELoginUserRepository(); } }
   
 public static EMenuRepository EMenuRepository { get { return new EMenuRepository(); } }
   
 public static EOrderRepository EOrderRepository { get { return new EOrderRepository(); } }
   
 public static EOrgRepository EOrgRepository { get { return new EOrgRepository(); } }
   
 public static EPackageRepository EPackageRepository { get { return new EPackageRepository(); } }
   
 public static EPayLogRepository EPayLogRepository { get { return new EPayLogRepository(); } }
   
 public static ERoleRepository ERoleRepository { get { return new ERoleRepository(); } }
   
 public static ESysUserRepository ESysUserRepository { get { return new ESysUserRepository(); } }
   
 public static EVideoLibRepository EVideoLibRepository { get { return new EVideoLibRepository(); } }
   
 public static EVideoStudyLogRepository EVideoStudyLogRepository { get { return new EVideoStudyLogRepository(); } }
   
 public static EVideoStudySumRepository EVideoStudySumRepository { get { return new EVideoStudySumRepository(); } }
   
 public static RChapterPaperRepository RChapterPaperRepository { get { return new RChapterPaperRepository(); } }
   
 public static RChapterVideoLibRepository RChapterVideoLibRepository { get { return new RChapterVideoLibRepository(); } }
   
 public static RCoursePaperRepository RCoursePaperRepository { get { return new RCoursePaperRepository(); } }
   
 public static RCourseVideoLibRepository RCourseVideoLibRepository { get { return new RCourseVideoLibRepository(); } }
   
 public static RMenuURRepository RMenuURRepository { get { return new RMenuURRepository(); } }
   
 public static RPackageCourseRepository RPackageCourseRepository { get { return new RPackageCourseRepository(); } }
   
 public static RQuestionOptionRepository RQuestionOptionRepository { get { return new RQuestionOptionRepository(); } }
   
 public static RRoleUserRepository RRoleUserRepository { get { return new RRoleUserRepository(); } }
   
 public static TBannerRepository TBannerRepository { get { return new TBannerRepository(); } }
   
 public static TChapterRepository TChapterRepository { get { return new TChapterRepository(); } }
   
 public static TInviteCodeLogRepository TInviteCodeLogRepository { get { return new TInviteCodeLogRepository(); } }
   
 public static TLoginTokenRepository TLoginTokenRepository { get { return new TLoginTokenRepository(); } }
   
 public static TOptionRepository TOptionRepository { get { return new TOptionRepository(); } }
   
 public static TOrderRepository TOrderRepository { get { return new TOrderRepository(); } }
   
 public static TOrgAccountLogRepository TOrgAccountLogRepository { get { return new TOrgAccountLogRepository(); } }
   
 public static TPaperRepository TPaperRepository { get { return new TPaperRepository(); } }
   
 public static TPaperOptionTestHistoryRepository TPaperOptionTestHistoryRepository { get { return new TPaperOptionTestHistoryRepository(); } }
   
 public static TPaperQuestionRepository TPaperQuestionRepository { get { return new TPaperQuestionRepository(); } }
   
 public static TPaperQuestionTestHistoryRepository TPaperQuestionTestHistoryRepository { get { return new TPaperQuestionTestHistoryRepository(); } }
   
 public static TPaperTestHistoryRepository TPaperTestHistoryRepository { get { return new TPaperTestHistoryRepository(); } }
   
 public static TQuestionRepository TQuestionRepository { get { return new TQuestionRepository(); } }
   
 public static TQuestionItemRepository TQuestionItemRepository { get { return new TQuestionItemRepository(); } }
   
 public static TQuestionTypeRepository TQuestionTypeRepository { get { return new TQuestionTypeRepository(); } }
   
 public static TRegisterLogRepository TRegisterLogRepository { get { return new TRegisterLogRepository(); } }
   
 public static TSendMsgLogRepository TSendMsgLogRepository { get { return new TSendMsgLogRepository(); } }
   
 public static TTencentLogRepository TTencentLogRepository { get { return new TTencentLogRepository(); } }
   
 public static TUserCollectQuestionRepository TUserCollectQuestionRepository { get { return new TUserCollectQuestionRepository(); } }
   
 public static TUserErrorQuestionRepository TUserErrorQuestionRepository { get { return new TUserErrorQuestionRepository(); } }
   
 public static TUserPackageRepository TUserPackageRepository { get { return new TUserPackageRepository(); } }
   
 public static TVideoLiveRepository TVideoLiveRepository { get { return new TVideoLiveRepository(); } }
      }
}

