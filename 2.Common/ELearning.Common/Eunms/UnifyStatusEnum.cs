﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Common
{
    /// <summary>
    /// 平台公共业务状态
    /// </summary>
    public enum UnifyStatusEnum
    {
        /// <summary>
        /// 草稿
        /// </summary>
        [Description("草稿")]
        Draft = 1,
        /// <summary>
        /// 保存
        /// </summary>
        [Description("保存")]
        Save = 2,
        /// <summary>
        /// 提交
        /// </summary>
        [Description("提交")]
        Submit = 3,
        /// <summary>
        /// 发布
        /// </summary>
        [Description("发布")]
        Publish = 4,
        /// <summary>
        /// 审核中
        /// </summary>
        [Description("审核中")]
        CheckIng = 11,
        /// <summary>
        /// 审核通过
        /// </summary>
        [Description("审核通过")]
        CheckY = 22,
        /// <summary>
        /// 审核不通过
        /// </summary>
        [Description("审核不通过")]
        CheckN = 33,

    }
}
