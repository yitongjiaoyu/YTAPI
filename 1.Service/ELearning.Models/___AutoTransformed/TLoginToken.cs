//-------------------------------------------------------------------
//文件名称：TLoginToken.cs
//模块名称：TLoginTokenModel层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ELearning.Models
{	
	 /// <summary>
    /// TLoginToken表实体类
    /// </summary>
	[Table("TLoginToken")]
	public partial class TLoginToken
    {
		
		/// <summary>
		/// Id
		/// </summary>
				[Key]
	[Display(Name = "Id")]
		public Guid Id { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Token { get; set; }
		
		/// <summary>
		/// 当前登录人Id
		/// </summary>
			[Display(Name = "当前登录人Id")]
		public Guid UserId { get; set; }
		
		/// <summary>
		/// 过期时间
		/// </summary>
			[Display(Name = "过期时间")]
		public DateTime PassDateTime { get; set; }
		
		/// <summary>
		/// 创建时间
		/// </summary>
			[Display(Name = "创建时间")]
		public DateTime CreateTime { get; set; }
		
		/// <summary>
		/// 更新时间
		/// </summary>
			[Display(Name = "更新时间")]
		public DateTime UpdateTime { get; set; }
		
		/// <summary>
		/// 创建人Id
		/// </summary>
			[Display(Name = "创建人Id")]
		public Guid CreateUserId { get; set; }
		
		/// <summary>
		/// 编辑人id
		/// </summary>
			[Display(Name = "编辑人id")]
		public Guid UpdateUserId { get; set; }
		
		/// <summary>
		/// 是否删除
		/// </summary>
			[Display(Name = "是否删除")]
		public bool IsDelete { get; set; }
		
		/// <summary>
		/// 来源：1.web，2.admin后台
		/// </summary>
			[Display(Name = "来源：1.web，2.admin后台")]
		public int Source { get; set; }
		   
    }
}

