//-------------------------------------------------------------------
//文件名称：EVideoStudySum.cs
//模块名称：EVideoStudySumModel层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ELearning.Models
{	
	 /// <summary>
    /// EVideoStudySum表实体类
    /// </summary>
	[Table("EVideoStudySum")]
	public partial class EVideoStudySum
    {
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public Guid Id { get; set; }
		
		/// <summary>
		/// 套餐id
		/// </summary>
			[Display(Name = "套餐id")]
		public Guid PackageId { get; set; }
		
		/// <summary>
		/// 课程ID
		/// </summary>
			[Display(Name = "课程ID")]
		public Guid CourseId { get; set; }
		
		/// <summary>
		/// 章节id
		/// </summary>
			[Display(Name = "章节id")]
		public Guid ChapterId { get; set; }
		
		/// <summary>
		/// 视频id
		/// </summary>
			[Display(Name = "视频id")]
		public Guid VideoId { get; set; }
		
		/// <summary>
		/// 学习时长，单位：秒
		/// </summary>
			[Display(Name = "学习时长，单位：秒")]
		public int TimeSpan { get; set; }
		
		/// <summary>
		/// 用户id
		/// </summary>
			[Display(Name = "用户id")]
		public Guid UserId { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string OrgCode { get; set; }
		
		/// <summary>
		/// 上次学习到的位置，上次学习到第几秒
		/// </summary>
			[Display(Name = "上次学习到的位置，上次学习到第几秒")]
		public int LastStudyTime { get; set; }
		
		/// <summary>
		/// 是否学习完成
		/// </summary>
			[Display(Name = "是否学习完成")]
		public bool IsFinish { get; set; }
		
		/// <summary>
		/// 创建时间
		/// </summary>
			[Display(Name = "创建时间")]
		public DateTime CreateTime { get; set; }
		
		/// <summary>
		/// 更新时间
		/// </summary>
			[Display(Name = "更新时间")]
		public DateTime UpdateTime { get; set; }
		   
    }
}

