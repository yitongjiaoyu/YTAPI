﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security;

namespace ELearning.Common
{
    public static class RSAHelper
    {
        /// <summary>  
        /// 加密  
        /// </summary>  
        /// <param name="publickey">公钥</param>  
        /// <param name="content">所加密的内容</param>  
        /// <returns>加密后的内容</returns>  
        static public string RSAEncrypt(string publickey, string content)
        {
            publickey = RSAUtil.RSAPublicKeyJava2DotNet(publickey);
            string sign = RSAUtil.RSAEncryptMore(publickey, content);
            return sign;
        }

        /// <summary>  
        /// 解密  
        /// </summary>  
        /// <param name="privatekey">私钥</param>  
        /// <param name="content">加密后的内容</param>  
        /// <returns>解密后的内容</returns>  
        static public string RSADecrypt(string privatekey, string content)
        {
            string data = RSAUtil.DecryptByPublicKey(content, privatekey);
            return data;
        }

    }
}
