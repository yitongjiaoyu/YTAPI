﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ELearning.BLL;
using ELearning.Common.Azure;
using ELearning.Common.CommonStr;
using ELearning.Common.Extensions;
using ELearning.Common.Model;
using ELearning.Models.ViewModel.News;
using ELearning.Common;
using ELearning.Common.Http;
using RestSharp;
using static Microsoft.Graph.CoreConstants;

namespace ELearning.API.Controllers.AdminControllers
{
    /// <summary>
    /// 新增相关功能
    /// </summary>
    public class NewsController : BaseApiController
    {
        /// <summary>
        /// 新增操作service
        /// </summary>
        private ENewsService NewsService => new ENewsService();

        /// <summary>
        /// 消息与平台code管理操作service
        /// </summary>
        private RNewsPlatfromService rNewsPlatfromService = new RNewsPlatfromService();

        private RNewsImageService rNewsImageService = new RNewsImageService();

        /// <summary>
        /// 获取新闻列表
        /// </summary>
        [HttpPost, Route("api/news/GetNewsList")]
        public object GetNewsList(NewsRequest request)
        {
            var viewModel = new NewsViewModel();
            var news = NewsService.GetNewsPageList(request);
            if (news != null)
            {
                viewModel.TotalCount = news.TotalItems.ObjToInt();
                var newsIds = news.Items.Select(n => n.Id).ToList();
                var newsTypeList = NewsService.GetNewsTypes(newsIds);
                var rnewPlatCodes = rNewsPlatfromService.FindList(r => newsIds.Contains(r.NewsId)).ToList();
                var rnewimages = rNewsImageService.FindList(r => newsIds.Contains(r.NewsID)).ToList();
                foreach (var item in news.Items)
                {
                    var n = new NewsItem
                    {
                        Id = item.Id,
                        Code = item.Code,
                        CreateTime = item.CreateTime.ToString("yyyy-MM-dd HH:mm"),
                        Creater = item.Creater,
                        IsStick = item.IsStick.ToString("yyyy-MM-dd HH:mm"),//item.IsStick.Equals(new DateTime(1900, 1, 2, 0, 0, 0)) ? "否" : "是",
                        ModifyTime = item.ModifyTime.ToString("yyyy-MM-dd HH:mm"),
                        Modify = item.Modifyer,
                        ReadCount = item.ReadCount,
                        ReleaseTime = item.ReleaseTime.ToString("yyyy-MM-dd HH:mm"),
                        Title = item.Title,
                        Text = item.Text,
                        IsSingle=item.IsSingle,
                        Signature = item.Signature,
                        Sort = item.Sort,
                        SourceUrl = item.SourceURL,
                        States = item.States,
                        SubTitle = item.SubTitle,
                        URLType = item.URLType,
                        HttpURL = item.HttpURL,
                        PlatformCodeList = rnewPlatCodes.Where(r => r.NewsId == item.Id).Select(r => r.PlatformCode).ToList()
                    };
                    var newsTypeIds = newsTypeList.Where(r => r.NewsId == n.Id).Select(r => r.NewsTypeId).ToList();
                    n.NewsTypeIdList.AddRange(newsTypeIds);

                    var sourseurl = rnewimages.Where(w => w.NewsID == n.Id).OrderBy(w=>w.Sort).Select(w => w.SourceURL).ToList();
                    n.SourceURLList.AddRange(sourseurl);
                    viewModel.NewsItems.Add(n);
                }
            }

            return ServiceResponse<NewsViewModel>.SuccessResponse(CommonConst.OprateSuccessStr, viewModel);
        }

        /// <summary>
        /// 添加新闻
        /// </summary>
        [HttpPost, Route("api/news/AddNews")]
        public object AddNews(NewsItem viewModel)
        {
            viewModel.Creater = CurrentUserModel.LoginName;
            viewModel.Modify = CurrentUserModel.LoginName;
            NewsService.AddNews(viewModel);
            return ServiceResponse<NewsItem>.SuccessResponse(CommonConst.OprateSuccessStr, viewModel);
        }

        /// <summary>
        /// 更新新闻
        /// </summary>
        [HttpPost, Route("api/news/UpdateNews")]
        public object UpdateNews(NewsItem viewModel)
        {
            //SendMsg("B110002", "测试发送消息", "B1100021363");
            viewModel.Modify = CurrentUserModel.LoginName;
            NewsService.UpdateNews(viewModel);
            return ServiceResponse<NewsItem>.SuccessResponse(CommonConst.OprateSuccessStr, viewModel);
        }

        /// <summary>
        /// 删除新闻
        /// </summary>
        /// <param name="id">新闻id</param>
        [HttpPost, Route("api/news/DeleteNews")]
        public object DeleteNews(Guid id)
        {
            var news = NewsService.GetNews(id);
            if (news != null)
            {
                news.IsDelete = true;
                NewsService.Update(news);
            }
            return ServiceResponse<int>.SuccessResponse(CommonConst.OprateSuccessStr, 1);
        }

        /// <summary>
        /// 发布新闻
        /// </summary>
        [HttpPost, Route("api/news/PublishNews")]
        public object PublishNews(Guid id, int status)
        {
            var news = NewsService.GetNews(id);
            if (news != null)
            {
                news.States = status;
                if (status == 1)
                    news.ReleaseTime = DateTime.Now;
                NewsService.Update(news);
            }
            return ServiceResponse<int>.SuccessResponse(CommonConst.OprateSuccessStr, 1);
        }

        /// <summary>
        /// 置顶和取消置顶
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpPost, Route("api/news/TopModel")]
        public object TopModel(Guid id, int status)
        {
            var b = false;
            var news = NewsService.GetNews(id);
            if (news != null)
            {
                if (status == 1)
                {
                    news.IsStick = DateTime.Now;
                }
                else
                {
                    news.IsStick = new DateTime(2000,1,1);
                }
                    news.ReleaseTime = DateTime.Now;
                b=NewsService.Update(news);
            }
            return ServiceResponse<bool>.SuccessResponse(CommonConst.OprateSuccessStr, b);
        }
    }
}