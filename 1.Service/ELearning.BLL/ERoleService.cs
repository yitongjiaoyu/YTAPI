//-------------------------------------------------------------------
//文件名称：ERole.cs
//模块名称：ERole业务逻辑接口层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//修改时间：2019年6月20日
//-----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using ELearning.Common.Extensions;
using ELearning.Common.Model;
using ELearning.DAL;
using ELearning.Models;
using ELearning.Models.ViewModel.Role;

namespace ELearning.BLL
{
    /// <summary>
    /// ERole业务逻辑接口层
    /// </summary>
	public partial class ERoleService : BaseService<ERole>
	{
	    /// <summary>
	    /// 获取角色下面的子节点
	    /// </summary>
	    /// <param name="roleId">角色id</param>
	    public List<ERole> GetRoleSubList(Guid roleId)
	    {
	        return RepositoryFactory.ERoleRepository.GetRoleSubList(roleId);
	    }

	    /// <summary>
	    /// 获取角色的父级节点
	    /// </summary>
	    /// <param name="roleId">角色id</param>
        public List<ERole> GetRoleParentList(Guid roleId)
	    {
	        return RepositoryFactory.ERoleRepository.GetRoleParentList(roleId);
	    }

        /// <summary>
        /// 获取角色下面的用户列表
        /// </summary>
	    public List<SysUserModel> GetRoleUserList(GetRoleUserRequest request)
	    {
	        return RepositoryFactory.ERoleRepository.GetRoleUserList(request);
        }

	    /// <summary>
	    /// 获取角色不关联的用户列表
	    /// </summary>
	    public List<SysUserModel> GetBatchRoleUserList(GetRoleUserRequest request)
	    {
	        return RepositoryFactory.ERoleRepository.GetBatchRoleUserList(request);
        }
    }
}


