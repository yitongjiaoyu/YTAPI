﻿

using System;
using System.Collections.Generic;
using ELearning.Models;

namespace ELearning.Common.Model
{
    /// <summary>
    ///    控制请求的返回数据
    /// </summary>
    /// <typeparam name="T">实际请求的数据的类型</typeparam>
    public class Page<T>
    {
        /// <summary>
        ///     当前页
        /// </summary>
        public long CurrentPage { get; set; }

        /// <summary>
        ///     数据总页数
        /// </summary>
        public long TotalPages
        {
            get
            {
                if (ItemsPerPage <= 0)
                    ItemsPerPage = 1;
                var tp = (TotalItems + ItemsPerPage - 1) / ItemsPerPage;
                return tp;
            }
        }

        /// <summary>
        ///     总记录数量
        /// </summary>
        public long TotalItems { get; set; }
        /// <summary>
        /// Gets the page count.
        /// </summary>
        /// <value>
        /// The page count.
        /// </value>
        public long PageCount
        {
            get
            {
                if (ItemsPerPage < 1)
                {
                    ItemsPerPage = 1;
                }
                return TotalItems % ItemsPerPage == 0 ? TotalItems / ItemsPerPage : (TotalItems / ItemsPerPage) + 1;
            }
        }
        /// <summary>
        ///     每一页的数量
        /// </summary>
        public long ItemsPerPage { get; set; }

        /// <summary>
        ///  本页实际记录
        /// </summary>
        public List<T> Items { get; set; }

    }
}
