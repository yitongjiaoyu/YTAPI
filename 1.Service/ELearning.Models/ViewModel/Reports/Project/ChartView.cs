﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel
{
    public class ChartView
    {
        /// <summary>
        /// 图例
        /// </summary>
        public List<string> legend { get; set; } = new List<string>();
        /// <summary>
        /// series
        /// </summary>
        public List<ProjectSeries> series { get; set; } = new List<ProjectSeries>();
    }
    public class ProjectSeries
    {
        public string name { get; set; } = string.Empty;
        public List<double> data { get; set; } = new List<double>();
        public string stack { get; set; } = string.Empty;
    }
}
