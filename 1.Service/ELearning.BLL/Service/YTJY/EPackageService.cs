//-------------------------------------------------------------------
//文件名称：EPackage.cs
//模块名称：EPackage业务逻辑接口层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearning.Common.Extensions;
using ELearning.Common.Helpers;
using ELearning.DAL;
using ELearning.Models;
using ELearning.Models.ViewModel.YTJYAdmin.Course;
using ELearning.Models.ViewModel.YTJYAdmin.Package;

namespace ELearning.BLL
{
    /// <summary>
    /// EPackage业务逻辑接口层
    /// </summary>
	public partial class EPackageService : BaseService<EPackage>
    {
        /// <summary>
        /// 获取套餐列表
        /// </summary>
        public PackageViewModel GetModelList(PackageRequest request)
        {
            var models = RepositoryFactory.EPackageRepository.GetModelList(request);
            var viewModel = new PackageViewModel { TotalCount = models.FirstOrDefault()?.TotalCount ?? 0 };
            foreach (var m in models)
            {
                var v = m.MapTo<PackageItem, PackageModel>();
                v.ShowCoverUrl = PictureHelper.GetFileFullPath(v.CoverUrl);
                viewModel.Items.Add(v);
            }
            return viewModel;
        }

        /// <summary>
        /// 获取套餐课程关联或者不关联列表
        /// </summary>
        public PackageCourseViewModel GetPackageCourseList(PackageCourseRequest request)
        {
            var models = RepositoryFactory.EPackageRepository.GetPackageCourseList(request);
            var viewModel = new PackageCourseViewModel { TotalCount = models.FirstOrDefault()?.TotalCount ?? 0 };
            foreach (var m in models)
            {
                var v = m.MapTo<PackageCourseItem, PackageCourseModel>();
                viewModel.Items.Add(v);
            }
            return viewModel;
        }

        /// <summary>
        /// 获取套餐用户关联或者不关联列表
        /// </summary>
        public PackageUserViewModel GetPackageUserList(PackageUserRequest request)
        {
            var models = RepositoryFactory.EPackageRepository.GetPackageUserList(request);
            var viewModel = new PackageUserViewModel { TotalCount = models.FirstOrDefault()?.TotalCount ?? 0 };
            foreach (var m in models)
            {
                var v = m.MapTo<PackageUserItem, PackageUserModel>();
                viewModel.Items.Add(v);
            }
            return viewModel;
        }

        /// <summary>
        /// 获取套餐列表
        /// </summary>
        public SitePackageViewModel GetSiteModelList(SitePackageRequest request, Guid userId)
        {
            var models = RepositoryFactory.EPackageRepository.GetSiteModelList(request, userId);
            var viewModel = new SitePackageViewModel { TotalCount = models.FirstOrDefault()?.TotalCount ?? 0 };
            foreach (var m in models)
            {
                var v = m.MapTo<SitePackageItem, SitePackageModel>();
                v.IsBuy = m.BuyCount > 0;
                v.ShowCoverUrl = PictureHelper.GetFileFullPath(v.CoverUrl);
                viewModel.Items.Add(v);
            }
            return viewModel;
        }

        /// <summary>
        /// 获取套餐列表
        /// </summary>
        public UserPackageViewModel GetUserModelList(Guid userId)
        {
            var models = RepositoryFactory.EPackageRepository.GetUserModelList(userId);
            var viewModel = new UserPackageViewModel { TotalCount = models.Count };
            foreach (var m in models)
            {
                var v = m.MapTo<UserPackageItem, UserPackageModel>();
                v.ShowCoverUrl = PictureHelper.GetFileFullPath(v.CoverUrl);
                viewModel.Items.Add(v);
            }
            return viewModel;
        }
    }
}


