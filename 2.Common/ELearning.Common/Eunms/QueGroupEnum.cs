﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Common
{
    public enum QueGroupEnum
    {

        /// <summary>
        /// 考试题库
        /// </summary>
        [Description("考试题库")]
        Exam = 1,
        /// <summary>
        /// 问卷题库
        /// </summary>
        [Description("问卷题库")]
        Qna = 2,

    }
}
