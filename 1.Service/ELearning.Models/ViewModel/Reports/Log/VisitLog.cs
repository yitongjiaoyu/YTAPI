﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel
{
    /// <summary>
    /// 系统功能模块访问日志
    /// </summary>
    public class VisitLog
    {
        /// <summary>
        /// 模块Code
        /// </summary>
        public string ModelCode { get; set; } = string.Empty;
        /// <summary>
        /// 模块名称
        /// </summary>
        public string ModelName { get; set; } = string.Empty;
        /// <summary>
        /// 访问来源
        /// </summary>
        public string VisitSource { get; set; } = string.Empty;
        /// <summary>
        /// 访问来源
        /// </summary>
        public int Total { get; set; } = 0;
    }
}
