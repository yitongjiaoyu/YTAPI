//-------------------------------------------------------------------
//文件名称：TVideoLive.cs
//模块名称：TVideoLive业务逻辑接口层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearning.DAL;
using ELearning.Models;

namespace ELearning.BLL
{
    /// <summary>
    /// TVideoLive业务逻辑接口层
    /// </summary>
	public partial class TVideoLiveService : BaseService<TVideoLive>
	{
     public  TVideoLiveService() : base(RepositoryFactory.TVideoLiveRepository) { }
	}
}


