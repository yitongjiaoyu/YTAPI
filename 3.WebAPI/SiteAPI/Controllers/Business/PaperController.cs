﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Http;
using ELearning.BLL;
using ELearning.Common.CommonStr;
using ELearning.Common.Extensions;
using ELearning.Common.Helpers;
using ELearning.Common.Model;
using ELearning.Models;
using ELearning.Models.ViewModel.YTJYAdmin.Package;
using ELearning.Models.ViewModel.YTJYAdmin.Paper;
using ELearning.Models.ViewModel.YTJYAdmin.Study;
using SiteAPI.Controllers.Common;

namespace SiteAPI.Controllers.Business
{
    /// <summary>
    /// 试卷相关控制器
    /// </summary>
    public class PaperController : BaseApiController
    {
        /// <summary>
        /// 试卷相关操作service
        /// </summary>
        private TPaperService tPaperService => new TPaperService();

        /// <summary>
        /// 获取客户端试卷列表
        /// </summary>
        [HttpPost, Route("api/ytjysite/TPaper/GetModelList")]
        [AllowAnonymous]
        public ServiceResponse<SitePaperViewModel> GetModelList(SitePaperRequest request)
        {
            var userId = CurrentUserModel.UserId;
            var viewModel = tPaperService.GetSitePaperList(request, userId);

            return ServiceResponse<SitePaperViewModel>.SuccessResponse(CommonConst.OprateSuccessStr, viewModel);
        }

        /// <summary>
        /// 获取试卷详情
        /// </summary>
        [HttpPost, Route("api/ytjysite/TPaper/GetModelDetail")]
        [AllowAnonymous]
        public ServiceResponse<SitePaperDetailViewModel> GetModelDetail(SitePaperDetailRequest request)
        {
            var viewModel = tPaperService.GetSitePaperDetail(request, CurrentUserModel.UserId);

            return ServiceResponse<SitePaperDetailViewModel>.SuccessResponse(CommonConst.OprateSuccessStr, viewModel);
        }

        /// <summary>
        /// 交卷
        /// </summary>
        [HttpPost, Route("api/ytjysite/TPaper/SubmitPaper")]
        [AllowAnonymous]
        public ServiceResponse<SubmitPaperViewModel> SubmitPaper(SubmitPaperRequest request)
        {
            var viewModel = tPaperService.SubmitPaper(request, CurrentUserModel.UserId);

            return ServiceResponse<SubmitPaperViewModel>.SuccessResponse(CommonConst.OprateSuccessStr, viewModel);
        }
    }
}