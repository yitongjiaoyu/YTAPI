//-------------------------------------------------------------------
//文件名称：EVideoLib.cs
//模块名称：EVideoLibModel层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ELearning.Models
{	
	 /// <summary>
    /// EVideoLib表实体类
    /// </summary>
	[Table("EVideoLib")]
	public partial class EVideoLib
    {
		
		/// <summary>
		/// 主键
		/// </summary>
				[Key]
	[Display(Name = "主键")]
		public Guid Id { get; set; }
		
		/// <summary>
		/// 名称
		/// </summary>
			[Display(Name = "名称")]
		public string Name { get; set; }
		
		/// <summary>
		/// 描述
		/// </summary>
			[Display(Name = "描述")]
		public string Description { get; set; }
		
		/// <summary>
		/// 封面图片地址
		/// </summary>
			[Display(Name = "封面图片地址")]
		public string CoverUrl { get; set; }
		
		/// <summary>
		/// 大类分类Code
		/// </summary>
			[Display(Name = "大类分类Code")]
		public string CategoryId { get; set; }
		
		/// <summary>
		/// 班级基础分类code,基础班，高级版
		/// </summary>
			[Display(Name = "班级基础分类code,基础班，高级版")]
		public string TypeCode { get; set; }
		
		/// <summary>
		/// 序号
		/// </summary>
			[Display(Name = "序号")]
		public double Sort { get; set; }
		
		/// <summary>
		/// 文件Id
		/// </summary>
			[Display(Name = "文件Id")]
		public string FileId { get; set; }
		
		/// <summary>
		/// 文件大小,单位：KB
		/// </summary>
			[Display(Name = "文件大小,单位：KB")]
		public double Size { get; set; }
		
		/// <summary>
		/// 组织编码
		/// </summary>
			[Display(Name = "组织编码")]
		public string OrgCode { get; set; }
		
		/// <summary>
		/// 视频时长，单位:秒
		/// </summary>
			[Display(Name = "视频时长，单位:秒")]
		public int Duration { get; set; }
		
		/// <summary>
		/// 视频下载地址
		/// </summary>
			[Display(Name = "视频下载地址")]
		public string DownloadUrl { get; set; }
		
		/// <summary>
		/// 视频播放地址,Definition:0
		/// </summary>
			[Display(Name = "视频播放地址,Definition:0")]
		public string PlayUrl0 { get; set; }
		
		/// <summary>
		/// 视频播放地址,Definition:10
		/// </summary>
			[Display(Name = "视频播放地址,Definition:10")]
		public string PlayUrl1 { get; set; }
		
		/// <summary>
		/// 视频播放地址,Definition:20
		/// </summary>
			[Display(Name = "视频播放地址,Definition:20")]
		public string PlayUrl2 { get; set; }
		
		/// <summary>
		/// 视频播放地址,Definition:30
		/// </summary>
			[Display(Name = "视频播放地址,Definition:30")]
		public string PlayUrl3 { get; set; }
		
		/// <summary>
		/// 转码状态：0.不需要转码，1.待转码，2.转码中，3.转码成功，4.转码失败，5.部分转码成功
		/// </summary>
			[Display(Name = "转码状态：0.不需要转码，1.待转码，2.转码中，3.转码成功，4.转码失败，5.部分转码成功")]
		public int Status { get; set; }
		
		/// <summary>
		/// 转码视频备注
		/// </summary>
			[Display(Name = "转码视频备注")]
		public string ConvertVideoNote { get; set; }
		
		/// <summary>
		/// 创建时间
		/// </summary>
			[Display(Name = "创建时间")]
		public DateTime CreateTime { get; set; }
		
		/// <summary>
		/// 更新时间
		/// </summary>
			[Display(Name = "更新时间")]
		public DateTime UpdateTime { get; set; }
		
		/// <summary>
		/// 创建人Id
		/// </summary>
			[Display(Name = "创建人Id")]
		public Guid CreateUserId { get; set; }
		
		/// <summary>
		/// 编辑人id
		/// </summary>
			[Display(Name = "编辑人id")]
		public Guid UpdateUserId { get; set; }
		
		/// <summary>
		/// 是否删除
		/// </summary>
			[Display(Name = "是否删除")]
		public bool IsDelete { get; set; }
		   
    }
}

