﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.YTJYAdmin.Course
{
    /// <summary>
    /// 课程视频关联请求参数
    /// </summary>
    public class CourseVideoRequest : BaseSearchRequest
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { set; get; }

        /// <summary>
        /// 0.不关联，1.已关联
        /// </summary>
        public int Type { set; get; }

        /// <summary>
        /// 课程id
        /// </summary>
        public Guid CourseId { set; get; }
    }

    public class CourseVideoViewModel
    {
        /// <summary>
        /// 视频列表
        /// </summary>
        public List<CourseVideoItem> Items { set; get; } = new List<CourseVideoItem>();

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { set; get; }
    }

    /// <summary>
    /// 列表元素
    /// </summary>
    public class CourseVideoItem
    {
        public Guid Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 大类分类名称
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// 班级基础分类名称
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// 转码状态：0.不需要转码，1.待转码，2.转码中，3.转码成功，4.转码失败，5.部分转码成功
        /// </summary>
        [Display(Name = "转码状态：0.不需要转码，1.待转码，2.转码中，3.转码成功，4.转码失败，5.部分转码成功")]
        public int Status { get; set; }

        /// <summary>
        /// 转码状态字符串
        /// </summary>
        public string StatusStr { get; set; }

    }

    /// <summary>
    /// 视频列表实体
    /// </summary>
    public class CourseVideoModel
    {
        public Guid Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 大类分类名称
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// 班级基础分类名称
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { set; get; }

        /// <summary>
        /// 转码状态：0.不需要转码，1.待转码，2.转码中，3.转码成功，4.转码失败，5.部分转码成功
        /// </summary>
        [Display(Name = "转码状态：0.不需要转码，1.待转码，2.转码中，3.转码成功，4.转码失败，5.部分转码成功")]
        public int Status { get; set; }

    }

    /// <summary>
    /// 课程视频关联实体
    /// </summary>
    public class CourseVideoPostModel
    {
        /// <summary>
        /// 传入的视频关联类型：0.不关联,需要进行关联操作，1.已关联,需要进行取消关联操作
        /// </summary>
        public int Type { set; get; }

        /// <summary>
        /// 课程id
        /// </summary>
        public Guid CourseId { set; get; }

        /// <summary>
        /// 视频Id列表
        /// </summary>
        public List<Guid> VideoIdList { set; get; }
    }

}
