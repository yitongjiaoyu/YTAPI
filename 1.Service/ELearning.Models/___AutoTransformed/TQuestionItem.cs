//-------------------------------------------------------------------
//文件名称：TQuestionItem.cs
//模块名称：TQuestionItemModel层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ELearning.Models
{	
	 /// <summary>
    /// TQuestionItem表实体类
    /// </summary>
	[Table("TQuestionItem")]
	public partial class TQuestionItem
    {
		
		/// <summary>
		/// Id
		/// </summary>
				[Key]
	[Display(Name = "Id")]
		public Guid Id { get; set; }
		
		/// <summary>
		/// 题目id
		/// </summary>
			[Display(Name = "题目id")]
		public Guid QuestionId { get; set; }
		
		/// <summary>
		/// 组织编码
		/// </summary>
			[Display(Name = "组织编码")]
		public string OrgCode { get; set; }
		
		/// <summary>
		/// 名称
		/// </summary>
			[Display(Name = "名称")]
		public string Name { get; set; }
		
		/// <summary>
		/// 正确答案,例如：/id1/id2/
		/// </summary>
			[Display(Name = "正确答案,例如：/id1/id2/")]
		public string RightAnswer { get; set; }
		
		/// <summary>
		/// 序号
		/// </summary>
			[Display(Name = "序号")]
		public double Sort { get; set; }
		
		/// <summary>
		/// 分数
		/// </summary>
			[Display(Name = "分数")]
		public double Grade { get; set; }
		
		/// <summary>
		/// 大类分类Code
		/// </summary>
			[Display(Name = "大类分类Code")]
		public string CategoryId { get; set; }
		
		/// <summary>
		/// 班级基础分类code,基础班，高级版
		/// </summary>
			[Display(Name = "班级基础分类code,基础班，高级版")]
		public string TypeCode { get; set; }
		
		/// <summary>
		/// 题目类型，关联类型EQuestionType表中code字段
		/// </summary>
			[Display(Name = "题目类型，关联类型EQuestionType表中code字段")]
		public string QuestionTypeCode { get; set; }
		
		/// <summary>
		/// 前提（配伍选择题用）
		/// </summary>
			[Display(Name = "前提（配伍选择题用）")]
		public string Precondition { get; set; }
		
		/// <summary>
		/// 题目答案，主观题，简答题等类型使用该字段
		/// </summary>
			[Display(Name = "题目答案，主观题，简答题等类型使用该字段")]
		public string Answer { get; set; }
		
		/// <summary>
		/// 题目解析
		/// </summary>
			[Display(Name = "题目解析")]
		public string Analysis { get; set; }
		
		/// <summary>
		/// 题目难度，1.简单，2.一般，3.困难
		/// </summary>
			[Display(Name = "题目难度，1.简单，2.一般，3.困难")]
		public int Difficulty { get; set; }
		
		/// <summary>
		/// 创建时间
		/// </summary>
			[Display(Name = "创建时间")]
		public DateTime CreateTime { get; set; }
		
		/// <summary>
		/// 更新时间
		/// </summary>
			[Display(Name = "更新时间")]
		public DateTime UpdateTime { get; set; }
		
		/// <summary>
		/// 创建人Id
		/// </summary>
			[Display(Name = "创建人Id")]
		public Guid CreateUserId { get; set; }
		
		/// <summary>
		/// 编辑人id
		/// </summary>
			[Display(Name = "编辑人id")]
		public Guid UpdateUserId { get; set; }
		
		/// <summary>
		/// 是否删除
		/// </summary>
			[Display(Name = "是否删除")]
		public bool IsDelete { get; set; }
		   
    }
}

