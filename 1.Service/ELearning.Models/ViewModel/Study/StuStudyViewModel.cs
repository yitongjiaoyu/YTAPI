﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models 
{ 
    public class StuStudyViewModel
    {
        //public EStudentStudySum stusum = new EStudentStudySum();
        public Guid ID { get; set; }

        
        public Guid StuID { get; set; }

        
        public Guid CSID { get; set; }
         
         
        public int overcount { get; set; }
         
        public int countsum { get; set; }
         
        public int percentage { get; set; }
          
         
        public string Creater { get; set; }
            
        public string PTitle { set; get; }
        /// <summary>
        /// 新闻列表
        /// </summary>
        //public List<sturecord> RecordItems = new List<sturecord>();

        public string ZJTitle { set; get; }

        public DateTime StartTime { get; set; }


        public DateTime EndTime { get; set; }
         


        public int LearnCount { get; set; }


    }
    public class sturecord
    {
        
        public Guid ID { get; set; }

       
        public Guid StuID { get; set; }

        
        public Guid TeaCoursePubID { get; set; }

        public string PTitle { set; get; }

        public DateTime StartTime { get; set; }

     
        public DateTime EndTime { get; set; }

       
        public int StudyMinute { get; set; }

       
        public bool IsStudy { get; set; }

      
        public bool IsPass { get; set; }

        
        public int LearnCount { get; set; }

        
        public string Creater { get; set; }

       
        public DateTime CreateTime { get; set; }

     
        public string Modifyer { get; set; }

       
        public DateTime ModifyTime { get; set; }

        
        public bool IsDelete { get; set; }

       
        public bool IsAudit { get; set; }
 
        public string OrgCode { get; set; }
    }
}
