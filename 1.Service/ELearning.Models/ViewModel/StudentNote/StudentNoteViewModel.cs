﻿using ELearning.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models
{
    public class StudentNoteViewModel
    {
        /// <summary>
        /// 笔记列表
        /// </summary>
        public List<NoteItem> NoteItems = new List<NoteItem>();

        /// <summary>
        /// 总条数
        /// </summary>
        public int TotalCount { set; get; }
    }
    public class NoteItem
    {
        public Guid ID { get; set; }

        /// <summary>
        /// 学生标识
        /// </summary>
        public Guid StuID { get; set; }

        /// <summary>
        /// 课程ID
        /// </summary>
        public Guid CourseID { get; set; }

        /// <summary>
        /// 课程名
        /// </summary>
        public string CourseTitle { get; set; }
        /// <summary>
        /// 笔记标题
        /// </summary>
        public string NoteTitle { get; set; }

        /// <summary>
        /// 笔记内容
        /// </summary>
        public string Notes { get; set; }

        /// <summary>
        /// 创建者
        /// </summary>
        public string Creater { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 修改者
        /// </summary>
        public string Modifyer { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime ModifyTime { get; set; }

        /// <summary>
        /// 是否删除
        /// </summary>
        public bool IsDelete { get; set; }


    }

    /// <summary>
    /// 新闻请求参数
    /// </summary>
    public class StudentNoteRequest : BaseRequest
    {
        /// <summary>
        /// 字典挂靠id(目前放在课程系列id)
        /// </summary>
        public Guid TopicID { get; set; }
        /// <summary>
        /// 笔记搜索字段
        /// </summary>          
        public string Title { get; set; }
    }
}
