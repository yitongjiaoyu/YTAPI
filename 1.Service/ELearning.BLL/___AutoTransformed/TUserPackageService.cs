//-------------------------------------------------------------------
//文件名称：TUserPackage.cs
//模块名称：TUserPackage业务逻辑接口层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearning.DAL;
using ELearning.Models;

namespace ELearning.BLL
{
    /// <summary>
    /// TUserPackage业务逻辑接口层
    /// </summary>
	public partial class TUserPackageService : BaseService<TUserPackage>
	{
     public  TUserPackageService() : base(RepositoryFactory.TUserPackageRepository) { }
	}
}


