﻿using ELearning.BLL;
using ELearning.Common;
using ELearning.Common.Model;
using ELearning.Models;
using ELearning.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Http;

namespace ELearning.API.Controllers
{
    /// <summary>
    /// 公共控制器
    /// </summary>
    public class CommonController : BaseApiController
    {
        EDictionaryService dictionaryService => new EDictionaryService();
        ERoleService roleService => new ERoleService();

        /// <summary>
        /// 获取产品分类树
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/common/getSubjectTree")]
        public ServiceResponse<List<TreeViewModel>> GetSubjectTree()
        {
            var topID = dictionaryService.Find(x => !x.IsDelete && x.TypeCode == "Subject").ID;
            var subjects = dictionaryService.GetDicNoParent("Subject");
            List<TreeViewModel> lists = new List<TreeViewModel>();
            foreach (var x in subjects)
            {
                TreeViewModel model = new TreeViewModel
                {
                    id = x.ID,
                    ParentID = x.ParentID,
                    label = x.TypeName,
                    value = x.TypeCode,
                    children = null,
                };
                lists.Add(model);
            }

            var tree = RecursionExtensions.GetTree(lists, topID);

            return ServiceResponse<List<TreeViewModel>>.SuccessResponse(tree);
        }

        /// <summary>
        /// 获取角色树
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/common/getRoleTree")]
        public ServiceResponse<List<TreeViewModel>> GetRoleTree()
        {
            var subjects = roleService.FindList(x => !x.IsDelete);
            List<TreeViewModel> lists = new List<TreeViewModel>();
            foreach (var x in subjects)
            {
                TreeViewModel model = new TreeViewModel
                {
                    id = x.RoleID,
                    ParentID = x.ParentID,
                    value = x.RoleID.ToString(),
                    label = x.RoleName,
                    children = null,
                };

                lists.Add(model);
            }

            var tree = RecursionExtensions.GetTree(lists, Guid.Empty);

            return ServiceResponse<List<TreeViewModel>>.SuccessResponse(tree);
        }

        /// <summary>
        /// 获取科目、年级、等级……集合 - for select
        /// </summary>
        /// <returns></returns>
        [Route("api/common/getBaseInfoSel")]
        [HttpGet]
        public object GetBaseInfoSel()
        {
            DictCommonViewModel viewModel = new DictCommonViewModel();

            SelectKeyValue kv = new SelectKeyValue();
            kv.key = "";
            kv.label = "不限";
            viewModel.Subject.Add(kv);
            viewModel.Grade.Add(kv);
            viewModel.Level.Add(kv);

            var Subject = dictionaryService.GetDicNoParent("Subject");
            foreach (var x in Subject)
            {
                kv = new SelectKeyValue();
                kv.key = x.TypeCode;
                kv.label = x.TypeName;
                viewModel.Subject.Add(kv);
            }

            var Grade = dictionaryService.GetDicNoParent("Grade");
            foreach (var x in Grade)
            {
                kv = new SelectKeyValue();
                kv.key = x.TypeCode;
                kv.label = x.TypeName;
                viewModel.Grade.Add(kv);
            }

            var Level = dictionaryService.GetDicNoParent("Level");
            foreach (var x in Level)
            {
                kv = new SelectKeyValue();
                kv.key = x.TypeCode;
                kv.label = x.TypeName;
                viewModel.Level.Add(kv);
            }

            return ServiceResponse<object>.SuccessResponse(viewModel);
        }



        ///// <summary>
        ///// 获取分类树
        ///// </summary>
        ///// <returns></returns>
        //[Route("api/common/getSubjectTree")]
        //[HttpGet]
        //public object GetSubjectTree()
        //{
        //    var subjects = dictionaryService.GetDicInParent("Subject");

        //    List<TreeViewModel> lists = new List<TreeViewModel>();
        //    TreeViewModel model = null;
        //    foreach (var x in subjects)
        //    {
        //        model = new TreeViewModel();
        //        model.id = x.ID;
        //        model.ParentID = x.ParentID;
        //        model.value = x.TypeCode;
        //        model.label = x.TypeName;
        //        model.children = null;
        //        lists.Add(model);
        //    }

        //    var tree = RecursionExtensions.SubjectTree(lists, Guid.Parse("00000000-0000-0000-0000-000000000000"));

        //    return ServiceResponse<object>.SuccessResponse(tree);
        //}


        /// <summary>
        /// 获取文件保存路径
        /// </summary>
        public string GetPath(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            var fileFullPath = $"{path}{DateTime.Now:yyyyMMddHHmmss}.xlsx";
            return fileFullPath;
        }

        /// <summary>
        /// 获取文件保存文件夹
        /// </summary>
        public string GetFolder(string name)
        {
            var virtualPath = "/UploadFile/Files/" + $"{name}-{DateTime.Now:yyyyMMddHHmmss}/";
            var path = HttpContext.Current.Server.MapPath(virtualPath);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            return path;
        }

        /// <summary>
        /// 文件上传到blob
        /// </summary>
        public string SaveFile(string fileName, string fileFullPath)
        {
            var fs = new FileStream(fileFullPath, FileMode.Open);
            var str = "#";
            //var bc = new BlobController();
            //var blobName = BlobHelper.GetBlobName(fileName);
            //var blobResult = bc.CreateBlod(blobName);
            //if (blobResult == ApiResultEunm.Success || blobResult == ApiResultEunm.Exist)
            //{
            //    //创建文件
            //    var apiResult = bc.UploadFileByStream(fs, fileName);
            //    if (apiResult.apiResultEunm == ApiResultEunm.Success && !string.IsNullOrEmpty(apiResult.info))
            //    {
            //        str = apiResult.info;
            //    }
            //}
            //fs.Dispose();
            //File.Delete(fileFullPath);
            return str;
        }

        /// <summary>
        /// 获取课程分类
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/common/getCourseTypes")]
        public ServiceResponse<List<TreeViewModel>> GetCourseTypes()
        {
            var topID = dictionaryService.Find(x => !x.IsDelete && x.TypeCode == "Category").ID;
            var subjects = dictionaryService.GetDicNoParent("Category");
            List<TreeViewModel> lists = new List<TreeViewModel>();
            foreach (var x in subjects)
            {
                TreeViewModel model = new TreeViewModel
                {
                    id = x.ID,
                    ParentID = x.ParentID,
                    label = x.TypeName,
                    value = x.TypeCode,
                    children = null,
                };
                lists.Add(model);
            }

            var tree = RecursionExtensions.GetTree(lists, topID);

            return ServiceResponse<List<TreeViewModel>>.SuccessResponse(tree);
        }


    }
}