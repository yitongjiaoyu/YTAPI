﻿//-------------------------------------------------------------------
//文件名称：EVideoStudySum.cs
//模块名称：EVideoStudySum数据访问层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearning.Models;
using ELearning.Models.ViewModel.YTJYAdmin.Study;

namespace ELearning.DAL
{
    /// <summary>
    /// EVideoStudySum数据访问层
    /// </summary>
    public partial class EVideoStudySumRepository : BaseRepository<EVideoStudySum>
    {
        /// <summary>
        /// 获取我的视频学习记录
        /// </summary>
        public List<VideoLogModel> GetVideoLogList(VideoLogRequest request, Guid userId)
        {
            var sql = $@"
                        SELECT
	                        *
                        FROM
	                        (
		                        SELECT 
			                        vss.Id,
			                        v.CoverUrl,
			                        vss.PackageId,
			                        p.Name PackageName,
			                        vss.CourseId,
			                        c.Name CourseName,
			                        vss.VideoId,
			                        v.Name VideoName,
			                        vss.TimeSpan,
			                        vss.LastStudyTime,
			                        vss.IsFinish,
			                        ROW_NUMBER() OVER(ORDER BY vss.UpdateTime DESC) rn,
			                        COUNT(1) OVER() TotalCount
		                        FROM 
			                        dbo.EVideoStudySum vss
			                        LEFT JOIN dbo.EPackage p ON p.Id = vss.PackageId
			                        LEFT JOIN dbo.ECourse c ON c.Id = vss.CourseId
			                        LEFT JOIN dbo.EVideoLib v ON v.Id = vss.VideoId
		                        WHERE
			                        vss.IsDelete = 0 AND p.IsDelete = 0
			                        AND c.IsDelete = 0 AND v.IsDelete = 0
			                        AND vss.UserId = @userId
	                        )a
                        WHERE
	                        a.rn BETWEEN {request.StartIndex} AND {request.EndIndex}";
            var args = new List<SqlParameter>
            {
                new SqlParameter("@userId",userId)
            }.ToArray();

            return nContext.Database.SqlQuery<VideoLogModel>(sql, args).ToList();
        }
    }
}


