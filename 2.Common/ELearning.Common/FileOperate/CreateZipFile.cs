﻿using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ELearning.Common
{
    public class CreateZipFile
    {
        public byte[] GetFiles()
        {
            MemoryStream ms = new MemoryStream();
            byte[] buffer = null;
            using (ZipFile file = ZipFile.Create(ms))
            {
                file.BeginUpdate();
                //foreach (var item in mList)
                //{
                //    UrlDataSource data = new UrlDataSource("dd");
                //    file.Add(data, item.Name);
                //}
                               
                UrlDataSource data = new UrlDataSource("https://devstorgec.blob.core.chinacloudapi.cn/document/510题库50道20191113152258.docx");
                file.Add(data, "测试1.docx");
            
                file.CommitUpdate();
                buffer = new byte[ms.Length];
                ms.Position = 0;
                ms.Read(buffer, 0, buffer.Length);
                //var saveName = $"{fs[0]}{DateTime.Now:yyyyMMddHHmmss}.{fs[fs.Length - 1]}";
               
                return buffer;
                
               // return blobService.UploadFileByStream(blobName, stream, saveName);
            }
             //HttpContext.Response
             //Response.AddHeader("content-disposition", "attachment;filename=" + IntNumber + ".zip");
            //Response.BinaryWrite(buffer);
            //Response.Flush();
            //Response.End();
        }

        public byte[] DownLoadall()
        {
            string urlStr= "https://devstorgec.blob.core.chinacloudapi.cn/document/510题库50道20191113152258.docx";
            string[] urlArray = urlStr.TrimEnd('$').Split('$');            
           
            //使用WebClient 下载文件
            System.Net.WebClient myWebClient = new System.Net.WebClient();

            //存 文件名 和 数据流
            Dictionary<string, Stream> dc = new Dictionary<string, Stream>();

            //取出字符串中信息 (文件名和地址)
            for (int i = 0; i < urlArray.Length; i++)
            {
                //使用 ',' 分隔 文件名和路径 [0]位置是文件名, [1] 位置是路径
                string[] urlSp = urlArray[i].Split(',');

                //调用WebClient 的 DownLoadData 方法 下载文件
                byte[] data = myWebClient.DownloadData("https://devstorgec.blob.core.chinacloudapi.cn/document/510题库50道20191113152258.docx");
                Stream stream = new MemoryStream(data);//byte[] 转换成 流

                //放入 文件名 和 stream
                dc.Add("510题库50道20191113152258.docx", stream);//这里指定为 .doc格式 (自己可以随时改)
            }

            //调用压缩方法 进行压缩 (接收byte[] 数据)
            byte[] fileBytes = ConvertZipStream(dc);
            return fileBytes;
        }
        public static byte[] ConvertZipStream(Dictionary<string, Stream> streams)
        {
            byte[] buffer = new byte[6500];
            MemoryStream returnStream = new MemoryStream();
            var zipMs = new MemoryStream();
            using (ICSharpCode.SharpZipLib.Zip.ZipOutputStream zipStream = new ICSharpCode.SharpZipLib.Zip.ZipOutputStream(zipMs))
            {
                zipStream.SetLevel(9);//设置 压缩等级 (9级 500KB 压缩成了96KB)
                foreach (var kv in streams)
                {
                    string fileName = kv.Key;
                    using (var streamInput = kv.Value)
                    {
                        zipStream.PutNextEntry(new ICSharpCode.SharpZipLib.Zip.ZipEntry(fileName));
                        while (true)
                        {
                            var readCount = streamInput.Read(buffer, 0, buffer.Length);
                            if (readCount > 0)
                            {
                                zipStream.Write(buffer, 0, readCount);
                            }
                            else
                            {
                                break;
                            }
                        }
                        zipStream.Flush();
                    }
                }
                zipStream.Finish();
                zipMs.Position = 0;
                zipMs.CopyTo(returnStream, 5600);
            }
            returnStream.Position = 0;

            //Stream转Byte[]
            byte[] returnBytes = new byte[returnStream.Length];
            returnStream.Read(returnBytes, 0, returnBytes.Length);
            returnStream.Seek(0, SeekOrigin.Begin);

            return returnBytes;
        }

        public static MemoryStream ConvertZipToStream(Dictionary<string, Stream> streams)
        {
            byte[] buffer = new byte[6500];
            MemoryStream returnStream = new MemoryStream();
            var zipMs = new MemoryStream();
            using (ICSharpCode.SharpZipLib.Zip.ZipOutputStream zipStream = new ICSharpCode.SharpZipLib.Zip.ZipOutputStream(zipMs))
            {
                zipStream.SetLevel(9);//设置 压缩等级 (9级 500KB 压缩成了96KB)
                foreach (var kv in streams)
                {
                    string fileName = kv.Key;
                    using (var streamInput = kv.Value)
                    {
                        zipStream.PutNextEntry(new ICSharpCode.SharpZipLib.Zip.ZipEntry(fileName));
                        while (true)
                        {
                            var readCount = streamInput.Read(buffer, 0, buffer.Length);
                            if (readCount > 0)
                            {
                                zipStream.Write(buffer, 0, readCount);
                            }
                            else
                            {
                                break;
                            }
                        }
                        zipStream.Flush();
                    }
                }
                zipStream.Finish();
                zipMs.Position = 0;
                zipMs.CopyTo(returnStream, 5600);
            }
            //returnStream.Position = 0;

            ////Stream转Byte[]
            //byte[] returnBytes = new byte[returnStream.Length];
            //returnStream.Read(returnBytes, 0, returnBytes.Length);
            //returnStream.Seek(0, SeekOrigin.Begin);

            return returnStream;
        }

    }

    /// <summary>
    /// Zip File Model
    /// </summary>
    public class ZipFileModel
    {
        public ZipFileModel(string fileName, string filePathUrl)
        {
            FileName = fileName;
            FilePathUrl = filePathUrl;
        }
        /// <summary>
        /// 文件名
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// 文件Url路径
        /// </summary>
        public string FilePathUrl { get; set; }
    }
}
