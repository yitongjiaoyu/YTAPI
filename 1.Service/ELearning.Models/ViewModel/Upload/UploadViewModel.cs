﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.Upload
{
    public class UploadViewModel
    {
        /// <summary>
        /// 资产id
        /// </summary>
        public string AssetId { set; get; }

        /// <summary>
        /// 作业id
        /// </summary>
        public string JobId { set; get; }

        /// <summary>
        /// 其他类型文件地址
        /// </summary>
        public string Url { set; get; }

        /// <summary>
        /// 文件下载地址
        /// </summary>
        public string DownloadUrl { set; get; }

        /// <summary>
        /// 类型：0.视频，1.其他
        /// </summary>
        public int Type { set; get; }
    }

    public class UploadRespModel
    {
        /// <summary>
        /// 文件相对地址
        /// </summary>
        public string SaveUrl { set; get; }

        /// <summary>
        /// 文件下载地址
        /// </summary>
        public string DownloadUrl { set; get; }

        /// <summary>
        /// 文件扩展名
        /// </summary>
        public string Ext { set; get; }

    }

    public class ReEncodeViewModel
    {
        /// <summary>
        /// 资产id
        /// </summary>
        public string AssetId { set; get; }

        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { set; get; }
    }

    public class SignatureModel
    {
        /// <summary>
        /// 签名
        /// </summary>
        public string Signature { set; get; }
    }
}
