//-------------------------------------------------------------------
//文件名称：ESysUser.cs
//模块名称：ESysUser业务逻辑接口层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//修改时间：2019年6月20日
//-----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using ELearning.Models;
using ELearning.Models.ViewModel.User;
using ELearning.Common.Extensions;
using ELearning.Common.Model;
using ELearning.DAL;

namespace ELearning.BLL
{
    /// <summary>
    /// ESysUser业务逻辑接口层
    /// </summary>
	public partial class ESysUserService : BaseService<ESysUser>
    {
        /// <summary>
        /// 根据用户登录账号获取用户信息
        /// </summary>
        /// <param name="loginName">登录账号</param>
        /// <returns>用户个人信息</returns>
        public ESysUser GetUserByLoginName(string loginName)
        {
            var user = CurrentRepository.Find(u => u.IsDelete == false && u.LoginName == loginName);
            return user;
        }

        /// <summary>
        /// 根据用户id获取用户信息
        /// </summary>
        /// <param name="userId">用户id</param>
        /// <returns>用户个人信息</returns>
        public ESysUser GetUserById(Guid userId)
        {
            var user = CurrentRepository.Find(u => u.IsDelete == false && u.UserID == userId);
            return user;
        }

        /// <summary>
        /// 获取用户列表
        /// </summary>
        public Page<ESysUser> GetUserList(GetUserRequest request)
        {
            Expression<Func<ESysUser, bool>> whereLamdba = (u) => u.IsDelete == false;

            if (!string.IsNullOrEmpty(request.UserName))
                whereLamdba = whereLamdba.And(u => u.UserName.Contains(request.UserName));
            if (!string.IsNullOrEmpty(request.LoginName))
                whereLamdba = whereLamdba.And(u => u.LoginName.Contains(request.LoginName));
            if (!string.IsNullOrEmpty(request.MobilePhone))
                whereLamdba = whereLamdba.And(u => u.MobilePhone.Contains(request.MobilePhone));
            if (!string.IsNullOrEmpty(request.Unumber))
                whereLamdba = whereLamdba.And(u => u.Unumber.Contains(request.Unumber));
            if (!string.IsNullOrEmpty(request.UserType))
                whereLamdba = whereLamdba.And(u => u.UserType.Equals(request.UserType));
            if (!string.IsNullOrEmpty(request.OrgCode))
                whereLamdba = whereLamdba.And(u => u.OrgCode.Equals(request.OrgCode));
            var items = CurrentRepository.FindPageList(request.Page, request.PageSize, out int total, whereLamdba, "CreateTime", false).ToList();
            Page<ESysUser> page = new Page<ESysUser>
            {
                TotalItems = total,
                CurrentPage = request.Page,
                Items = items,
                ItemsPerPage = request.PageSize
            };
            return page;
        }

        /// <summary>
        /// 获取经销商用户列表
        /// </summary>
        public Page<SysUserModel> GetISVUserList(GetUserRequest request)
        {
            var page = RepositoryFactory.ESysUserRepository.GetISVUserList(request);
            return page;
        }

        /// <summary>
        /// 获取用户列表
        /// </summary>
        public Page<SysUserModel> GetAdminUserList(GetUserRequest request)
        {
            var page = RepositoryFactory.ESysUserRepository.GetAdminUserList(request);
            return page;
        }
        
        public Page<ESysUser> GetUserListByOrgCode(string code, string username)
        {
            Expression<Func<ESysUser, bool>> whereLamdba = (u) => u.IsDelete == false;
            whereLamdba = whereLamdba.And(u => u.OrgCode == code);
            if (!string.IsNullOrEmpty(username))
                whereLamdba = whereLamdba.And(u => u.UserName.Contains(username));
            var items = CurrentRepository.FindList(whereLamdba, "CreateTime", true).ToList();
            Page<ESysUser> page = new Page<ESysUser>
            {
                TotalItems = items.Count,
                CurrentPage = 1,
                Items = items,
                ItemsPerPage = items.Count
            };
            return page;
        }
        /// <summary>
        /// 获取注册用户列表
        /// </summary>
        public Page<ESysUser> GetRegesterList(GetUserRequest request)
        {
            Expression<Func<ESysUser, bool>> whereLamdba = (u) => u.IsDelete == false;

            if (!string.IsNullOrEmpty(request.UserName))
                whereLamdba = whereLamdba.And(u => u.UserName.Contains(request.UserName));
            if (!string.IsNullOrEmpty(request.LoginName))
                whereLamdba = whereLamdba.And(u => u.LoginName.Contains(request.LoginName));
            if (!string.IsNullOrEmpty(request.MobilePhone))
                whereLamdba = whereLamdba.And(u => u.MobilePhone.Contains(request.MobilePhone));
            if (!string.IsNullOrEmpty(request.UserType))
                whereLamdba = whereLamdba.And(u => u.UserType == request.UserType);
            if (!string.IsNullOrEmpty(request.OrgCode))
                whereLamdba = whereLamdba.And(u => u.OrgCode == request.OrgCode);

            var items = CurrentRepository.FindPageList(request.Page, request.PageSize, out int total, whereLamdba, request.OrderBy, request.IsAsc).ToList();
            Page<ESysUser> page = new Page<ESysUser>
            {
                TotalItems = total,
                CurrentPage = request.Page,
                Items = items,
                ItemsPerPage = request.PageSize
            };
            return page;
        }

        /// <summary>
        /// 根据消息id获取对应的接收消息用户
        /// </summary>
        /// <param name="messageId">消息id</param>
        public List<ESysUser> GetSendUserByMsgId(Guid messageId)
        {
            return RepositoryFactory.ESysUserRepository.GetSendUserByMsgId(messageId);
        }
    }
}


