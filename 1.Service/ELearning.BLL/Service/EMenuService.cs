using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearning.DAL;
using ELearning.Models;

namespace ELearning.BLL
{
    /// <summary>
    /// EMenu业务逻辑接口层
    /// </summary>
	public partial class EMenuService : BaseService<EMenu>
    {
        /// <summary>
        /// 登录账号
        /// </summary>
        /// <param name="loginName"></param>
        /// <returns></returns>
        public List<EMenu> GetUserMenusByLoginInfo(string loginName)
        {
            return RepositoryFactory.EMenuRepository.GetUserMenusByLoginInfo(loginName);
        }
        /// <summary>
        /// 根据用户ID
        /// </summary>       
        /// <returns></returns>
        public List<EMenu> GetUserMenusByLoginInfo(Guid userId)
        {
            return RepositoryFactory.EMenuRepository.GetUserMenusByLoginInfo(userId);
        }

        /// <summary>
        /// 通过用户ID查找菜单权限
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<MenuViewModel> GetUserTreeMenusByUserId(Guid userId)
        {
            var menuList = RepositoryFactory.EMenuRepository.GetUserMenusByLoginInfo(userId);
            var treeList = new List<MenuViewModel>();
            GetMenuTreeJson(treeList, menuList, Guid.Parse("00000000-0000-0000-0000-000000000000"));
            return treeList;
        }
        public List<EMenu> GetUserMenusByUserId(string userId)
        {
            var menuList = RepositoryFactory.EMenuRepository.GetUserMenusByUserId(userId);
            return menuList;
        }

        /// <summary>
        /// 获取该菜单的所有父节点（包含本菜单）
        /// </summary>
        /// <param name="menuId">菜单id</param>
        /// <returns></returns>
        public List<EMenu> GetParentMenusById(Guid menuId)
        {
            var menuList = RepositoryFactory.EMenuRepository.GetParentMenusById(menuId);
            return menuList;
        }

        /// <summary>
        /// 获取该菜单的所有子节点（包含本菜单）
        /// </summary>
        /// <param name="menuId">菜单id</param>
        public List<EMenu> GetSubMenusById(Guid menuId)
        {
            var menuList = RepositoryFactory.EMenuRepository.GetSubMenusById(menuId);
            return menuList;
        }

        /// <summary>
        /// 根据登录账号查找所有权限
        /// </summary>
        /// <param name="UserID">用户Id</param>
        /// <returns></returns>
        public List<string> GetUserPermission(Guid userID)
        {
            var menuList = RepositoryFactory.EMenuRepository.GetUserPermission(userID);
            return menuList;
        }

        #region  菜单递归方法
        /// <summary>
        /// 取得兄弟节点
        /// </summary>
        /// <param name="categoryList"></param>
        /// <param name="parentCode"></param>
        private void GetMenuTreeJson(List<MenuViewModel> list, List<EMenu> source, Guid parentId)
        {
            var items = source.Where(i => i.ParentId == parentId).ToList();
            if (items.Count > 0)
            {
                foreach (var item in items)
                {
                    var dm = new MenuViewModel()
                    {
                        Id = item.Id,
                        ParentId = item.ParentId,
                        Path = item.Path,
                        Component = item.Component,
                        Redirect = item.Redirect,
                        Name = item.Name,
                        Type=item.Type,
                        Code=item.Code,
                        Hidden = !item.NoCache,
                        Meta = new MetaModel() { Title = item.Title, Icon = item.Icon, Affix = item.Affix, NoCache = item.NoCache }
                    };
                    if (item.ParentId == Guid.Parse("00000000-0000-0000-0000-000000000000"))
                    {
                        dm.PlatformType = item.Code;
                    }
                    //else
                    //{
                    //    dm.PlatformType = items.FirstOrDefault(m => m.Id == parentId)?.PlatformType ?? "";
                    //}
                    GetChildMenuTreeJson(dm, source, item.Id);
                    list.Add(dm);
                }
            }
        }
        /// <summary>
        /// 递归子菜单节点
        /// </summary>
        /// <param name="sbCategory"></param>
        /// <param name="source"></param>
        /// <param name="parentId"></param>
        private void GetChildMenuTreeJson(MenuViewModel sbCategory, List<EMenu> source, Guid parentId)
        {
            var items = source.Where(i => i.ParentId == parentId).ToList();
            if (items.Count > 0)
            {
                foreach (var item in items)
                {
                    var dm = new MenuViewModel()
                    {
                        Id = item.Id,
                        ParentId = item.ParentId,
                        Path = item.Path,
                        Component = item.Component,
                        Redirect = item.Redirect,
                        Name = item.Name,
                        Type = item.Type,
                        Code = item.Code,
                        Hidden = !item.NoCache,
                        Meta = new MetaModel() { Title = item.Title, Icon = item.Icon, Affix = item.Affix, NoCache = item.NoCache }
                    };
                    GetChildMenuTreeJson(dm, source, item.Id);
                    sbCategory.Children.Add(dm);
                }
            }
        }
        #endregion

    }
}


