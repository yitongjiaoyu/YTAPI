//-------------------------------------------------------------------
//文件名称：流媒体服务表.cs
//模块名称：流媒体服务表数据访问层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Xiaosheng
//修改时间：2019年8月8日
//-----------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearning.Models;

namespace ELearning.DAL
{
    /// <summary>
    /// 流媒体服务表数据访问层
    /// </summary>
	public partial class 流媒体服务表Repository : BaseRepository<流媒体服务表>
	{
	
	}
}


