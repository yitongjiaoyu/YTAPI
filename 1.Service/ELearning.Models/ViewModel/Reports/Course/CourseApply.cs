﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel
{
    public class CourseApply
    {
        /// <summary>
        /// 图例
        /// </summary>
        public List<string> legend { get; set; } = new List<string>();
        /// <summary>
        /// series
        /// </summary>
        public List<Series> series { get; set; } = new List<Series>();
    }
    public class Series
    {
        public string name { get; set; } = string.Empty;
        public List<int> data { get; set; } = new List<int>();
    }
}
