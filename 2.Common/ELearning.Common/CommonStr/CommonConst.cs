﻿
using System.Web;
using ELearning.Common.Extensions;

namespace ELearning.Common.CommonStr
{
    public class CommonConst
    {

        /// <summary>
        /// 操作成功
        /// </summary>
        public static string OprateSuccessStr => "操作成功";

        /// <summary>
        /// 操作失败
        /// </summary>
        public static string OprateFailStr => "操作失败";

        /// <summary>
        /// 用户未登录
        /// </summary>
        public static string USER_NO_LOGIN => "用户未登录";

        /// <summary>
        /// 原密码错误
        /// </summary>
        public static string OLD_PASSWORD_ERROR => "原密码错误";

        /// <summary>
        /// 登录密码错误
        /// </summary>
        public static string PASSWORD_ERROR => "密码错误";

        /// <summary>
        /// 手机号已被注册
        /// </summary>
        public static string USER_EXIST => "手机号已被注册";

        /// <summary>
        /// 手机号未注册
        /// </summary>
        public static string USER_NOT_EXIST => "手机号未注册";

        /// <summary>
        /// 登陆名错误
        /// </summary>
        public static string USER_ERR_LOGIN => "登陆名错误！";

        /// <summary>
        /// 手机号已被注册编码
        /// </summary>
        public static int USER_EXIST_CODE => 209;

        /// <summary>
        /// 手机号未注册编码
        /// </summary>
        public static int USER_NOT_EXIST_CODE => 208;

        /// <summary>
        /// 密码错误编码
        /// </summary>
        public static int PASSWORD_ERROR_CODE => 210;

        /// <summary>
        /// 默认组织编码
        /// </summary>
        public static string DEFAULT_ORGCODE => "default";

        /// <summary>
        /// 课程默认封面图
        /// </summary>
        public static string DEFAULT_COURSE_COVER_URL => "http://81.70.41.226:8003///UploadFile/Files//picture/20200801093426201902210927573142.jpg";

        /// <summary>
        /// 组织码重复
        /// </summary>
        public static string ORGCODE_REPEAT => "组织码重复，请重新输入";

        /// <summary>
        /// 组织码不能为空
        /// </summary>
        public static string ORGCODE_NOT_NULL => "组织码不能为空";

        /// <summary>
        /// 组织学生数量达到上限
        /// </summary>
        public static string ORGCODE_STUDENT_COUNT_UPPER_LIMIT => "组织分配的学生数量达到上限，不可继续分配。";

        /// <summary>
        /// 组织码重复编码
        /// </summary>
        public static int ORGCODE_REPEAT_CODE => 210;

        /// <summary>
        /// 上传失败默认图片
        /// </summary>
        public static string UploadFailDefaultUrl => ConfigHelper.GetConfigString("UploadFailDefaultUrl");

        /// <summary>
        /// 用户默认头像
        /// </summary>
        public static string UserDefaultHeadPhoto  => ConfigHelper.GetConfigString("UserDefaultHeadPhoto");

        /// <summary>
        /// 默认简介
        /// </summary>
        public static string UserDefaultRemark => "暂无简介";

        /// <summary>
        /// 用户未登陆
        /// </summary>
        public static int Code_NoLogin = 401;

        /// <summary>
        /// 登录失败
        /// </summary>
        public static int Code_LoginError = 402;

        /// <summary>
        /// 接口操作错误代码
        /// </summary>
        public static int Code_OprateError = 402;

        /// <summary>
        /// 接口操作成功代码
        /// </summary>
        public static int Code_OprateSuccess = 200;

        /// <summary>
        /// 用户未登陆
        /// </summary>
        public static string Msg_NoLogin = "用户未登陆";

        /// <summary>
        /// 用户名或密码错误
        /// </summary>
        public static string Msg_LoginError = "用户名或密码错误";

        /// <summary>
        /// 您的用户信息暂未同步到学习中心，请稍后再试。如有问题，请致电0772-2650611。
        /// </summary>
        public static string Msg_LoginErrorTips = "您的用户信息暂未同步到学习中心，请稍后再试。如有问题，请致电0772-2650611。";

        /// <summary>
        /// 用户默认密码:123456
        /// </summary>
        public static string UserDefaultPsw = "e10adc3949ba59abbe56e057f20f883e";

        /// <summary>
        /// 用户默认组织码
        /// </summary>
        public static string UserDefaultOrgCode = "default";

        /// <summary>
        /// 课程名称重复
        /// </summary>
        public static string CourseNameRepeat => "课程名称重复";

        /// <summary>
        /// 分页获取列表默认排序字段
        /// </summary>
        public static string SORT_BY_DEFAULT_STR => "CreateTime";

        /// <summary>
        /// 分页获取列表默认排序顺序
        /// </summary>
        public static string SORT_DIR_DEFAULT_STR => "DESC";

        /// <summary>
        /// 课程分类编码重复
        /// </summary>
        public static string TeachTypeRepeat => "分类编码重复";

        /// <summary>
        /// 视频转码中
        /// </summary>
        public static string CourseProcessing => HttpUtility.UrlEncode("转码中");

        /// <summary>
        /// 转码失败
        /// </summary>
        public static string CourseFailed => HttpUtility.UrlEncode("转码失败");

        /// <summary>
        /// 课程发布失败提示
        /// </summary>
        public static string CoursePublishTips => "课程中包含未发布的课件，确认课件都已发布，可以先暂存为草稿";

        /// <summary>
        /// 课程转码失败
        /// </summary>
        public static string CoursePublishFailTips => "课程中包含发布失败的课件，确认课件都已发布成功，可以先暂存为草稿";

        /// <summary>
        /// 绑定相同作业提示
        /// </summary>
        public static string CourseBindSameJobTips = "不能绑定相同作业，请重新选择";

        /// <summary>
        /// 消息内容格式错误
        /// </summary>
        public static string MessageTextNotVaild = "消息内容不能包含特殊字符（回车或者斜杠）！";

        /// <summary>
        /// 系统消息不可操作
        /// </summary>
        public static string SysMessageTextNotOprate = "系统消息不可操作！";

        /// <summary>
        /// 冠军名字/年/月重复
        /// </summary>
        public static string WinnerNewsNotOprate = "冠军名字/年/月重复！";

        /// <summary>
        /// 当前操作人类型与操作要求用户类型不符合
        /// </summary>
        public static string IntegralLogError = "当前操作人类型与操作要求用户类型不符合";

    }

    /// <summary>
    /// 文件上传配置项
    /// </summary>
    public class FileUploadConst
    {
        /// <summary>
        /// 上传地址
        /// </summary>
        public static string UploadPath => ConfigHelper.GetConfigString("UploadPath");

        /// <summary>
        /// 文件访问/下载地址
        /// </summary>
        public static string DownloadPath => ConfigHelper.GetConfigString("DownloadPath");

        /// <summary>
        /// 访问共享目录用户名
        /// </summary>
        public static string UserName => ConfigHelper.GetConfigString("UserName");

        /// <summary>
        /// 访问共享目录密码
        /// </summary>
        public static string Password => ConfigHelper.GetConfigString("Password");
    }

    /// <summary>
    /// 用户类型字典
    /// </summary>
    public class UserType
    {
        /// <summary>
        /// 用户
        /// </summary>
        public static string UserTypeUSER => "USER";

        /// <summary>
        /// 讲师
        /// </summary>
        public static string UserTypeTEACHER => "TEACHER";
        
        /// <summary>
        /// 学生
        /// </summary>
        public static string UserTypeSTUDENT => "STUDENT";
    }


    public class MessageConfig
    {

        public static string PushUrl { set; get; } = ConfigHelper.GetConfigString("PushUrl");
        public static string AppId { set; get; } = ConfigHelper.GetConfigString("appId");
        public static string AppKey { set; get; } = ConfigHelper.GetConfigString("appKey");

    }

    public class MessageSendModel
    {
        ///// <summary>
        ///// 分配给第三方应用的id
        ///// header
        ///// 必选
        ///// </summary>
        //public string appId { set; get; } = "rpjp5f9ew1p1pbkechun";

        ///// <summary>
        ///// 时间戳，格式：yyyyMMddHHmmss (距离服务端时间120s内有效)
        ///// header
        ///// 必选
        ///// </summary>
        //public string timestamp { set; get; }

        ///// <summary>
        ///// 随机值，生成规则客户端自定义
        ///// header
        ///// 必选
        ///// </summary>
        //public string nonce { set; get; }

        ///// <summary>
        ///// 签名,加密方式:MD5 32位加密方式(appId+appKey+timestamp+nonce)
        ///// header
        ///// 必选
        ///// </summary>
        //public string sign { set; get; }

        /// <summary>
        /// 经销商号
        /// body
        /// 必选
        /// </summary>
        public string jxsh { set; get; }

        /// <summary>
        /// 操作员代码
        /// body
        /// </summary>
        public string czydm { set; get; }

        /// <summary>
        /// 操作员名称
        /// body
        /// </summary>
        public string czymc { set; get; }

        /// <summary>
        /// 岗位，如销售顾问，销售经理等
        /// body
        /// </summary>
        public string zw { set; get; }

        /// <summary>
        /// 来源。（注：如果登陆来源于培训系统，请传值：TrainingSystem）
        /// body
        /// 必选
        /// </summary>
        public string source { set; get; } = "TrainingSystem";

        /// <summary>
        /// 消息ID
        /// body
        /// </summary>
        public string id { set; get; }

        /// <summary>
        /// 消息标题
        /// body
        /// 必选
        /// </summary>
        public string title { set; get; }

        /// <summary>
        /// 消息内容
        /// body
        /// 必选
        /// </summary>
        public string content { set; get; }
    }

    /// <summary>
    /// 积分配置项操作编码
    /// </summary>
    public class IntegralConfigCode
    {
        /// <summary>
        /// 完成课程学习,完成率100%
        /// oprateUserId 当前人用户id
        /// objectId 对应课程id
        /// </summary>
        public const string FinishStudy100 = "FinishStudy100";

        ///// <summary>
        ///// 完成课程学习,课程作业分数：100
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程作业id
        ///// </summary>
        //public const string FinishTask100 = "FinishTask100";

        ///// <summary>
        ///// 完成课程学习,课程作业分数：90-99
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程作业id
        ///// </summary>
        //public const string FinishTask9099 = "FinishTask9099";

        ///// <summary>
        ///// 完成课程学习,课程作业分数：80-89
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程作业id
        ///// </summary>
        //public const string FinishTask8089 = "FinishTask8089";

        ///// <summary>
        ///// 完成课程学习,课程作业分数：70-79
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程作业id
        ///// </summary>
        //public const string FinishTask7079 = "FinishTask7079";

        ///// <summary>
        ///// 完成课程学习,课程作业分数：60-69
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程作业id
        ///// </summary>
        //public const string FinishTask6069 = "FinishTask6069";

        ///// <summary>
        ///// 完成课程学习, 课程作业分数：小于 60
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程作业id
        ///// </summary>
        //public const string FinishTask0059 = "FinishTask0059";

        /// <summary>
        /// 完成课程学习课程作业分数：100；90-99；80-89；70-79；60-69；小于60	
        ///                         6，5，4，3，2，1
        /// oprateUserId 当前人用户id
        /// objectId 对应课程作业id
        /// </summary>
        public const string FinishTask = "FinishTask";

        /// <summary>
        /// 进行课程讨论, 讨论成功
        /// oprateUserId 当前人用户id
        /// objectId 对应课程id
        /// </summary>
        public const string CourseDiscuss = "CourseDiscuss";

        /// <summary>
        /// 删除课程讨论, 删除讨论成功
        /// oprateUserId 当前人用户id
        /// objectId 对应课程id
        /// </summary>
        public const string DeleteCourseDiscuss = "DeleteCourseDiscuss";

        /// <summary>
        /// 进行课程评价	评价成功
        /// oprateUserId 当前人用户id
        /// objectId 对应课程id
        /// </summary>
        public const string CourseEvaluate = "CourseEvaluate";

        /// <summary>
        /// 进行课程讨论, 讨论成功
        /// oprateUserId 当前人用户id
        /// objectId 对应课程id
        /// </summary>
        public const string ISVCourseDiscuss = "ISVCourseDiscuss";

        ///// <summary>
        ///// 讲师获得课程评价 评价成功:一星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string CSOneStar = "CSOneStar";

        ///// <summary>
        ///// 讲师获得课程评价 评价成功:二星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string CSTwoStar = "CSTwoStar";

        ///// <summary>
        ///// 讲师获得课程评价 评价成功:三星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string CSThreeStar = "CSThreeStar";

        ///// <summary>
        ///// 讲师获得课程评价 评价成功:四星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string CSFourStar = "CSFourStar";

        ///// <summary>
        ///// 讲师获得课程评价 评价成功:五星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string CSFiveStar = "CSFiveStar";

        /// <summary>
        /// 讲师获得课程评价
        /// oprateUserId 当前人用户id
        /// objectId 对应课程id
        /// </summary>
        public const string CSStar = "CSStar";

        ///// <summary>
        ///// 删除课程评价 一星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string DeleteOneStar = "DeleteOneStar";

        ///// <summary>
        ///// 删除课程评价 二星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string DeleteTwoStar = "DeleteTwoStar";

        ///// <summary>
        ///// 删除课程评价 三星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string DeleteThreeStar = "DeleteThreeStar";

        ///// <summary>
        ///// 删除课程评价  四星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string DeleteFourStar = "DeleteFourStar";

        ///// <summary>
        ///// 删除课程评价  五星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string DeleteFiveStar = "DeleteFiveStar";

        /// <summary>
        /// 删除课程评价
        /// oprateUserId 当前人用户id
        /// objectId 对应课程id
        /// </summary>
        public const string DeleteStar = "DeleteStar";

        ///// <summary>
        ///// 讲师获得讲师评价 评价成功:一星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string TeacherOneStar = "TeacherOneStar";

        ///// <summary>
        ///// 讲师获得讲师评价 评价成功:二星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string TeacherTwoStar = "TeacherTwoStar";

        ///// <summary>
        ///// 讲师获得讲师评价 评价成功:三星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string TeacherThreeStar = "TeacherThreeStar";

        ///// <summary>
        ///// 讲师获得讲师评价 评价成功:四星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string TeacherFourStar = "TeacherFourStar";

        ///// <summary>
        ///// 讲师获得讲师评价 评价成功:五星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string TeacherFiveStar = "TeacherOneStar";
        
        /// <summary>
        /// 讲师获得讲师评价
        /// oprateUserId 当前人用户id
        /// objectId 对应课程id
        /// </summary>
        public const string TeacherStar = "TeacherStar";

        ///// <summary>
        ///// 删除讲师评价  一星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string DeleteTeacherOneStar = "DeleteTeacherOneStar";

        ///// <summary>
        ///// 删除讲师评价 二星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string DeleteTeacherTwoStar = "DeleteTeacherTwoStar";

        ///// <summary>
        ///// 删除讲师评价 三星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string DeleteTeacherThreeStar = "DeleteTeacherThreeStar";

        ///// <summary>
        ///// 删除讲师评价 四星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string DeleteTeacherFourStar = "DeleteTeacherFourStar";

        ///// <summary>
        ///// 删除讲师评价  五星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string DeleteTeacherFiveStar = "DeleteTeacherFiveStar";

        /// <summary>
        /// 删除讲师评价
        /// oprateUserId 当前人用户id
        /// objectId 对应课程id
        /// </summary>
        public const string DeleteTeacherStar = "DeleteTeacherStar";

        /// <summary>
        /// 制作课程上传 制作课程中章节有该位老师的绑定
        /// oprateUserId 当前人用户id
        /// objectId 对应课程id
        /// </summary>
        public const string MakeWare = "MakeWare";

        /// <summary>
        /// 删除课程上传 删除的课程中章节有该位老师的绑定
        /// oprateUserId 当前人用户id
        /// objectId 对应课程id
        /// </summary>
        public const string DeleteWare = "DeleteWare";

        /// <summary>
        /// 点击阅读咨询，首次阅读才有积分
        /// oprateUserId 当前人用户id
        /// objectId 对应资讯id
        /// </summary>
        public const string FirstRead = "FirstRead";

        /// <summary>
        /// 成为销冠 后台配置为销冠
        /// oprateUserId 当前人用户id
        /// objectId 对应用户id
        /// </summary>
        public const string ConfigXiaoGuan = "ConfigXiaoGuan";

        /// <summary>
        /// 点击阅读咨询，首次阅读才有积分
        /// oprateUserId 当前人用户id
        /// objectId 对应状元说id
        /// </summary>
        public const string ZYSFirstRead = "ZYSFirstRead";

        ///// <summary>
        ///// 完成考试 完成考试100分
        ///// oprateUserId 当前人用户id
        ///// objectId 对应考试id
        ///// </summary>
        //public const string FinishExam100 = "FinishExam100";
        
        /// <summary>
        /// 完成考试 完成考试100分
        /// 完成考试,考试分数：100；90-99；80-89；70-79；60-69；小于60 
        ///                     6，5，4，3，2，1
        /// oprateUserId 当前人用户id
        /// objectId 对应考试id
        /// </summary>
        public const string FinishExam = "FinishExam";

        ///// <summary>
        ///// 完成考试 完成考试90-99分
        ///// oprateUserId 当前人用户id
        ///// objectId 对应考试id
        ///// </summary>
        //public const string FinishExam9099 = "FinishExam9099";

        ///// <summary>
        ///// 完成考试 完成考试80-89分
        ///// oprateUserId 当前人用户id
        ///// objectId 对应考试id
        ///// </summary>
        //public const string FinishExam8089 = "FinishExam8089";

        ///// <summary>
        ///// 完成考试 完成考试70-79分
        ///// oprateUserId 当前人用户id
        ///// objectId 对应考试id
        ///// </summary>
        //public const string FinishExam7079 = "FinishExam7079";

        ///// <summary>
        ///// 完成考试 完成考试60-69分
        ///// oprateUserId 当前人用户id
        ///// objectId 对应考试id
        ///// </summary>
        //public const string FinishExam6069 = "FinishExam6069";

        ///// <summary>
        ///// 完成考试 完成考试小于 60 分
        ///// oprateUserId 当前人用户id
        ///// objectId 对应考试id
        ///// </summary>
        //public const string FinishExam0059 = "FinishExam0059";

        /// <summary>
        ///  直播观看 直播观看完成
        /// oprateUserId 当前人用户id
        /// objectId 对应直播课件id
        /// </summary>
        public const string VideoLiveFinish = "VideoLiveFinish";

        /// <summary>
        ///  进行直播讨论 讨论成功
        /// oprateUserId 当前人用户id
        /// objectId 对应……id
        /// </summary>
        public const string VideoLiveDiscuss = "VideoLiveDiscuss";

        /// <summary>
        ///  删除直播讨论 删除成功
        /// oprateUserId 当前人用户id
        /// objectId 对应……id
        /// </summary>
        public const string DeleteVideoLiveDiscuss = "DeleteVideoLiveDiscuss";

        /// <summary>
        ///  进行直播评价 评价成功
        /// oprateUserId 当前人用户id
        /// objectId 对应……id
        /// </summary>
        public const string VideoLiveEvaluate = "VideoLiveEvaluate";

        /// <summary>
        ///  学员进行讲师评价 评价成功
        /// oprateUserId 当前人用户id
        /// objectId 对应讲师id
        /// </summary>
        public const string VideoLiveTeacherEvaluate = "VideoLiveTeacherEvaluate";

        /// <summary>
        ///  讲师进行直播讨论 讨论成功
        /// oprateUserId 当前人用户id
        /// objectId 对应……id
        /// </summary>
        public const string VideoLiveTeacherDiscuss = "VideoLiveTeacherDiscuss";

        ///// <summary>
        ///// 讲师获得课程评价 评价成功:一星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string VideoLiveOneStar = "VideoLiveOneStar";

        ///// <summary>
        ///// 讲师获得课程评价 评价成功:二星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string VideoLiveTwoStar = "VideoLiveTwoStar";

        ///// <summary>
        ///// 讲师获得课程评价 评价成功:三星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string VideoLiveThreeStar = "VideoLiveThreeStar";

        ///// <summary>
        ///// 讲师获得课程评价 评价成功:四星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string VideoLiveFourStar = "VideoLiveFourStar";

        ///// <summary>
        ///// 讲师获得课程评价 评价成功:五星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string VideoLiveFiveStar = "VideoLiveFiveStar";

        /// <summary>
        /// 删除直播讲师课程评价 
        /// oprateUserId 当前人用户id
        /// objectId 对应课程id
        /// </summary>
        public const string DeleteVideoLiveStar = "DeleteVideoLiveStar";

        ///// <summary>
        ///// 删除直播讲师课程评价 一星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string DeleteVideoLiveOneStar = "DeleteVideoLiveOneStar";

        ///// <summary>
        ///// 删除直播讲师课程评价 二星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string DeleteVideoLiveTwoStar = "DeleteVideoLiveTwoStar";

        ///// <summary>
        ///// 删除直播讲师课程评价 三星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string DeleteVideoLiveThreeStar = "DeleteVideoLiveThreeStar";

        ///// <summary>
        ///// 删除直播讲师课程评价 四星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string DeleteVideoLiveFourStar = "DeleteVideoLiveFourStar";

        ///// <summary>
        ///// 删除直播讲师课程评价 五星评价
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string DeleteVideoLiveFiveStar = "DeleteVideoLiveFiveStar";

        ///// <summary>
        ///// 讲师获得讲师评价 获得讲师评价一星
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string VideoLiveTeacherOneStar = "VideoLiveTeacherOneStar";

        ///// <summary>
        ///// 讲师获得讲师评价 获得讲师评价二星
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string VideoLiveTeacherTwoStar = "VideoLiveTeacherTwoStar";

        ///// <summary>
        ///// 讲师获得讲师评价 获得讲师评价三星
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string VideoLiveTeacherThreeStar = "VideoLiveTeacherThreeStar";

        ///// <summary>
        ///// 讲师获得讲师评价 获得讲师评价四星
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string VideoLiveTeacherFourStar = "VideoLiveTeacherFourStar";

        ///// <summary>
        ///// 讲师获得讲师评价 获得讲师评价五星
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string VideoLiveTeacherFiveStar = "VideoLiveTeacherFiveStar";
        
        /// <summary>
        ///删除讲师评价 
        /// oprateUserId 当前人用户id
        /// objectId 对应课程id
        /// </summary>
        public const string DeleteVideoLiveTeacherStar = "DeleteVideoLiveTeacherStar";

        ///// <summary>
        /////删除讲师评价  一星
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string DeleteVideoLiveTeacherOneStar = "DeleteVideoLiveTeacherOneStar";

        ///// <summary>
        ///// 删除讲师评价  二星
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string DeleteVideoLiveTeacherTwoStar = "DeleteVideoLiveTeacherTwoStar";

        ///// <summary>
        ///// 删除讲师评价  三星
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string DeleteVideoLiveTeacherThreeStar = "DeleteVideoLiveTeacherThreeStar";

        ///// <summary>
        ///// 删除讲师评价  四星
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string DeleteVideoLiveTeacherFourStar = "DeleteVideoLiveTeacherFourStar";

        ///// <summary>
        ///// 删除讲师评价  五星
        ///// oprateUserId 当前人用户id
        ///// objectId 对应课程id
        ///// </summary>
        //public const string DeleteVideoLiveTeacherFiveStar = "DeleteVideoLiveTeacherFiveStar";

        /// <summary>
        /// 制作直播课程 制作直播课程中章节有该位老师的绑定
        /// oprateUserId 当前人用户id
        /// objectId 对应课件id
        /// </summary>
        public const string MakeVideoLive = "MakeVideoLive";

        /// <summary>
        /// 删除直播课程 删除的直播课程中章节有该位老师的绑定
        /// oprateUserId 当前人用户id
        /// objectId 对应课件id
        /// </summary>
        public const string DeleteVideoLive = "DeleteVideoLive";

        /// <summary>
        /// 论坛发帖 
        /// oprateUserId 当前人用户id
        /// objectId 对应帖子id
        /// </summary>
        public const string Posting = "Posting";

        /// <summary>
        /// 论坛删帖
        /// oprateUserId 当前人用户id
        /// objectId 对应帖子id
        /// </summary>
        public const string DeletePost = "DeletePost";

        /// <summary>
        /// 论坛发帖  作为楼主，每有一个一级回复加分
        /// oprateUserId 当前人用户id
        /// objectId 对应帖子回复id
        /// </summary>
        public const string GetFirstReply = "GetFirstReply";

        /// <summary>
        /// 论坛发帖  作为楼主，删除一级回复减分
        /// oprateUserId 当前人用户id
        /// objectId 对应帖子回复id
        /// </summary>
        public const string DeleteGetFirstReply = "DeleteGetFirstReply";

        /// <summary>
        /// 论坛发帖  作为楼主，每个点赞加分
        /// oprateUserId 当前人用户id
        /// objectId 对应帖子点赞id
        /// </summary>
        public const string GetZan = "GetZan";

        /// <summary>
        /// 论坛发帖  作为楼主，取消点赞减分
        /// oprateUserId 当前人用户id
        /// objectId 对应帖子点赞id
        /// </summary>
        public const string CancelZan = "CancelZan";

        /// <summary>
        /// 论坛发帖  作为楼主，每个收藏加分
        /// oprateUserId 当前人用户id
        /// objectId 对应帖子收藏id
        /// </summary>
        public const string GetCollected = "GetCollected";

        /// <summary>
        /// 论坛发帖  作为楼主，取消收藏减分
        /// oprateUserId 当前人用户id
        /// objectId 对应帖子收藏id
        /// </summary>
        public const string CancelCollected = "CancelCollected";

        /// <summary>
        /// 论坛回帖 进行一级回复加分
        /// oprateUserId 当前人用户id
        /// objectId 对应帖子收藏id
        /// </summary>
        public const string ReplyPost = "ReplyPost";

        /// <summary>
        /// 论坛回帖 删除一级回复减分
        /// oprateUserId 当前人用户id
        /// objectId 对应帖子收藏id
        /// </summary>
        public const string DeleteReplyPost = "DeleteReplyPost";

        /// <summary>
        /// 执行内训 上传培训记录
        /// oprateUserId 当前人用户id
        /// objectId 对应培训记录id
        /// </summary>
        public const string UploadTrainLog = "UploadTrainLog";

        /// <summary>
        /// 完成线下培训课程 报名成功，结业状态为合格
        /// oprateUserId 当前人用户id
        /// objectId 对应培训课程id
        /// </summary>
        public const string StudyPass = "StudyPass";

        /// <summary>
        /// 线下培训班主讲 培训班结束，判定条件待定
        /// oprateUserId 当前人用户id
        /// objectId 对应班主讲id
        /// </summary>
        public const string ClassSpeaker = "ClassSpeaker";

        /// <summary>
        /// 进行投票 投票提交成功
        /// oprateUserId 当前人用户id
        /// objectId 对应投票id
        /// </summary>
        public const string SubmitVote = "SubmitVote";

        /// <summary>
        /// 学员进行讲师评价 评价成功
        /// oprateUserId 当前人用户id
        /// objectId 对应评价id
        /// </summary>
        public const string TeacherEvaluate = "TeacherEvaluate";

        /// <summary>
        /// 每日登陆成功
        /// oprateUserId 当前人用户id
        /// objectId 对应登录记录id
        /// </summary>
        public const string LoginSuccess = "LoginSuccess";

        /// <summary>
        /// 连续登陆
        /// oprateUserId 当前人用户id
        /// objectId 对应登录记录id
        /// </summary>
        public const string ContinueLogin = "ContinueLogin";
    }
}
