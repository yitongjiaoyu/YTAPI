﻿
//------------------------------------------------------------------
//文件名称：DbConetext
//模块名称：DbConetext
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using ELearning.Models;
namespace ELearning.DAL
{
    public  partial class EduDbContext : DbContext
    {
	 public EduDbContext()
            : base("name=ConnectionString")
       {
      }
		
	 
 
        /// <summary>
        /// EApplicationLog
        /// </summary>
        public virtual DbSet<EApplicationLog> EApplicationLog { get; set; }
   
 
        /// <summary>
        /// EAppVersion
        /// </summary>
        public virtual DbSet<EAppVersion> EAppVersion { get; set; }
   
 
        /// <summary>
        /// ECategory
        /// </summary>
        public virtual DbSet<ECategory> ECategory { get; set; }
   
 
        /// <summary>
        /// ECourse
        /// </summary>
        public virtual DbSet<ECourse> ECourse { get; set; }
   
 
        /// <summary>
        /// EDictionary
        /// </summary>
        public virtual DbSet<EDictionary> EDictionary { get; set; }
   
 
        /// <summary>
        /// EFeedback
        /// </summary>
        public virtual DbSet<EFeedback> EFeedback { get; set; }
   
 
        /// <summary>
        /// ELoginUser
        /// </summary>
        public virtual DbSet<ELoginUser> ELoginUser { get; set; }
   
 
        /// <summary>
        /// EMenu
        /// </summary>
        public virtual DbSet<EMenu> EMenu { get; set; }
   
 
        /// <summary>
        /// EOrder
        /// </summary>
        public virtual DbSet<EOrder> EOrder { get; set; }
   
 
        /// <summary>
        /// EOrg
        /// </summary>
        public virtual DbSet<EOrg> EOrg { get; set; }
   
 
        /// <summary>
        /// EPackage
        /// </summary>
        public virtual DbSet<EPackage> EPackage { get; set; }
   
 
        /// <summary>
        /// EPayLog
        /// </summary>
        public virtual DbSet<EPayLog> EPayLog { get; set; }
   
 
        /// <summary>
        /// ERole
        /// </summary>
        public virtual DbSet<ERole> ERole { get; set; }
   
 
        /// <summary>
        /// ESysUser
        /// </summary>
        public virtual DbSet<ESysUser> ESysUser { get; set; }
   
 
        /// <summary>
        /// EVideoLib
        /// </summary>
        public virtual DbSet<EVideoLib> EVideoLib { get; set; }
   
 
        /// <summary>
        /// EVideoStudyLog
        /// </summary>
        public virtual DbSet<EVideoStudyLog> EVideoStudyLog { get; set; }
   
 
        /// <summary>
        /// EVideoStudySum
        /// </summary>
        public virtual DbSet<EVideoStudySum> EVideoStudySum { get; set; }
   
 
        /// <summary>
        /// RChapterPaper
        /// </summary>
        public virtual DbSet<RChapterPaper> RChapterPaper { get; set; }
   
 
        /// <summary>
        /// RChapterVideoLib
        /// </summary>
        public virtual DbSet<RChapterVideoLib> RChapterVideoLib { get; set; }
   
 
        /// <summary>
        /// RCoursePaper
        /// </summary>
        public virtual DbSet<RCoursePaper> RCoursePaper { get; set; }
   
 
        /// <summary>
        /// RCourseVideoLib
        /// </summary>
        public virtual DbSet<RCourseVideoLib> RCourseVideoLib { get; set; }
   
 
        /// <summary>
        /// RMenuUR
        /// </summary>
        public virtual DbSet<RMenuUR> RMenuUR { get; set; }
   
 
        /// <summary>
        /// RPackageCourse
        /// </summary>
        public virtual DbSet<RPackageCourse> RPackageCourse { get; set; }
   
 
        /// <summary>
        /// RQuestionOption
        /// </summary>
        public virtual DbSet<RQuestionOption> RQuestionOption { get; set; }
   
 
        /// <summary>
        /// RRoleUser
        /// </summary>
        public virtual DbSet<RRoleUser> RRoleUser { get; set; }
   
 
        /// <summary>
        /// TBanner
        /// </summary>
        public virtual DbSet<TBanner> TBanner { get; set; }
   
 
        /// <summary>
        /// TChapter
        /// </summary>
        public virtual DbSet<TChapter> TChapter { get; set; }
   
 
        /// <summary>
        /// TInviteCodeLog
        /// </summary>
        public virtual DbSet<TInviteCodeLog> TInviteCodeLog { get; set; }
   
 
        /// <summary>
        /// TLoginToken
        /// </summary>
        public virtual DbSet<TLoginToken> TLoginToken { get; set; }
   
 
        /// <summary>
        /// TOption
        /// </summary>
        public virtual DbSet<TOption> TOption { get; set; }
   
 
        /// <summary>
        /// TOrder
        /// </summary>
        public virtual DbSet<TOrder> TOrder { get; set; }
   
 
        /// <summary>
        /// TOrgAccountLog
        /// </summary>
        public virtual DbSet<TOrgAccountLog> TOrgAccountLog { get; set; }
   
 
        /// <summary>
        /// TPaper
        /// </summary>
        public virtual DbSet<TPaper> TPaper { get; set; }
   
 
        /// <summary>
        /// TPaperOptionTestHistory
        /// </summary>
        public virtual DbSet<TPaperOptionTestHistory> TPaperOptionTestHistory { get; set; }
   
 
        /// <summary>
        /// TPaperQuestion
        /// </summary>
        public virtual DbSet<TPaperQuestion> TPaperQuestion { get; set; }
   
 
        /// <summary>
        /// TPaperQuestionTestHistory
        /// </summary>
        public virtual DbSet<TPaperQuestionTestHistory> TPaperQuestionTestHistory { get; set; }
   
 
        /// <summary>
        /// TPaperTestHistory
        /// </summary>
        public virtual DbSet<TPaperTestHistory> TPaperTestHistory { get; set; }
   
 
        /// <summary>
        /// TQuestion
        /// </summary>
        public virtual DbSet<TQuestion> TQuestion { get; set; }
   
 
        /// <summary>
        /// TQuestionItem
        /// </summary>
        public virtual DbSet<TQuestionItem> TQuestionItem { get; set; }
   
 
        /// <summary>
        /// TQuestionType
        /// </summary>
        public virtual DbSet<TQuestionType> TQuestionType { get; set; }
   
 
        /// <summary>
        /// TRegisterLog
        /// </summary>
        public virtual DbSet<TRegisterLog> TRegisterLog { get; set; }
   
 
        /// <summary>
        /// TSendMsgLog
        /// </summary>
        public virtual DbSet<TSendMsgLog> TSendMsgLog { get; set; }
   
 
        /// <summary>
        /// TTencentLog
        /// </summary>
        public virtual DbSet<TTencentLog> TTencentLog { get; set; }
   
 
        /// <summary>
        /// TUserCollectQuestion
        /// </summary>
        public virtual DbSet<TUserCollectQuestion> TUserCollectQuestion { get; set; }
   
 
        /// <summary>
        /// TUserErrorQuestion
        /// </summary>
        public virtual DbSet<TUserErrorQuestion> TUserErrorQuestion { get; set; }
   
 
        /// <summary>
        /// TUserPackage
        /// </summary>
        public virtual DbSet<TUserPackage> TUserPackage { get; set; }
   
 
        /// <summary>
        /// TVideoLive
        /// </summary>
        public virtual DbSet<TVideoLive> TVideoLive { get; set; }
  	 protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

      }

    }
}

