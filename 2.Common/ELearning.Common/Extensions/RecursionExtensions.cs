﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearning.Models;
using ELearning.Models.ViewModel;

namespace ELearning.Common
{
    /// <summary>
    /// 递归算法
    /// </summary>
    public static class RecursionExtensions
    {
        /// <summary>
        /// 递归list-组合树型结构
        /// </summary>
        /// <param name="alllist"></param>
        /// <param name="topid"></param>
        public static List<TreeViewModel> GetTree(List<TreeViewModel> alllist, Guid topid)
        {
            //下面是处理方法
            Dictionary<Guid, TreeViewModel> dic = new Dictionary<Guid, TreeViewModel>();

            foreach (TreeViewModel res in alllist)
            {
                dic.Add(res.id, res);
            }
            foreach (var res in dic.Values)
            {
                if (dic.ContainsKey(res.ParentID))
                {
                    if (dic[res.ParentID].children == null)
                    {
                        dic[res.ParentID].children = new List<TreeViewModel>();
                    }
                    dic[res.ParentID].children.Add(res);
                }
            }

            //得到最终的数据List
            var reslist = dic.Values.Where(t => t.ParentID == topid).ToList();
            return reslist;
        }

        /// <summary>
        /// 递归list
        /// </summary>
        /// <param name="alllist"></param>
        /// <param name="topid"></param>
        public static List<TreeViewModel> SubjectTree(List<TreeViewModel> alllist, Guid topid)
        {
            //下面是处理方法
            Dictionary<Guid, TreeViewModel> dic = new Dictionary<Guid, TreeViewModel>();

            foreach (TreeViewModel res in alllist)
            {
                dic.Add(res.id, res);

            }
            foreach (var res in dic.Values)
            {
                if (dic.ContainsKey(res.ParentID))
                {
                    if (dic[res.ParentID].children == null)
                    {
                        dic[res.ParentID].children = new List<TreeViewModel>();
                    }
                    dic[res.ParentID].children.Add(res);
                }
            }

            //得到最终的数据List
            var reslist = dic.Values.Where(t => t.ParentID == topid).ToList();
            return reslist;
        }

        /// <summary>
        /// 递归list-递归子节点的所有父节点
        /// </summary>
        /// <param name="alllist"></param>
        /// <param name="topid"></param>
        public static List<TreeViewModel> SubjectTreeF(List<TreeViewModel> alllist)
        {
            //下面是处理方法
            Dictionary<Guid, TreeViewModel> dic = new Dictionary<Guid, TreeViewModel>();

            foreach (TreeViewModel res in alllist)
            {
                dic.Add(res.id, res);

            }
            foreach (var res in dic.Values)
            {
                if (dic.ContainsKey(res.ParentID))
                {
                    if (dic[res.id].children == null)
                    {
                        dic[res.id].children = new List<TreeViewModel>();
                    }
                    dic[res.id].children.Add(dic[res.ParentID]);
                }
            }

            //得到最终的数据List
            var reslist = dic.Values.ToList();
            return reslist;
        }

        /// <summary>
        /// 递归list
        /// </summary>
        /// <param name="alllist"></param>
        public static List<TreeViewModel> SubjectTree(List<TreeViewModel> alllist)
        {
            //下面是处理方法
            Dictionary<Guid, TreeViewModel> dic = new Dictionary<Guid, TreeViewModel>();

            foreach (TreeViewModel res in alllist)
            {
                dic.Add(res.id, res);

            }
            foreach (var res in dic.Values)
            {
                if (dic.ContainsKey(res.ParentID))
                {
                    if (dic[res.ParentID].children == null)
                    {
                        dic[res.ParentID].children = new List<TreeViewModel>();
                    }
                    dic[res.ParentID].children.Add(res);
                }
            }

            //得到最终的数据List
            var reslist = dic.Values.ToList();
            return reslist;
        }


        /// <summary>
        /// 递归list
        /// </summary>
        /// <param name="alllist"></param>
        /// <param name="topid"></param>
        public static List<MenuViewModel> MenuTree(List<MenuViewModel> alllist, Guid topid)
        {
            //下面是处理方法
            Dictionary<Guid, MenuViewModel> dic = new Dictionary<Guid, MenuViewModel>();

            foreach (MenuViewModel res in alllist)
            {
                dic.Add(res.Id, res);
            }
            foreach (var res in dic.Values)
            {
                if (dic.ContainsKey(res.ParentId))
                {
                    if (dic[res.ParentId].Children == null)
                    {
                        dic[res.ParentId].Children = new List<MenuViewModel>();
                    }
                    dic[res.ParentId].Children.Add(res);
                }
            }

            //得到最终的数据List
            var reslist = dic.Values.Where(t => t.ParentId == topid).ToList();
            return reslist;
        }

        /// <summary>
        /// 遍历所有子节点，获取子节点的名称
        /// </summary>
        /// <param name="tn"></param>
        /// <param name="tnIDList"></param>
        public static void GetChildrenNode(TreeViewModel tn, ref List<string> tnIDList)
        {
            tnIDList.Add(tn.label);
            if (tn.children != null && tn.children.Count > 0)
            {
                foreach (var childrenNode in tn.children)
                {
                    GetChildrenNode(childrenNode, ref tnIDList);
                }
            }
            else return;
        }

    }
}
