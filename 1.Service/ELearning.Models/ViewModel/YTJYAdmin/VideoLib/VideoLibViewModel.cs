﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.YTJYAdmin.VideoLib
{
    /// <summary>
    /// 获取视频列表参数实体
    /// </summary>
    public class VideoLibRequest : BaseSearchRequest
    {
        /// <summary>
        /// 视频名称
        /// </summary>
        public string VideoName { set; get; }

        /// <summary>
        /// 大类分类编码
        /// </summary>
        public string CategoryId { set; get; }

        /// <summary>
        /// 基础分类编码
        /// </summary>
        public string TypeCode { set; get; }
    }

    public class VideoLibViewModel
    {
        /// <summary>
        /// 视频列表
        /// </summary>
        public List<VideoLibItem> Items { set; get; } = new List<VideoLibItem>();

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { set; get; }
    }

    public class VideoLibItem
    {
        public Guid Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 封面图片地址
        /// </summary>
        [Display(Name = "封面图片地址")]
        public string CoverUrl { get; set; }

        /// <summary>
        /// 展示所用
        /// </summary>
        public string ShowCoverUrl { get; set; }

        /// <summary>
        /// 大类分类Code
        /// </summary>
        [Display(Name = "大类分类Code")]
        public string CategoryId { get; set; }

        /// <summary>
        /// 大类分类名称
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// 班级基础分类code,基础班，高级版
        /// </summary>
        [Display(Name = "班级基础分类code,基础班，高级版")]
        public string TypeCode { get; set; }

        /// <summary>
        /// 班级基础分类名称
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [Display(Name = "序号")]
        public double Sort { get; set; }

        /// <summary>
        /// 文件Id
        /// </summary>
        [Display(Name = "文件Id")]
        public string FileId { get; set; }

        /// <summary>
        /// 视频下载地址
        /// </summary>
        [Display(Name = "视频下载地址")]
        public string DownloadUrl { get; set; }

        /// <summary>
        /// 视频播放地址,"Definition":0
        /// </summary>
        [Display(Name = "视频播放地址,Definition:0")]
        public string PlayUrl0 { get; set; }

        /// <summary>
        /// 视频播放地址,"Definition":10
        /// </summary>
        [Display(Name = "视频播放地址,Definition:10")]
        public string PlayUrl1 { get; set; }

        /// <summary>
        /// 视频播放地址,"Definition":20
        /// </summary>
        [Display(Name = "视频播放地址,Definition:20")]
        public string PlayUrl2 { get; set; }

        /// <summary>
        /// 视频播放地址,"Definition":30
        /// </summary>
        [Display(Name = "视频播放地址,Definition:30")]
        public string PlayUrl3 { get; set; }

        /// <summary>
        /// 转码状态：0.不需要转码，1.待转码，2.转码中，3.转码成功，4.转码失败，5.部分转码成功
        /// </summary>
        [Display(Name = "转码状态：0.不需要转码，1.待转码，2.转码中，3.转码成功，4.转码失败，5.部分转码成功")]
        public int Status { get; set; }

        /// <summary>
        /// 转码状态字符串
        /// </summary>
        public string StatusStr { get; set; }

        /// <summary>
        /// 文件大小,单位：KB
        /// </summary>
        [Display(Name = "文件大小,单位：KB")]
        public double Size { get; set; }

        /// <summary>
        /// 文件大小字符串
        /// </summary>
        public string SizeStr
        {
            get
            {
                var str = $"{Size:F0}KB";

                if (Size > 1024)
                {
                    Size = Size / 1024;
                    str = $"{Size:F1}MB";
                }

                if (Size > 1024)
                {
                    Size = Size / 1024;
                    str = $"{Size:F1}GB";
                }
                return str;
            }
        }

        /// <summary>
        /// 视频时长，单位:秒
        /// </summary>
        [Display(Name = "视频时长，单位:秒")]
        public int Duration { get; set; }

        /// <summary>
        /// 时长字符串
        /// </summary>
        public string DurationStr
        {
            get
            {
                var dt = new DateTime(2020, 1, 1, 0, 0, 0);
                dt = dt.AddSeconds(Duration);
                var str = dt.ToString("HH:mm:ss");
                if (Duration <= 0)
                    str = "--:--:--";
                return str;
            }
        }

        /// <summary>
        /// 转码视频备注
        /// </summary>
        [Display(Name = "转码视频备注")]
        public string ConvertVideoNote { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public string CreateTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        [Display(Name = "更新时间")]
        public string UpdateTime { get; set; }

        /// <summary>
        /// 创建人Id
        /// </summary>
        [Display(Name = "创建人Id")]
        public Guid CreateUserId { get; set; }

        /// <summary>
        /// 编辑人id
        /// </summary>
        [Display(Name = "编辑人id")]
        public Guid UpdateUserId { get; set; }
    }

    public class VideoLibModel
    {
        public Guid Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 封面图片地址
        /// </summary>
        [Display(Name = "封面图片地址")]
        public string CoverUrl { get; set; }

        /// <summary>
        /// 大类分类Code
        /// </summary>
        [Display(Name = "大类分类Code")]
        public string CategoryId { get; set; }

        /// <summary>
        /// 大类分类名称
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// 班级基础分类code,基础班，高级版
        /// </summary>
        [Display(Name = "班级基础分类code,基础班，高级版")]
        public string TypeCode { get; set; }

        /// <summary>
        /// 班级基础分类名称
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [Display(Name = "序号")]
        public double Sort { get; set; }

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { set; get; }

        /// <summary>
        /// 文件Id
        /// </summary>
        [Display(Name = "文件Id")]
        public string FileId { get; set; }

        /// <summary>
        /// 视频下载地址
        /// </summary>
        [Display(Name = "视频下载地址")]
        public string DownloadUrl { get; set; }

        /// <summary>
        /// 视频播放地址,"Definition":0
        /// </summary>
        [Display(Name = "视频播放地址,Definition:0")]
        public string PlayUrl0 { get; set; }

        /// <summary>
        /// 视频播放地址,"Definition":10
        /// </summary>
        [Display(Name = "视频播放地址,Definition:10")]
        public string PlayUrl1 { get; set; }

        /// <summary>
        /// 视频播放地址,"Definition":20
        /// </summary>
        [Display(Name = "视频播放地址,Definition:20")]
        public string PlayUrl2 { get; set; }

        /// <summary>
        /// 视频播放地址,"Definition":30
        /// </summary>
        [Display(Name = "视频播放地址,Definition:30")]
        public string PlayUrl3 { get; set; }

        /// <summary>
        /// 转码状态：0.不需要转码，1.待转码，2.转码中，3.转码成功，4.转码失败，5.部分转码成功
        /// </summary>
        [Display(Name = "转码状态：0.不需要转码，1.待转码，2.转码中，3.转码成功，4.转码失败，5.部分转码成功")]
        public int Status { get; set; }

        /// <summary>
        /// 文件大小,单位：KB
        /// </summary>
        [Display(Name = "文件大小,单位：KB")]
        public double Size { get; set; }

        /// <summary>
        /// 视频时长，单位:秒
        /// </summary>
        [Display(Name = "视频时长，单位:秒")]
        public int Duration { get; set; }

        /// <summary>
        /// 转码视频备注
        /// </summary>
        [Display(Name = "转码视频备注")]
        public string ConvertVideoNote { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        [Display(Name = "更新时间")]
        public DateTime UpdateTime { get; set; }

        /// <summary>
        /// 创建人Id
        /// </summary>
        [Display(Name = "创建人Id")]
        public Guid CreateUserId { get; set; }

        /// <summary>
        /// 编辑人id
        /// </summary>
        [Display(Name = "编辑人id")]
        public Guid UpdateUserId { get; set; }
    }

    /// <summary>
    /// 视频更新或者添加实体
    /// </summary>
    public class VideoLibPostModel
    {
        public Guid Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 封面图片地址
        /// </summary>
        [Display(Name = "封面图片地址")]
        public string CoverUrl { get; set; }

        /// <summary>
        /// 大类分类Code
        /// </summary>
        [Display(Name = "大类分类Code")]
        public string CategoryId { get; set; }

        /// <summary>
        /// 班级基础分类code,基础班，高级版
        /// </summary>
        [Display(Name = "班级基础分类code,基础班，高级版")]
        public string TypeCode { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [Display(Name = "序号")]
        public double Sort { get; set; }

        /// <summary>
        /// 文件Id
        /// </summary>
        [Display(Name = "文件Id")]
        public string FileId { get; set; }

        /// <summary>
        /// 文件大小,单位：KB
        /// </summary>
        [Display(Name = "文件大小,单位：KB")]
        public double Size { get; set; }

        /// <summary>
        /// 视频下载地址
        /// </summary>
        [Display(Name = "视频下载地址")]
        public string DownloadUrl { get; set; }

    }

    /// <summary>
    /// 视频更新或者添加实体
    /// </summary>
    public class VideoLibAddModel
    {
        public Guid Id { get; set; }

        /// <summary>
        /// 大类分类Code
        /// </summary>
        [Display(Name = "大类分类Code")]
        public string CategoryId { get; set; }

        /// <summary>
        /// 班级基础分类code,基础班，高级版
        /// </summary>
        [Display(Name = "班级基础分类code,基础班，高级版")]
        public string TypeCode { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [Display(Name = "序号")]
        public double Sort { get; set; }

        /// <summary>
        /// 视频列表
        /// </summary>
        public List<VideoLibAddItem> Items { get; set; } = new List<VideoLibAddItem>();
    }

    /// <summary>
    /// 视频更新或者添加实体
    /// </summary>
    public class VideoLibAddItem
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 文件Id
        /// </summary>
        [Display(Name = "文件Id")]
        public string FileId { get; set; }

        /// <summary>
        /// 文件大小,单位：KB
        /// </summary>
        [Display(Name = "文件大小,单位：KB")]
        public double Size { get; set; }

        /// <summary>
        /// 视频下载地址
        /// </summary>
        [Display(Name = "视频下载地址")]
        public string DownloadUrl { get; set; }
    }
}
