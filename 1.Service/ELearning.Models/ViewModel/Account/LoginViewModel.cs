﻿

namespace ELearning.Models.ViewModel.Account
{
    public class LoginRequest
    {
        /// <summary>
        /// 经销商号
        /// </summary>
        public string Jxsh { get; set; }
        /// <summary>
        /// 登录名
        /// </summary>
        public string LoginName { get; set; }

        /// <summary>
        /// 登录密码
        /// </summary>
        public string Password { get; set; }
    }

    public class LoginViewModel
    {
        /// <summary>
        /// 登录名
        /// </summary>
        public string LoginName { get; set; }

        /// <summary>
        /// 登录密码
        /// </summary>
        public string Password { get; set; }
    }
    
}
