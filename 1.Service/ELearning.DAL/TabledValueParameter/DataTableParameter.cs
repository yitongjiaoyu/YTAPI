﻿using System.Data;
using System.Data.SqlClient;

namespace ELearning.DAL
{
    public class DataTableParameter
    {
        private SqlParameter _sqlParameter;
        public DataTableParameter(DataTable dt)
        {
            _sqlParameter = new SqlParameter("@DT", dt);
            _sqlParameter.SqlDbType = SqlDbType.Structured;
            _sqlParameter.TypeName = "dbo.DT_" + dt.TableName;
        }
        public SqlParameter SqlParameter
        {
            get
            {
                return _sqlParameter;
            }
        }
        public string CommandText { get; set; }
    }
}
