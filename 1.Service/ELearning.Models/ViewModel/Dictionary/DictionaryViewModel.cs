﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel
{
    [DataContract]
    public class DictionaryViewModel
    {
        /// <summary>
        /// 字典ID
        /// </summary>
        [DataMember(Name = "value")]
        public Guid id { get; set; }

        /// <summary>
        /// 父级类别ID
        /// </summary>
          [DataMember(Name = "ParentID")]
        public Guid ParentID { get; set; }

        /// <summary>
        /// 父级类别名称
        /// </summary>
        [DataMember(Name = "ParentName")]
        public string ParentName { set; get; }
        /// <summary>
        /// 类别Code
        /// </summary>
        [DataMember(Name = "TypeCode")]
        public string TypeCode { get; set; }

        /// <summary>
        /// 类别名称
        /// </summary>
        [DataMember(Name = "label")]
        public string TypeName { get; set; }

        /// <summary>
        /// 类别排序
        /// </summary>
        [DataMember(Name = "Sort")]
        public int Sort { get; set; }

        /// <summary>
        /// 是否有子节点
        /// </summary>
        [DataMember(Name = "hasChildren")]
        public bool hasChildren { get; set; }
        /// <summary>
        /// 子字典项列表
        /// </summary>
        [DataMember(Name = "children")]
        public List<DictionaryViewModel> children { set; get; } = new List<DictionaryViewModel>();
       
    }

    /// <summary>
    /// select字典数据视图
    /// </summary>
    public class SelectDicViewModel
    {
        /// <summary>
        /// 选项列表
        /// </summary>
        public List<SelectDicItem> Items { get; set; } = new List<SelectDicItem>();
    }

    /// <summary>
    /// select字典数据视图
    /// </summary>
    public class SelectDicItem
    {
        /// <summary>
        /// 字典ID
        /// </summary>
        public Guid id { get; set; }

        /// <summary>
        /// 类别Code
        /// </summary>
        public string value { get; set; }

        /// <summary>
        /// 类别名称
        /// </summary>
        public string label { get; set; }

        /// <summary>
        /// 类别排序
        /// </summary>
        public int Sort { get; set; }
    }
}
