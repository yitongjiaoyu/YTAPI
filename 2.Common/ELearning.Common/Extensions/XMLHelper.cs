﻿using System.Collections;
using System.Xml;

namespace ELearning.Common.Extensions
{
    public class XMLHelper
    {
        /// <summary>
        /// 读取xml文件到hashtable
        /// </summary>
        public static XMLReadModel ReadToHashtable(string path)
        {
            var xr = new XMLReadModel();
            var xmldoc = new XmlDocument();
            xmldoc.Load(path);
            //获取节点列表 
            var topM = xmldoc.SelectNodes("//ColumnName");
            foreach (XmlElement element in topM)
            {
                var enabled = element.Attributes[0].Value;
                if (enabled == "true") //字段启用
                {
                    var dbProperty = element.GetElementsByTagName("DbProperty")[0].InnerText;
                    var excelProperty = element.GetElementsByTagName("ExcelProperty")[0].InnerText;
                    xr.ImportHashtable.Add(excelProperty, dbProperty);
                    xr.ExportHashtable.Add(dbProperty, excelProperty);
                }
            }
            return xr;
        }
    }


    public class XMLReadModel
    {
        /// <summary>
        /// 导入所需键值对
        /// </summary>
        public Hashtable ImportHashtable { set; get; } = new Hashtable();

        /// <summary>
        /// 导出所需键值对
        /// </summary>
        public Hashtable ExportHashtable { set; get; } = new Hashtable();
    }
}
