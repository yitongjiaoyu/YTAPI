/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2012                    */
/* Created on:     2020/6/22 20:00:45                           */
/*==============================================================*/


if exists (select 1
            from  sysobjects
           where  id = object_id('ECourse')
            and   type = 'U')
   drop table ECourse
go

if exists (select 1
            from  sysobjects
           where  id = object_id('EPackage')
            and   type = 'U')
   drop table EPackage
go

if exists (select 1
            from  sysobjects
           where  id = object_id('EVideoLib')
            and   type = 'U')
   drop table EVideoLib
go

if exists (select 1
            from  sysobjects
           where  id = object_id('RCourseVideoLib')
            and   type = 'U')
   drop table RCourseVideoLib
go

if exists (select 1
            from  sysobjects
           where  id = object_id('RPackageCourse')
            and   type = 'U')
   drop table RPackageCourse
go

/*==============================================================*/
/* Table: ECourse                                               */
/*==============================================================*/
create table ECourse (
   Id                   uniqueidentifier     not null,
   Name                 nvarchar(100)        null,
   Description          nvarchar(1000)       null,
   CoverUrl             nvarchar(300)        null,
   CategoryId           nvarchar(300)        null,
   TypeCode             nvarchar(300)        null,
   Sort                 float                null,
   CreateTime           datetime             null,
   UpdateTime           datetime             null,
   CreateUserId         uniqueidentifier     null,
   UpdateUserId         uniqueidentifier     null,
   IsDelete             bit                  null,
   IsUsed               bit                  null,
   constraint PK_ECOURSE primary key (Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('ECourse') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'ECourse' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   N'课程表', 
   'user', @CurrentUser, 'table', 'ECourse'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('ECourse')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Id')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'ECourse', 'column', 'Id'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'主键',
   'user', @CurrentUser, 'table', 'ECourse', 'column', 'Id'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('ECourse')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Name')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'ECourse', 'column', 'Name'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'名称',
   'user', @CurrentUser, 'table', 'ECourse', 'column', 'Name'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('ECourse')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Description')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'ECourse', 'column', 'Description'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'描述',
   'user', @CurrentUser, 'table', 'ECourse', 'column', 'Description'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('ECourse')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'CoverUrl')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'ECourse', 'column', 'CoverUrl'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'封面图片地址',
   'user', @CurrentUser, 'table', 'ECourse', 'column', 'CoverUrl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('ECourse')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'CategoryId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'ECourse', 'column', 'CategoryId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'大类分类Code',
   'user', @CurrentUser, 'table', 'ECourse', 'column', 'CategoryId'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('ECourse')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'TypeCode')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'ECourse', 'column', 'TypeCode'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'班级基础分类code,基础班，高级版',
   'user', @CurrentUser, 'table', 'ECourse', 'column', 'TypeCode'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('ECourse')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Sort')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'ECourse', 'column', 'Sort'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'序号',
   'user', @CurrentUser, 'table', 'ECourse', 'column', 'Sort'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('ECourse')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'CreateTime')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'ECourse', 'column', 'CreateTime'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'创建时间',
   'user', @CurrentUser, 'table', 'ECourse', 'column', 'CreateTime'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('ECourse')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'UpdateTime')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'ECourse', 'column', 'UpdateTime'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'更新时间',
   'user', @CurrentUser, 'table', 'ECourse', 'column', 'UpdateTime'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('ECourse')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'CreateUserId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'ECourse', 'column', 'CreateUserId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'创建人Id',
   'user', @CurrentUser, 'table', 'ECourse', 'column', 'CreateUserId'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('ECourse')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'UpdateUserId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'ECourse', 'column', 'UpdateUserId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'编辑人id',
   'user', @CurrentUser, 'table', 'ECourse', 'column', 'UpdateUserId'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('ECourse')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'IsDelete')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'ECourse', 'column', 'IsDelete'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'是否删除',
   'user', @CurrentUser, 'table', 'ECourse', 'column', 'IsDelete'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('ECourse')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'IsUsed')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'ECourse', 'column', 'IsUsed'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'是否启用',
   'user', @CurrentUser, 'table', 'ECourse', 'column', 'IsUsed'
go

/*==============================================================*/
/* Table: EPackage                                              */
/*==============================================================*/
create table EPackage (
   Id                   uniqueidentifier     not null,
   Name                 nvarchar(100)        null,
   Description          nvarchar(1000)       null,
   CoverUrl             nvarchar(300)        null,
   CategoryId           nvarchar(300)        null,
   TypeCode             nvarchar(300)        null,
   Sort                 float                null,
   OldPrice             float                null,
   NowPrice             float                null,
   CreateTime           datetime             null,
   UpdateTime           datetime             null,
   CreateUserId         uniqueidentifier     null,
   UpdateUserId         uniqueidentifier     null,
   IsDelete             bit                  null,
   IsUsed               bit                  null,
   constraint PK_EPACKAGE primary key (Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('EPackage') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'EPackage' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   N'套餐信息表', 
   'user', @CurrentUser, 'table', 'EPackage'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EPackage')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Id')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'Id'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Id',
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'Id'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EPackage')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Name')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'Name'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'名称',
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'Name'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EPackage')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Description')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'Description'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'描述',
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'Description'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EPackage')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'CoverUrl')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'CoverUrl'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'封面图片地址',
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'CoverUrl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EPackage')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'CategoryId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'CategoryId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'大类分类Code',
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'CategoryId'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EPackage')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'TypeCode')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'TypeCode'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'班级基础分类code,基础班，高级版',
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'TypeCode'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EPackage')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Sort')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'Sort'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'序号',
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'Sort'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EPackage')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'OldPrice')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'OldPrice'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'原价',
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'OldPrice'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EPackage')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'NowPrice')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'NowPrice'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'现在价格',
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'NowPrice'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EPackage')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'CreateTime')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'CreateTime'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'创建时间',
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'CreateTime'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EPackage')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'UpdateTime')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'UpdateTime'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'更新时间',
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'UpdateTime'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EPackage')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'CreateUserId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'CreateUserId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'创建人Id',
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'CreateUserId'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EPackage')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'UpdateUserId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'UpdateUserId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'编辑人id',
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'UpdateUserId'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EPackage')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'IsDelete')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'IsDelete'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'是否删除',
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'IsDelete'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EPackage')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'IsUsed')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'IsUsed'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'是否启用',
   'user', @CurrentUser, 'table', 'EPackage', 'column', 'IsUsed'
go

/*==============================================================*/
/* Table: EVideoLib                                             */
/*==============================================================*/
create table EVideoLib (
   Id                   uniqueidentifier     not null,
   Name                 nvarchar(100)        null,
   Description          nvarchar(1000)       null,
   CoverUrl             nvarchar(300)        null,
   CategoryId           nvarchar(300)        null,
   TypeCode             nvarchar(300)        null,
   Sort                 float                null,
   FileId               nvarchar(100)        null,
   DownloadUrl          nvarchar(1000)       null,
   PlayUrl0             nvarchar(1000)       null,
   PlayUrl1             nvarchar(1000)       null,
   PlayUrl2             nvarchar(1000)       null,
   PlayUrl3             nvarchar(1000)       null,
   Status               int                  null,
   ConvertVideoNote     nvarchar(1000)       null,
   CreateTime           datetime             null,
   UpdateTime           datetime             null,
   CreateUserId         uniqueidentifier     null,
   UpdateUserId         uniqueidentifier     null,
   IsDelete             bit                  null,
   constraint PK_EVIDEOLIB primary key (Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('EVideoLib') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'EVideoLib' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   N'视频库表', 
   'user', @CurrentUser, 'table', 'EVideoLib'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Id')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'Id'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'主键',
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'Id'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Name')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'Name'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'名称',
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'Name'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Description')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'Description'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'描述',
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'Description'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'CoverUrl')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'CoverUrl'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'封面图片地址',
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'CoverUrl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'CategoryId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'CategoryId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'大类分类Code',
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'CategoryId'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'TypeCode')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'TypeCode'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'班级基础分类code,基础班，高级版',
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'TypeCode'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Sort')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'Sort'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'序号',
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'Sort'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'FileId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'FileId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'文件Id',
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'FileId'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'DownloadUrl')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'DownloadUrl'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'视频下载地址',
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'DownloadUrl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PlayUrl0')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'PlayUrl0'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'视频播放地址,"Definition":0',
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'PlayUrl0'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PlayUrl1')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'PlayUrl1'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'视频播放地址,"Definition":10',
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'PlayUrl1'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PlayUrl2')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'PlayUrl2'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'视频播放地址,"Definition":20',
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'PlayUrl2'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PlayUrl3')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'PlayUrl3'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'视频播放地址,"Definition":30',
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'PlayUrl3'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Status')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'Status'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'转码状态：0.不需要转码，1.待转码，2.转码中，3.转码成功，4.转码失败，5.部分转码成功',
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'Status'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ConvertVideoNote')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'ConvertVideoNote'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'转码视频备注',
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'ConvertVideoNote'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'CreateTime')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'CreateTime'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'创建时间',
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'CreateTime'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'UpdateTime')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'UpdateTime'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'更新时间',
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'UpdateTime'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'CreateUserId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'CreateUserId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'创建人Id',
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'CreateUserId'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'UpdateUserId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'UpdateUserId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'编辑人id',
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'UpdateUserId'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('EVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'IsDelete')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'IsDelete'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'是否删除',
   'user', @CurrentUser, 'table', 'EVideoLib', 'column', 'IsDelete'
go

/*==============================================================*/
/* Table: RCourseVideoLib                                       */
/*==============================================================*/
create table RCourseVideoLib (
   Id                   uniqueidentifier     not null,
   CourseId             uniqueidentifier     null,
   VideoLibId           uniqueidentifier     null,
   Sort                 float                null,
   CreateTime           datetime             null,
   UpdateTime           datetime             null,
   CreateUserId         uniqueidentifier     null,
   UpdateUserId         uniqueidentifier     null,
   IsDelete             bit                  null,
   constraint PK_RCOURSEVIDEOLIB primary key (Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('RCourseVideoLib') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'RCourseVideoLib' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   N'课程视频关联表', 
   'user', @CurrentUser, 'table', 'RCourseVideoLib'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('RCourseVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Id')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'RCourseVideoLib', 'column', 'Id'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Id',
   'user', @CurrentUser, 'table', 'RCourseVideoLib', 'column', 'Id'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('RCourseVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'CourseId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'RCourseVideoLib', 'column', 'CourseId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'课程Id',
   'user', @CurrentUser, 'table', 'RCourseVideoLib', 'column', 'CourseId'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('RCourseVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'VideoLibId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'RCourseVideoLib', 'column', 'VideoLibId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'视频id',
   'user', @CurrentUser, 'table', 'RCourseVideoLib', 'column', 'VideoLibId'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('RCourseVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Sort')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'RCourseVideoLib', 'column', 'Sort'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'序号',
   'user', @CurrentUser, 'table', 'RCourseVideoLib', 'column', 'Sort'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('RCourseVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'CreateTime')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'RCourseVideoLib', 'column', 'CreateTime'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'创建时间',
   'user', @CurrentUser, 'table', 'RCourseVideoLib', 'column', 'CreateTime'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('RCourseVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'UpdateTime')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'RCourseVideoLib', 'column', 'UpdateTime'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'更新时间',
   'user', @CurrentUser, 'table', 'RCourseVideoLib', 'column', 'UpdateTime'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('RCourseVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'CreateUserId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'RCourseVideoLib', 'column', 'CreateUserId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'创建人Id',
   'user', @CurrentUser, 'table', 'RCourseVideoLib', 'column', 'CreateUserId'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('RCourseVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'UpdateUserId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'RCourseVideoLib', 'column', 'UpdateUserId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'编辑人id',
   'user', @CurrentUser, 'table', 'RCourseVideoLib', 'column', 'UpdateUserId'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('RCourseVideoLib')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'IsDelete')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'RCourseVideoLib', 'column', 'IsDelete'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'是否删除',
   'user', @CurrentUser, 'table', 'RCourseVideoLib', 'column', 'IsDelete'
go

/*==============================================================*/
/* Table: RPackageCourse                                        */
/*==============================================================*/
create table RPackageCourse (
   Id                   uniqueidentifier     not null,
   CourseId             uniqueidentifier     null,
   Package              uniqueidentifier     null,
   Sort                 float                null,
   CreateTime           datetime             null,
   UpdateTime           datetime             null,
   CreateUserId         uniqueidentifier     null,
   UpdateUserId         uniqueidentifier     null,
   IsDelete             bit                  null,
   constraint PK_RPACKAGECOURSE primary key (Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('RPackageCourse') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'RPackageCourse' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   N'套餐课程关联表', 
   'user', @CurrentUser, 'table', 'RPackageCourse'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('RPackageCourse')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Id')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'RPackageCourse', 'column', 'Id'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Id',
   'user', @CurrentUser, 'table', 'RPackageCourse', 'column', 'Id'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('RPackageCourse')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'CourseId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'RPackageCourse', 'column', 'CourseId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'课程Id',
   'user', @CurrentUser, 'table', 'RPackageCourse', 'column', 'CourseId'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('RPackageCourse')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Package')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'RPackageCourse', 'column', 'Package'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'套餐Id',
   'user', @CurrentUser, 'table', 'RPackageCourse', 'column', 'Package'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('RPackageCourse')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Sort')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'RPackageCourse', 'column', 'Sort'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'序号',
   'user', @CurrentUser, 'table', 'RPackageCourse', 'column', 'Sort'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('RPackageCourse')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'CreateTime')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'RPackageCourse', 'column', 'CreateTime'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'创建时间',
   'user', @CurrentUser, 'table', 'RPackageCourse', 'column', 'CreateTime'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('RPackageCourse')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'UpdateTime')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'RPackageCourse', 'column', 'UpdateTime'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'更新时间',
   'user', @CurrentUser, 'table', 'RPackageCourse', 'column', 'UpdateTime'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('RPackageCourse')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'CreateUserId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'RPackageCourse', 'column', 'CreateUserId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'创建人Id',
   'user', @CurrentUser, 'table', 'RPackageCourse', 'column', 'CreateUserId'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('RPackageCourse')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'UpdateUserId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'RPackageCourse', 'column', 'UpdateUserId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'编辑人id',
   'user', @CurrentUser, 'table', 'RPackageCourse', 'column', 'UpdateUserId'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('RPackageCourse')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'IsDelete')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'RPackageCourse', 'column', 'IsDelete'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   N'是否删除',
   'user', @CurrentUser, 'table', 'RPackageCourse', 'column', 'IsDelete'
go

