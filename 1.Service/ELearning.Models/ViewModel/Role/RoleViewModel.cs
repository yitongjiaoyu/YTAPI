﻿using System;
using System.Collections.Generic;

namespace ELearning.Models.ViewModel.Role
{
    /// <summary>
    /// 获取数据请求类
    /// </summary>
    public class GetRoleRequest:BaseRequest
    {
        /// <summary>
        /// 角色名称
        /// </summary>
        public string RoleName { set; get; }

        /// <summary>
        /// 平台类型:JXS,SGMW,ISV
        /// </summary>
        public string Type { get; set; }
    }

    /// <summary>
    /// 返回的数据model
    /// </summary>
    public class RoleViewModel
    {
        /// <summary>
        /// 总数量
        /// </summary>
        public int Total { get; set; }

        /// <summary>
        /// 角色信息列表
        /// </summary>
        public List<RoleItem> Items { set; get; }=new List<RoleItem>();
    }


    public class RoleItem
    {
        /// <summary>
        /// 角色ID
        /// </summary>          
        public Guid id { get; set; }

        /// <summary>
        /// 父级ID
        /// </summary>
        public Guid ParentID { get; set; }

        /// <summary>
        /// 角色名称
        /// </summary>          
        public string RoleName { get; set; }

        /// <summary>
        /// 父级角色名称
        /// </summary>          
        public string ParentName { get; set; }

        /// <summary>
        /// 角色类别
        /// </summary>          
        public string RoleAuthorizationType { get; set; }

        /// <summary>
        /// 标识码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 组织编码
        /// </summary>
        public string OrgCode { get; set; }

        /// <summary>
        /// 备注
        /// </summary>          
        public string Text { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>          
        public string Creater { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>          
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 平台类型:JXS,SGMW,ISV
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 是否逻辑删除
        /// </summary>          
        public bool IsDelete { get; set; }

        /// <summary>
        /// 菜单id数组
        /// </summary>
        public List<Guid> MenuIdList { get; set; } = new List<Guid>();

        /// <summary>
        /// 全部菜单权限id，包括父级、父级的父级一直到根节点的菜单id
        /// </summary>
        public List<Guid> FullMenuIdList { get; set; } = new List<Guid>();

        /// <summary>
        /// 半选中节点id列表
        /// </summary>
        public List<Guid> HalfMenuIdList { get; set; } = new List<Guid>();

        /// <summary>
        /// 子节点列表
        /// </summary>
        public List<RoleItem> children { get; set; } = new List<RoleItem>();
    }

    public class RoleMenuModel
    {
        /// <summary>
        /// 菜单id
        /// </summary>
        public Guid Id { set; get; }

        /// <summary>
        /// 角色ID
        /// </summary>          
        public Guid RoleID { get; set; }

        /// <summary>
        /// 父级菜单id
        /// </summary>
        public Guid ParentId { set; get; }

        /// <summary>
        /// 菜单名称
        /// </summary>
        public string Title { set; get; }

        /// <summary>
        /// 菜单图标
        /// </summary>
        public string Icon { set; get; }

        /// <summary>
        /// 菜单跳转路径
        /// </summary>
        public string Path { set; get; }
        public string Redirect { set; get; }

        /// <summary>
        /// 菜单别称
        /// </summary>
        public string Name { set; get; }

        /// <summary>
        /// 菜单对应页面地址
        /// </summary>
        public string Component { set; get; }

        /// <summary>
        /// 是否删除
        /// </summary>
        public bool IsDelete { set; get; }
        public bool NoCache { set; get; }

        /// <summary>
        /// 是否固定
        /// </summary>
        public bool Affix { set; get; }

        /// <summary>
        /// 是否启用
        /// </summary>
        public bool IsUsed { set; get; }
    }
}
