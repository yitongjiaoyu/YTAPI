﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Common
{
    /// <summary>
    /// 开放级别
    /// </summary>
    public enum OpenLevelEnum
    {
        /// <summary>
        /// 全平台
        /// </summary>
        [Description("全平台")]
        Level1 = 1,
        /// <summary>
        /// 组织内
        /// </summary>
        [Description("组织内")]
        Level2 = 2,
        /// <summary>
        /// 仅自己
        /// </summary>
        [Description("仅自己")]
        Level3 = 3,
        /// <summary>
        /// 其他
        /// </summary>
        [Description("其他")]
        Level4 = 4,
    }
}
