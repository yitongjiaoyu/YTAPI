﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.YTJYAdmin.Paper
{
    public class SitePaperDetailRequest
    {
        /// <summary>
        /// 试卷id
        /// </summary>
        public Guid PaperId { set; get; }

        /// <summary>
        /// 课程Id,可空
        /// </summary>
        [Display(Name = "课程Id")]
        public Guid CourseId { get; set; }

        /// <summary>
        /// 套餐Id,可空
        /// </summary>
        [Display(Name = "套餐Id")]
        public Guid PackageId { get; set; }

        /// <summary>
        /// 章节id
        /// </summary>
        public Guid ChapterId { get; set; }
    }

    public class SitePaperDetailViewModel
    {
        /// <summary>
        /// 试卷id
        /// </summary>
        public Guid PaperId { set; get; }

        /// <summary>
        /// 做题历史记录id
        /// </summary>
        public Guid PaperTestHistoryId { set; get; }

        /// <summary>
        /// 试卷名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 类型：1.真题，2.模拟题
        /// </summary>
        [Display(Name = "类型：1.真题，2.模拟题")]
        public int Type { get; set; }

        public string TypeStr
        {
            get
            {
                var str = "真题";
                if (Type == 2)
                    str = "模拟题";
                return str;
            }
        }

        /// <summary>
        /// 考试时长,单位：分钟
        /// </summary>
        public int TimeSpan { get; set; }

        /// <summary>
        /// 试卷总分数
        /// </summary>
        public double TotalScore { get; set; }

        /// <summary>
        /// 题目类型列表
        /// </summary>
        public List<SiteQuestionTypeModel> Items = new List<SiteQuestionTypeModel>();
    }

    /// <summary>
    /// 题目类型实体
    /// </summary>
    public class SiteQuestionTypeModel
    {
        /// <summary>
        /// 题目类型编码
        /// </summary>
        public string QuestionTypeCode { get; set; }

        /// <summary>
        /// 题目类型名称
        /// </summary>
        public string QuestionTypeName { get; set; }

        /// <summary>
        /// 题目列表
        /// </summary>
        public List<SiteQuestionModel> Items = new List<SiteQuestionModel>();
    }

    /// <summary>
    /// 题目实体
    /// </summary>
    public class SiteQuestionModel
    {
        /// <summary>
        /// 题目id
        /// </summary>
        public Guid QuestionId { get; set; }

        /// <summary>
        /// 子题目ID
        /// </summary>
        public Guid QuestionItemId { get; set; }

        /// <summary>
        /// 题目主题干
        /// </summary>
        public string MainName { get; set; }

        /// <summary>
        /// 题目题干
        /// </summary>
        public string QuestionName { get; set; }

        /// <summary>
        /// 题目答案，主观题，简答题等类型使用该字段
        /// </summary>
        [Display(Name = "题目答案，主观题，简答题等类型使用该字段")]
        public string Answer { get; set; }

        /// <summary>
        /// 解析
        /// </summary>
        public string Analysis { get; set; }

        /// <summary>
        /// 成绩
        /// </summary>
        public double Grade { get; set; }
        
        /// <summary>
        /// 选项列表
        /// </summary>
        public List<SiteQuestionOptionModel> Items = new List<SiteQuestionOptionModel>();
    }

    /// <summary>
    /// 题目选项实体
    /// </summary>
    public class SiteQuestionOptionModel
    {
        /// <summary>
        /// 选项id
        /// </summary>
        public Guid OptionId { get; set; }

        /// <summary>
        /// 选项内容
        /// </summary>
        public string OptionName { get; set; }

        /// <summary>
        /// 是否是正确答案
        /// </summary>
        public bool IsRight { get; set; }
    }

    public class SitePaperDetailModel
    {
        /// <summary>
        /// 题目id
        /// </summary>
        public Guid QuestionId { get; set; }
        
        /// <summary>
        /// 子题目ID
        /// </summary>
        public Guid QuestionItemId { get; set; }

        /// <summary>
        /// 题目主题干
        /// </summary>
        public string MainName { get; set; }

        /// <summary>
        /// 题目题干
        /// </summary>
        public string QuestionName { get; set; }

        /// <summary>
        /// 题目类型编码
        /// </summary>
        public string QuestionTypeCode { get; set; }

        /// <summary>
        /// 题目类型名称
        /// </summary>
        public string QuestionTypeName { get; set; }

        /// <summary>
        /// 题目答案，主观题，简答题等类型使用该字段
        /// </summary>
        [Display(Name = "题目答案，主观题，简答题等类型使用该字段")]
        public string Answer { get; set; }

        /// <summary>
        /// 解析
        /// </summary>
        public string Analysis { get; set; }

        /// <summary>
        /// 正确答案
        /// </summary>
        public string RightAnswer { get; set; }

        /// <summary>
        /// 题目分数
        /// </summary>
        public double Grade { get; set; }

        /// <summary>
        /// 组织编码
        /// </summary>
        public string OrgCode { get; set; }

        /// <summary>
        /// 选项id
        /// </summary>
        public Guid? OptionId { get; set; }

        /// <summary>
        /// 选项内容
        /// </summary>
        public string OptionName { get; set; }

        /// <summary>
        /// 是否是正确答案
        /// </summary>
        public bool? IsRight { get; set; }
        
        /// <summary>
        /// 试卷名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 基础分类编码
        /// </summary>
        public string TypeCode { get; set; }

        /// <summary>
        /// 分类编码
        /// </summary>
        public string CategoryId { get; set; }

        /// <summary>
        /// 类型：1.真题，2.模拟题
        /// </summary>
        [Display(Name = "类型：1.真题，2.模拟题")]
        public int Type { get; set; }

        /// <summary>
        /// 考试时长,单位：秒
        /// </summary>
        public int TimeSpan { get; set; }

        /// <summary>
        /// 题目总数量
        /// </summary>
        public int TotalCount { get; set; }

        /// <summary>
        /// 年份
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// 题目难度
        /// </summary>
        public int Difficulty { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public double Sort { get; set; }

        /// <summary>
        /// 选项序号
        /// </summary>
        public double? QoSort { get; set; }

        /// <summary>
        /// 做对题目数量
        /// </summary>
        [Display(Name = "做对题目数量")]
        public int RightCount { get; set; }

        /// <summary>
        /// 做错题目数量
        /// </summary>
        [Display(Name = "做错题目数量")]
        public int ErrorCount { get; set; }

        /// <summary>
        /// 上次做的题目id
        /// </summary>
        [Display(Name = "上次做的题目id")]
        public Guid LastQuestionId { get; set; }

        /// <summary>
        /// 成绩
        /// </summary>
        [Display(Name = "成绩")]
        public double TotalScore { get; set; }

        /// <summary>
        /// 试卷时长，单位：分钟
        /// </summary>
        [Display(Name = "试卷时长，单位：分钟")]
        public int PaperTimeSpan { get; set; }

        /// <summary>
        /// 试卷总分
        /// </summary>
        [Display(Name = "试卷总分")]
        public double PaperTotalScore { get; set; }
    }
}
