//-------------------------------------------------------------------
//文件名称：TUserCollectQuestion.cs
//模块名称：TUserCollectQuestionModel层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ELearning.Models
{	
	 /// <summary>
    /// TUserCollectQuestion表实体类
    /// </summary>
	[Table("TUserCollectQuestion")]
	public partial class TUserCollectQuestion
    {
		
		/// <summary>
		/// Id
		/// </summary>
				[Key]
	[Display(Name = "Id")]
		public Guid Id { get; set; }
		
		/// <summary>
		/// 用户Id
		/// </summary>
			[Display(Name = "用户Id")]
		public Guid UserId { get; set; }
		
		/// <summary>
		/// 试卷Id
		/// </summary>
			[Display(Name = "试卷Id")]
		public Guid PaperId { get; set; }
		
		/// <summary>
		/// 题目Id
		/// </summary>
			[Display(Name = "题目Id")]
		public Guid QuestionId { get; set; }
		
		/// <summary>
		/// 创建时间
		/// </summary>
			[Display(Name = "创建时间")]
		public DateTime CreateTime { get; set; }
		
		/// <summary>
		/// 更新时间
		/// </summary>
			[Display(Name = "更新时间")]
		public DateTime UpdateTime { get; set; }
		
		/// <summary>
		/// 创建人Id
		/// </summary>
			[Display(Name = "创建人Id")]
		public Guid CreateUserId { get; set; }
		
		/// <summary>
		/// 编辑人id
		/// </summary>
			[Display(Name = "编辑人id")]
		public Guid UpdateUserId { get; set; }
		
		/// <summary>
		/// 是否删除
		/// </summary>
			[Display(Name = "是否删除")]
		public bool IsDelete { get; set; }
		   
    }
}

