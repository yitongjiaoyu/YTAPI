//-------------------------------------------------------------------
//文件名称：RRoleUser.cs
//模块名称：RRoleUser业务逻辑接口层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//修改时间：2019年8月25日
//-----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearning.DAL;
using ELearning.Models;

namespace ELearning.BLL
{
    /// <summary>
    /// RRoleUser业务逻辑接口层
    /// </summary>
	public partial class RRoleUserService : BaseService<RRoleUser>
	{
        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="ids">id列表</param>
	    public int BatchDelete(List<Guid>ids)
        {
            return RepositoryFactory.RRoleUserRepository.BatchDelete(ids);
        }
	}
}


