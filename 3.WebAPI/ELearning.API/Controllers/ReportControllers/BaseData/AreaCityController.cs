﻿using ELearning.BLL;
using ELearning.Common;
using ELearning.Common.Extensions;
using ELearning.Common.Model;
using ELearning.Models;
using ELearning.Models.ViewModel;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Http;

namespace ELearning.API.Controllers.ReportControllers
{
    /// <summary>
    /// 区域、省份、城市基础信息相关业务
    /// </summary>
    public partial class AreaCityController : BaseApiController
    {
        EAreaCityService areaCityService => new EAreaCityService();

        /// <summary>
        /// 获取区域、省份、城市信息
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost, Route("api/area/getAreaCitys")]
        public ServiceResponse<Page<EAreaCity>> GetAreaCitys(AreaCityRequest req)
        {
            Expression<Func<EAreaCity, bool>> whereLamdba = (x) => x.IsDelete == false;

            if (!string.IsNullOrEmpty(req.AreaName)) whereLamdba = whereLamdba.And(x => x.AreaName.Contains(req.AreaName));
            if (!string.IsNullOrEmpty(req.ProvinceName)) whereLamdba = whereLamdba.And(x => x.ProvinceName.Contains(req.ProvinceName));
            if (!string.IsNullOrEmpty(req.CityName)) whereLamdba = whereLamdba.And(x => x.CityName.Contains(req.CityName));
            if (req.TypeLevel != -1) whereLamdba = whereLamdba.And(x => x.TypeLevel == req.TypeLevel);

            var data = areaCityService.FindPageList(req.Page, req.PageSize, out int total, req.OrderBy, req.IsAsc ? "asc" : "desc", whereLamdba).ToList();

            Page<EAreaCity> page = new Page<EAreaCity>
            {
                Items = data,
                TotalItems = total,
            };

            return ServiceResponse<Page<EAreaCity>>.SuccessResponse(page);
        }

        /// <summary>
        /// 添加区域、省份、城市信息
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        [HttpPost, Route("api/area/addAreaCity")]
        public ServiceResponse<bool> AddAreaCity(EAreaCity post)
        {
            post.ID = Guid.NewGuid();

            if (post.TypeLevel == AreaLevelEnum.Level1.IntValue())
            {
                var exist = areaCityService.Find(x => !x.IsDelete && x.AreaName == post.AreaName);
                if (exist.IsNotNull()) return ServiceResponse<bool>.SuccessResponse("名称已存在", false);

                post.AreaCode = areaCityService.FindList(x => x.TypeLevel == post.TypeLevel).OrderByDescending(m => m.AreaCode).FirstOrDefault().AreaCode;
                post.AreaCode += 1;
            }
            else if (post.TypeLevel == AreaLevelEnum.Level2.IntValue())
            {
                var exist = areaCityService.Find(x => !x.IsDelete && x.ProvinceName == post.ProvinceName);
                if (exist.IsNotNull()) return ServiceResponse<bool>.SuccessResponse("名称已存在", false);

                post.ProvinceCode = areaCityService.FindList(x => x.TypeLevel == post.TypeLevel).OrderByDescending(m => m.ProvinceCode).FirstOrDefault().ProvinceCode;
                post.ProvinceCode += 1;
            }
            else if (post.TypeLevel == AreaLevelEnum.Level3.IntValue())
            {
                var exist = areaCityService.Find(x => !x.IsDelete && x.CityName == post.CityName);
                if (exist.IsNotNull()) return ServiceResponse<bool>.SuccessResponse("名称已存在", false);

                post.CityCode = areaCityService.FindList(x => x.TypeLevel == post.TypeLevel).OrderByDescending(m => m.CityCode).FirstOrDefault().CityCode;
                post.CityCode += 1;
            }

            post.Creater = post.Modifyer = CurrentUserModel.UserId.ToString();
            post.CreateTime = post.ModifyTime = DateTime.Now;
            post.IsDelete = false;

            areaCityService.Add(post);

            return ServiceResponse<bool>.SuccessResponse(true);
        }

        /// <summary>
        /// 更新区域、省份、城市信息
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        [HttpPost, Route("api/area/updateAreaCity")]
        public ServiceResponse<bool> UpdateAreaCity(EAreaCity post)
        {
            if (post.TypeLevel == AreaLevelEnum.Level1.IntValue())
            {
                var exist = areaCityService.Find(x => !x.IsDelete && x.AreaName == post.AreaName && x.ID != post.ID);
                if (exist.IsNotNull()) return ServiceResponse<bool>.SuccessResponse("名称已存在", false);
            }
            else if (post.TypeLevel == AreaLevelEnum.Level2.IntValue())
            {
                var exist = areaCityService.Find(x => !x.IsDelete && x.ProvinceName == post.ProvinceName && x.ID != post.ID);
                if (exist.IsNotNull()) return ServiceResponse<bool>.SuccessResponse("名称已存在", false);
            }
            else if (post.TypeLevel == AreaLevelEnum.Level3.IntValue())
            {
                var exist = areaCityService.Find(x => !x.IsDelete && x.CityName == post.CityName && x.ID != post.ID);
                if (exist.IsNotNull()) return ServiceResponse<bool>.SuccessResponse("名称已存在", false);
            }
            post.Modifyer = CurrentUserModel.UserId.ToString();
            post.ModifyTime = DateTime.Now;

            areaCityService.Update(post);

            return ServiceResponse<bool>.SuccessResponse(true);
        }

    }
}