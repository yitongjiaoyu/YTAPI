﻿using System;
using System.Collections.Generic;

namespace ELearning.Models.ViewModel.WinnerNews
{
    public class WinnerNewsViewModel
    {
        /// <summary>
        /// 新闻列表
        /// </summary>
        public List<WinnerNewsItem> NewsItems = new List<WinnerNewsItem>();

        /// <summary>
        /// 总条数
        /// </summary>
        public int TotalCount { set; get; }
    }


    public class WinnerNewsItem
    {
        /// <summary>
        /// ID
        /// </summary>
        public Guid ID { get; set; }

        /// <summary>
        /// ??
        /// </summary>
        
        public string Name { get; set; }

        /// <summary>
        /// ??
        /// </summary>
        
        public string ImageURL { get; set; }

        /// <summary>
        /// ????
        /// </summary>
        
        public string StoreName { get; set; }

        /// <summary>
        /// ??
        /// </summary>
        
        public string YearName { get; set; }

        /// <summary>
        /// ??
        /// </summary>
        
        public string MonthName { get; set; }

        /// <summary>
        /// ????
        /// </summary>
        
        public int MonthCount { get; set; }

        /// <summary>
        /// 月份百分比
        /// </summary>
        public int MonthPercentage { get; set; }

        /// <summary>
        /// ????
        /// </summary>

        public int YearCount { get; set; }

        /// <summary>
        /// 年份百分比
        /// </summary>
        public int YearPercentage { get; set; }

        /// <summary>
        /// ??
        /// </summary>

        public string TypeName { get; set; }

        /// <summary>
        /// ??
        /// </summary>
        
        public string LableName { get; set; }

        /// <summary>
        /// ??
        /// </summary>
        
        public int CountSum { get; set; }

        /// <summary>
        /// ??
        /// </summary>
        
        public int Sort { get; set; }

        /// <summary>
        /// ????
        /// </summary>
        
        public bool IsShow { get; set; }

        /// <summary>
        /// ??
        /// </summary>
        
        public string Mark { get; set; }

        /// <summary>
        /// ???
        /// </summary>
        
        public string Creater { get; set; }

        /// <summary>
        /// ????
        /// </summary>
        
        public string CreateTime { get; set; }

        /// <summary>
        /// ???
        /// </summary>
        
        public string Modifyer { get; set; }

        /// <summary>
        /// ????
        /// </summary>
       
        public string ModifyTime { get; set; }
    }

    /// <summary>
    /// 新闻请求参数
    /// </summary>
    public class WinnerNewsRequest : BaseRequest
    {
        /// <summary>
        /// 新闻标题
        /// </summary>          
        public string Title { get; set; }

        /// <summary>
        /// 冠军名字
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 所属门店
        /// </summary>
        public string StoreName { get; set; }

        /// <summary>
        /// 新闻副标题
        /// </summary>          
        public string SubTitle { get; set; }

        /// <summary>
        /// 显示状态：0.未发布，1.已发布，2.已下架
        /// </summary>          
        public int States { get; set; }

        /// <summary>
        /// 类别ID
        /// </summary>
        public Guid TypeID { get; set; }

        /// <summary>
        /// 编码：notice.系统通知，publish.公告，banner
        /// </summary>      
        public string Code { get; set; }
    }
}

