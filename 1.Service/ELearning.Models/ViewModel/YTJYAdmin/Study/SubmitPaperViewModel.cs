﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.YTJYAdmin.Study
{
    public class SubmitPaperRequest
    {
        /// <summary>
        /// 试卷id
        /// </summary>
        public Guid PaperId { set; get; }

        /// <summary>
        /// 做题历史记录id
        /// </summary>
        public Guid PaperTestHistoryId { set; get; }

        /// <summary>
        /// 本次最新做的题目id
        /// </summary>
        [Display(Name = "本次最新做的题目id")]
        public Guid LastQuestionId { get; set; }

        /// <summary>
        /// 题目列表
        /// </summary>
        public List<SubmitPaperQuestionItem> Items { get; set; } = new List<SubmitPaperQuestionItem>();
    }

    /// <summary>
    /// 题目元素
    /// </summary>
    public class SubmitPaperQuestionItem
    {
        /// <summary>
        /// 题目id
        /// </summary>
        public Guid QuestionId { get; set; }

        /// <summary>
        /// 子题目ID
        /// </summary>
        public Guid QuestionItemId { get; set; }

        /// <summary>
        /// 题目状态：0.未做，1.正确，2.错误
        /// </summary>
        [Display(Name = "题目状态：0.未做，1.正确，2.错误")]
        public int Status { get; set; }

        /// <summary>
        /// 用户输入的答案，主观题使用的字段
        /// </summary>
        public string InputAnswer { get; set; }

        /// <summary>
        /// 选中选项id列表
        /// </summary>
        public List<Guid> ChoosedIdList { get; set; } = new List<Guid>();
        
    }

    public class SubmitPaperViewModel
    {
        /// <summary>
        /// 做题历史记录id
        /// </summary>
        public Guid PaperTestHistoryId { set; get; }

        /// <summary>
        /// 成绩
        /// </summary>
        [Display(Name = "成绩")]
        public double TotalScore { get; set; }

        /// <summary>
        /// 试卷总分
        /// </summary>
        [Display(Name = "试卷总分")]
        public double PaperTotalScore { get; set; }
    }
}
