﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel
{
    public class ProjectByProvince
    {
        /// <summary>
        /// DealerCode
        /// </summary>
        public int ProvinceCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int FxType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int DealerNum { get; set; } = 0;
    }
}
