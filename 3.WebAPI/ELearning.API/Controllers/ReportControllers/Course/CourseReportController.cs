﻿using ELearning.BLL;
using ELearning.Common;
using ELearning.Common.Extensions;
using ELearning.Common.Model;
using ELearning.Models;
using ELearning.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace ELearning.API.Controllers.ReportControllers
{
    /// <summary>
    /// 课程统计相关报表
    /// </summary>
    public partial class CourseReportController : BaseApiController
    {
        ECourseSeriesService courseSeriesService => new ECourseSeriesService();
        EDealerInfoService dealerInfoService => new EDealerInfoService();
        EDictionaryService dictionaryService => new EDictionaryService();
        EProductService productService => new EProductService();
        CommonController common => new CommonController();

        /// <summary>
        /// 课程评论报表
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost, Route("api/report/commentReport")]
        public ServiceResponse<Page<CommentReportViewModel>> CommentReport(CourseReportRequest req)
        {
            var data = courseSeriesService.CommentReport(req);

            data.Items.ForEach(x => x.CommentAvg = Math.Round(x.CommentAvg, 1));

            return ServiceResponse<Page<CommentReportViewModel>>.SuccessResponse("成功", data);
        }

        /// <summary>
        /// 课程报名报表
        /// </summary>
        /// <param name="courseID"></param>
        /// <returns></returns>
        [HttpGet, Route("api/report/courseApplyReport")]
        public ServiceResponse<CourseApply> CourseApplyReport(Guid courseID)
        {
            var courseApply = courseSeriesService.CourseApplyReport(courseID);
            var areaPeoples = dealerInfoService.AreaPeples();

            CourseApply view = new CourseApply();

            Series series1 = new Series();
            series1.name = "已报名";
            Series series2 = new Series();
            series2.name = "未报名";

            foreach (var a in areaPeoples)
            {
                view.legend.Add(a.Key);
                var Applyd = courseApply.Find(x => x.Key == a.Key)?.Values;

                series1.data.Add(Applyd ?? 0);
                series2.data.Add(a.Values - (Applyd ?? 0));
            }
            view.series.Add(series1);
            view.series.Add(series2);

            return ServiceResponse<CourseApply>.SuccessResponse("成功", view);
        }

        /// <summary>
        /// 课程报名报表-详情
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost, Route("api/report/courseApplyDetailReport")]
        public ServiceResponse<Page<CourseApplyDetail>> CourseApplyDetailReport(CourseReportRequest req)
        {
            var data = courseSeriesService.CourseApplyDetailReport(req);

            var courseTypes = dictionaryService.GetDicNoParent("Category");
            data.Items.ForEach(x => x.CourseTypeCode = courseTypes.Find(m => m.TypeCode == x.CourseTypeCode)?.TypeName);

            return ServiceResponse<Page<CourseApplyDetail>>.SuccessResponse("成功", data);
        }

        /// <summary>
        /// 获取已发布的所有课程
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/report/getCourses")]
        public ServiceResponse<List<EProduct>> GetCourses()
        {
            var data = productService.FindList(x => !x.IsDelete && x.PutawayStatus == 1).OrderByDescending(m => m.ModifyTime).ToList();

            return ServiceResponse<List<EProduct>>.SuccessResponse("成功", data);
        }

        /// <summary>
        /// 导出课程报名数据
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost, Route("api/report/exportCourseApplyReport")]
        public ServiceResponse<string> ExportCourseApplyReport(CourseReportRequest req)
        {
            req.PageSize = 10000000;
            var data = courseSeriesService.CourseApplyDetailReport(req);

            var courseTypes = dictionaryService.GetDicNoParent("Category");
            data.Items.ForEach(x => x.CourseTypeCode = courseTypes.Find(m => m.TypeCode == x.CourseTypeCode)?.TypeName);

            var views = data.Items;

            string fileName = "课程报名";
            var folder = common.GetFolder(fileName);

            var batchN = Math.Ceiling((float)views.Count / 50000);
            for (var i = 0; i < batchN; i++)
            {
                var data1 = new List<CourseApplyDetail>();
                if (views.Count <= 50000)
                {
                    data1 = views.Take(views.Count).ToList();
                }
                else
                {
                    data1 = views.Take(50000).ToList();
                    views.RemoveRange(0, 50000);
                }
                var dt = ConvertHelper.ToDataTable(data1);
                var fileFullPath = common.GetPath(folder);
                ExcelHelp.TableToExcelForXLSX(dt, fileFullPath);
            }

            if (!Directory.Exists(folder))
            {
                return ServiceResponse<string>.ErrorResponse("操作失败");
            }
            folder = folder.Substring(0, folder.Length - 1);
            var zipFolder = folder + ".zip";
            ZipHelper.CreateZip(folder, zipFolder);

            Directory.Delete(folder, true);

            var url = common.SaveFile(fileName + "_明细数据_.zip", zipFolder);

            return ServiceResponse<string>.SuccessResponse("操作成功", url);
        }


    }
}