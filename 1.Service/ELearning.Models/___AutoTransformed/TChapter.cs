//-------------------------------------------------------------------
//文件名称：TChapter.cs
//模块名称：TChapterModel层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ELearning.Models
{	
	 /// <summary>
    /// TChapter表实体类
    /// </summary>
	[Table("TChapter")]
	public partial class TChapter
    {
		
		/// <summary>
		/// Id
		/// </summary>
				[Key]
	[Display(Name = "Id")]
		public Guid Id { get; set; }
		
		/// <summary>
		/// 课程Id
		/// </summary>
			[Display(Name = "课程Id")]
		public Guid CourseId { get; set; }
		
		/// <summary>
		/// 名称
		/// </summary>
			[Display(Name = "名称")]
		public string Name { get; set; }
		
		/// <summary>
		/// 描述
		/// </summary>
			[Display(Name = "描述")]
		public string Description { get; set; }
		
		/// <summary>
		/// 看完该视频必须做完一套题目达到这个分数才能看下一章
		/// </summary>
			[Display(Name = "看完该视频必须做完一套题目达到这个分数才能看下一章")]
		public double PassGrade { get; set; }
		
		/// <summary>
		/// 组织编码
		/// </summary>
			[Display(Name = "组织编码")]
		public string OrgCode { get; set; }
		
		/// <summary>
		/// 序号
		/// </summary>
			[Display(Name = "序号")]
		public double Sort { get; set; }
		
		/// <summary>
		/// 创建时间
		/// </summary>
			[Display(Name = "创建时间")]
		public DateTime CreateTime { get; set; }
		
		/// <summary>
		/// 更新时间
		/// </summary>
			[Display(Name = "更新时间")]
		public DateTime UpdateTime { get; set; }
		
		/// <summary>
		/// 创建人Id
		/// </summary>
			[Display(Name = "创建人Id")]
		public Guid CreateUserId { get; set; }
		
		/// <summary>
		/// 编辑人id
		/// </summary>
			[Display(Name = "编辑人id")]
		public Guid UpdateUserId { get; set; }
		
		/// <summary>
		/// 是否删除
		/// </summary>
			[Display(Name = "是否删除")]
		public bool IsDelete { get; set; }
		   
    }
}

