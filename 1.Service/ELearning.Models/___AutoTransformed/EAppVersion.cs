//-------------------------------------------------------------------
//文件名称：EAppVersion.cs
//模块名称：EAppVersionModel层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ELearning.Models
{	
	 /// <summary>
    /// EAppVersion表实体类
    /// </summary>
	[Table("EAppVersion")]
	public partial class EAppVersion
    {
		
		/// <summary>
		/// Id
		/// </summary>
				[Key]
	[Display(Name = "Id")]
		public Guid Id { get; set; }
		
		/// <summary>
		/// 版本类型：0.安卓，1.IOS
		/// </summary>
			[Display(Name = "版本类型：0.安卓，1.IOS")]
		public int Type { get; set; }
		
		/// <summary>
		/// 版本号
		/// </summary>
			[Display(Name = "版本号")]
		public int VersionCode { get; set; }
		
		/// <summary>
		/// 版本名
		/// </summary>
			[Display(Name = "版本名")]
		public string VersionName { get; set; }
		
		/// <summary>
		/// 更新内容
		/// </summary>
			[Display(Name = "更新内容")]
		public string Note { get; set; }
		
		/// <summary>
		/// 安装包下载地址
		/// </summary>
			[Display(Name = "安装包下载地址")]
		public string DownloadUrl { get; set; }
		
		/// <summary>
		/// 是否需强制更新
		/// </summary>
			[Display(Name = "是否需强制更新")]
		public bool NeedUpdate { get; set; }
		
		/// <summary>
		/// 创建时间
		/// </summary>
			[Display(Name = "创建时间")]
		public DateTime CreateTime { get; set; }
		
		/// <summary>
		/// 更新时间
		/// </summary>
			[Display(Name = "更新时间")]
		public DateTime UpdateTime { get; set; }
		
		/// <summary>
		/// 创建人Id
		/// </summary>
			[Display(Name = "创建人Id")]
		public Guid CreateUserId { get; set; }
		
		/// <summary>
		/// 编辑人id
		/// </summary>
			[Display(Name = "编辑人id")]
		public Guid UpdateUserId { get; set; }
		
		/// <summary>
		/// 是否删除
		/// </summary>
			[Display(Name = "是否删除")]
		public bool IsDelete { get; set; }
		   
    }
}

