﻿using ELearning.BLL;
using ELearning.Common.Extensions;
using ELearning.Common.Model;
using ELearning.Models;
using ELearning.Models.ViewModel;
using ELearning.Models.ViewModel.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Http;

namespace ELearning.API.Controllers.ElearningController
{
    [AllowAnonymous]
    public class EOrderController: BaseApiController
    {
        private EOrderService orderService = new EOrderService();
        private EOrderProductDetailService productService = new EOrderProductDetailService();
        [HttpPost, Route("api/EOrder/GetOrderListByUser")]
        public ServiceResponse<OrderModel> GetOrderListByUser(GetOrderRequest request)
        {
            Expression<Func<EOrder, bool>> explist = null;
            var userid = CurrentUserModel.UserId;
            //explist = u => u.IsDelete == false && u.BuyUserID== userid;
            explist = u => u.IsDelete == false;
            var modelList = orderService.FindPageList(request.Page, request.PageSize, out int total, request.OrderBy, request.SortDir, explist).ToList();
            var viewModel = new OrderModel { TotalCount = total };
            foreach (var product in modelList)
            {
                var v = product.MapTo<OrderItem, EOrder>();
                v.ProductInfo = productService.Find(w=>w.OrderNo==product.OrderNo);
                viewModel.OrderItems.Add(v);
            }
            return ServiceResponse<OrderModel>.SuccessResponse(viewModel);
        }
    }
}