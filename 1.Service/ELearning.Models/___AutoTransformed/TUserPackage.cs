//-------------------------------------------------------------------
//文件名称：TUserPackage.cs
//模块名称：TUserPackageModel层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ELearning.Models
{	
	 /// <summary>
    /// TUserPackage表实体类
    /// </summary>
	[Table("TUserPackage")]
	public partial class TUserPackage
    {
		
		/// <summary>
		/// Id
		/// </summary>
				[Key]
	[Display(Name = "Id")]
		public Guid Id { get; set; }
		
		/// <summary>
		/// 用户id
		/// </summary>
			[Display(Name = "用户id")]
		public Guid UserId { get; set; }
		
		/// <summary>
		/// 套餐Id
		/// </summary>
			[Display(Name = "套餐Id")]
		public Guid PackageId { get; set; }
		
		/// <summary>
		/// 关联订单id，可空，可以由后台直接添加
		/// </summary>
			[Display(Name = "关联订单id，可空，可以由后台直接添加")]
		public Guid OrderId { get; set; }
		
		/// <summary>
		/// 购买来源：1.后台直接添加，2.网页，3.安卓，4.IOS
		/// </summary>
			[Display(Name = "购买来源：1.后台直接添加，2.网页，3.安卓，4.IOS")]
		public int SourceType { get; set; }
		
		/// <summary>
		/// 创建时间
		/// </summary>
			[Display(Name = "创建时间")]
		public DateTime CreateTime { get; set; }
		
		/// <summary>
		/// 更新时间
		/// </summary>
			[Display(Name = "更新时间")]
		public DateTime UpdateTime { get; set; }
		
		/// <summary>
		/// 创建人Id
		/// </summary>
			[Display(Name = "创建人Id")]
		public Guid CreateUserId { get; set; }
		
		/// <summary>
		/// 编辑人id
		/// </summary>
			[Display(Name = "编辑人id")]
		public Guid UpdateUserId { get; set; }
		
		/// <summary>
		/// 是否删除
		/// </summary>
			[Display(Name = "是否删除")]
		public bool IsDelete { get; set; }
		   
    }
}

