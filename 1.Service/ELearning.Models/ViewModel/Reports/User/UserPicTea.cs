﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel
{
    public class UserPicTea
    {
        /// <summary>
        /// 用户姓名
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 公司
        /// </summary>
        public string Company { get; set; }
        /// <summary>
        /// 积分
        /// </summary>
        public decimal Credit { get; set; }
        /// <summary>
        /// 头衔
        /// </summary>
        public string ForumTitle { get; set; }
        /// <summary>
        /// 发布的课程数量
        /// </summary>
        public int CourseOwnerNum { get; set; }
        /// <summary>
        /// 教过的课程数量
        /// </summary>
        public int CourseTeachedNum { get; set; }
        /// <summary>
        /// 教过的课程评价
        /// </summary>
        public decimal CourseScoreAvg { get; set; }
        /// <summary>
        /// 个人评价
        /// </summary>
        public double PeopleScoreAvg { get; set; }

    }
}
