﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.YTJYAdmin.Course
{
    public class SiteCourseRequeset
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { set; get; }

        /// <summary>
        /// 大类分类编码
        /// </summary>
        public string CategoryId { set; get; }

        /// <summary>
        /// 基础分类编码
        /// </summary>
        public string TypeCode { set; get; }

        /// <summary>
        /// 套餐id
        /// </summary>
        public Guid PackageId { set; get; }
    }

    public class SiteCourseViewModel
    {
        /// <summary>
        /// 课程列表
        /// </summary>
        public List<SiteCourseItem> Items { set; get; } = new List<SiteCourseItem>();

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { set; get; }

        /// <summary>
        /// 套餐描述
        /// </summary>
        public string Description { get; set; }
    }

    public class SiteCourseItem
    {
        public Guid Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 封面图片地址
        /// </summary>
        [Display(Name = "封面图片地址")]
        public string CoverUrl { get; set; }

        /// <summary>
        /// 展示所用
        /// </summary>
        public string ShowCoverUrl { get; set; }

        /// <summary>
        /// 大类分类Code
        /// </summary>
        [Display(Name = "大类分类Code")]
        public string CategoryId { get; set; }

        /// <summary>
        /// 大类分类名称
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// 班级基础分类code,基础班，高级版
        /// </summary>
        [Display(Name = "班级基础分类code,基础班，高级版")]
        public string TypeCode { get; set; }

        /// <summary>
        /// 班级基础分类名称
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [Display(Name = "序号")]
        public double Sort { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        [Display(Name = "更新时间")]
        public DateTime UpdateTime { get; set; }

        /// <summary>
        /// 创建人Id
        /// </summary>
        [Display(Name = "创建人Id")]
        public Guid CreateUserId { get; set; }

        /// <summary>
        /// 编辑人id
        /// </summary>
        [Display(Name = "编辑人id")]
        public Guid UpdateUserId { get; set; }
        
        /// <summary>
        /// 上次看的章节id
        /// </summary>
        public Guid? LastStudyChapterId { get; set; }

        /// <summary>
        /// 章节列表
        /// </summary>
        public List<SiteChapterItem> Items { get; set; } = new List<SiteChapterItem>();
    }

    /// <summary>
    /// 视频元素实体
    /// </summary>
    public class SiteChapterItem
    {
        /// <summary>
        /// 视频id
        /// </summary>
        public Guid VideoLibId { get; set; }

        /// <summary>
        /// 章节id
        /// </summary>
        public Guid ChapterId { get; set; }

        /// <summary>
        /// 章节名称
        /// </summary>
        public string ChapterName { get; set; }
        
        /// <summary>
        /// 章节序号
        /// </summary>
        public double ChapterSort { get; set; }

        /// <summary>
        /// 及格分数
        /// </summary>
        public double PassGrade { get; set; }

        /// <summary>
        /// 是否可以看下一个视频
        /// </summary>
        public bool IsCanNext { get; set; }

        /// <summary>
        /// 试卷id
        /// </summary>
        public Guid? PaperId { get; set; }

        /// <summary>
        /// 是否已经学习过该视频
        /// </summary>
        public bool IsStudy { get; set; }

        /// <summary>
        /// 转码状态：0.不需要转码，1.待转码，2.转码中，3.转码成功，4.转码失败，5.部分转码成功
        /// </summary>
        [Display(Name = "转码状态：0.不需要转码，1.待转码，2.转码中，3.转码成功，4.转码失败，5.部分转码成功")]
        public int? VideoLibStatus { get; set; }

        /// <summary>
        /// 上次学习到的位置，上次学习到第几秒
        /// </summary>
        public int LastStudyTime { get; set; }

        /// <summary>
        /// 转码状态字符串
        /// </summary>
        public string StatusStr
        {
            get
            {
                var str = string.Empty;
                if (VideoLibStatus != 0 && VideoLibStatus != 3)
                    str = "维护中";
                return str;
            }
        }

        /// <summary>
        /// 视频名称
        /// </summary>
        public string VideoLibName { get; set; }

        /// <summary>
        /// 播放地址
        /// </summary>
        public string PlayUrl { get; set; }

        /// <summary>
        /// 下载地址
        /// </summary>
        public string DownloadUrl { get; set; }

        /// <summary>
        /// 课件类型：0.视频，1.pdf，2.office文件，3.直播
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 是否学习完成该章节
        /// </summary>
        public bool IsFinish { get; set; }
    }

    public class SiteCourseModel
    {
        public Guid Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 封面图片地址
        /// </summary>
        [Display(Name = "封面图片地址")]
        public string CoverUrl { get; set; }

        /// <summary>
        /// 大类分类Code
        /// </summary>
        [Display(Name = "大类分类Code")]
        public string CategoryId { get; set; }

        /// <summary>
        /// 大类分类名称
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// 班级基础分类code,基础班，高级版
        /// </summary>
        [Display(Name = "班级基础分类code,基础班，高级版")]
        public string TypeCode { get; set; }

        /// <summary>
        /// 班级基础分类名称
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [Display(Name = "序号")]
        public double Sort { get; set; }

        /// <summary>
        /// 章节序号
        /// </summary>
        public double ChapterSort { get; set; }

        /// <summary>
        /// 及格分数
        /// </summary>
        public double PassGrade { get; set; }

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { set; get; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        [Display(Name = "更新时间")]
        public DateTime UpdateTime { get; set; }

        /// <summary>
        /// 创建人Id
        /// </summary>
        [Display(Name = "创建人Id")]
        public Guid CreateUserId { get; set; }

        /// <summary>
        /// 编辑人id
        /// </summary>
        [Display(Name = "编辑人id")]
        public Guid UpdateUserId { get; set; }

        /// <summary>
        /// 视频id
        /// </summary>
        public Guid? VideoLibId { get; set; }

        /// <summary>
        /// 章节id
        /// </summary>
        public Guid? ChapterId { get; set; }

        /// <summary>
        /// 试卷id
        /// </summary>
        public Guid? PaperId { get; set; }

        /// <summary>
        /// 章节名称
        /// </summary>
        public string ChapterName { get; set; }

        /// <summary>
        /// 学习该视频的次数
        /// </summary>
        public int StudyCount { get; set; }

        /// <summary>
        /// 上次看的章节id
        /// </summary>
        public Guid? LastStudyChapterId { get; set; }

        /// <summary>
        /// 是否学习完成该章节
        /// </summary>
        public bool? IsFinish { get; set; }

        /// <summary>
        /// 转码状态：0.不需要转码，1.待转码，2.转码中，3.转码成功，4.转码失败，5.部分转码成功
        /// </summary>
        [Display(Name = "转码状态：0.不需要转码，1.待转码，2.转码中，3.转码成功，4.转码失败，5.部分转码成功")]
        public int? VideoLibStatus { get; set; }

        /// <summary>
        /// 上次学习到的位置，上次学习到第几秒
        /// </summary>
        public int? LastStudyTime { get; set; }

        /// <summary>
        /// 试卷得分，最高一次得分
        /// </summary>
        public double? Score { get; set; }

        /// <summary>
        /// 视频名称
        /// </summary>
        public string VideoLibName { get; set; }

        /// <summary>
        /// 播放地址
        /// </summary>
        public string PlayUrl { get; set; }

        /// <summary>
        /// 下载地址
        /// </summary>
        public string DownloadUrl { get; set; }
    }
}
