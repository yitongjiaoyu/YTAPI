﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Common
{
    /// <summary>
    /// 区域信息等级
    /// </summary>
    public enum AreaLevelEnum
    {
        /// <summary>
        /// 区域
        /// </summary>
        [Description("区域")]
        Level1 = 1,
        /// <summary>
        /// 省份
        /// </summary>
        [Description("省份")]
        Level2 = 2,
        /// <summary>
        /// 城市
        /// </summary>
        [Description("城市")]
        Level3 = 3,
        /// <summary>
        /// 其他
        /// </summary>
        [Description("其他")]
        Level4 = 4,
    }
}
