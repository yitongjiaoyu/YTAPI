﻿using ELearning.Common.Extensions;
using ELearning.Common.Model;
using ELearning.Models;
using ELearning.Models.ViewModel.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.BLL
{
    public partial class EApplicationLogService : BaseService<EApplicationLog>
    {
        public Page<EApplicationLog> GetLogsList(GetLogsRequest request)
        {
            Expression<Func<EApplicationLog, bool>> whereLamdba = (u) => u.LogPriority >= 0;

            if (!string.IsNullOrEmpty(request.LoginName))
                whereLamdba = whereLamdba.And(u => u.AspnetUserIdentity.Contains(request.LoginName));
            if (!string.IsNullOrEmpty(request.LogLevel) && request.LogLevel != "全部")
                whereLamdba = whereLamdba.And(u => u.LogLevel.Contains(request.LogLevel));
            if (!string.IsNullOrEmpty(request.LogMessage))
                whereLamdba = whereLamdba.And(u => u.Message.Contains(request.LogMessage));
            if (request.StartTime > DateTime.MinValue)
                whereLamdba = whereLamdba.And(u => u.LogDate > request.StartTime);
            if (request.EndTime > DateTime.MinValue)
                whereLamdba = whereLamdba.And(u => u.LogDate < request.EndTime);
            var items = CurrentRepository.FindPageList(request.Page, request.PageSize, out int total, whereLamdba, request.OrderBy, request.IsAsc).ToList();
            Page<EApplicationLog> page = new Page<EApplicationLog>
            {
                TotalItems = total,
                CurrentPage = request.Page,
                Items = items,
                ItemsPerPage = request.PageSize
            };
            return page;
        }
    }
}
