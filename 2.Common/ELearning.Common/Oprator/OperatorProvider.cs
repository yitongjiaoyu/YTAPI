﻿using System;
using System.Web;
using ELearning.Common.Buffer;
using ELearning.Common.Extensions;
using ELearning.Common.Helpers;

namespace ELearning.Common.Oprator
{
    /// <summary>
    /// 用户信息缓存操作类
    /// </summary>
    public class OperatorProvider
    {
        /// <summary>
        /// 当前类实例
        /// </summary>
        public static OperatorProvider Provider => new OperatorProvider();

        /// <summary>
        /// 缓存key
        /// </summary>
        private string LoginUserKey => $"{KeyPrefix}loginuser";

        /// <summary>
        /// 缓存key前缀
        /// </summary>
        private string KeyPrefix
        {
            get
            {
                var urlHost = HttpContext.Current.Request.Url.Host.ToLower();
                var urlHostArray = urlHost.Split('.');
                if (urlHostArray.Length > 1)
                {
                    urlHost = urlHost.Remove(urlHost.LastIndexOf(".", StringComparison.Ordinal));
                }
                var keyPrefixPre = urlHost.Replace('.', '_');
                return $"{keyPrefixPre}_".Trim();
            }
        }

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <returns>用户信息实体</returns>
        public OperatorModel GetCurrent()
        {
            OperatorModel operatorModel;
            var v = CookieHelper.GetcookieValue(LoginUserKey);
            if (string.IsNullOrEmpty(v))
            {
                operatorModel = null;
            }
            else
            {
                operatorModel = DESProvider.DecryptString(CookieHelper.GetcookieValue(LoginUserKey)).ToObject<OperatorModel>();
            }
            return operatorModel;
        }

        /// <summary>
        /// 添加cookie到本地
        /// </summary>
        /// <param name="operatorModel">用户信息</param>
        public void AddCurrent(OperatorModel operatorModel)
        {
            CookieHelper.SetCookie(LoginUserKey, DESProvider.EncryptString(operatorModel.ToJson()), 3600);
        }

        /// <summary>
        /// 移除cookie
        /// </summary>
        public void RemoveCurrent()
        {
            CookieHelper.RemoveCookie(LoginUserKey);
        }

        #region 根据key获取用户信息

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <returns>用户信息实体</returns>
        public OperatorModel GetCurrent(string key)
        {
            OperatorModel operatorModel;
            //var v = CookieHelper.GetcookieValue(key);

            //WriteHelper.WriteFile($"GetCurrent -- key----{key}");
            //WriteHelper.WriteFile($"GetCurrent -- v----{v}");

            var v = CacheHelper.GetCache(key);
            //if (string.IsNullOrEmpty(v))
            if (v.IsNull())
            {
                operatorModel = null;
            }
            else
            {
                operatorModel = (OperatorModel)CacheHelper.GetCache(key); //DESProvider.DecryptString(CookieHelper.GetcookieValue(key)).ToObject<OperatorModel>();
                //operatorModel = DESProvider.DecryptString(v).ToObject<OperatorModel>();
            }
            return operatorModel;
        }

        /// <summary>
        /// 添加cookie到本地
        /// </summary>
        /// <param name="key">key值</param>
        /// <param name="operatorModel">用户信息</param>
        public void AddCurrent(string key, OperatorModel operatorModel, DateTime? expiresAt = null)
        {
            //WriteHelper.WriteFile($"key----{key}");
            //CookieHelper.SetCookie(key, DESProvider.EncryptString(operatorModel.ToJson()), 3600);

            //WriteHelper.WriteFile($"GetCurrent----{GetCurrent(key).ToJson()}");
            if (!expiresAt.HasValue)
                expiresAt = DateTime.Now.AddHours(8);
            CacheHelper.SetCache(key, operatorModel, expiresAt.Value);
        }

        /// <summary>
        /// 移除cookie
        /// </summary>
        public void RemoveCurrent(string key)
        {
            //CookieHelper.RemoveCookie(key);
            CacheHelper.RemoveAllCache(key);
        }

        #endregion
    }
}
