//-------------------------------------------------------------------
//文件名称：ECourse.cs
//模块名称：ECourse业务逻辑接口层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearning.Common.Extensions;
using ELearning.Common.Helpers;
using ELearning.DAL;
using ELearning.Models;
using ELearning.Models.ViewModel.YTJYAdmin.Course;

namespace ELearning.BLL
{
    /// <summary>
    /// ECourse业务逻辑接口层
    /// </summary>
	public partial class ECourseService : BaseService<ECourse>
    {
        /// <summary>
        /// 获取课程列表
        /// </summary>
        public CourseViewModel GetModelList(CourseRequest request)
        {
            var models = RepositoryFactory.ECourseRepository.GetModelList(request);
            var viewModel = new CourseViewModel { TotalCount = models.FirstOrDefault()?.TotalCount ?? 0 };
            foreach (var m in models)
            {
                var v = viewModel.Items.FirstOrDefault(t => t.Id == m.Id);
                if (v.IsNull())
                {
                    v = m.MapTo<CourseItem, CourseModel>();
                    v.ShowCoverUrl = PictureHelper.GetFileFullPath(v.CoverUrl);
                    viewModel.Items.Add(v);
                }
                if (m.PaperId.HasValue)
                    v.PaperIds.Add(m.PaperId.Value);

            }
            return viewModel;
        }

        /// <summary>
        /// 获取课程视频关联或者不关联列表
        /// </summary>
        public CourseVideoViewModel GetCourseVideoList(CourseVideoRequest request)
        {
            var models = RepositoryFactory.ECourseRepository.GetCourseVideoList(request);
            var viewModel = new CourseVideoViewModel { TotalCount = models.FirstOrDefault()?.TotalCount ?? 0 };
            foreach (var m in models)
            {
                var v = m.MapTo<CourseVideoItem, CourseVideoModel>();
                v.StatusStr = IntToStringHelper.GetConvertStatsString(v.Status);
                viewModel.Items.Add(v);
            }
            return viewModel;
        }

        /// <summary>
        /// 获取课程列表
        /// </summary>
        public SiteCourseViewModel GetSiteModelList(SiteCourseRequeset request, Guid userId)
        {
            var models = RepositoryFactory.ECourseRepository.GetSiteModelList(request, userId);
            var viewModel = new SiteCourseViewModel { TotalCount = models.FirstOrDefault()?.TotalCount ?? 0 };
            foreach (var m in models)
            {
                var v = viewModel.Items.FirstOrDefault(t => t.Id == m.Id);
                if (v.IsNull())
                {
                    v = m.MapTo<SiteCourseItem, SiteCourseModel>();
                    v.LastStudyChapterId = m.LastStudyChapterId ?? Guid.Empty;
                    v.ShowCoverUrl = PictureHelper.GetFileFullPath(v.CoverUrl);
                    viewModel.Items.Add(v);
                }

                if (m.ChapterId.HasValue)
                {
                    var chapter = new SiteChapterItem
                    {
                        VideoLibId = m.VideoLibId ?? Guid.Empty,
                        ChapterId = m.ChapterId.Value,
                        ChapterName = m.ChapterName,
                        ChapterSort = m.ChapterSort,
                        IsCanNext = m.Score >= m.PassGrade,
                        PassGrade = m.PassGrade,
                        PaperId = m.PaperId,
                        IsFinish = m.IsFinish ?? false,
                        VideoLibName = m.VideoLibName,
                        DownloadUrl = m.DownloadUrl,
                        PlayUrl = m.PlayUrl,
                        VideoLibStatus = m.VideoLibStatus ?? 0,
                        IsStudy = m.StudyCount > 0,
                        LastStudyTime = m.LastStudyTime ?? 0
                    };

                    if (v.LastStudyChapterId == Guid.Empty)//如果第一次看该课程视频，那么上次学习视频id为当前课程第一个视频id
                        v.LastStudyChapterId = chapter.ChapterId;

                    v.Items.Add(chapter);
                }
            }
            return viewModel;
        }

        /// <summary>
        /// 获取课程列表
        /// </summary>
        public FreeCourseViewModel GetFreeModelList(FreeCourseRequest request)
        {
            var models = RepositoryFactory.ECourseRepository.GetFreeModelList(request);
            var viewModel = new FreeCourseViewModel { TotalCount = models.FirstOrDefault()?.TotalCount ?? 0 };
            foreach (var m in models)
            {
                var v = viewModel.Items.FirstOrDefault(t => t.Id == m.Id);
                if (v.IsNull())
                {
                    v = m.MapTo<FreeCourseItem, FreeCourseModel>();
                    v.ShowCoverUrl = PictureHelper.GetFileFullPath(v.CoverUrl);
                    viewModel.Items.Add(v);
                }
            }
            return viewModel;
        }

        /// <summary>
        /// 获取课程列表
        /// </summary>
        public CourseDetailViewModel GetModelDetailList(CourseDetailRequest request, Guid userId)
        {
            var models = RepositoryFactory.ECourseRepository.GetModelDetailList(request, userId);
            var viewModel = new CourseDetailViewModel { TotalCount = models.Count };
            foreach (var m in models)
            {
                var v = viewModel.Items.FirstOrDefault(t => t.ChapterId == m.ChapterId);
                if (v.IsNull())
                {
                    v = m.MapTo<CourseDetailItem, CourseDetailModel>();
                    v.IsFinish = m.IsFinish ?? false;
                    v.IsStudy = m.StudyCount > 0;
                    v.IsCanNext = m.Score > m.PassGrade;
                    v.StatusStr = IntToStringHelper.GetConvertStatsString(v.Status);

                    if (viewModel.LastStudyChapterId == Guid.Empty)//如果第一次看该课程视频，那么上次学习视频id为当前课程第一个视频id
                        viewModel.LastStudyChapterId = v.ChapterId;

                    viewModel.Items.Add(v);
                }
            }
            return viewModel;
        }
    }
}


