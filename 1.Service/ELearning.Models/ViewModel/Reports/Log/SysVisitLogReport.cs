﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel
{
    public class SysVisitLogReport
    {
        /// <summary>
        /// 日期
        /// </summary>
        public DateTime StoreDate { get; set; }
        /// <summary>
        /// 日期
        /// </summary>
        public long LongDate { get; set; }
        /// <summary>
        /// 访问数量
        /// </summary>
        public int VisitNum { get; set; } = 0;
    }
}
