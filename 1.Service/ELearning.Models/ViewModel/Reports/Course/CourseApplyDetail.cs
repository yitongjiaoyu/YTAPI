﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel
{
    public class CourseApplyDetail : CommentReportViewModel
    {
        /// <summary>
        /// 课程分类
        /// </summary>
        public string CourseCategory { get; set; }
        /// <summary>
        /// 课程类型
        /// </summary>
        public string CourseTypeCode { get; set; }
        /// <summary>
        /// 已报名学员姓名
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 已报名学员操作员代码
        /// </summary>
        public string Unumber { get; set; }
        /// <summary>
        /// 已报名学员经销商代码
        /// </summary>
        public string AgentNumber { get; set; }
        /// <summary>
        /// 已报名学员经销商名称
        /// </summary>
        public string AgentName { get; set; }
        /// <summary>
        /// 已报名学员SGMW备案岗位
        /// </summary>
        public string SGMWRecordJob { get; set; }

        /// <summary>
        /// 区域
        /// </summary>
        public string AreaName { get; set; }

        /// <summary>
        /// 区域-Code
        /// </summary>
        public int AreaCode { get; set; }

        /// <summary>
        /// 省份
        /// </summary>
        public string ProvinceName { get; set; }

        /// <summary>
        /// 省份-Code
        /// </summary>
        public int ProvinceCode { get; set; }

        /// <summary>
        /// 城市
        /// </summary>
        public string CityName { get; set; }

        /// <summary>
        /// 城市-Code
        /// </summary>
        public int CityCode { get; set; }
        /// <summary>
        /// 一级代码
        /// </summary>
        public string ManagementCode { get; set; }
        /// <summary>
        /// 体系名称
        /// </summary>
        public string ManagementName { get; set; }
        /// <summary>
        /// 报名时间
        /// </summary>
        public DateTime ApplyDate { get; set; }
        /// <summary>
        /// 学习进度
        /// </summary>
        public string StudyProcess { get; set; }

    }
}
