﻿using System;
using ELearning.Common.Model;

namespace ELearning.Common.Extensions
{
    /// <summary>
    /// The <see cref="System.Exception"/> extensions.
    /// </summary>
    public static class ExceptionExtensions
    {
        /// <summary>
        /// Logs the specified exception.
        /// </summary>
        /// <param name="ex">The exception.</param>
        public static void Log(this Exception ex)
        {
            //Logger.Default.Error(ex.Message, ex);
        }
        /// <summary>
        /// Logs the specified exception and return a service error response.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ex">The exception.</param>
        /// <returns></returns>
        public static ServiceResponse<T> LogAndResponse<T>(this Exception ex)
        {
            ex.Log();
            return ServiceResponse<T>.ErrorResponse($"{ex.Message}--{ex}");
        }
    }
}
