﻿using System.Runtime.Serialization;

namespace ELearning.API.Models
{
    /// <summary>
    /// 骏客消息实体
    /// </summary>
    [DataContract]
    public class JunKeMessage
    {
        /// <summary>
        /// Gets or sets the dealer code.
        /// </summary>
        /// <value>
        /// The dealer code.
        /// </value>
        [DataMember]
        public string dealerCode { get; set; }
        /// <summary>
        /// Gets or sets the content of the MSG.
        /// </summary>
        /// <value>
        /// The content of the MSG.
        /// </value>
        [DataMember]
        public string msgContent { get; set; }
        /// <summary>
        /// Gets or sets the employee no.
        /// </summary>
        /// <value>
        /// The employee no.
        /// </value>
        [DataMember]
        public string employeeNo { get; set; }
    }

    /// <summary>
    /// 骏客返回的错误信息
    /// </summary>
    [DataContract]
    public class JunkeLoginErrorMsg
    {
        /// <summary>
        /// Gets or sets the error MSG.
        /// </summary>
        /// <value>
        /// The error MSG.
        /// </value>
        [DataMember]
        public string errorMsg { get; set; }
    }
}