﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.YTJYAdmin.Package
{
    public class UserPackageViewModel
    {
        /// <summary>
        /// 列表
        /// </summary>
        public List<UserPackageItem> Items { set; get; } = new List<UserPackageItem>();

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { set; get; }
    }

    /// <summary>
    /// 套餐视图
    /// </summary>
    public class UserPackageItem
    {
        /// <summary>
        /// 套餐id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 封面图片地址
        /// </summary>
        [Display(Name = "封面图片地址")]
        public string CoverUrl { get; set; }

        /// <summary>
        /// 展示所用
        /// </summary>
        public string ShowCoverUrl { get; set; }
    }

    /// <summary>
    /// 套餐元素
    /// </summary>
    public class UserPackageModel
    {
        /// <summary>
        /// 套餐id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 封面图片地址
        /// </summary>
        [Display(Name = "封面图片地址")]
        public string CoverUrl { get; set; }
    }
}
