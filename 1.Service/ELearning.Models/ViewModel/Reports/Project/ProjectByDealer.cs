﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel
{
    public class ProjectByDealer
    {
        /// <summary>
        /// DealerCode
        /// </summary>
        public string DealerCode { get; set; } = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        public int ConfirmStatus { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int UserNum { get; set; } = 0;
    }
}
