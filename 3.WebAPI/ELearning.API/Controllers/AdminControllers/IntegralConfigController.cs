﻿using ELearning.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Linq.Expressions;
using ELearning.Common;
using ELearning.Common.CommonStr;
using ELearning.Common.Extensions;
using ELearning.Models;
using ELearning.Common.Model;
using ELearning.Models.ViewModel.IntegralConfig;
using ELearning.Models.ViewModel.Project;

namespace ELearning.API.Controllers.AdminControllers
{
    public class IntegralConfigController : BaseApiController
    {
        /// <summary>
        /// 积分配置项操作service
        /// </summary>
        private EIntegralConfigService IntegralConfigService => new EIntegralConfigService();

        /// <summary>
        /// 获取积分配置项列表
        /// </summary>
        [HttpPost, Route("api/integralConfig/GetModelList")]
        public ServiceResponse<IntegralConfigViewModel> GetModelList(IntegralConfigRequest request)
        {
            Expression<Func<EIntegralConfig, bool>> whereLamdba = u => !u.IsDelete;
            if (!string.IsNullOrEmpty(request.Code))
                whereLamdba = whereLamdba.And(i => i.Code.Contains(request.Code));
            if (!string.IsNullOrEmpty(request.ModuleName))
                whereLamdba = whereLamdba.And(i => i.ModuleName.Contains(request.ModuleName));
            if (!string.IsNullOrEmpty(request.ModuleNumber))
                whereLamdba = whereLamdba.And(i => i.ModuleNumber.Contains(request.ModuleNumber));
            if (!string.IsNullOrEmpty(request.OprateName))
                whereLamdba = whereLamdba.And(i => i.OprateName.Contains(request.OprateName));
            if (!string.IsNullOrEmpty(request.Identification))
                whereLamdba = whereLamdba.And(i => i.Identification.Contains(request.Identification));
            if (!string.IsNullOrEmpty(request.UserType))
                whereLamdba = whereLamdba.And(i => i.UserType.Contains(request.UserType));

            var models = IntegralConfigService.FindPageList(request.Page, request.PageSize, out var total, nameof(EIntegralConfig.CreateTime), request.SortDir, whereLamdba).ToList();
            var viewModel = new IntegralConfigViewModel { TotalCount = total };
            foreach (var m in models)
            {
                var v = m.MapTo<IntegralConfigItem, EIntegralConfig>();
                v.CreateTime = m.CreateTime.ToHSString();
                v.ModifyTime = m.ModifyTime.ToHSString();
                viewModel.Items.Add(v);
            }

            return ServiceResponse<IntegralConfigViewModel>.SuccessResponse(CommonConst.OprateSuccessStr, viewModel);
        }

        /// <summary>
        /// 添加积分配置项
        /// </summary>
        [HttpPost, Route("api/integralConfig/AddModel")]
        [AllowAnonymous]
        public ServiceResponse<bool> AddModel(IntegralConfigItem viewModel)
        {
            var model = viewModel.MapTo<EIntegralConfig, IntegralConfigItem>();

            model.Id = GuidUtil.NewSequentialId();
            model.Creater = CurrentUserModel?.LoginName ?? string.Empty;
            model.CreateTime = DateTime.Now;
            model.Modifyer = CurrentUserModel?.LoginName ?? string.Empty;
            model.ModifyTime = DateTime.Now;
            model.IsDelete = false;
            IntegralConfigService.Add(model);

            return ServiceResponse<bool>.SuccessResponse(CommonConst.OprateSuccessStr, true);
        }

        /// <summary>
        /// 更新积分配置项
        /// </summary>
        [HttpPost, Route("api/integralConfig/UpdateModel")]
        public ServiceResponse<bool> UpdateModel(IntegralConfigItem viewModel)
        {
            var model = IntegralConfigService.Find(p => !p.IsDelete && p.Id == viewModel.Id);
            if (model != null)
            {
                model.ModifyTime = DateTime.Now;
                model.Modifyer = CurrentUserModel?.LoginName ?? string.Empty;

                model.Code = viewModel.Code;
                model.Credit = viewModel.Credit;
                model.Identification = viewModel.Identification;
                model.OprateName = viewModel.OprateName;
                model.Identification = viewModel.Identification;
                model.ModuleName = viewModel.ModuleName;
                model.ModuleNumber = viewModel.ModuleNumber;
                model.UserTypeName = viewModel.UserTypeName;
                model.UserType = viewModel.UserType;
                model.Note = viewModel.Note;

                IntegralConfigService.Update(model);
            }
            return ServiceResponse<bool>.SuccessResponse(CommonConst.OprateSuccessStr, true);
        }

        /// <summary>
        /// 删除积分配置项
        /// </summary>
        /// <param name="id">项目id</param>
        [HttpPost, Route("api/integralConfig/DeleteModel")]
        public ServiceResponse<bool> DeleteModel(Guid id)
        {
            var model = IntegralConfigService.Find(p => !p.IsDelete && p.Id == id);
            if (model != null)
            {
                model.IsDelete = true;
                IntegralConfigService.Update(model);
            }
            return ServiceResponse<bool>.SuccessResponse(CommonConst.OprateSuccessStr, true);
        }

    }
}