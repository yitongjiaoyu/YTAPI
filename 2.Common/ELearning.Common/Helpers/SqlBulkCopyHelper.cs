﻿
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace ELearning.Common.Helpers
{
    public class SqlBulkCopyHelper
    {
        public static void SaveTable(DataTable dtTable)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
            var sbc = new SqlBulkCopy(connectionString, SqlBulkCopyOptions.UseInternalTransaction) {BulkCopyTimeout = 5000};
            try
            {
                sbc.DestinationTableName = dtTable.TableName;
                sbc.WriteToServer(dtTable);
            }
            catch (Exception ex)
            {
                //处理异常
                throw ex;
            }
            finally
            {
                //sqlcmd.Clone();
                //srcConnection.Close();
                sbc.Close();
            }
        }
    }
}
