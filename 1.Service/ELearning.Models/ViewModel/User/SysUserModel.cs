//-------------------------------------------------------------------
//文件名称：ESysUser.cs
//模块名称：ESysUserModel层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//修改时间：2019年8月22日
//-----------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ELearning.Models
{	
	 /// <summary>
    /// ESysUser表实体类
    /// </summary>
	public class SysUserModel
    {
		
		/// <summary>
		/// 用户标识
		/// </summary>
		public Guid UserID { get; set; }
		
		/// <summary>
		/// 登录名
		/// </summary>
			[Display(Name = "登录名")]
		public string LoginName { get; set; }
		
		/// <summary>
		/// 姓名
		/// </summary>
			[Display(Name = "姓名")]
		public string UserName { get; set; }
		
		/// <summary>
		/// 用户类别
		/// </summary>
			[Display(Name = "用户类别")]
		public string UserType { get; set; }
		
		/// <summary>
		/// 所属机构
		/// </summary>
			[Display(Name = "所属机构")]
		public string OrgCode { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Unumber { get; set; }
		
		/// <summary>
		/// 登录密码
		/// </summary>
			[Display(Name = "登录密码")]
		public string Password { get; set; }
		
		/// <summary>
		/// 性别
		/// </summary>
			[Display(Name = "性别")]
		public int Sex { get; set; }
		
		/// <summary>
		/// 出生日期
		/// </summary>
			[Display(Name = "出生日期")]
		public DateTime Birthday { get; set; }
		
		/// <summary>
		/// 头像
		/// </summary>
			[Display(Name = "头像")]
		public string HeadPhoto { get; set; }
		
		/// <summary>
		/// 公司名称
		/// </summary>
			[Display(Name = "公司名称")]
		public string Company { get; set; }
		
		/// <summary>
		/// 部门信息
		/// </summary>
			[Display(Name = "部门信息")]
		public string Department { get; set; }
		
		/// <summary>
		/// 职务
		/// </summary>
			[Display(Name = "职务")]
		public string Position { get; set; }
		
		/// <summary>
		/// 办公电话
		/// </summary>
			[Display(Name = "办公电话")]
		public string OfficeTel { get; set; }
		
		/// <summary>
		/// 移动电话
		/// </summary>
			[Display(Name = "移动电话")]
		public string MobilePhone { get; set; }
		
		/// <summary>
		/// 电子邮件
		/// </summary>
			[Display(Name = "电子邮件")]
		public string Email { get; set; }
		
		/// <summary>
		/// 个人简介
		/// </summary>
			[Display(Name = "个人简介")]
		public string Remark { get; set; }
		
		/// <summary>
		/// 用户状态
		/// </summary>
			[Display(Name = "用户状态")]
		public int AccountStatus { get; set; }
		
		/// <summary>
		/// 注册来源
		/// </summary>
			[Display(Name = "注册来源")]
		public string RegisterDevice { get; set; }
		
		/// <summary>
		/// 创建人
		/// </summary>
			[Display(Name = "创建人")]
		public string Creater { get; set; }
		
		/// <summary>
		/// 创建时间
		/// </summary>
			[Display(Name = "创建时间")]
		public DateTime CreateTime { get; set; }
		
		/// <summary>
		/// 修改人
		/// </summary>
			[Display(Name = "修改人")]
		public string Modifyer { get; set; }
		
		/// <summary>
		/// 修改时间
		/// </summary>
			[Display(Name = "修改时间")]
		public DateTime ModifyTime { get; set; }
		
		/// <summary>
		/// 是否逻辑删除
		/// </summary>
			[Display(Name = "是否逻辑删除")]
		public bool IsDelete { get; set; }
		
		/// <summary>
		/// 经销商号码
		/// </summary>
			[Display(Name = "经销商号码")]
		public string AgentNumber { get; set; }
		
		/// <summary>
		/// SGMW备案岗位
		/// </summary>
			[Display(Name = "SGMW备案岗位")]
		public string SGMWRecordJob { get; set; }
		
		/// <summary>
		/// 状态:0.离职，1.在职
		/// </summary>
			[Display(Name = "状态:0.离职，1.在职")]
		public int JobStatus { get; set; }
		
		/// <summary>
		/// 汽车行业销售管理工作年限
		/// </summary>
			[Display(Name = "汽车行业销售管理工作年限")]
		public int CarSaleManageYear { get; set; }
		
		/// <summary>
		/// 汽车行业市场运作管理工作年限
		/// </summary>
			[Display(Name = "汽车行业市场运作管理工作年限")]
		public int CarMarketManageYear { get; set; }
		
		/// <summary>
		/// 其他行业销售管理工作年限
		/// </summary>
			[Display(Name = "其他行业销售管理工作年限")]
		public int OtherSaleManageYear { get; set; }
		
		/// <summary>
		/// 其他行业市场运作管理工作年限
		/// </summary>
			[Display(Name = "其他行业市场运作管理工作年限")]
		public int OtherMarketManageYear { get; set; }
		
		/// <summary>
		/// 汽车售后服务管理工作年限
		/// </summary>
			[Display(Name = "汽车售后服务管理工作年限")]
		public int CarServiceManageYear { get; set; }
		
		/// <summary>
		/// 汽车维修技术管理工作年限
		/// </summary>
			[Display(Name = "汽车维修技术管理工作年限")]
		public int CarRepairManageYear { get; set; }
		
		/// <summary>
		/// 其他行业维修工作年限
		/// </summary>
			[Display(Name = "其他行业维修工作年限")]
		public int OtherRepairManageYear { get; set; }
		
		/// <summary>
		/// 经销商名称
		/// </summary>
			[Display(Name = "经销商名称")]
		public string AgentName { get; set; }
		
		/// <summary>
		/// 经销商岗位
		/// </summary>
			[Display(Name = "经销商岗位")]
		public string AgentJobName { get; set; }
		
		/// <summary>
		/// 身份证号码
		/// </summary>
			[Display(Name = "身份证号码")]
		public string CardNo { get; set; }
		
		/// <summary>
		/// 经销商部门
		/// </summary>
			[Display(Name = "经销商部门")]
		public string AgentDepart { get; set; }

        /// <summary>
        /// 总记录数
        /// </summary>
        public int Total { set; get; }

        /// <summary>
        /// 区域标识
        /// </summary>       
        public int? AreaCode { get; set; }

        /// <summary>
        /// 省份标识
        /// </summary>       
        public int? ProvinceCode { get; set; }

        /// <summary>
        /// 城市标识
        /// </summary>      
        public int? CityCode { get; set; }
    }
}

