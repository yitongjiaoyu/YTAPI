//-------------------------------------------------------------------
//文件名称：EApplicationLog.cs
//模块名称：EApplicationLog数据访问层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearning.Models;

namespace ELearning.DAL
{
    /// <summary>
    /// EApplicationLog数据访问层
    /// </summary>
	public partial class EApplicationLogRepository : BaseRepository<EApplicationLog>
	{
	
	}
}


