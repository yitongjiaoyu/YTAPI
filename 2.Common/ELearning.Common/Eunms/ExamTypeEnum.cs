﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Common
{
    /// <summary>
    /// 考试类型
    /// </summary>
    public enum ExamTypeEnum
    {
        /// <summary>
        /// 等级考试
        /// </summary>
        [Description("等级考试")]
        Level = 1,
        /// <summary>
        /// 普通考试
        /// </summary>
        [Description("普通考试")]
        General = 2,
        /// <summary>
        /// 预习作业
        /// </summary>
        [Description("预习作业")]
        Practice = 3,
        /// <summary>
        /// 课后作业
        /// </summary>
        [Description("课后作业")]
        Tasker = 4,
        /// <summary>
        /// 调查问卷
        /// </summary>
        [Description("调查问卷")]
        Qna = 5,
        /// <summary>
        /// 投票
        /// </summary>
        [Description("投票")]
        Vote = 6,
    }

    /// <summary>
    /// 考试分组
    /// </summary>
    public enum ExamGroupEnum
    {
        /// <summary>
        /// 普通问卷
        /// </summary>
        [Description("普通问卷")]
        Paper = 1,
        /// <summary>
        /// 投票
        /// </summary>
        [Description("投票")]
        Vote = 2,
    }
}
