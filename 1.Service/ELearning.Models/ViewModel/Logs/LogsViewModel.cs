﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.Logs
{
  public  class LogsViewModel
    {
        public List<EApplicationLog> LogsItems { set; get; } = new List<EApplicationLog>();

        /// <summary>
        /// 总数量
        /// </summary>
        public int TotalCount { set; get; }
    }

    /// <summary>
    /// 获取用户列表参数
    /// </summary>
    public class GetLogsRequest : BaseRequest
    {
        /// <summary>
        /// 登录名
        /// </summary>          
        public string LoginName { get; set; }

        /// <summary>
        /// 日志等级
        /// </summary>          
        public string LogLevel { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        public string LogMessage { get; set; }

        /// <summary>
        /// 日志开始时间
        /// </summary>
        public DateTime StartTime { get; set; }

        /// <summary>
        /// 日志结束时间
        /// </summary>
        public DateTime EndTime { get; set; }
    }
}
