﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using ELearning.BLL;
using ELearning.Common.CommonStr;
using ELearning.Common.Extensions;
using ELearning.Common.Helpers;
using ELearning.Common.Model;
using ELearning.Models;
using ELearning.Models.ViewModel.YTJYAdmin.Package;

namespace ELearning.API.Controllers.YTJYAdminController
{
    /// <summary>
    /// 套餐相关控制器
    /// </summary>
    public class PackageController : BaseApiController
    {
        /// <summary>
        /// 套餐相关操作service
        /// </summary>
        private EPackageService ePackageService => new EPackageService();

        /// <summary>
        /// 套餐课程相关操作service
        /// </summary>
        private RPackageCourseService rPackageCourseService => new RPackageCourseService();

        /// <summary>
        /// 用户套餐关联操作service
        /// </summary>
        private TUserPackageService tUserPackageService => new TUserPackageService();

        /// <summary>
        /// 获取课程表
        /// </summary>
        [HttpPost, Route("api/ytjyadmin/EPackage/GetModelList")]
        public ServiceResponse<PackageViewModel> GetModelList(PackageRequest request)
        {
            var viewModel = ePackageService.GetModelList(request);

            return ServiceResponse<PackageViewModel>.SuccessResponse(CommonConst.OprateSuccessStr, viewModel);
        }

        /// <summary>
        /// 新增或者更新信息
        /// </summary>
        [HttpPost, Route("api/ytjyadmin/EPackage/PostModel")]
        public ServiceResponse<bool> PostModel(PackagePostModel viewModel)
        {
            var model = ePackageService.Find(v => v.Id == viewModel.Id);
            if (model.IsNotNull())//编辑
            {
                model.Name = viewModel.Name;
                model.CoverUrl = viewModel.CoverUrl;
                model.CategoryId = viewModel.CategoryId;
                model.TypeCode = viewModel.TypeCode;
                model.Description = viewModel.Description;
                model.Sort = viewModel.Sort;
                model.OldPrice = viewModel.OldPrice;
                model.NowPrice = viewModel.NowPrice;

                model.UpdateTime = DateTime.Now;
                model.UpdateUserId = CurrentUserModel?.UserId ?? Guid.Empty;

                ePackageService.Update(model);
            }
            else//新增
            {
                model = viewModel.MapTo<EPackage, PackagePostModel>();
                model.UpdateTime = DateTime.Now;
                model.UpdateUserId = CurrentUserModel?.UserId ?? Guid.Empty;

                model.CreateTime = DateTime.Now;
                model.CreateUserId = CurrentUserModel?.UserId ?? Guid.Empty;
                model.OrgCode = CurrentUserModel?.OrgCode ?? string.Empty;
                model.Id = Guid.NewGuid();
                model.IsUsed = true;

                ePackageService.Add(model);
            }

            return ServiceResponse<bool>.SuccessResponse(CommonConst.OprateSuccessStr, true);
        }

        /// <summary>
        /// 删除
        /// </summary>
        [HttpGet, Route("api/ytjyadmin/EPackage/DeleteModel")]
        public ServiceResponse<bool> DeleteModel(Guid id)
        {
            var model = ePackageService.Find(v => v.Id == id);

            if (model.IsNotNull())
            {
                model.IsDelete = true;
                model.UpdateTime = DateTime.Now;
                model.UpdateUserId = CurrentUserModel?.UserId ?? Guid.Empty;
                ePackageService.Update(model);
            }

            return ServiceResponse<bool>.SuccessResponse(CommonConst.OprateSuccessStr, true);
        }

        /// <summary>
        /// 获取套餐关联或者不关联的课程列表
        /// </summary>
        [HttpPost, Route("api/ytjyadmin/EPackage/GetPackageCourseList")]
        public ServiceResponse<PackageCourseViewModel> GetPackageCourseList(PackageCourseRequest request)
        {
            var viewModel = ePackageService.GetPackageCourseList(request);

            return ServiceResponse<PackageCourseViewModel>.SuccessResponse(CommonConst.OprateSuccessStr, viewModel);
        }

        /// <summary>
        /// 保存套餐关联或者不关联的课程
        /// </summary>
        [HttpPost, Route("api/ytjyadmin/EPackage/SavePackageCourse")]
        public ServiceResponse<bool> SavePackageCourse(PackageCoursePostModel request)
        {
            if (request.Type == 0)//要进行关联操作
            {
                if (request.CourseIdList.Any())
                {
                    var addList = new List<RPackageCourse>();
                    foreach (var id in request.CourseIdList)
                    {
                        var addModel = new RPackageCourse
                        {
                            Id = Guid.NewGuid(),
                            CourseId = id,
                            CreateUserId = CurrentUserModel.UserId,
                            CreateTime = DateTime.Now,
                            IsDelete = false,
                            Sort = 0,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId,
                            PackageId = request.PackageId
                        };
                        addList.Add(addModel);
                    }
                    if (addList.Any())
                    {
                        var dt = addList.ToDataTable(null);
                        dt.TableName = "RPackageCourse";
                        SqlBulkCopyHelper.SaveTable(dt);
                    }
                }
            }
            else//要进行取消关联操作
            {
                if (request.CourseIdList.Any())
                {
                    var deleteList = rPackageCourseService.FindList(r => !r.IsDelete && r.PackageId == request.PackageId && request.CourseIdList.Contains(r.CourseId)).ToList();
                    if (deleteList.Any())
                    {
                        foreach (var d in deleteList)
                        {
                            d.IsDelete = true;
                            d.UpdateTime = DateTime.Now;
                            d.UpdateUserId = CurrentUserModel.UserId;
                            rPackageCourseService.UpdateNoSaveChange(d);
                        }

                        rPackageCourseService.SaveChange();
                    }
                }
            }

            return ServiceResponse<bool>.SuccessResponse(CommonConst.OprateSuccessStr, true);
        }

        /// <summary>
        /// 获取套餐关联的用户列表
        /// </summary>
        [HttpPost, Route("api/ytjyadmin/EPackage/GetPackageUserList")]
        public ServiceResponse<PackageUserViewModel> GetPackageUserList(PackageUserRequest request)
        {
            var viewModel = ePackageService.GetPackageUserList(request);

            return ServiceResponse<PackageUserViewModel>.SuccessResponse(CommonConst.OprateSuccessStr, viewModel);
        }

        /// <summary>
        /// 保存套餐用户关联
        /// </summary>
        [HttpPost, Route("api/ytjyadmin/EPackage/SavePackageUser")]
        public ServiceResponse<bool> SavePackageUser(PackageUserPostModel request)
        {
            if (request.Type == 0)//要进行关联操作
            {
                if (request.UserIdList.Any())
                {
                    var addList = new List<TUserPackage>();
                    foreach (var id in request.UserIdList)
                    {
                        var addModel = new TUserPackage
                        {
                            Id = Guid.NewGuid(),
                            UserId = id,
                            CreateUserId = CurrentUserModel.UserId,
                            CreateTime = DateTime.Now,
                            IsDelete = false,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId,
                            PackageId = request.PackageId
                        };
                        addList.Add(addModel);
                    }
                    if (addList.Any())
                    {
                        var dt = addList.ToDataTable(null);
                        dt.TableName = "TUserPackage";
                        SqlBulkCopyHelper.SaveTable(dt);
                    }
                }
            }
            else//要进行取消关联操作
            {
                if (request.UserIdList.Any())
                {
                    var deleteList = tUserPackageService.FindList(r => !r.IsDelete && r.PackageId == request.PackageId && request.UserIdList.Contains(r.UserId)).ToList();
                    if (deleteList.Any())
                    {
                        foreach (var d in deleteList)
                        {
                            d.IsDelete = true;
                            d.UpdateTime = DateTime.Now;
                            d.UpdateUserId = CurrentUserModel.UserId;
                            tUserPackageService.UpdateNoSaveChange(d);
                        }

                        rPackageCourseService.SaveChange();
                    }
                }
            }

            return ServiceResponse<bool>.SuccessResponse(CommonConst.OprateSuccessStr, true);
        }

    }
}