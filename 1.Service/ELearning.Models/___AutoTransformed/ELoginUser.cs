//-------------------------------------------------------------------
//文件名称：ELoginUser.cs
//模块名称：ELoginUserModel层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ELearning.Models
{	
	 /// <summary>
    /// ELoginUser表实体类
    /// </summary>
	[Table("ELoginUser")]
	public partial class ELoginUser
    {
		
		/// <summary>
		/// 
		/// </summary>
				[Key]
	[Display(Name = "")]
		public Guid LoginUserID { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string LoginName { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string UserName { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string UserType { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string OrgCode { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Unumber { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Password { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public int Sex { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public DateTime Birthday { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string HeadPhoto { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Company { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Department { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Position { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string OfficeTel { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string MobilePhone { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Email { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Remark { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public int AccountStatus { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string RegisterDevice { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Creater { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public DateTime CreateTime { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string Modifyer { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public DateTime ModifyTime { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public bool IsDelete { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string AgentNumber { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string SGMWRecordJob { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public int JobStatus { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public int CarSaleManageYear { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public int CarMarketManageYear { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public int OtherSaleManageYear { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public int OtherMarketManageYear { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public int CarServiceManageYear { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public int CarRepairManageYear { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public int OtherRepairManageYear { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string AgentName { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string AgentJobName { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string CardNo { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string AgentDepart { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public bool IsLock { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string BranchNumber { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string StationNumber { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public Guid PostsID { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public string JpushRegistID { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
			[Display(Name = "")]
		public Guid MainUserID { get; set; }
		   
    }
}

