//-------------------------------------------------------------------
//文件名称：RChapterPaper.cs
//模块名称：RChapterPaper业务逻辑接口层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearning.DAL;
using ELearning.Models;

namespace ELearning.BLL
{
    /// <summary>
    /// RChapterPaper业务逻辑接口层
    /// </summary>
	public partial class RChapterPaperService : BaseService<RChapterPaper>
	{
     public  RChapterPaperService() : base(RepositoryFactory.RChapterPaperRepository) { }
	}
}


