﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Common
{
    public static class AppConfig
    {
        /// <summary>
        /// SGMW用户登录appId
        /// </summary>
        public static string AppId => ConfigHelper.GetConfigString("appId");
        /// <summary>
        /// SGMW用户登录appKey
        /// </summary>
        public static string AppKey => ConfigHelper.GetConfigString("appKey");
        /// <summary>
        /// SGMW用户登录请URL
        /// </summary>
        public static string LoginRequest => ConfigHelper.GetConfigString("loginRequest");

        /// <summary>
        /// 发送消息接口
        /// </summary>
        public static string SendMsgRequest => ConfigHelper.GetConfigString("sendMessageUrl");

        /// <summary>
        /// 登陆来源
        /// </summary>
        public static string Source => ConfigHelper.GetConfigString("source");
        /// <summary>
        /// 骏客APP用户登录请URL
        /// </summary>
        public static string LoginRequestJK => ConfigHelper.GetConfigString("loginRequestJK");
        /// <summary>
        /// 骏客APP用户登录-RSA加密-公匙
        /// </summary>
        public static string RSAPublicKey => ConfigHelper.GetConfigString("RSAPublicKey");
        /// <summary>
        /// 报名成功通知内容模版
        /// </summary>
        public static string ApplySuccess => ConfigHelper.GetConfigString("ApplySuccess");
        /// <summary>
        /// 报名失败通知内容模版
        /// </summary>
        public static string ApplyFail => ConfigHelper.GetConfigString("ApplyFail");
    }
}
