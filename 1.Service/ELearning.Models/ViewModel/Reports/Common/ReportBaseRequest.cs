﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel
{
    public class ReportBaseRequest : BaseRequest
    {
        /// <summary>
        /// 区域标识
        /// </summary>       
        public int AreaCode { get; set; } = 0;

        /// <summary>
        /// 省份标识
        /// </summary>       
        public int ProvinceCode { get; set; } = 0;

        /// <summary>
        /// 城市标识
        /// </summary>      
        public int CityCode { get; set; } = 0;

        /// <summary>
        /// 经销商代码
        /// </summary>       
        public string DealerCode { get; set; } = string.Empty;

        /// <summary>
        /// 经销商名称
        /// </summary>       
        public string DealerName { get; set; } = string.Empty;

        /// <summary>
        /// 一级代码
        /// </summary>
        public string ManagementCode { get; set; } = string.Empty;

        /// <summary>
        /// 管理体系简称
        /// </summary>
        public string ManagementName { get; set; } = string.Empty;

        /// <summary>
        /// 搜索开始日期
        /// </summary>
        public DateTime SearchBegin { get; set; } = DateTime.Parse("2019-08-01");

        /// <summary>
        /// 搜索结束日期
        /// </summary>
        public DateTime SearchEnd { get; set; } = DateTime.Now;

        /// <summary>
        /// 访问来源
        /// </summary>
        public string VisitSource { get; set; } = string.Empty;
    }
}
