﻿using ELearning.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models
{
    
        public class StudentSubscribeViewModel
        {
            /// <summary>
            /// 学生关注教师列表
            /// </summary>
            public List<TeacherInfo> TeacherItems = new List<TeacherInfo>();

            /// <summary>
            /// 总条数
            /// </summary>
            public int TotalCount { set; get; }
        }
    public class TeacherInfo
    {
        public Guid ID { get; set; }
 
       /// <summary>
       /// 学生ID
       /// </summary>
        public Guid StuID { get; set; }

        /// <summary>
        /// 教师ID
        /// </summary>
        public Guid TopicID { get; set; }

      /// <summary>
      /// 关注时间
      /// </summary>
        public DateTime PraiseTime { get; set; }

        /// <summary>
        /// 教师名
        /// </summary>
        public string UserName { set; get; }

        /// <summary>
        /// 登录名
        /// </summary>          
        public string LoginName { get; set; }

        /// <summary>
        /// 移动电话
        /// </summary>          
        public string MobilePhone { get; set; }

        /// <summary>
        /// 用户类型
        /// </summary>
        public string UserType { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public string Sex { get; set; }

        /// <summary>
        /// 用户状态
        /// </summary>
        public int AccountStatus { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public string HeadPhoto { get; set; }

        /// <summary>
        /// 职务
        /// </summary>
        public string Position { get; set; }

        /// <summary>
        /// 个人简介
        /// </summary>
        public string Remark { get; set; }

 
        /// <summary>
        /// 创建者
        /// </summary>
        public string Creater { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 修改者
        /// </summary>
        public string Modifyer { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime ModifyTime { get; set; }

        /// <summary>
        /// 是否删除
        /// </summary>
        public bool IsDelete { get; set; }


    }

    /// <summary>
    /// 新闻请求参数
    /// </summary>
    public class StudentSubscribeRequest : BaseRequest
    {
        
    }
}
