﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.YTJYAdmin.Course
{
    public class FreeCourseRequest : BaseSearchRequest
    {

    }

    public class FreeCourseViewModel
    {
        /// <summary>
        /// 课程列表
        /// </summary>
        public List<FreeCourseItem> Items { set; get; } = new List<FreeCourseItem>();

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { set; get; }
    }

    public class FreeCourseItem
    {
        public Guid Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 封面图片地址
        /// </summary>
        [Display(Name = "封面图片地址")]
        public string CoverUrl { get; set; }

        /// <summary>
        /// 展示所用
        /// </summary>
        public string ShowCoverUrl { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }

    }

    public class FreeCourseModel
    {
        public Guid Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 封面图片地址
        /// </summary>
        [Display(Name = "封面图片地址")]
        public string CoverUrl { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { get; set; }
    }

    /// <summary>
    /// 提交视频记录id
    /// </summary>
    public class SubmitVideoLogRequest
    {
        /// <summary>
        /// 套餐id
        /// </summary>
        public Guid PackageId { set; get; }

        /// <summary>
        /// 课程id
        /// </summary>
        public Guid CourseId { set; get; }

        /// <summary>
        /// 章节id
        /// </summary>
        public Guid ChapterId { get; set; }

        /// <summary>
        /// 视频id
        /// </summary>
        public Guid VideoId { set; get; }

        /// <summary>
        /// 历史记录id
        /// </summary>
        public Guid StudyLogId { set; get; }

        /// <summary>
        /// 是否看完该视频
        /// </summary>
        public bool IsFinish { set; get; }

        /// <summary>
        /// 看视频看到第几秒
        /// </summary>
        public int StudyTime { set; get; }
    }

    /// <summary>
    /// 提交历史记录返回的数据实体
    /// </summary>
    public class SubmitVideoLogViewModel
    {
        /// <summary>
        /// 历史记录id
        /// </summary>
        public Guid StudyLogId { set; get; }
    }

}
