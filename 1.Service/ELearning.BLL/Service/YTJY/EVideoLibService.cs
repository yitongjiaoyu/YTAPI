//-------------------------------------------------------------------
//文件名称：EVideoLib.cs
//模块名称：EVideoLib业务逻辑接口层
//功能说明：
//-----------------------------------------------------------------
//修改记录：
//修改人：Dawen
//-----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearning.Common;
using ELearning.Common.Extensions;
using ELearning.Common.Helpers;
using ELearning.DAL;
using ELearning.Models;
using ELearning.Models.ViewModel.YTJYAdmin.VideoLib;

namespace ELearning.BLL
{
    /// <summary>
    /// EVideoLib业务逻辑接口层
    /// </summary>
	public partial class EVideoLibService : BaseService<EVideoLib>
    {
        /// <summary>
        /// 后台获取视频列表
        /// </summary>
        public VideoLibViewModel GetVideoLibList(VideoLibRequest request, string orgCode)
        {
            var models = RepositoryFactory.EVideoLibRepository.GetVideoLibList(request, orgCode);
            var viewModel = new VideoLibViewModel { TotalCount = models.FirstOrDefault()?.TotalCount ?? 0 };
            foreach (var m in models)
            {
                var v = m.MapTo<VideoLibItem, VideoLibModel>();
                v.CreateTime = m.CreateTime.ToCommonStr();
                v.UpdateTime = m.UpdateTime.ToCommonStr();
                v.ShowCoverUrl = PictureHelper.GetFileFullPath(v.CoverUrl);
                v.StatusStr = IntToStringHelper.GetConvertStatsString(v.Status);
                viewModel.Items.Add(v);
            }

            return viewModel;
        }

    }
}


