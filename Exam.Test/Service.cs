﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELearning.BLL;
using ELearning.Models.ViewModel;

namespace Exam.Test
{
    public class Service
    {

        EExamService examService => new EExamService();
        EDictionaryService dictionaryService => new EDictionaryService();
        ESysUserService sysUserService => new ESysUserService();

        public void Run()
        {
            var ttt = sysUserService.Find(x => !x.IsDelete);
            var userIDs = sysUserService.FindList(x => !x.IsDelete).Select(m => m.UserID).ToList();

            ExamInfoRequest req = new ExamInfoRequest();
            req.EType = new List<int>()
            {
                2,
            };
            string text = string.Empty;
            List<double> show = new List<double>();
            foreach (var uid in userIDs)
            {
                var tStart = DateTime.Now;
                req.StuID = uid;
                var data = SearchExamByStu_Test(req);
                var tEnd = DateTime.Now;
                var useSS = ExecDateDiff(tStart, tEnd);
                Console.WriteLine(useSS);
                show.Add(useSS);
            }
            Console.WriteLine(show.Average());
            Console.ReadLine();
        }

        /// <summary>
        /// 搜索考试、作业、问卷列表-学生
        /// </summary>
        /// <returns></returns>
        public bool SearchExamByStu_Test(ExamInfoRequest req)
        {

            var data = examService.SearchExamByStu(req);

            // 查询分类名称
            var dictionarys = dictionaryService.FindList(x => !x.IsDelete).ToList();
            if (data.Data.Items.Count > 0) data.Data.Items.ForEach(x => x.SubjectName = (string.Join(",", dictionarys.FindAll(a => x.SubjectName.Split(',').ToList().Contains(a.TypeCode)).Select(m => m.TypeName).ToList())));

            // 查询关联的课程名称
            //if (req.EType.Contains(ExamTypeEnum.Tasker.IntValue()) || req.EType.Contains(ExamTypeEnum.Practice.IntValue()))
            //    data.Data.Items.ForEach(x => x.CourseName = GetCourseNameByEid(x.Eid).Data);

            return true;
        }

        /// <summary>
        /// 程序执行时间测试
        /// </summary>
        /// <param name="dateBegin">开始时间</param>
        /// <param name="dateEnd">结束时间</param>
        /// <returns>返回(秒)单位，比如: 0.00239秒</returns>
        public static double ExecDateDiff(DateTime dateBegin, DateTime dateEnd)
        {
            TimeSpan ts1 = new TimeSpan(dateBegin.Ticks);
            TimeSpan ts2 = new TimeSpan(dateEnd.Ticks);
            TimeSpan ts3 = ts1.Subtract(ts2).Duration();
            //你想转的格式
            return ts3.TotalMilliseconds;
        }
    }
}
