﻿using ELearning.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Linq.Expressions;
using ELearning.Common.Extensions;
using ELearning.Models;
using ELearning.Common.Model;

namespace ELearning.API.Controllers.AdminControllers
{
    public class AppVersionController : BaseApiController
    {
        /// <summary>
        /// app服务
        /// </summary>
        protected EAppVersionService appService = new EAppVersionService();
        
        /// <summary>
        /// 获取app版本列表
        /// </summary>
        [HttpPost, Route("api/AppVersion/GetAppVersionList")]
        public ServiceResponse<AppVersionViewModel> GetAppVersionList(GetAppVersionRequest request)
        {
            var viewModel = new AppVersionViewModel();
            var orgPage = appService.GetVersionList(request);
            if (orgPage != null)
            {
                viewModel.TotalCount = orgPage.TotalItems.ObjToInt();
                foreach (var log in orgPage.Items)
                {
                    viewModel.VersionItems.Add(log);
                }
            }
            return ServiceResponse<AppVersionViewModel>.SuccessResponse("success", viewModel);
        }

        /// <summary>
        /// 根据code 获取最新的版本信息
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet, Route("api/AppVersion/GetAppVersionByCode")]
        public ServiceResponse<EAppVersion> GetAppVersionByCode(string  code)
        {
            Expression<Func<EAppVersion, bool>> whereLamdba = (u) => u.IsDelete == false&& u.AppCode==code;
              
            var appversion = appService.FindList(whereLamdba).OrderByDescending(w=>w.CreateTime).ToList().FirstOrDefault();
            //if (appversion != null)
            //{
            //    return ServiceResponse<EAppVersion>.SuccessResponse("success", appversion);
            //}
            return ServiceResponse<EAppVersion>.SuccessResponse("success", appversion);
        }
        /// <summary>
        /// 添加版本
        /// </summary>
        [HttpPost, Route("api/AppVersion/AddAppVersion")]
        public ServiceResponse<EAppVersion> AddAppVersion(EAppVersion o)
        {
            var org = new EAppVersion
            {
                ID = Guid.NewGuid(),
                AppCode = o.AppCode,
                 AppUrl=o.AppUrl,
                 AppVersion=o.AppVersion,
                 UpdateText=o.UpdateText,
                Creater = CurrentUserModel.LoginName,
                CreateTime = DateTime.Now,
               Modifyer=CurrentUserModel.LoginName,
                IsDelete = false,
                ModifyTime = DateTime.Now
            };
            var u =appService.Add(org);
            return ServiceResponse<EAppVersion>.SuccessResponse("操作成功", u);
        }

        /// <summary>
        /// 更新版本
        /// </summary>
        [HttpPost, Route("api/AppVersion/UpdateAppVersion")]
        public ServiceResponse<bool> UpdateAppVersion(EAppVersion o)
        {
            var app = appService.Find(r => !r.IsDelete && r.ID == o.ID);
            if (app != null)
            {
                app.AppCode = o.AppCode;
                app.AppUrl = o.AppUrl;
                app.AppVersion = o.AppVersion;
                app.UpdateText = o.UpdateText;
                app.Modifyer = CurrentUserModel.LoginName;
                app.ModifyTime = DateTime.Now;
                var b = appService.Update(app);
                return ServiceResponse<bool>.SuccessResponse("操作成功", b);
            }
            else
            {
                return ServiceResponse<bool>.SuccessResponse("未找到EAppVersion对象", false);
            }
        }

        /// <summary>
        /// 删除版本
        /// </summary>
        /// <param name="id">版本id</param>
        [HttpPost, Route("api/AppVersion/DeleteAppVersion")]
        public ServiceResponse<bool> DeleteOrg(Guid id)
        {
            var role = appService.Find(r => !r.IsDelete && r.ID == id);
            role.IsDelete = true;
            var b= appService.Update(role);
            return ServiceResponse<bool>.SuccessResponse("操作成功", b);
        }

    }
}