﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.YTJYAdmin.Paper
{
    public class TPaperQuestionRequest : BaseSearchRequest
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { set; get; }

        /// <summary>
        /// 大类分类编码
        /// </summary>
        public string CategoryId { set; get; }

        /// <summary>
        /// 基础分类编码
        /// </summary>
        public string TypeCode { set; get; }

        /// <summary>
        /// 0.不关联，1.已关联
        /// </summary>
        public int Type { set; get; }

        /// <summary>
        /// 试卷ID
        /// </summary>
        public Guid PaperId { set; get; }
    }

    public class TPaperQuestionViewModel
    {
        /// <summary>
        /// 列表
        /// </summary>
        public List<TPaperQuestionItem> Items { set; get; } = new List<TPaperQuestionItem>();

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { set; get; }
    }

    public class TPaperQuestionItem
    {
        /// <summary>
        /// 题目ID
        /// </summary>
        public Guid Id { set; get; }

        /// <summary>
        /// 主题干，材料题，解析题等类型需要
        /// </summary>
        [Display(Name = "主题干，材料题，解析题等类型需要")]
        public string MainName { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 大类分类Code
        /// </summary>
        [Display(Name = "大类分类Code")]
        public string CategoryId { get; set; }

        /// <summary>
        /// 分类名称
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// 班级基础分类code,基础班，高级版
        /// </summary>
        [Display(Name = "班级基础分类code,基础班，高级版")]
        public string TypeCode { get; set; }

        /// <summary>
        /// 基础分类名称
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// 题目解析
        /// </summary>
        [Display(Name = "题目解析")]
        public string Analysis { get; set; }

        /// <summary>
        /// 题目难度，1.简单，2.一般，3.困难
        /// </summary>
        [Display(Name = "题目难度，1.简单，2.一般，3.困难")]
        public int Difficulty { get; set; }

        public string DifficultyStr
        {
            get
            {
                var str = string.Empty;
                switch (Difficulty)
                {
                    case 1:
                        str = "简单";
                        break;
                    case 2:
                        str = "一般";
                        break;
                    case 3:
                        str = "困难";
                        break;
                }
                return str;
            }
        }

        /// <summary>
        /// 题目类型名称
        /// </summary>
        public string QuestionTypeName { get; set; }
    }

    public class TPaperQuestionModel
    {
        /// <summary>
        /// 题目ID
        /// </summary>
        public Guid Id { set; get; }

        /// <summary>
        /// 主题干，材料题，解析题等类型需要
        /// </summary>
        [Display(Name = "主题干，材料题，解析题等类型需要")]
        public string MainName { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 大类分类Code
        /// </summary>
        [Display(Name = "大类分类Code")]
        public string CategoryId { get; set; }

        /// <summary>
        /// 分类名称
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// 班级基础分类code,基础班，高级版
        /// </summary>
        [Display(Name = "班级基础分类code,基础班，高级版")]
        public string TypeCode { get; set; }

        /// <summary>
        /// 基础分类名称
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// 题目解析
        /// </summary>
        [Display(Name = "题目解析")]
        public string Analysis { get; set; }

        /// <summary>
        /// 题目难度，1.简单，2.一般，3.困难
        /// </summary>
        [Display(Name = "题目难度，1.简单，2.一般，3.困难")]
        public int Difficulty { get; set; }

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { get; set; }

        /// <summary>
        /// 题目类型名称
        /// </summary>
        public string QuestionTypeName { get; set; }
    }

    public class TPaperQuestionPostModel
    {
        /// <summary>
        /// 传入的关联类型：0.不关联,需要进行关联操作，1.已关联,需要进行取消关联操作
        /// </summary>
        public int Type { set; get; }

        /// <summary>
        /// 试卷ID
        /// </summary>
        public Guid PaperId { set; get; }

        /// <summary>
        /// 题目Id列表
        /// </summary>
        public List<Guid> QuestionIdList { set; get; }
    }
}
