﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;
using ELearning.BLL;
using ELearning.Common.CommonStr;
using ELearning.Common.Extensions;
using ELearning.Common.Helpers;
using ELearning.Common.Model;
using ELearning.Models;
using ELearning.Models.ViewModel.Question;

namespace ELearning.API.Controllers.YTJYAdminController
{
    /// <summary>
    /// 题库相关操作
    /// </summary>
    public class QueBankController : BaseApiController
    {
        /// <summary>
        /// 题库相关操作service
        /// </summary>  
        private TQuestionService tQuestionService => new TQuestionService();

        /// <summary>
        /// 题目类型操作service
        /// </summary>
        private TQuestionTypeService tQuestionTypeService => new TQuestionTypeService();

        /// <summary>
        /// 子题目操作service
        /// </summary>
        private TQuestionItemService tQuestionItemService => new TQuestionItemService();

        /// <summary>
        /// 选项操作service
        /// </summary>
        private TOptionService tOptionService => new TOptionService();

        /// <summary>
        /// 题目选项关联操作service
        /// </summary>
        private RQuestionOptionService rQuestionOptionService => new RQuestionOptionService();

        /// <summary>
        /// 字典操作service  
        /// </summary>
        private EDictionaryService eDictionaryService => new EDictionaryService();

        /// <summary>
        /// 获取列表
        /// </summary>
        [HttpPost, Route("api/ytjyadmin/QueBank/GetModelList")]
        [AllowAnonymous]
        public ServiceResponse<TQuestionViewModel> GetModelList(TQuestionRequest request)
        {
            var viewModel = tQuestionService.GetModelList(request);

            return ServiceResponse<TQuestionViewModel>.SuccessResponse(CommonConst.OprateSuccessStr, viewModel);
        }

        /// <summary>
        /// 获取题目详情
        /// </summary>
        /// <param name="id">题目id</param>
        [HttpGet, Route("api/ytjyadmin/QueBank/GetModelDetail")]
        public ServiceResponse<TQuestionPostModel> GetModelDetail(Guid id)
        {
            var model = tQuestionService.Find(v => v.Id == id);
            var viewModel = new TQuestionPostModel();

            if (model.IsNotNull())
            {
                int.TryParse(model.QuestionTypeCode, out var qtc);
                //获取原有的题目id
                var havedQueItems = tQuestionItemService.FindList(q => !q.IsDelete && q.QuestionId == model.Id).ToList();
                //获取原有的选项列表
                var havedOptions = rQuestionOptionService.FindList(r => !r.IsDelete && r.QuestionId == model.Id).ToList();
                var optionIds = havedOptions.Select(r => r.OptionId).Distinct().ToList();
                var options = tOptionService.FindList(o => !o.IsDelete && optionIds.Contains(o.Id)).ToList();

                viewModel = model.MapTo<TQuestionPostModel, TQuestion>();
                viewModel.QuestionItemId = havedQueItems.FirstOrDefault()?.Id ?? Guid.Empty;

                if (qtc <= 5) //单选、多选、判断、填空题
                {
                    foreach (var option in options)
                    {
                        var t = viewModel.Items.FirstOrDefault(i => i.OptionId == option.Id);
                        if (t.IsNull())
                        {
                            var rqo = havedOptions.FirstOrDefault(h => h.OptionId == option.Id) ?? new RQuestionOption();
                            t = new TOptionItem
                            {
                                OptionId = option.Id,
                                OptionName = option.Name,
                                IsRight = rqo.IsRight,
                                OptionSort = rqo.Sort
                            };
                            viewModel.Items.Add(t);
                        }
                    }
                }
                if (qtc >= 5) //配伍选择题 ,案例分析、综合分析、主观题、简答题
                {
                    // 构造子题目实体
                    foreach (var que in havedQueItems)
                    {
                        var t = viewModel.QueItems.FirstOrDefault(i => i.Id == que.Id);
                        if (t.IsNull())
                        {
                            // 把题目添加到列表中
                            var qi = que.MapTo<TQuestionItemModel, TQuestionItem>();
                            viewModel.QueItems.Add(qi);

                            if (qtc >= 5 || qtc == 6 || qtc == 7) //配伍选择题 ,案例分析、综合分析
                            {
                                // 构造子题目中的选项列表
                                var qios = havedOptions.Where(r => r.QuestionItemId == qi.Id).ToList();
                                var qioIds = qios.Select(i => i.OptionId).ToList();
                                var qiops = options.Where(o => qioIds.Contains(o.Id)).ToList();
                                foreach (var option in qiops)
                                {
                                    var tp = qi.Items.FirstOrDefault(i => i.OptionId == option.Id);
                                    if (tp.IsNull())
                                    {
                                        var rqo = qios.FirstOrDefault(h => h.OptionId == option.Id) ?? new RQuestionOption();
                                        tp = new TOptionItem
                                        {
                                            OptionId = option.Id,
                                            OptionName = option.Name,
                                            IsRight = rqo.IsRight,
                                            OptionSort = rqo.Sort
                                        };
                                        qi.Items.Add(tp);
                                    }
                                }
                            }

                        }
                    }
                }
            }

            return ServiceResponse<TQuestionPostModel>.SuccessResponse(CommonConst.OprateSuccessStr, viewModel);
        }

        /// <summary>
        /// 新增或者更新信息
        /// </summary>
        [HttpPost, Route("api/ytjyadmin/QueBank/PostModel")]
        public ServiceResponse<bool> PostModel(TQuestionPostModel viewModel)
        {
            var model = tQuestionService.Find(v => v.Id == viewModel.Id);
            if (string.IsNullOrEmpty(viewModel.OrgCode))
                viewModel.OrgCode = CurrentUserModel.OrgCode;

            int.TryParse(viewModel.QuestionTypeCode, out var qtc);

            if (model.IsNotNull())//编辑
            {
                model.MainName = viewModel.MainName;
                model.Name = viewModel.Name;
                model.CategoryId = viewModel.CategoryId;
                model.TypeCode = viewModel.TypeCode;
                model.QuestionTypeCode = viewModel.QuestionTypeCode;
                model.OrgCode = viewModel.OrgCode;
                model.Analysis = viewModel.Analysis;
                model.Sort = viewModel.Sort;
                model.Grade = viewModel.Grade;
                model.Difficulty = viewModel.Difficulty;

                model.UpdateTime = DateTime.Now;
                model.UpdateUserId = CurrentUserModel.UserId;

                tQuestionService.UpdateNoSaveChange(model);

                //获取原有的题目id
                var havedQueItems = tQuestionItemService.FindList(q => !q.IsDelete && q.QuestionId == model.Id).ToList();
                //获取原有的选项列表
                var havedOptions = rQuestionOptionService.FindList(r => !r.IsDelete && r.QuestionId == model.Id).ToList();
                var optionIds = havedOptions.Select(r => r.OptionId).Distinct().ToList();
                var options = tOptionService.FindList(o => !o.IsDelete && optionIds.Contains(o.Id)).ToList();
                if (havedOptions.Any())//删除原来全部的题目选项关联关系
                    rQuestionOptionService.DeletesNoSaveChange(havedOptions);

                #region 单选、多选、判断、填空题

                //单选、多选、判断、填空题
                if (qtc < 5)
                {
                    var qi = havedQueItems.FirstOrDefault(q => q.Id == viewModel.QuestionItemId);
                    if (qi.IsNotNull())
                    {
                        //保存子题目
                        qi.Name = viewModel.Name;
                        qi.CategoryId = viewModel.CategoryId;
                        qi.TypeCode = viewModel.TypeCode;
                        qi.QuestionTypeCode = viewModel.QuestionTypeCode;
                        qi.OrgCode = viewModel.OrgCode;
                        qi.Analysis = viewModel.Analysis;
                        qi.Sort = viewModel.Sort;
                        qi.Grade = viewModel.Grade;
                        qi.Difficulty = viewModel.Difficulty;
                        qi.UpdateTime = DateTime.Now;
                        qi.UpdateUserId = CurrentUserModel.UserId;
                        tQuestionItemService.UpdateNoSaveChange(qi);

                        //添加子题目下面的选项
                        foreach (var optionItem in viewModel.Items)
                        {
                            var op = options.FirstOrDefault(o => o.Id == optionItem.OptionId);
                            if (op.IsNull())//数据库中不存在该选项，则新增
                            {
                                op = new TOption
                                {
                                    CreateTime = DateTime.Now,
                                    CreateUserId = CurrentUserModel.UserId,
                                    UpdateTime = DateTime.Now,
                                    UpdateUserId = CurrentUserModel.UserId,
                                    IsDelete = false,
                                    Id = optionItem.OptionId,
                                    Name = optionItem.OptionName
                                };
                                tOptionService.AddNoSaveChange(op);
                            }
                            else//数据库中存在该选项，则更新
                            {
                                op.Name = optionItem.OptionName;
                                op.UpdateTime = DateTime.Now;
                                op.UpdateUserId = CurrentUserModel.UserId;
                                tOptionService.UpdateNoSaveChange(op);
                            }

                            //保存选项与题目的关联关系
                            var rqo = new RQuestionOption
                            {
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId,
                                IsDelete = false,
                                Id = Guid.NewGuid(),
                                IsRight = optionItem.IsRight,
                                Sort = optionItem.OptionSort,
                                OptionId = op.Id,
                                QuestionId = model.Id,
                                QuestionItemId = qi.Id
                            };

                            if (optionItem.IsRight)
                            {
                                model.RightAnswer = $"{model.RightAnswer}{op.Id}/";
                                qi.RightAnswer = $"{qi.RightAnswer}{op.Id}/";
                            }
                            rQuestionOptionService.AddNoSaveChange(rqo);
                        }
                    }
                }

                #endregion

                #region 配伍选择题

                if (qtc == 5)
                {
                    model.MainName = string.Join("/", viewModel.Items.Select(i => i.OptionName).ToList());
                    // 先更新或者保存选项
                    foreach (var optionItem in viewModel.Items)
                    {
                        var op = options.FirstOrDefault(o => o.Id == optionItem.OptionId);
                        if (op.IsNull())//数据库中不存在该选项，则新增
                        {
                            op = new TOption
                            {
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId,
                                IsDelete = false,
                                Id = optionItem.OptionId,
                                Name = optionItem.OptionName
                            };
                            tOptionService.AddNoSaveChange(op);
                        }
                        else//数据库中存在该选项，则更新
                        {
                            op.Name = optionItem.OptionName;
                            op.UpdateTime = DateTime.Now;
                            op.UpdateUserId = CurrentUserModel.UserId;
                            tOptionService.UpdateNoSaveChange(op);
                        }

                    }

                    //添加子题目
                    foreach (var queItem in viewModel.QueItems)
                    {
                        var qi = havedQueItems.FirstOrDefault(q => q.Id == queItem.Id);
                        if (qi.IsNull())
                        {
                            qi = new TQuestionItem
                            {
                                Id = Guid.NewGuid(),
                                Analysis = queItem.Analysis,
                                CategoryId = model.CategoryId,
                                TypeCode = model.TypeCode,
                                QuestionId = model.Id,
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId,
                                Difficulty = queItem.Difficulty,
                                IsDelete = false,
                                OrgCode = model.OrgCode,
                                Sort = queItem.Sort,
                                Name = queItem.Name,
                                Grade = queItem.Grade,
                                RightAnswer = "/",
                                QuestionTypeCode = model.QuestionTypeCode,
                                Precondition = queItem.Precondition
                            };
                            tQuestionItemService.AddNoSaveChange(qi);
                        }
                        else
                        {
                            qi.Sort = queItem.Sort;
                            qi.Name = queItem.Name;
                            qi.Grade = queItem.Grade;
                            qi.Analysis = queItem.Analysis;
                            qi.Difficulty = queItem.Difficulty;
                            qi.Precondition = queItem.Precondition;
                            qi.UpdateTime = DateTime.Now;
                            qi.UpdateUserId = CurrentUserModel.UserId;
                            tQuestionItemService.UpdateNoSaveChange(qi);
                        }

                        //添加子题目下面的选项,把主题干的选项与每个小题进行关联，然后找到每个题目对应的正确答案与之对应保存
                        foreach (var optionItem in viewModel.Items)
                        {
                            var temp = queItem.Items.FirstOrDefault(i => i.OptionId == optionItem.OptionId) ?? new TOptionItem();
                            //保存选项与题目的关联关系
                            var rqo = new RQuestionOption
                            {
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId,
                                IsDelete = false,
                                Id = Guid.NewGuid(),
                                IsRight = temp.IsRight,
                                Sort = temp.OptionSort,
                                OptionId = optionItem.OptionId,
                                QuestionId = model.Id,
                                QuestionItemId = qi.Id
                            };

                            if (temp.IsRight)
                                qi.RightAnswer = $"{qi.RightAnswer}{optionItem.OptionId}/";

                            rQuestionOptionService.AddNoSaveChange(rqo);
                        }
                    }
                }

                #endregion

                #region 案例分析、综合分析

                if (qtc == 6 || qtc == 7)
                {
                    //添加子题目
                    foreach (var queItem in viewModel.QueItems)
                    {
                        var qi = havedQueItems.FirstOrDefault(q => q.Id == queItem.Id);
                        if (qi.IsNull())
                        {
                            qi = new TQuestionItem
                            {
                                Id = Guid.NewGuid(),
                                Analysis = queItem.Analysis,
                                CategoryId = model.CategoryId,
                                TypeCode = model.TypeCode,
                                QuestionId = model.Id,
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId,
                                Difficulty = queItem.Difficulty,
                                IsDelete = false,
                                OrgCode = model.OrgCode,
                                Sort = queItem.Sort,
                                Name = queItem.Name,
                                Grade = queItem.Grade,
                                RightAnswer = "/",
                                QuestionTypeCode = model.QuestionTypeCode,
                                Precondition = queItem.Precondition
                            };
                            tQuestionItemService.AddNoSaveChange(qi);
                        }
                        else
                        {
                            qi.Sort = queItem.Sort;
                            qi.Name = queItem.Name;
                            qi.Grade = queItem.Grade;
                            qi.Analysis = queItem.Analysis;
                            qi.Difficulty = queItem.Difficulty;
                            qi.Precondition = queItem.Precondition;
                            qi.UpdateTime = DateTime.Now;
                            qi.UpdateUserId = CurrentUserModel.UserId;
                            tQuestionItemService.UpdateNoSaveChange(qi);
                        }

                        //添加子题目下面的选项
                        foreach (var optionItem in queItem.Items)
                        {
                            var op = options.FirstOrDefault(o => o.Id == optionItem.OptionId);
                            if (op.IsNull())//数据库中不存在该选项，则新增
                            {
                                op = new TOption
                                {
                                    CreateTime = DateTime.Now,
                                    CreateUserId = CurrentUserModel.UserId,
                                    UpdateTime = DateTime.Now,
                                    UpdateUserId = CurrentUserModel.UserId,
                                    IsDelete = false,
                                    Id = optionItem.OptionId,
                                    Name = optionItem.OptionName
                                };
                                tOptionService.AddNoSaveChange(op);
                            }
                            else//数据库中存在该选项，则更新
                            {
                                op.Name = optionItem.OptionName;
                                op.UpdateTime = DateTime.Now;
                                op.UpdateUserId = CurrentUserModel.UserId;
                                tOptionService.UpdateNoSaveChange(op);
                            }

                            //保存选项与题目的关联关系
                            var rqo = new RQuestionOption
                            {
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId,
                                IsDelete = false,
                                Id = Guid.NewGuid(),
                                IsRight = optionItem.IsRight,
                                Sort = optionItem.OptionSort,
                                OptionId = op.Id,
                                QuestionId = model.Id,
                                QuestionItemId = qi.Id
                            };

                            if (optionItem.IsRight)
                                qi.RightAnswer = $"{qi.RightAnswer}{op.Id}/";

                            rQuestionOptionService.AddNoSaveChange(rqo);
                        }
                    }
                }

                #endregion

                #region 主观题、综合分析简答题

                if (qtc == 8 || qtc == 9)
                {
                    //添加子题目
                    foreach (var queItem in viewModel.QueItems)
                    {
                        var qi = havedQueItems.FirstOrDefault(q => q.Id == queItem.Id);
                        if (qi.IsNull())
                        {
                            qi = new TQuestionItem
                            {
                                Id = Guid.NewGuid(),
                                Analysis = queItem.Analysis,
                                Answer = queItem.Answer,
                                CategoryId = model.CategoryId,
                                TypeCode = model.TypeCode,
                                QuestionId = model.Id,
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId,
                                Difficulty = queItem.Difficulty,
                                IsDelete = false,
                                OrgCode = model.OrgCode,
                                Sort = queItem.Sort,
                                Name = queItem.Name,
                                Grade = queItem.Grade,
                                RightAnswer = "/",
                                QuestionTypeCode = model.QuestionTypeCode,
                                Precondition = queItem.Precondition
                            };
                            tQuestionItemService.AddNoSaveChange(qi);
                        }
                        else
                        {
                            qi.Sort = queItem.Sort;
                            qi.Name = queItem.Name;
                            qi.Grade = queItem.Grade;
                            qi.Analysis = queItem.Analysis;
                            qi.Answer = queItem.Answer;
                            qi.Difficulty = queItem.Difficulty;
                            qi.Precondition = queItem.Precondition;
                            qi.UpdateTime = DateTime.Now;
                            qi.UpdateUserId = CurrentUserModel.UserId;
                            tQuestionItemService.UpdateNoSaveChange(qi);
                        }
                    }
                }

                #endregion

            }
            else//新增题目以及选项
            {
                model = viewModel.MapTo<TQuestion, TQuestionPostModel>();
                model.UpdateTime = DateTime.Now;
                model.UpdateUserId = CurrentUserModel.UserId;

                model.CreateTime = DateTime.Now;
                model.CreateUserId = CurrentUserModel.UserId;
                model.Id = Guid.NewGuid();
                model.RightAnswer = "/";

                tQuestionService.AddNoSaveChange(model);

                #region 单选、多选、判断、填空题

                //单选、多选、判断、填空题
                if (qtc < 5)
                {
                    //添加一个子题目
                    var qi = new TQuestionItem
                    {
                        Id = Guid.NewGuid(),
                        Analysis = model.Analysis,
                        CategoryId = model.CategoryId,
                        TypeCode = model.TypeCode,
                        QuestionId = model.Id,
                        CreateTime = DateTime.Now,
                        CreateUserId = CurrentUserModel.UserId,
                        UpdateTime = DateTime.Now,
                        UpdateUserId = CurrentUserModel.UserId,
                        Difficulty = model.Difficulty,
                        IsDelete = false,
                        OrgCode = model.OrgCode,
                        Sort = model.Sort,
                        Name = model.Name,
                        Grade = model.Grade,
                        RightAnswer = "/",
                        QuestionTypeCode = model.QuestionTypeCode,
                        Precondition = model.Precondition
                    };
                    tQuestionItemService.AddNoSaveChange(qi);

                    //添加子题目下面的选项
                    foreach (var optionItem in viewModel.Items)
                    {
                        var op = new TOption
                        {
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId,
                            IsDelete = false,
                            Id = optionItem.OptionId,
                            Name = optionItem.OptionName
                        };

                        //保存选项与题目的关联关系
                        var rqo = new RQuestionOption
                        {
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId,
                            IsDelete = false,
                            Id = Guid.NewGuid(),
                            IsRight = optionItem.IsRight,
                            Sort = optionItem.OptionSort,
                            OptionId = op.Id,
                            QuestionId = model.Id,
                            QuestionItemId = qi.Id
                        };

                        if (optionItem.IsRight)
                        {
                            model.RightAnswer = $"{model.RightAnswer}{op.Id}/";
                            qi.RightAnswer = $"{qi.RightAnswer}{op.Id}/";
                        }

                        tOptionService.AddNoSaveChange(op);
                        rQuestionOptionService.AddNoSaveChange(rqo);
                    }
                }

                #endregion

                #region 配伍选择题

                if (qtc == 5)
                {
                    model.MainName = string.Join("/", viewModel.Items.Select(i => i.OptionName).ToList());
                    // 先把选项保存到数据库
                    foreach (var optionItem in viewModel.Items)
                    {
                        var op = new TOption
                        {
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId,
                            IsDelete = false,
                            Id = optionItem.OptionId,
                            Name = optionItem.OptionName
                        };
                        tOptionService.AddNoSaveChange(op);
                    }

                    //添加子题目
                    foreach (var queItem in viewModel.QueItems)
                    {
                        var qi = new TQuestionItem
                        {
                            Id = Guid.NewGuid(),
                            Analysis = queItem.Analysis,
                            CategoryId = model.CategoryId,
                            TypeCode = model.TypeCode,
                            QuestionId = model.Id,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId,
                            Difficulty = queItem.Difficulty,
                            IsDelete = false,
                            OrgCode = model.OrgCode,
                            Sort = queItem.Sort,
                            Name = queItem.Name,
                            Grade = queItem.Grade,
                            RightAnswer = "/",
                            QuestionTypeCode = model.QuestionTypeCode,
                            Precondition = queItem.Precondition
                        };
                        tQuestionItemService.AddNoSaveChange(qi);

                        // 把主题干的选项与每个小题进行关联，然后找到每个题目对应的正确答案与之对应保存
                        foreach (var optionItem in viewModel.Items)
                        {
                            var temp = queItem.Items.FirstOrDefault(i => i.OptionId == optionItem.OptionId) ?? new TOptionItem();
                            //保存选项与题目的关联关系
                            var rqo = new RQuestionOption
                            {
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId,
                                IsDelete = false,
                                Id = Guid.NewGuid(),
                                IsRight = temp.IsRight,
                                Sort = temp.OptionSort,
                                OptionId = optionItem.OptionId,
                                QuestionId = model.Id,
                                QuestionItemId = qi.Id
                            };

                            if (temp.IsRight)
                                qi.RightAnswer = $"{qi.RightAnswer}{optionItem.OptionId}/";
                            rQuestionOptionService.AddNoSaveChange(rqo);
                        }
                    }
                }

                #endregion

                #region 案例分析、综合分析选择题

                if (qtc == 6 || qtc == 7)
                {
                    //添加子题目
                    foreach (var queItem in viewModel.QueItems)
                    {
                        var qi = new TQuestionItem
                        {
                            Id = Guid.NewGuid(),
                            Analysis = queItem.Analysis,
                            Answer = queItem.Answer,
                            CategoryId = model.CategoryId,
                            TypeCode = model.TypeCode,
                            QuestionId = model.Id,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId,
                            Difficulty = queItem.Difficulty,
                            IsDelete = false,
                            OrgCode = model.OrgCode,
                            Sort = queItem.Sort,
                            Name = queItem.Name,
                            Grade = queItem.Grade,
                            RightAnswer = "/",
                            QuestionTypeCode = model.QuestionTypeCode,
                            Precondition = queItem.Precondition
                        };
                        tQuestionItemService.AddNoSaveChange(qi);
                        //添加子题目下面的选项
                        foreach (var optionItem in queItem.Items)
                        {
                            var op = new TOption
                            {
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId,
                                IsDelete = false,
                                Id = optionItem.OptionId,
                                Name = optionItem.OptionName
                            };

                            //保存选项与题目的关联关系
                            var rqo = new RQuestionOption
                            {
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId,
                                IsDelete = false,
                                Id = Guid.NewGuid(),
                                IsRight = optionItem.IsRight,
                                Sort = optionItem.OptionSort,
                                OptionId = op.Id,
                                QuestionId = model.Id,
                                QuestionItemId = qi.Id
                            };

                            if (optionItem.IsRight)
                                qi.RightAnswer = $"{qi.RightAnswer}{op.Id}/";

                            tOptionService.AddNoSaveChange(op);
                            rQuestionOptionService.AddNoSaveChange(rqo);
                        }
                    }
                }

                #endregion

                #region 主观题、综合分析简答题

                if (qtc == 8 || qtc == 9)
                {
                    //添加子题目
                    foreach (var queItem in viewModel.QueItems)
                    {
                        var qi = new TQuestionItem
                        {
                            Id = Guid.NewGuid(),
                            Analysis = queItem.Analysis,
                            Answer = queItem.Answer,
                            CategoryId = model.CategoryId,
                            TypeCode = model.TypeCode,
                            QuestionId = model.Id,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId,
                            Difficulty = queItem.Difficulty,
                            IsDelete = false,
                            OrgCode = model.OrgCode,
                            Sort = queItem.Sort,
                            Name = queItem.Name,
                            Grade = queItem.Grade,
                            RightAnswer = "/",
                            QuestionTypeCode = model.QuestionTypeCode,
                            Precondition = queItem.Precondition
                        };
                        tQuestionItemService.AddNoSaveChange(qi);
                    }
                }

                #endregion

            }

            tQuestionService.SaveChange();

            return ServiceResponse<bool>.SuccessResponse(CommonConst.OprateSuccessStr, true);
        }

        /// <summary>
        /// 删除
        /// </summary>
        [HttpGet, Route("api/ytjyadmin/QueBank/DeleteModel")]
        public ServiceResponse<bool> DeleteModel(Guid id)
        {
            var model = tQuestionService.Find(v => v.Id == id && !v.IsDelete);

            if (model.IsNotNull())
            {
                model.IsDelete = true;
                model.UpdateTime = DateTime.Now;
                model.UpdateUserId = CurrentUserModel.UserId;
                tQuestionService.Update(model);
            }

            return ServiceResponse<bool>.SuccessResponse(CommonConst.OprateSuccessStr, true);
        }

        /// <summary>
        /// 获取题目类型列表
        /// </summary>
        [HttpGet, Route("api/ytjyadmin/QueBank/GetQuestionTypeList")]
        [AllowAnonymous]
        public ServiceResponse<object> GetQuestionTypeList()
        {
            var models = tQuestionTypeService.FindList(q => !q.IsDelete).ToList();
            var viewModel = models.Select(q => new { q.TypeCode, q.TypeName }).ToList();
            return ServiceResponse<object>.SuccessResponse(CommonConst.OprateSuccessStr, viewModel);
        }

        /// <summary>
        /// 导入题目
        /// </summary>
        [HttpPost, Route("api/ytjyadmin/QueBank/ImportModels")]
        public object ImportModels()
        {
            var filelist = HttpContext.Current.Request.Files;
            var models = new List<ImportQuestionModel>();
            var path = HttpContext.Current.Server.MapPath("/ImportConfig/importQueestionConfig.xml");
            var xr = XMLHelper.ReadToHashtable(path);
            if (filelist.Count > 0)
            {
                var file = filelist[0];
                var fileName = file.FileName;
                var fn = fileName.Split('\\');
                if (fn.Length > 1)
                {
                    fileName = fn[fn.Length - 1];
                }
                DataTable dataTable = null;
                var fs = fileName.Split('.');
                if (fs.Length > 1)
                {
                    var ext = fs[fs.Length - 1];
                    if (ext == "xlsx")
                        dataTable = ExcelHelp.ExcelToTableForXLSX(file.InputStream, xr.ImportHashtable, true); //excel转成datatable
                    else
                        dataTable = ExcelHelp.ExcelToTableForXLS(file.InputStream); //excel转成datatable
                }
                models = dataTable.ToDataList<ImportQuestionModel>(); //datatable转成list
            }

            var faile = new List<ImportQuestionModel>();
            var exportList = new List<ImportQuestionModel>();
            var dics = eDictionaryService.FindList(d => !d.IsDelete).ToList();
            var categroy = dics.FirstOrDefault(d => d.TypeCode == DictionaryConst.CATEGORY_CODE);
            var categorys = dics.Where(d => d.ParentID == categroy?.ID).ToList();

            var cls = dics.FirstOrDefault(d => d.TypeCode == DictionaryConst.CLASS_TYPE_CODE);
            var clss = dics.Where(d => d.ParentID == cls?.ID).ToList();

            var succeQues = new List<TQuestion>();
            var queItems = new List<TQuestionItem>();
            var options = new List<TOption>();
            var rQuestionOptions = new List<RQuestionOption>();

            TQuestion que = null;
            TQuestionItem queItem = null;

            var qts = tQuestionTypeService.FindList(x => !x.IsDelete).ToList();

            var tempOptions = new List<TOption>();//配伍选择题使用
            var i = 2;//第几行,第一行为须知，第二行为列名
            //数据list转成数据库实体对应的list
            foreach (var m in models)
            {
                i++;
                //判断题目类型输入是否正确
                var qt = qts.FirstOrDefault(x => x.TypeName == m.QuestionTypeName.Trim())?.TypeCode ?? string.Empty;
                if (string.IsNullOrEmpty(qt))
                {
                    m.Reason = $"第{i}行题目类型错误";
                    faile.Add(m);
                    exportList.Add(m);
                    continue;
                }

                var ca = categorys.FirstOrDefault(x => x.TypeName == m.CategoryName.Trim());
                if (ca.IsNull())
                {
                    m.Reason = $"第{i}行类型名称不存在，请确认后重新上传！";
                    faile.Add(m);
                    exportList.Add(m);
                    continue;
                }

                var ct = clss.FirstOrDefault(x => x.TypeName == m.TypeName.Trim());
                if (ct.IsNull())
                {
                    m.Reason = $"第{i}行班级类型不存在，请确认后重新上传！";
                    faile.Add(m);
                    exportList.Add(m);
                    continue;
                }

                int.TryParse(qt, out var iqt);

                //判断当前记录与上一条记录是否为同一个题目
                if (que.IsNotNull() && que.QuestionTypeCode != qt)
                {
                    que = null;
                    queItem = null;
                }

                // 单选题、多选题、判断题、填空题
                if (iqt < 5)
                {
                    que = null;
                    queItem = null;

                    if (que.IsNull() && string.IsNullOrEmpty(m.Name))
                    {
                        m.Reason = $"第{i}行题目必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }
                    if (que.IsNull() && string.IsNullOrEmpty(m.QuestionTypeName))
                    {
                        m.Reason = $"第{i}行试题类型必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }
                    if (que.IsNull() && string.IsNullOrEmpty(m.OptionNameA))
                    {
                        m.Reason = $"第{i}行选项A必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }
                    //填空题可以写一个选项
                    if (que.IsNull() && string.IsNullOrEmpty(m.OptionNameB) && iqt != 4)
                    {
                        m.Reason = $"第{i}行选项B必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }
                    if (que.IsNull() && string.IsNullOrEmpty(m.RightAnswer))
                    {
                        m.Reason = $"第{i}行正确答案必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }

                    que = new TQuestion
                    {
                        Id = Guid.NewGuid(),
                        Analysis = m.Analysis,
                        CategoryId = ca.TypeCode,
                        TypeCode = ct.TypeCode,
                        IsDelete = false,
                        CreateTime = DateTime.Now,
                        OrgCode = CurrentUserModel.OrgCode,
                        CreateUserId = CurrentUserModel.UserId,
                        UpdateTime = DateTime.Now,
                        UpdateUserId = CurrentUserModel.UserId,
                        Name = m.Name,
                        Sort = 0,
                        RightAnswer = "/",
                        Grade = 0,
                        QuestionTypeCode = qt,
                        Difficulty = m.Difficulty,
                        MainName = string.Empty,
                        Precondition = string.Empty
                    };
                    succeQues.Add(que);

                    queItem = new TQuestionItem
                    {
                        Id = Guid.NewGuid(),
                        Analysis = m.Analysis,
                        CategoryId = ca.TypeCode,
                        TypeCode = ct.TypeCode,
                        IsDelete = false,
                        CreateTime = DateTime.Now,
                        OrgCode = CurrentUserModel.OrgCode,
                        CreateUserId = CurrentUserModel.UserId,
                        UpdateTime = DateTime.Now,
                        UpdateUserId = CurrentUserModel.UserId,
                        Name = m.Name,
                        Sort = 0,
                        RightAnswer = "/",
                        Grade = 0,
                        QuestionTypeCode = qt,
                        Difficulty = m.Difficulty,
                        Precondition = string.Empty,
                        QuestionId = que.Id
                    };
                    queItems.Add(queItem);

                    #region 保存选项到列表

                    //所有的选项都放到一个列表中
                    var ops = new List<TOption>
                    {
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameA,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        },
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameB,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        },
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameC,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        },
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameD,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        },
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameE,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        },
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameF,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        },
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameG,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        },
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameH,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        }
                    };

                    var rops = new List<RQuestionOption>();
                    //先正确的选项加进去
                    foreach (var a in m.RightAnswer)
                    {
                        var index = 0;
                        switch (a)
                        {
                            case 'A':
                                index = 0;
                                break;
                            case 'B':
                                index = 1;
                                break;
                            case 'C':
                                index = 2;
                                break;
                            case 'D':
                                index = 3;
                                break;
                            case 'E':
                                index = 4;
                                break;
                            case 'F':
                                index = 5;
                                break;
                            case 'G':
                                index = 6;
                                break;
                            case 'H':
                                index = 7;
                                break;
                        }

                        var op = ops[index];
                        rops.Add(new RQuestionOption
                        {
                            Id = Guid.NewGuid(),
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId,
                            IsRight = true,
                            OptionId = op.Id,
                            QuestionId = que.Id,
                            QuestionItemId = queItem.Id,
                            Sort = 0
                        });
                    }

                    //把其他的选项也加入到这个列表中
                    foreach (var op in ops)
                    {
                        if (string.IsNullOrEmpty(op.Name))
                            continue;
                        options.Add(op);
                        var t = rops.FirstOrDefault(x => x.OptionId == op.Id);
                        if (t.IsNull())
                        {
                            rops.Add(new RQuestionOption
                            {
                                Id = Guid.NewGuid(),
                                IsDelete = false,
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId,
                                IsRight = false,
                                OptionId = op.Id,
                                QuestionId = que.Id,
                                QuestionItemId = queItem.Id,
                                Sort = 0
                            });
                        }
                    }
                    rQuestionOptions.AddRange(rops);

                    #endregion
                }
                else if (iqt == 5) //配伍选择题
                {
                    queItem = null;

                    //所有的选项都放到一个列表中
                    var ops = new List<TOption>
                    {
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameA,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        },
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameB,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        },
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameC,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        },
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameD,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        },
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameE,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        },
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameF,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        },
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameG,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        },
                        new TOption
                        {
                            Id = Guid.NewGuid(),
                            Name = m.OptionNameH,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId
                        }
                    };

                    var haveOps = ops.Where(x => !string.IsNullOrEmpty(x.Name)).Select(x => x.Name).ToList();
                    var mainName = string.Empty;
                    if (haveOps.Any())
                    {
                        mainName = string.Join("/", haveOps);
                        tempOptions = ops;//临时变量里面的选项置为下一个配伍题的选项
                        foreach (var op in ops)
                        {
                            if (!string.IsNullOrEmpty(op.Name))
                                options.Add(op);
                        }
                    }

                    //下一个配伍题
                    if (!string.IsNullOrEmpty(mainName) && que.IsNotNull() && !que.MainName.Equals(mainName))
                    {
                        que = null;
                    }

                    if (que.IsNull() && string.IsNullOrEmpty(m.Name))
                    {
                        m.Reason = $"第{i}行题目必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }
                    if (que.IsNull() && string.IsNullOrEmpty(m.QuestionTypeName))
                    {
                        m.Reason = $"第{i}行试题类型必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }
                    if (que.IsNull() && string.IsNullOrEmpty(m.OptionNameA))
                    {
                        m.Reason = $"第{i}行选项A必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }
                    if (que.IsNull() && string.IsNullOrEmpty(m.OptionNameB))
                    {
                        m.Reason = $"第{i}行选项B必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }
                    if (que.IsNull() && string.IsNullOrEmpty(m.RightAnswer))
                    {
                        m.Reason = $"第{i}行正确答案必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }

                    if (que.IsNull())
                    {
                        que = new TQuestion
                        {
                            Id = Guid.NewGuid(),
                            Analysis = m.Analysis,
                            CategoryId = ca.TypeCode,
                            TypeCode = ct.TypeCode,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            OrgCode = CurrentUserModel.OrgCode,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId,
                            Name = m.Name,
                            Sort = 0,
                            RightAnswer = "/",
                            Grade = 0,
                            QuestionTypeCode = qt,
                            Difficulty = m.Difficulty,
                            MainName = mainName,
                            Precondition = string.Empty
                        };
                        succeQues.Add(que);
                    }

                    if (queItem.IsNull())
                    {
                        queItem = new TQuestionItem
                        {
                            Id = Guid.NewGuid(),
                            Analysis = m.Analysis,
                            CategoryId = ca.TypeCode,
                            TypeCode = ct.TypeCode,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            OrgCode = CurrentUserModel.OrgCode,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId,
                            Name = m.Name,
                            Sort = 0,
                            RightAnswer = "/",
                            Grade = 0,
                            QuestionTypeCode = qt,
                            Difficulty = m.Difficulty,
                            Precondition = string.Empty,
                            QuestionId = que.Id
                        };
                        queItems.Add(queItem);
                    }

                    var rops = new List<RQuestionOption>();
                    //先正确的选项加进去
                    foreach (var a in m.RightAnswer)
                    {
                        var index = 0;
                        switch (a)
                        {
                            case 'A':
                                index = 0;
                                break;
                            case 'B':
                                index = 1;
                                break;
                            case 'C':
                                index = 2;
                                break;
                            case 'D':
                                index = 3;
                                break;
                            case 'E':
                                index = 4;
                                break;
                            case 'F':
                                index = 5;
                                break;
                            case 'G':
                                index = 6;
                                break;
                            case 'H':
                                index = 7;
                                break;
                        }

                        var op = tempOptions[index];
                        rops.Add(new RQuestionOption
                        {
                            Id = Guid.NewGuid(),
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId,
                            IsRight = true,
                            OptionId = op.Id,
                            QuestionId = que.Id,
                            QuestionItemId = queItem.Id,
                            Sort = 0
                        });
                    }

                    //把其他的选项也加入到这个列表中
                    foreach (var op in tempOptions)
                    {
                        if (string.IsNullOrEmpty(op.Name))
                            continue;
                        var t = rops.FirstOrDefault(x => x.OptionId == op.Id);
                        if (t.IsNull())
                        {
                            rops.Add(new RQuestionOption
                            {
                                Id = Guid.NewGuid(),
                                IsDelete = false,
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId,
                                IsRight = false,
                                OptionId = op.Id,
                                QuestionId = que.Id,
                                QuestionItemId = queItem.Id,
                                Sort = 0
                            });
                        }
                    }
                    rQuestionOptions.AddRange(rops);
                }
                //案例分析选择题、综合分析选择题
                else if (iqt == 6 || iqt == 7) //案例分析选择题、综合分析选择题
                {
                    //当前记录类型与上一条记录类型不一致，实体置为空
                    if (que.IsNotNull() && que.QuestionTypeCode != qt)
                    {
                        que = null;
                        queItem = null;
                    }
                    // 共用题干不一致，说明是下一道题目
                    if (que.IsNotNull() && !string.IsNullOrEmpty(m.MainName) && !que.MainName.Equals(m.MainName))
                    {
                        que = null;
                        queItem = null;
                    }

                    if (que.IsNotNull() && !string.IsNullOrEmpty(m.Name) && !queItem.Name.Equals(m.Name))
                    {
                        queItem = null;
                    }

                    if (que.IsNull() && string.IsNullOrEmpty(m.MainName))
                    {
                        m.Reason = $"第{i}行共用题干必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }

                    if (string.IsNullOrEmpty(m.Name))
                    {
                        m.Reason = $"第{i}行题目必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }

                    if (string.IsNullOrEmpty(m.QuestionTypeName))
                    {
                        m.Reason = $"第{i}行试题类型必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }
                    if (string.IsNullOrEmpty(m.OptionNameA))
                    {
                        m.Reason = $"第{i}行选项A必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }

                    if (string.IsNullOrEmpty(m.OptionNameB))
                    {
                        m.Reason = $"第{i}行选项B必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }
                    if (string.IsNullOrEmpty(m.RightAnswer))
                    {
                        m.Reason = $"第{i}行正确答案必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }

                    if (que.IsNull())
                    {
                        que = new TQuestion
                        {
                            Id = Guid.NewGuid(),
                            Analysis = m.Analysis,
                            CategoryId = ca.TypeCode,
                            TypeCode = ct.TypeCode,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            OrgCode = CurrentUserModel.OrgCode,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId,
                            Name = m.Name,
                            Sort = 0,
                            RightAnswer = "/",
                            Grade = 0,
                            QuestionTypeCode = qt,
                            Difficulty = m.Difficulty,
                            MainName = m.MainName,
                            Precondition = string.Empty
                        };
                        succeQues.Add(que);
                    }

                    if (queItem.IsNull())
                    {
                        queItem = new TQuestionItem
                        {
                            Id = Guid.NewGuid(),
                            Analysis = m.Analysis,
                            CategoryId = ca.TypeCode,
                            TypeCode = ct.TypeCode,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            OrgCode = CurrentUserModel.OrgCode,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId,
                            Name = m.Name,
                            Sort = 0,
                            RightAnswer = "/",
                            Grade = 0,
                            QuestionTypeCode = qt,
                            Difficulty = m.Difficulty,
                            Precondition = string.Empty,
                            QuestionId = que.Id
                        };
                        queItems.Add(queItem);

                        #region 保存选项到列表

                        //所有的选项都放到一个列表中
                        var ops = new List<TOption>
                        {
                            new TOption
                            {
                                Id = Guid.NewGuid(),
                                Name = m.OptionNameA,
                                IsDelete = false,
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId
                            },
                            new TOption
                            {
                                Id = Guid.NewGuid(),
                                Name = m.OptionNameB,
                                IsDelete = false,
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId
                            },
                            new TOption
                            {
                                Id = Guid.NewGuid(),
                                Name = m.OptionNameC,
                                IsDelete = false,
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId
                            },
                            new TOption
                            {
                                Id = Guid.NewGuid(),
                                Name = m.OptionNameD,
                                IsDelete = false,
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId
                            },
                            new TOption
                            {
                                Id = Guid.NewGuid(),
                                Name = m.OptionNameE,
                                IsDelete = false,
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId
                            },
                            new TOption
                            {
                                Id = Guid.NewGuid(),
                                Name = m.OptionNameF,
                                IsDelete = false,
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId
                            },
                            new TOption
                            {
                                Id = Guid.NewGuid(),
                                Name = m.OptionNameG,
                                IsDelete = false,
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId
                            },
                            new TOption
                            {
                                Id = Guid.NewGuid(),
                                Name = m.OptionNameH,
                                IsDelete = false,
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId
                            }
                        };

                        var rops = new List<RQuestionOption>();
                        //先正确的选项加进去
                        foreach (var a in m.RightAnswer)
                        {
                            var index = 0;
                            switch (a)
                            {
                                case 'A':
                                    index = 0;
                                    break;
                                case 'B':
                                    index = 1;
                                    break;
                                case 'C':
                                    index = 2;
                                    break;
                                case 'D':
                                    index = 3;
                                    break;
                                case 'E':
                                    index = 4;
                                    break;
                                case 'F':
                                    index = 5;
                                    break;
                                case 'G':
                                    index = 6;
                                    break;
                                case 'H':
                                    index = 7;
                                    break;
                            }

                            var op = ops[index];
                            rops.Add(new RQuestionOption
                            {
                                Id = Guid.NewGuid(),
                                IsDelete = false,
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUserModel.UserId,
                                UpdateTime = DateTime.Now,
                                UpdateUserId = CurrentUserModel.UserId,
                                IsRight = true,
                                OptionId = op.Id,
                                QuestionId = que.Id,
                                QuestionItemId = queItem.Id,
                                Sort = 0
                            });
                        }

                        //把其他的选项也加入到这个列表中
                        foreach (var op in ops)
                        {
                            if (string.IsNullOrEmpty(op.Name))
                                continue;
                            options.Add(op);
                            var t = rops.FirstOrDefault(x => x.OptionId == op.Id);
                            if (t.IsNull())
                            {
                                rops.Add(new RQuestionOption
                                {
                                    Id = Guid.NewGuid(),
                                    IsDelete = false,
                                    CreateTime = DateTime.Now,
                                    CreateUserId = CurrentUserModel.UserId,
                                    UpdateTime = DateTime.Now,
                                    UpdateUserId = CurrentUserModel.UserId,
                                    IsRight = false,
                                    OptionId = op.Id,
                                    QuestionId = que.Id,
                                    QuestionItemId = queItem.Id,
                                    Sort = 0
                                });
                            }
                        }
                        rQuestionOptions.AddRange(rops);

                        #endregion
                    }
                }
                //主观题、综合分析简答题
                else if (iqt == 8 || iqt == 9) //主观题、综合分析简答题  -- 完成
                {
                    queItem = null;

                    //当前记录类型与上一条记录类型不一致，实体置为空
                    if (que.IsNotNull() && que.QuestionTypeCode != qt)
                    {
                        que = null;
                        queItem = null;
                    }
                    // 共用题干不一致，说明是下一道题目
                    if (que.IsNotNull() && !string.IsNullOrEmpty(m.MainName) && !que.MainName.Equals(m.MainName))
                    {
                        que = null;
                        queItem = null;
                    }

                    if (queItem.IsNotNull() && !string.IsNullOrEmpty(m.Name) && !queItem.Name.Equals(m.Name))
                    {
                        queItem = null;
                    }

                    if (que.IsNull() && string.IsNullOrEmpty(m.MainName))
                    {
                        m.Reason = $"第{i}行共用题干必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }
                    if (string.IsNullOrEmpty(m.Name))
                    {
                        m.Reason = $"第{i}行题目必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }
                    if (string.IsNullOrEmpty(m.QuestionTypeName))
                    {
                        m.Reason = $"第{i}行试题类型必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }
                    if (string.IsNullOrEmpty(m.OptionNameA))
                    {
                        m.Reason = $"第{i}行选项A必填";
                        faile.Add(m);
                        exportList.Add(m);
                        continue;
                    }

                    if (que.IsNull())
                    {
                        que = new TQuestion
                        {
                            Id = Guid.NewGuid(),
                            Analysis = m.Analysis,
                            CategoryId = ca.TypeCode,
                            TypeCode = ct.TypeCode,
                            IsDelete = false,
                            CreateTime = DateTime.Now,
                            OrgCode = CurrentUserModel.OrgCode,
                            CreateUserId = CurrentUserModel.UserId,
                            UpdateTime = DateTime.Now,
                            UpdateUserId = CurrentUserModel.UserId,
                            Name = m.Name,
                            Sort = 0,
                            RightAnswer = "/",
                            Grade = 0,
                            QuestionTypeCode = qt,
                            Difficulty = m.Difficulty,
                            MainName = m.MainName,
                            Precondition = string.Empty
                        };
                        succeQues.Add(que);
                    }

                    queItem = new TQuestionItem
                    {
                        Id = Guid.NewGuid(),
                        Analysis = m.Analysis,
                        CategoryId = ca.TypeCode,
                        TypeCode = ct.TypeCode,
                        IsDelete = false,
                        CreateTime = DateTime.Now,
                        OrgCode = CurrentUserModel.OrgCode,
                        CreateUserId = CurrentUserModel.UserId,
                        UpdateTime = DateTime.Now,
                        UpdateUserId = CurrentUserModel.UserId,
                        Name = m.Name,
                        Sort = 0,
                        RightAnswer = "/",
                        Grade = 0,
                        QuestionTypeCode = qt,
                        Difficulty = m.Difficulty,
                        Precondition = string.Empty,
                        QuestionId = que.Id,
                        Answer = m.OptionNameA
                    };
                    queItems.Add(queItem);
                }

            }

            //构造保存数据库的实体
            //批量保存题目
            if (succeQues.Any())
            {
                var dt = succeQues.ToDataTable(null);
                dt.TableName = "TQuestion";
                SqlBulkCopyHelper.SaveTable(dt);
            }
            //批量保存子题目
            if (queItems.Any())
            {
                var dt = queItems.ToDataTable(null);
                dt.TableName = "TQuestionItem";
                SqlBulkCopyHelper.SaveTable(dt);
            }
            //批量保存题目的选项
            if (options.Any())
            {
                var dt = options.ToDataTable(null);
                dt.TableName = "TOption";
                SqlBulkCopyHelper.SaveTable(dt);
            }
            //批量保存题目
            if (rQuestionOptions.Any())
            {
                var dt = rQuestionOptions.ToDataTable(null);
                dt.TableName = "RQuestionOption";
                SqlBulkCopyHelper.SaveTable(dt);
            }

            var url = string.Empty;
            if (exportList.Any())
            {
                var extDt = exportList.ToDataTable(xr.ExportHashtable);
                var str = GetSavePath();
                var strs = str.Split(',');
                if (strs.Any() && strs.Length > 1)
                {
                    var fileFullPath = strs[0];
                    url = strs[1];
                    ExcelHelp.TableToExcelForXLSX(extDt, fileFullPath);
                }
            }

            var list = new { failed = faile.Take(100).ToList(), failedCount = faile.Count }; //数据太多的话，浏览器会崩溃

            if (!exportList.Any())
            {
                var json = new { list, msg = "添加成功", url };
                return ServiceResponse<object>.SuccessResponse(json);
            }
            else
            {
                var json = new { list, msg = "添加失败", url };
                return ServiceResponse<object>.SuccessResponse(CommonConst.OprateFailStr, json);
            }
        }
    }
}