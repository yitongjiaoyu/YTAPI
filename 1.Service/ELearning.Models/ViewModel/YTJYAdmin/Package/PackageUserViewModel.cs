﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.YTJYAdmin.Package
{
    public class PackageUserRequest : BaseSearchRequest
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string UserName { set; get; }

        /// <summary>
        /// 登录名
        /// </summary>
        public string LoginName { set; get; }

        /// <summary>
        /// 0.不关联，1.已关联
        /// </summary>
        public int Type { set; get; }

        /// <summary>
        /// 套餐id
        /// </summary>
        public Guid PackageId { set; get; }
    }

    public class PackageUserViewModel
    {
        /// <summary>
        /// 列表
        /// </summary>
        public List<PackageUserItem> Items { set; get; } = new List<PackageUserItem>();

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { set; get; }
    }

    public class PackageUserItem
    {
        /// <summary>
        /// 用户id
        /// </summary>
        public Guid UserID { set; get; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { set; get; }

        /// <summary>
        /// 登陆名
        /// </summary>
        public string LoginName { set; get; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string MobilePhone { set; get; }
    }

    public class PackageUserModel
    {
        /// <summary>
        /// 用户id
        /// </summary>
        public Guid UserID { set; get; }

        /// <summary>
        /// 记录总数量
        /// </summary>
        public int TotalCount { set; get; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { set; get; }

        /// <summary>
        /// 登陆名
        /// </summary>
        public string LoginName { set; get; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string MobilePhone { set; get; }
    }

    /// <summary>
    /// 套餐用户关联保存实体
    /// </summary>
    public class PackageUserPostModel
    {
        /// <summary>
        /// 传入的关联类型：0.不关联,需要进行关联操作，1.已关联,需要进行取消关联操作
        /// </summary>
        public int Type { set; get; }

        /// <summary>
        /// 套餐id
        /// </summary>
        public Guid PackageId { set; get; }

        /// <summary>
        /// 用户Id列表
        /// </summary>
        public List<Guid> UserIdList { set; get; }
    }
}
