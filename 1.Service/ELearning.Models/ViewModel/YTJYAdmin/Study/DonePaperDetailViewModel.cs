﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.YTJYAdmin.Study
{
    public class DonePaperDetailRequest
    {
        /// <summary>
        /// 做题历史记录id
        /// </summary>
        public Guid PaperTestHistoryId { set; get; }
    }

    public class DonePaperDetailViewModel
    {
        /// <summary>
        /// 试卷id
        /// </summary>
        public Guid PaperId { set; get; }

        /// <summary>
        /// 做题历史记录id
        /// </summary>
        public Guid PaperTestHistoryId { set; get; }

        /// <summary>
        /// 试卷名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 类型：1.真题，2.模拟题
        /// </summary>
        [Display(Name = "类型：1.真题，2.模拟题")]
        public int Type { get; set; }

        public string TypeStr
        {
            get
            {
                var str = "真题";
                if (Type == 2)
                    str = "模拟题";
                return str;
            }
        }

        /// <summary>
        /// 考试时长,单位：分钟
        /// </summary>
        public int TimeSpan { get; set; }

        /// <summary>
        /// 试卷总分数
        /// </summary>
        public double TotalScore { get; set; }

        /// <summary>
        /// 题目类型列表
        /// </summary>
        public List<DonePaperDetailQuestionTypeModel> Items { get; set; } = new List<DonePaperDetailQuestionTypeModel>();
    }

    /// <summary>
    /// 题目类型实体
    /// </summary>
    public class DonePaperDetailQuestionTypeModel
    {
        /// <summary>
        /// 题目类型编码
        /// </summary>
        public string QuestionTypeCode { get; set; }

        /// <summary>
        /// 题目类型名称
        /// </summary>
        public string QuestionTypeName { get; set; }

        /// <summary>
        /// 题目列表
        /// </summary>
        public List<DonePaperDetailQuestionModel> Items = new List<DonePaperDetailQuestionModel>();
    }

    /// <summary>
    /// 题目实体
    /// </summary>
    public class DonePaperDetailQuestionModel
    {
        /// <summary>
        /// 题目id
        /// </summary>
        public Guid QuestionId { get; set; }

        /// <summary>
        /// 子题目ID
        /// </summary>
        public Guid QuestionItemId { get; set; }

        /// <summary>
        /// 题目主题干
        /// </summary>
        public string MainName { get; set; }

        /// <summary>
        /// 题目题干
        /// </summary>
        public string QuestionName { get; set; }

        /// <summary>
        /// 解析
        /// </summary>
        public string Analysis { get; set; }

        /// <summary>
        /// 题目分数
        /// </summary>
        public double Grade { get; set; }

        /// <summary>
        /// 题目答案，主观题，简答题等类型使用该字段
        /// </summary>
        [Display(Name = "题目答案，主观题，简答题等类型使用该字段")]
        public string Answer { get; set; }

        /// <summary>
        /// 正确答案
        /// </summary>
        [Display(Name = "正确答案")]
        public List<string> RightAnswer { get; set; }

        /// <summary>
        /// 用户输入的答案，主观题使用的字段
        /// </summary>
        public string InputAnswer { get; set; }

        /// <summary>
        /// 用户选择选项id字符串数组
        /// </summary>
        [Display(Name = "用户选择选项id字符串数组")]
        public List<string> ChoosedOptionId { get; set; }

        /// <summary>
        /// 题目状态：0.未做，1.正确，2.错误
        /// </summary>
        [Display(Name = "题目状态：0.未做，1.正确，2.错误")]
        public int Status { get; set; }

        /// <summary>
        /// 用户得分
        /// </summary>
        [Display(Name = "用户得分")]
        public double UserGrade { get; set; }

        /// <summary>
        /// 选项列表
        /// </summary>
        public List<DonePaperDetailQuestionOptionModel> Items = new List<DonePaperDetailQuestionOptionModel>();
    }

    /// <summary>
    /// 题目选项实体
    /// </summary>
    public class DonePaperDetailQuestionOptionModel
    {
        /// <summary>
        /// 选项id
        /// </summary>
        public Guid OptionId { get; set; }

        /// <summary>
        /// 选项内容
        /// </summary>
        public string OptionName { get; set; }

        /// <summary>
        /// 是否是正确答案
        /// </summary>
        public bool IsRight { get; set; }

        /// <summary>
        /// 是否选中
        /// </summary>
        [Display(Name = "是否选中")]
        public bool IsChoosed { get; set; }
    }

    /// <summary>
    /// 做题记录实体
    /// </summary>
    public class DonePaperDetailModel
    {
        /// <summary>
        /// 题目id
        /// </summary>
        public Guid QuestionId { get; set; }

        /// <summary>
        /// 子题目ID
        /// </summary>
        public Guid QuestionItemId { get; set; }

        /// <summary>
        /// 题目主题干
        /// </summary>
        public string MainName { get; set; }

        /// <summary>
        /// 题目题干
        /// </summary>
        public string QuestionName { get; set; }

        /// <summary>
        /// 题目类型编码
        /// </summary>
        public string QuestionTypeCode { get; set; }

        /// <summary>
        /// 题目类型名称
        /// </summary>
        public string QuestionTypeName { get; set; }

        /// <summary>
        /// 解析
        /// </summary>
        public string Analysis { get; set; }

        /// <summary>
        /// 题目答案，主观题，简答题等类型使用该字段
        /// </summary>
        [Display(Name = "题目答案，主观题，简答题等类型使用该字段")]
        public string Answer { get; set; }

        /// <summary>
        /// 用户输入的答案，主观题使用的字段
        /// </summary>
        public string InputAnswer { get; set; }

        /// <summary>
        /// 正确答案,例如：/id1/id2/
        /// </summary>
        [Display(Name = "正确答案,例如：/id1/id2/")]
        public string RightAnswer { get; set; }

        /// <summary>
        /// 用户选择选项id字符串，例如：/id1/id2/
        /// </summary>
        [Display(Name = "用户选择选项id字符串，例如：/id1/id2/")]
        public string ChoosedOptionId { get; set; }

        /// <summary>
        /// 题目状态：0.未做，1.正确，2.错误
        /// </summary>
        [Display(Name = "题目状态：0.未做，1.正确，2.错误")]
        public int QStatus { get; set; }

        /// <summary>
        /// 用户得分
        /// </summary>
        [Display(Name = "用户得分")]
        public double UserGrade { get; set; }

        /// <summary>
        /// 题目分数
        /// </summary>
        public double Grade { get; set; }

        /// <summary>
        /// 组织编码
        /// </summary>
        public string OrgCode { get; set; }

        /// <summary>
        /// 选项id
        /// </summary>
        public Guid? OptionId { get; set; }

        /// <summary>
        /// 选项内容
        /// </summary>
        public string OptionName { get; set; }

        /// <summary>
        /// 是否是正确答案
        /// </summary>
        public bool? IsRight { get; set; }

        /// <summary>
        /// 是否选中
        /// </summary>
        [Display(Name = "是否选中")]
        public bool? IsChoosed { get; set; }

        /// <summary>
        /// 试卷id
        /// </summary>
        public Guid Id { set; get; }

        /// <summary>
        /// 试卷名称
        /// </summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Display(Name = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 基础分类编码
        /// </summary>
        public string TypeCode { get; set; }

        /// <summary>
        /// 分类编码
        /// </summary>
        public string CategoryId { get; set; }

        /// <summary>
        /// 类型：1.真题，2.模拟题
        /// </summary>
        [Display(Name = "类型：1.真题，2.模拟题")]
        public int Type { get; set; }

        /// <summary>
        /// 考试时长,单位：分钟
        /// </summary>
        public int TimeSpan { get; set; }

        /// <summary>
        /// 题目总数量
        /// </summary>
        public int TotalCount { get; set; }

        /// <summary>
        /// 年份
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// 题目难度
        /// </summary>
        public int Difficulty { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public double Sort { get; set; }

        /// <summary>
        /// 选项序号
        /// </summary>
        public double QoSort { get; set; }

        /// <summary>
        /// 做对题目数量
        /// </summary>
        [Display(Name = "做对题目数量")]
        public int RightCount { get; set; }

        /// <summary>
        /// 做错题目数量
        /// </summary>
        [Display(Name = "做错题目数量")]
        public int ErrorCount { get; set; }

        /// <summary>
        /// 上次做的题目id
        /// </summary>
        [Display(Name = "上次做的题目id")]
        public Guid LastQuestionId { get; set; }

        /// <summary>
        /// 成绩
        /// </summary>
        [Display(Name = "成绩")]
        public double TotalScore { get; set; }

        /// <summary>
        /// 试卷时长，单位：分钟
        /// </summary>
        [Display(Name = "试卷时长，单位：分钟")]
        public int PaperTimeSpan { get; set; }

        /// <summary>
        /// 试卷总分
        /// </summary>
        [Display(Name = "试卷总分")]
        public double PaperTotalScore { get; set; }
    }
}
