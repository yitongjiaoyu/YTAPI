﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELearning.Models.ViewModel.Org
{
    public class OrgViewModel
    {
        public List<EOrg> OrgItems { set; get; } = new List<EOrg>();

        /// <summary>
        /// 总数量
        /// </summary>
        public int TotalCount { set; get; }
    }
    /// <summary>
    /// 获取组织机构列表参数
    /// </summary>
    public class GetOrgRequest : BaseRequest
    {
        /// <summary>
        /// 机构编码
        /// </summary>
        public string org_code { get; set; }

        /// <summary>
        /// 父级编码
        /// </summary>
        public string parent_code { get; set; }

        /// <summary>
        /// 机构名称
        /// </summary>
        public string org_name { get; set; }

        /// <summary>
        /// 机构类别(1-学校,2-公司,3-默认,4-其他)
        /// </summary>
        public string org_type { get; set; }

        /// <summary>
        /// 联系人姓名
        /// </summary>
        public string org_owner { get; set; }
    }
}
