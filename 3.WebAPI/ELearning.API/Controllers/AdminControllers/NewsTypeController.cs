﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using ELearning.BLL;
using ELearning.Common;
using ELearning.Common.CommonStr;
using ELearning.Common.Model;
using ELearning.Models;

namespace ELearning.API.Controllers.AdminControllers
{
    /// <summary>
    /// 新闻分类相关操作
    /// </summary>
    public class NewsTypeController : BaseApiController
    {
        /// <summary>
        /// 新闻类型操作service
        /// </summary>
        private ENewsTypeService NewsTypeService = new ENewsTypeService();

        /// <summary>
        /// 获取全部新闻分类列表
        /// </summary>
        [HttpPost, Route("api/newsType/GetAllNewsTypeList")]
        public object GetAllNewsTypeList()
        {
            var typeList = NewsTypeService.FindList(t => !t.IsDelete).ToList();
            return ServiceResponse<List<ENewsType>>.SuccessResponse(CommonConst.OprateSuccessStr, typeList);
        }

    }
}